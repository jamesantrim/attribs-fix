<?php
require dirname(__FILE__) . '/lessc.inc.php';

$joomlaJUiPath = '/../../media/jui';
$joomlaJUiPathAbs = realpath(dirname(__FILE__) . $joomlaJUiPath);

if (!is_dir($joomlaJUiPathAbs))
{
	die('Joomla! UI Path not found: ' . realpath($joomlaJUiPath));
}

include dirname(__FILE__) . '/custom.config.php';
$bootstrapCustomConfig;

$less = new lessc;

$variablesFile = 'variables.less';
$mixinsFile = 'mixins.less';

$lessVariables = file_get_contents($joomlaJUiPathAbs . '/less/' . $variablesFile) . "\n\n";
foreach ($bootstrapCustomConfig['vars'] as $lessVar => $lessVarValue)
{
	$lessVariables .= $lessVar . ': ' . $lessVarValue . ';' . "\n";
}

$destPath = dirname(__FILE__) . '/jui';
rrmdir($destPath);
mkdir($destPath);

$bootstrapLess = $lessVariables . "\n";
$bootstrapLess .= '@import "' . $joomlaJUiPathAbs . '/less/' . $mixinsFile . '";' . "\n";
$spritesLess = $bootstrapLess;
$spritesLess .= '@import "' . $joomlaJUiPathAbs . '/less/sprites.less";' . "\n";
$spritesCss = $less->compile($spritesLess);
$spritesCss = str_replace(
'.clearfix {
  *zoom: 1;
}
.clearfix:before,
.clearfix:after {
  display: table;
  content: "";
  line-height: 0;
}
.clearfix:after {
  clear: both;
}
.hide-text {
  font: 0/0 a;
  color: transparent;
  text-shadow: none;
  background-color: transparent;
  border: 0;
}
.input-block-level {
  display: block;
  width: 100%;
  min-height: 30px;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}
', '', $spritesCss);
file_put_contents($destPath . '/bootstrap-sprites.css', $spritesCss);

foreach ($bootstrapCustomConfig['css'] as $lessFile)
{
	if ($lessFile == 'sprites.less')
	{
		continue; // we use FontAwesome for Bootstrap
	}

	if (file_exists($joomlaJUiPathAbs . '/less/' . $lessFile))
	{
		$bootstrapLess .= '@import "' . $joomlaJUiPathAbs . '/less/' . $lessFile . '";' . "\n";
	}
}



preg_match('#\/\*\!(.*)\*\/#Uis', file_get_contents($joomlaJUiPathAbs . '/less/bootstrap.less'), $matches);
$cssHead = (isset($matches[0]) ? $matches[0] : '/* COPY HEAD NOT FOUND */') . "\n\n";

$bootstrapCss = $cssHead . $less->compile($bootstrapLess);
$bootstrapCss = str_replace(
'.clearfix {
  *zoom: 1;
}
.clearfix:before,
.clearfix:after {
  display: table;
  content: "";
  line-height: 0;
}
.clearfix:after {
  clear: both;
}
', '', $bootstrapCss);

$result = file_put_contents($destPath . '/bootstrap.css', $bootstrapCss);

function rrmdir($dir) {
	if (is_dir($dir)) {
		$objects = scandir($dir);
		foreach ($objects as $object) {
			if ($object != "." && $object != "..") {
				if (filetype($dir."/".$object) == "dir") rrmdir($dir."/".$object); else unlink($dir."/".$object);
			}
		}
		reset($objects);
		rmdir($dir);
	}
}


?>
<pre style="background:#000; color:#fff; padding:5px; text-align:left; margin:10px 0; overflow:auto;"><?php
	echo htmlentities(print_r($bootstrapCss, true));
?></pre>
<?php