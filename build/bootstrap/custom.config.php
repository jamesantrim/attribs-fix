<?php

$bootstrapCustomConfig = array(
	'js' => array(
			"bootstrap-transition.js",
			"bootstrap-modal.js",
			"bootstrap-dropdown.js",
			"bootstrap-scrollspy.js",
			"bootstrap-tab.js",
			"bootstrap-tooltip.js",
			"bootstrap-popover.js",
			"bootstrap-affix.js",
			"bootstrap-alert.js",
			"bootstrap-button.js",
			"bootstrap-collapse.js",
			"bootstrap-carousel.js",
			"bootstrap-typeahead.js"
		),
	'css' => array(
			"labels-badges.less",
			"forms.less",
			"buttons.less",
			"sprites.less",
			"button-groups.less",
			"navs.less",
			"navbar.less",
			"breadcrumbs.less",
			"pagination.less",
			"pager.less",
			"thumbnails.less",
			"alerts.less",
			"progress-bars.less",
			"hero-unit.less",
			"media.less",
			"tooltip.less",
			"popovers.less",
			"modals.less",
			"dropdowns.less",
			"accordion.less",
			"carousel.less",
			"media.less",
			"wells.less",
			"close.less",
			"utilities.less",
			"responsive-navbar.less"
		),
	'vars' => array(
			"@sansFontFamily" => "inherit",
			"@serifFontFamily" => "inherit",
			"@monoFontFamily" => "inherit",
			"@baseFontSize" => "14px",
			"@baseLineHeight" => "21px",
			"@iconSpritePath" => '"../../../../media/jui/img/glyphicons-halflings.png"',
			"@iconWhiteSpritePath" => '"../../../../media/jui/img/glyphicons-halflings-white.png"'
		),
	'img' => array(
			"glyphicons-halflings.png",
			"glyphicons-halflings-white.png"
		)
);