<?php

$url = 'http://bootstrap.herokuapp.com/';

include dirname(__FILE__) . '/custom.config.php';
$postfields = $bootstrapCustomConfig;

foreach ($postfields as $k => $v)
{
	$postfields[$k] = json_encode($v);
}

$ch = curl_init();

curl_setopt($ch,CURLOPT_URL, $url);
curl_setopt($ch,CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
curl_setopt($ch,CURLOPT_POSTFIELDS, $postfields);
$result = curl_exec($ch);
curl_close($ch);

$zipfile = dirname(__FILE__) . '/download/bootstrap.zip';

rrmdir(dirname($zipfile));
mkdir(dirname($zipfile));

file_put_contents($zipfile, $result);

$zip = new ZipArchive;
if ($zip->open($zipfile) === TRUE) {
	$zip->extractTo(dirname($zipfile));
	$zip->close();

	echo 'Downloaded custom Bootstrap configuration for (J)YAML';
}
unlink($zipfile);

// prepare files
	$cssFile = dirname(__FILE__) . '/download/css/bootstrap.css';
	$cssContent = file_get_contents($cssFile);
	$cssContent = str_replace('.clearfix {
  *zoom: 1;
}
.clearfix:before,
.clearfix:after {
  display: table;
  content: "";
  line-height: 0;
}
.clearfix:after {
  clear: both;
}', '', $cssContent);

	file_put_contents($cssFile, $cssContent);

	$cssFile = dirname(__FILE__) . '/download/css/bootstrap.min.css';
	$cssContent = file_get_contents($cssFile);
	$cssContent = str_replace('.clearfix{*zoom:1;}.clearfix:before,.clearfix:after{display:table;content:"";line-height:0;}
.clearfix:after{clear:both;}', '', $cssContent);
	file_put_contents($cssFile, $cssContent);
/////////

function rrmdir($dir) {
	if (is_dir($dir)) {
		$objects = scandir($dir);
		foreach ($objects as $object) {
			if ($object != "." && $object != "..") {
				if (filetype($dir."/".$object) == "dir") rrmdir($dir."/".$object); else unlink($dir."/".$object);
			}
		}
		reset($objects);
		rmdir($dir);
	}
}




