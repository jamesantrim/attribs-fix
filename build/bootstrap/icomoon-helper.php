<?php

$css = file_get_contents(dirname(__FILE__) . '/../../libraries/jyaml/html/css/icomoon.css');

preg_match_all('#(\.icon-.*)\:#U', $css, $matches);

if (isset($matches[1]) && is_array($matches[1]))
{
	$classes = implode(', ', $matches[1]);
	$classes .= " {\n\tbackground-image: none;\n}";

	?>
	Background-image: none
	<pre style="background:#000; color:#fff; padding:5px; text-align:left; margin:10px 0; overflow:auto;"><?php
		echo htmlentities(print_r($classes, true));
	?></pre>
	<?php

	$css = file_get_contents(dirname(__FILE__) . '/../../media/jui/less/sprites.less');
	preg_match_all('#(\.icon-.*)\s+\{#U', $css, $bootstrapMatches);

	$dublicates = array();
	if (isset($bootstrapMatches[1]) && is_array($bootstrapMatches[1]))
	{
		foreach ($bootstrapMatches[1] as $class)
		{
			if (in_array($class, $matches[1]))
			{
				$dublicates[] = $class;
			}
		}
	}
	?>
	Dubplicated icon classes
	<pre style="background:#000; color:#fff; padding:5px; text-align:left; margin:10px 0; overflow:auto;"><?php
		echo htmlentities(print_r($dublicates, true));
	?></pre>
	<?php
}