<?php

$cwd = dirname(dirname(__FILE__));
chdir($cwd);

function getDirectory($path = '.', $level = 0)
{
	$iterator  = new RecursiveDirectoryIterator($path, FilesystemIterator::KEY_AS_PATHNAME | FilesystemIterator::CURRENT_AS_FILEINFO | FilesystemIterator::SKIP_DOTS);
	$flattened = new RecursiveIteratorIterator($iterator, RecursiveIteratorIterator::SELF_FIRST);

	foreach ($flattened as $_path => $_dir)
	{
		if (!$_dir->isDir() || $_path == $path)
		{
			continue;
		}

		// Add an index.html if neither an index.html nor an index.php exist
		if (!(file_exists($_path . '/index.html') || file_exists($_path . '/index.php')))
		{
			echo 'Created missing index.html in: ' . $_path . "\n";
			file_put_contents($_path . '/index.html', '<!DOCTYPE html><title></title>' . "\n");
		}
	}
}
?>
<pre style="background:#000; color:#fff; padding:5px; text-align:left; margin:10px 0; overflow:auto;"><?php
	getDirectory($cwd . '/libraries/jyaml');
	getDirectory($cwd . '/plugins/system/jyaml');
	getDirectory($cwd . '/templates/jyaml');
?></pre>
