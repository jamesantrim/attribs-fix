<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  ModuleHelper
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

defined('_JEXEC') or die;

// Import library dependencies
jimport('joomla.application.module.helper');

/**
 * JYAMLModuleHelper class
 * Extends JModuleHelper
 *
 * @package     JYAML
 * @subpackage  ModuleHelper
 * @since       4.0.0
 */
class JYAMLModuleHelper extends JModuleHelper
{
	/**
	 * Get active Module Positions Information
	 *
	 * @param   bool  $fullInfo  If false returns only position names.
	 *                           If true returns short module array with positions.
	 *
	 * @static
	 * @access public
	 *
	 * @return array Module Positions
	 */
	public static function getLoadedModulePositions($fullInfo=false)
	{
		static $positions;
		static $positionModules;

		if ($positions == null)
		{
			$modules = JModuleHelper::load();

			$positions = array();
			$positionModules = array();

			foreach ($modules as $module)
			{
				if (empty($module->position))
				{
					continue;
				}

				if (!in_array($module->position, $positions))
				{
					$positions[] = $module->position;

					$positionModules[] = array(
						'type' => 'modules',
						'name' => $module->position
					);
				}
			}
		}

		if ($fullInfo)
		{
			return $positionModules;
		}
		else
		{
			return $positions;
		}
	}

	/**
	 * Render the module only with content (style=none)
	 * Does supports cacheing for better performance
	 *
	 * @param   object  $module  Module object
	 *
	 * @static
	 * @access public
	 * @return string Contents of the Module
	 */
	public static function getRawModuleContent($module)
	{
		static $_renderCache = array();

		if (is_object($module) && !isset($_renderCache[$module->id]))
		{
			$jinput = JFactory::getApplication()->input;

			// Force to disable preview chrome(outline style)
			$tp = $jinput->get('tp', 0, 'bool');
			$jinput->set('tp', 0);

			$attribs = array();
			$attribs['style'] = 'none';

			$conf = JFactory::getConfig();

			// Get module parameters
			$mod_params = new \Joomla\Registry\Registry;
			$mod_params->loadString($module->params, 'JSON');

			$contents = '';

			// Default for compatibility purposes. Set cachemode parameter or use JModuleHelper::moduleCache from within the module instead
			$cachemode = $mod_params->get('cachemode', 'oldstatic');
			if ($mod_params->get('cache', 0) == 1  && $conf->get('caching') >= 1 && $cachemode != 'id' && $cachemode != 'safeuri')
			{
				// Default to itemid creating method and workarounds on
				$cacheparams = new stdClass;
				$cacheparams->cachemode = $cachemode;
				$cacheparams->class = 'JModuleHelper';
				$cacheparams->method = 'renderModule';
				$cacheparams->methodparams = array($module, $attribs);

				$contents = JModuleHelper::moduleCache($module, $mod_params, $cacheparams);
			}
			else
			{
				$contents = JModuleHelper::renderModule($module, $attribs);
			}

			// Save raw contents to apply the chrome style before dispatch
			$module->__rawContents = $contents;

			// Revert settings
			$jinput->set('tp', $tp);

			$_renderCache[$module->id] = $contents;
		}

		return isset($_renderCache[$module->id]) ? $_renderCache[$module->id] : '';
	}

	/**
	 * Handler to call and output modChrome_[style]
	 * This is a special method to output valid debug informations on style nesting
	 * Does supports cacheing for better performance
	 *
	 * @param   string  $modChromeFunction  modChrome Function
	 *
	 * @static
	 * @access public
	 * @return void
	 */
	public static function renderSubstyle($modChromeFunction)
	{
		$arguments = func_get_args();

		if (isset($arguments[0]))
		{
			unset($arguments[0]);
		}

		// Fix for referenced variables
		$args = array();
		foreach ($arguments as $k => &$_arg)
		{
			$args[$k] = &$_arg;
		}

		@ob_start();
		call_user_func_array($modChromeFunction, $args);
		$contents = @ob_get_contents();
		@ob_end_clean();

		if (isset($args[3]) && isset($args[3]['____jyaml_extended_debug_outline_content']))
		{
			$contents = str_replace('${{JYAML_MODCHROME_MODULE_CONTENTS}}', $contents, $args[3]['____jyaml_extended_debug_outline_content']);
		}

		echo $contents;
	}
}
