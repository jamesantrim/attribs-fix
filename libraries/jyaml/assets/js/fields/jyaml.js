/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

if (typeof(JYAML) === 'undefined') { JYAML = {}; }
if (typeof(JYAML.createModals) === 'undefined') { JYAML.createModals = []; }

// Modification of JFormValidator for better performance on save
if(typeof(window.MooTools) != 'undefined' && typeof(Class.refactor) != 'undefined' && typeof(JFormValidator) != 'undefined'){
	var JYAMLValidationIgnoreFields = [];
	$$('.jyaml-move-group').each(function(el){
		el.getElements('input,textarea,select').each(function(el){
			JYAMLValidationIgnoreFields.push(el);
		});
	});
	Class.refactor(JFormValidator, {
		isValid: function(form)
		{
			var valid = true,
					elements = form.getElements('fieldset').concat(Array.from(form.elements));
			for (var i=0;i < elements.length; i++){
				if (!JYAMLValidationIgnoreFields.indexOf(elements[i])){
					if (this.validate(elements[i]) == false){
						valid = false;
					}
				}
			}
			new Hash(this.custom).each(function(validator){
				if (validator.exec() != true){
					valid = false;
				}
			});
			return valid;
		}
	});
}

JYAML.getFormToken = (function() {
	var els = document.getElementsByTagName('input');
	for (var i = 0; i < els.length; i++) {
		if ((els[i].type == 'hidden') && (els[i].name.length == 32) && els[i].value == '1') {
			return els[i].name;
			break;
		}
	}
	return '';
});

/**
 * FormField Script for some fields
 */
jQuery(document).ready(function($){
	var origJoomlaSumitButton = null;
	if (typeof Joomla != 'undefined' && typeof Joomla.submitbutton == 'function')
	{
		origJoomlaSumitButton = Joomla.submitbutton;
	}
	if (origJoomlaSumitButton != null)
	{
		Joomla.submitbutton = function(){

			// Remove groups contains empty data on specific elements
			$('.jyaml-group-entry').each(function(){
				var group = $(this);
				$(':input:enabled[data-remove-group="is_empty"]', group).each(function(){
					var $this = $(this),
							value = $(this).val();

					if ('' == value)
					{
						group.remove();
						return false;
					}
				});
			});

			return callUserFuncArray(origJoomlaSumitButton, arguments);
		}
	}

	if (typeof(JYAML.createModals) == 'object' && JYAML.createModals.length)
	{
		for (var i = 0; i < JYAML.createModals.length; i++) {
			if ('string' == typeof JYAML.createModals[i])
			{
				$(JYAML.createModals[i]).appendTo('body');
			}
		}
	}

	$('.jyaml-popover').each(function(){
		var $this = $(this),
				content = $('#' + $this.attr('for') + '_jyaml-popover-content');

		$this.popover({
			'trigger': 'hover',
			'title' : content.data('title'),
			'content': content.html()
		});
	});

	$('.jyaml-move-group').on('jyaml_refresh_group', function(){
		refreshGroup($(this));
	});

	$('.jyaml-move-group').sortable({
		items: '> .jyaml-group-entry',
		handle: '.jyaml-group-move-btn',
		placeholder: 'jyaml-group-entry-ghost',
		axis: "y",
		forcePlaceholderSize: true,
		stop: function(event, ui) {
			$(this).trigger('jyaml_refresh_group');
		}
	});

	$('.jyaml-group-entry-remove').on('click', function(){
		var $this = $(this),
				confirmText = $this.data('confirm'),
				groupEntry = $this.closest('.jyaml-group-entry'),
				group = groupEntry.closest('.jyaml-move-group');

		if (confirm)
		{
			var ok = confirm(confirmText);
			if (!ok)
			{
				return false;
			}
		}

		groupEntry.fadeOut('fast', function(){
			$(this).remove();
			group.trigger('jyaml_refresh_group');
		});

		return false;
	});

	$('.jyaml-group-entry-add').on('click', function(){
		var group = $(this).prev('.jyaml-move-group');
		addGroupEntry(group);
		return false;
	});

	var addGroupEntry= (function(group){
		var dummy = $('> .jyaml-group-entry-dummy', group),
				clone = dummy.clone(true, true),
				initElements = $(':input[data-dummy-init="1"]', clone),
				entryCount = $('> .jyaml-group-entry', group).length,
				randomId = Math.floor(Math.random()*999999);

		clone.removeClass('jyaml-group-entry-dummy').addClass('jyaml-group-entry');

		$(':input', clone).each(function(){
			var $this = $(this),
					id = $this.attr('id');

			if (id)
			{
				var	label = $('label[for="' + id + '"]', clone);
				id = id.replace(/(_dummy$|_dummy_)/g, ('_' + ('' + entryCount) + ('' + randomId)));
				label.attr('for', id);
				$this.attr('id', id);
			}
		});

		clone.appendTo(group);
		group.trigger('jyaml_refresh_group');

		initElements.css('display', '').removeAttr('disabled').removeAttr('data-dummy-init');

		clone.fadeIn('slow', function(){
			initElements.trigger('change');
		});
	});

	var refreshGroup= (function(group){
		var regex = group.data('group-regex');

		if (regex)
		{
			$('> .jyaml-group-entry', group).each(function(index){
				$(':input', this).each(function(){
					var $this = $(this),
							name = $this.attr('name'),
							reg = new RegExp(regex, ''),
							result = reg.exec(name);

					if(result && result.length==4)
					{
						name = result[1] + (index + '') + result[3];
					}

					$this.attr('name', name);
				});
			});
		}
	});

	$('select.jyaml-custom-select-input').each(function(){
		var $this = $(this),
				option = $('<option />'),
				optgroup = $('optgroup', $this),
				emptyOption = $('option[value=""]:first', $this),
				placeholdTxt = $this.data('type-custom-text'),
				noTrim = $this.data('type-custom-notrim'),
				regex = $this.data('type-custom-regex'),
				beforeSelected = [];

		if (!placeholdTxt)
		{
			placeholdTxt = '...type custom value';
		}

		option.text(placeholdTxt).val('').data('jyaml-custom-input', true);

		if (emptyOption.length == 1)
		{
			option.insertAfter(emptyOption);
		}
		else if (optgroup && optgroup.length > 0)
		{
			option.prependTo(optgroup.eq(0));
		}
		else
		{
			option.prependTo($this);
		}

		$this.on('click', function(){
			beforeSelected = $('option:selected', this);
		})
		$this.on('change', function(){
			var option = $('option:selected', this);

			if (option.data('jyaml-custom-input') === true)
			{
				var result = prompt(placeholdTxt, option.val());
				result = (result === null ? '' : result);

				if (!noTrim)
				{
					result = result.toLowerCase();
					result = ('' + result).replace(/[^a-z0-9_-]/g, ''); // only allow a-z, 0-9, -, _
				}

				if (regex)
				{
					reg = new RegExp(regex, ''),
					regResult = reg.exec(result);

					if (regResult === null)
					{
						result = '';
					}
				}

				if (result != "")
				{
					option.text('[' + result + ']').val(result);
				}
				else if (beforeSelected && beforeSelected.length > 0)
				{
					$('option', this).removeAttr('selected');
					beforeSelected.attr('selected', 'selected');
					return false;
				}
			}
		});
	});

	var callUserFuncArray = (function(cb, parameters) {
		var func;

		if (typeof cb === 'string') {
			func = (typeof this[cb] === 'function') ? this[cb] : func = (new Function(null, 'return ' + cb))();
		} else if (Object.prototype.toString.call(cb) === '[object Array]') {
			func = (typeof cb[0] == 'string') ? eval(cb[0] + "['" + cb[1] + "']") : func = cb[0][cb[1]];
		} else if (typeof cb === 'function') {
			func = cb;
		}
		if (typeof func !== 'function') {
			throw new Error(func + ' is not a valid function');
		}
		return (typeof cb[0] === 'string') ? func.apply(eval(cb[0]), parameters) : (typeof cb[0] !== 'object') ? func.apply(null, parameters) : func.apply(cb[0], parameters);
	});


	// Create nice spacers
	$('.accordion-inner').has('.jyaml_help_link')
		.find('div.control-group').has('> div.control-label > span.spacer')
		.after('<hr />').remove();
});
