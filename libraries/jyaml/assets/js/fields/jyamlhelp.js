/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

/**
 * FormField Script for Jyamlhelp
 */
jQuery(document).ready(function($){
	/*
	$('.jyaml_help_link').each(function(){
		var $this = $(this),
				group = $this.closest('.control-group');

			$this.insertBefore(group);
			group.remove();
	});
	*/
	// @TODO: re-add the help buttons (temporary removed)
	$('.jyaml_help_link').hide();
});
