/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

/**
 * FormField Script for Jyamltemplatechunkcache
 */
jQuery(document).ready(function($){
	$('.jyamltemplatechunkcache[data-template][data-clientid]').on('click', function(){
		var $this = $(this),
				template = $this.data('template'),
				clientid = $this.data('clientid');

		if ($this.hasClass('disabled'))
		{
			return false;
		}

	 	var data = {
	 		tmpl: 'component',
	  	task: 'delete',
	  	client: clientid,
	  	'cid[]': 'tpl_' + template +  '_head',
	  };
	  data[JYAML.getFormToken()] = '1';

	  $this.addClass('disabled');
	  $this.spin({
		  lines: 13,
		  length: 3,
		  width: 2,
		  radius: 4,
		  corners: 1,
		  rotate: 0,
		  color: '#fff',
		  speed: 1.3,
		  trail: 70,
		  shadow: true,
		  hwaccel: true
		});

	  $.post('index.php?option=com_cache', data, function(t){
	  	$this.removeClass('disabled');
	  	$this.spin(false); // stop the spinner
	  });

		return false;
	});
});
