/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

/**
 * FormField Script for Jyamltemplatecolumnorder
 */
jQuery(document).ready(function($){
	var elements = $('select.jyamltemplatecolumnorder');

	elements.filter('[name*="layout_static"]').each(function(){
		var value = $(':selected', this).val();
		elements.not(this).closest('.control-group')[(value ? 'hide' : 'show')]();
	});

	elements.on('change', function(){
		var $this = $(this),
				id = $this.attr('id'),
				value = $(':selected', this).val(),
				container = $('#' + id + '_columnlabels'),
				cols = [], colsState = [],
				isStaticSelect = $this.is('[name*="layout_static"]');

		if (isStaticSelect)
		{
			elements.not(this).closest('.control-group')[(value ? 'hide' : 'show')]();
		}

		if (value && value.indexOf('_') !== -1)
		{
			var tmp = value.split('_'),
					arr = tmp[1].split('');

				$.each(arr, function(i, col) {
				  cols.push('' + col);
				  colsState.push(true);
				});
		}

		$.each(["1", "3", "2"], function(i, col) {
			if (-1 === $.inArray(col, cols))
			{
				cols.push(col);
				colsState.push(false);
			}
		});

		$.each(cols, function(i, col) {
			var label = $('.jyaml-col-' + col, container);

			if (colsState[i])
			{
				label.removeClass('label-important').addClass('label-success');
			}
			else
			{
				label.removeClass('label-success').addClass('label-important');
			}

			label.appendTo(container);
			container.append(' ');
		});

		container.show();
		if ('' == value)
		{
			container.hide();
		}
	});
});
