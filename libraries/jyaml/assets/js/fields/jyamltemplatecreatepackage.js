/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

/**
 * FormField Script for Jyamltemplatechunkcache
 */
jQuery(document).ready(function($){
	var form = $('#jyamltemplatecreatepackageForm'),
			dlin = $('input[name="jyaml_create_package_download"]', form);
	form.submit(function() {
		return dlin.val() ? true : false;
	});

	$('#jyaml_createpackage_btn').on('click', function(){
		dlin.val('0');
		$.blockUI({message:null, baseZ: 1e9});
		$('body').spin({color: '#fff'});

		$.ajax({
		  url: form.attr('action'),
		  type: form.attr('method'),
		  data: form.serialize(),
		  dataType: 'json'
		}).done(function(response){
			if (response.success) {
				dlin.val('1');
				form.submit();
			} else {
				alert(response.message);
			}
		}).always(function(){
    	$.unblockUI();
    	$('body').spin(false);
    });

		return false;
	});
});
