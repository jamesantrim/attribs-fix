/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

/**
 * FormField Script for Jyamltemplategetip
 */
jQuery(document).ready(function($){
	$('.jyamltemplategetip[data-value][data-value-target]').on('click', function(){
		var $this = $(this),
				value = $this.data('value'),
				target = $this.data('value-target');

		$(target).val(value);

		return false;
	});
});
