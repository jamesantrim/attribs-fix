/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

/**
 * FormField Script for jyamltemplatepositions
 */
jQuery(document).ready(function($){

	/** Fix (for J3.2.0+): Show only one column for dynamic positions tab */
	$('#attrib-positions > div > .span6, #attrib-scripts_stylesheets > div > .span6').removeClass('span6').addClass('span12');

	$('.jyamltemplateposition-type').on('change', function(){
		var $this = $(this),
				entry = $this.closest('.jyaml-group-entry'),
				nameEls = $('.jyamltemplateposition-name', entry),
				styleEls = $('.jyamltemplateposition-style', entry)
				selected = $this.val();

		$.merge(nameEls, styleEls).css('display', 'none').attr('disabled', 'disabled');
		if (selected)
		{
			nameEls.filter('[data-position-type="' + selected + '"]').css('display', '').removeAttr('disabled');

			if (selected == 'modules')
			{
				styleEls.css('display', '').removeAttr('disabled');
			}
		}

		nameEls.trigger('change');
		return false;
	});

	$('.jyamltemplateposition-name').on('change', function(){
		var $this = $(this),
				entry = $this.closest('.jyaml-group-entry'),
				styleEls = $('.jyamltemplateposition-style', entry);

		styleEls.trigger('change');
		return false;
	});

	$('.jyamltemplateposition-style').on('change', function(){
		var $this = $(this),
				entry = $this.closest('.jyaml_position_chrome_style'), // Nested substyle
				selected = $this.val();

		var	chromeEls = $('.jyaml_position_chrome_style', entry),
			chromeInputEls = $(':input', chromeEls);

		if (0 === entry.length)
		{
			// If no nested substyle search in entry
			entry = $this.closest('.jyaml-group-entry');
			chromeEls = $('.jyaml_position_modchrome_options > .jyaml_position_chrome_style', entry);
			chromeInputEls = $(':input', chromeEls);
		}

		chromeEls.css('display', 'none');
		chromeInputEls.css('display', 'none').attr('disabled', 'disabled');

		if (selected && $this.is(':visible'))
		{
			var styleEls = chromeEls.filter('[data-style="' + selected + '"], > [data-style="' + selected + '"]');

			if (!styleEls.length){
				// if not exists clone from dummy
				styleEls = $this.closest('.jyaml-move-group')
					.find('> .jyaml-group-entry-dummy .jyaml_position_modchrome_options > .jyaml_position_chrome_style[data-style="' + selected + '"]')
					.clone(true, true);

				if (styleEls.length)
				{
					var groupEntry = $this.closest('.jyaml-group-entry'),
						isSubStyle = groupEntry.find('> .jyamltemplatepositions_form');

					if (isSubStyle.length){
						styleEls.appendTo(isSubStyle);
					} else {
						styleEls.appendTo(groupEntry.find('.jyaml_position_modchrome_options'));
					}
					groupEntry.closest('.jyaml-move-group').trigger('jyaml_refresh_group');
				}
			}

			$(':input', styleEls).css('display', '').removeAttr('disabled');
			styleEls.css('display', '');

			// trigger substyles
			$('.jyamltemplateposition-style', styleEls).trigger('change');
		}

		return false;
	});
});
