/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

/**
 * FormField Script for Jyamltemplaterename
 */
jQuery(document).ready(function($){
	$('.jyamltemplaterename').on('click', function(){
		var $this = $(this),
				token = JYAML.getFormToken(),
				template = $this.data('template'),
				client = $this.data('clientid'),
				newName = $.trim($('#' + $this.data('id')).val());

		if ('' == newName)
		{
			return false;
		}

		$.blockUI({message:null, baseZ: 1e9});
		$('body').spin({color: '#fff'});

		data = {
			'jyaml_rename_template': {
				from: template,
				to: newName,
				clientid: client
			},
			jyamladmincontroller: 'templaterename'
		}
		data[token] = 1;

    $.post("index.php", data, function(response){
				if(response.success)
				{
					// redirect
					window.location.reload(true);
				}
				else
				{
					alert(response.message);
					$.unblockUI();
					$('body').spin(false);
				}
    }, 'json').fail(function(){
    	$.unblockUI();
    	$('body').spin(false);
    });
	});
});
