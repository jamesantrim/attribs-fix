/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

/**
 * FormField Script for Jyamltemplateupgrade
 */
jQuery(document).ready(function($){
	var blockPageReload = false;
	// Prevent page reload
	$(window).bind('beforeunload', function(){
		if (blockPageReload){
			return '';
		}
	});

	var btn = $('.jyamltemplateupgradeModalBtn'),
			box = $('#jyamltemplateupgradeModal'),
			initFooter = $('#jyamltemplateupgradeInitFooter', box),
			initForm = $('#jyamltemplateupgradeInitForm', box);

	box.modal({
		backdrop: 'static',
		show: false
	});

	var sendRequest = (function(){
		var form = initForm.clone(),
				nextstep = $(this).data('nextstep'),
				updateFileSelect = $('select#jyaml_upgrade_select_update_file', box);

		if (typeof(nextstep) != 'undefined'){
			$("input[name='jyaml_upgrade\[step\]']", form).val(nextstep);
		}

		$.blockUI({message:null, baseZ: 1e9});
		$('body').spin({color: '#fff'});

		if (updateFileSelect.length){
			$("input[name='jyaml_upgrade\[tpl_file\]']", form).val(updateFileSelect.val());
		}

		$.ajax({
		  url: form.attr('action'),
		  type: form.attr('method'),
		  data: form.serialize(),
		  dataType: 'json',
		  cache: false,
		}).done(function(response){
			if (response.success) {
				if (response.body){
					$('.modal-body', box).html(response.body);
				}
				else
				{
					$('.modal-body', box).html('');
				}
				if (response.footer){
					$('.modal-footer', box).html(response.footer);
				}
				else
				{
					$('.modal-footer', box).html(initFooter.html());
				}
				box.modal('show');
			} else {
				if (typeof(nextstep) == 'undefined'){
					box.modal('hide');
				}
				else{
					blockPageReload = true;
				}

				alert("Error:\n" + response.message);
			}

			blockPageReload = ((response.blockClose || response.forceBlockReload) ? true : false);
			$('.modal-header .close', box)[(response.blockClose ? 'hide' : 'show')]();

		}).always(function(){
    	$.unblockUI();
    	$('body').spin(false);
    });
	});
	btn.on('click', sendRequest);
	box.on('click', '#jyamltemplateupgradeContinueStep', sendRequest);
	box.on('change', '#jyaml_upgrade_select_update_file', sendRequest);
	box.on('click', '#jyamltemplateupgradeReloadPage', function(){
		$.blockUI({message:null, baseZ: 1e9});
		$('body').spin({color: '#fff'});
		blockPageReload = false;
		window.location.replace((window.location + '').split('#')[0]);
	});
	box.on('click', '#jyamltemplateupgradeDownloadBackup', function(){
		var form = initForm.clone();
		$("input[name='jyaml_upgrade\[step\]']", form).val('download_backup');
		form.submit();
	});

	if(window.location.hash == '#jyamlupgrade'){
		btn.trigger('click');
	}
});
