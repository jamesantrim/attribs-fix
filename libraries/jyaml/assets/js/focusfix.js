/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

/*
 * Workaround for IE8 und Webkit browsers to fix focus problems when using skiplinks
 * inspired by Paul Ratcliffe's article
 * http://www.communis.co.uk/blog/2009-06-02-skip-links-chrome-safari-and-added-wai-aria
 * Many thanks to Mathias Schäfer (http://molily.de/) for his code improvements
 *
 * @copyright       Copyright 2005-2012, Dirk Jesse
 * @license         CC-BY 2.0 (http://creativecommons.org/licenses/by/2.0/),
 *                  YAML-CDL (http://www.yaml.de/license.html)
 * @link            http://www.yaml.de
 * @package         yaml
 * @version         4.0+
 */

var YAML_focusFix = {
	skipClass: 'ym-skip',

	init: function () {
		if (YAML_focusFix.loaded) {
			return;
		}

		var userAgent = navigator.userAgent.toLowerCase();
		var	is_webkit = userAgent.indexOf('webkit') > -1;
		var	is_ie = userAgent.indexOf('msie') > -1;

		if (is_webkit || is_ie) {
			var body = document.body,
				handler = YAML_focusFix.click;
			if (body.addEventListener) {
				body.addEventListener('click', handler, false);
			} else if (body.attachEvent) {
				body.attachEvent('onclick', handler);
			}
		}
		YAML_focusFix.loaded = true;
	},

	trim: function (str) {
		return str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
	},

	click: function (e) {
		e = e || window.event;
		var target = e.target || e.srcElement;
		var a = target.className.split(' ');

		for (var i=0; i < a.length; i++) {
			var cls = YAML_focusFix.trim(a[i]);
			if ( cls === YAML_focusFix.skipClass) {
				YAML_focusFix.focus(target);
				break;
			}
		}
	},

	focus: function (link) {
		if (link.href) {
			var href = link.href,
				id = href.substr(href.indexOf('#') + 1),
				target = document.getElementById(id);
			if (target) {
				target.setAttribute("tabindex", "-1");
				target.focus();
			}
		}
	},

	loaded: null
};
