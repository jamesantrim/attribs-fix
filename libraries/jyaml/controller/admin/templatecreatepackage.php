<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  Controller
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

// No direct access
defined('_JEXEC') or die;

// Load dependencies
jimport('joomla.controller.base');
jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.archive');

/**
 * JYAML Templatecreatepackage Controller
 *
 * @package     JYAML
 * @subpackage  Controller
 * @since       4.5
 */
class JYAMLControllerAdminTemplatecreatepackage extends JControllerBase
{

	/**
	 * Execute the controller.
	 *
	 * @return void
	 */
	public function execute()
	{
		// Check for request forgeries
		JSession::checkToken() or die(JText::_('JINVALID_TOKEN'));

		$response = new \Joomla\Registry\Registry(array('success' => false, 'message' => ''));

		if ($this->getInput()->post->get('jyaml_create_package_download', 0, 'int'))
		{
			$this->downloadPackage();
		}

		try
		{
			if ($this->createPackage())
			{
				$response->set('success', true);
			}
		}
		catch (Exception $e)
		{
			$response->set('success', false);
			$response->set('message', $e->getMessage());
		}

		// Send json response
		echo json_encode($response->jsonSerialize());

		// Close the application.
		JFactory::getApplication()->close();
	}

	/**
	 * Method to create a jyaml template package including plugin and library
	 *
	 * @since 4.5.0
	 * @access private
	 * @return bool
	 *
	 * @throws  RuntimeException
	 */
	private function createPackage()
	{
		$db = JFactory::getDbo();
		$input = $this->getInput();
		$inputFilter = JFilterInput::getInstance();
		$config = JFactory::getConfig();

		$data = new \Joomla\Registry\Registry($input->post->get('jyaml_create_package', null, 'array'));

		$template = $inputFilter->clean($data->get('template'), 'cmd');
		$clientId = $inputFilter->clean($data->get('client_id', 0), 'int');
		$styleId = $inputFilter->clean($data->get('style_id', 0), 'int');

		$client = JApplicationHelper::getClientInfo($clientId);
		$clientPath = $client->path;

		$templatePath = $clientPath . '/templates/' . $template;
		$libraryPath = JPATH_ROOT . '/libraries/jyaml';
		$pluginPath = JPATH_ROOT . '/plugins/system/jyaml';

		$templateName = 'tpl_' . $client->name . '_' . $template . '.zip';

		$tmp_dest	= $config->get('tmp_path') . '/jyaml_create_package';
		$build_dest = $tmp_dest . '/_build';
		$packages_dest = $tmp_dest . '/packages';

		// Create required folders
		if (JFolder::exists($tmp_dest))
		{
			JFolder::delete($tmp_dest);
		}

		JFolder::create($tmp_dest);
		JFolder::create($build_dest);
		JFolder::create($packages_dest);

		// Copy required directories to build path
		JFolder::copy($templatePath, $build_dest . '/template');
		JFolder::copy($libraryPath, $build_dest . '/library');
		JFolder::copy($pluginPath, $build_dest . '/plugin');

		// Copy the library xml from Joomla manifest into the library package path
		$libManifestXml = JPATH_ADMINISTRATOR . '/manifests/libraries/jyaml.xml';
		if (JFile::exists($libManifestXml))
		{
			JFile::copy($libManifestXml, $build_dest . '/library/jyaml.xml');
		}

		$plgXML = file_get_contents($build_dest . '/plugin/jyaml.xml');
		$tmplXML = file_get_contents($build_dest . '/template/templateDetails.xml');

		// Define xml parameter 'create_module_examples_on_install'
		$installExampleModules = $inputFilter->clean($data->get('install_example_modules', 0), 'int');
		$_search = '#<create_module_examples_on_install>(.*)</create_module_examples_on_install>#Uis';
		$_replace = '<create_module_examples_on_install>' . ($installExampleModules ? '1' : '0') . '</create_module_examples_on_install>';
		$plgXML = preg_replace($_search, $_replace, $plgXML);

		// Define xml parameter 'template_layout_name'
		$_search = '#<template_layout_name>(.*)</template_layout_name>#Uis';
		$_replace = '<template_layout_name>' . $template . '</template_layout_name>';
		$plgXML = preg_replace($_search, $_replace, $plgXML);

		$includeCurrentConfiguration = $inputFilter->clean($data->get('include_current_configuration', 0), 'int');

		if ($includeCurrentConfiguration)
		{
			$query = $db->getQuery(true);
			$query->clear();
			$query->select('*');
			$query->from($db->qn('#__template_styles'));
			$query->where($db->qn('id') . ' = ' . $db->q($styleId));
			$db->setQuery($query);
			$result = $db->loadObject();

			if ($result)
			{
				$tmplCfg = new \Joomla\Registry\Registry;
				$tmplCfg->loadObject(json_decode($result->params));
				$tmplCfgJSON = $tmplCfg->toString('JSON');

				$_search = '#<defaultparams>(.*)</defaultparams>#Uis';
				$_replace = '<defaultparams><json><![CDATA[' . $tmplCfgJSON . ']]></json></defaultparams>';
				$tmplXML = preg_replace($_search, $_replace, $tmplXML);
			}
			else
			{
				throw new RuntimeException('Template style id not found: #' . $styleId);
			}
		}

		JFile::write($build_dest . '/plugin/jyaml.xml', $plgXML);
		JFile::write($build_dest . '/template/templateDetails.xml', $tmplXML);

		// Archive the template, libraray and system plugin in packages
		$zipSuccess = true;
		$result = $this->createZipFromFolder($packages_dest . '/' . $templateName, $build_dest . '/template');
		if (!$result)
		{
			$zipSuccess = false;
			throw new RuntimeException('Error on creating template archive');
		}

		$result = $this->createZipFromFolder($packages_dest . '/lib_jyaml.zip', $build_dest . '/library');
		if (!$result)
		{
			$zipSuccess = false;
			throw new RuntimeException('Error on creating library archiv');
		}

		if ($zipSuccess)
		{
			if (JFolder::exists($build_dest . '/plugin/packages'))
			{
				JFolder::delete($build_dest . '/plugin/packages');
			}
			JFolder::create($build_dest . '/plugin/packages');

			JFile::copy($packages_dest . '/' . $templateName, $build_dest . '/plugin/packages/' . $templateName);
			JFile::copy($packages_dest . '/lib_jyaml.zip', $build_dest . '/plugin/packages/lib_jyaml.zip');
		}

		$result = $this->createZipFromFolder($packages_dest . '/pkg_jyaml--tpl_' . $template . '.zip', $build_dest . '/plugin');
		if (!$result)
		{
			$zipSuccess = false;
			throw new RuntimeException('Error on creating package archive');
		}

		if ($zipSuccess)
		{
			return true;
		}

		return false;
	}

	/**
	 * Send download response of the created packages
	 *
	 * @since 4.5.0
	 * @access protected
	 * @return void
	 */
	protected function downloadPackage()
	{
		$input = $this->getInput();
		$inputFilter = JFilterInput::getInstance();

		$data = new \Joomla\Registry\Registry($input->post->get('jyaml_create_package', null, 'array'));
		$template = $inputFilter->clean($data->get('template'), 'cmd');

		$config = JFactory::getConfig();
		$tmp_dest = JPath::clean($config->get('tmp_path') . '/jyaml_create_package/packages/pkg_jyaml--tpl_' . $template . '.zip');

		header("HTTP/1.1 200 OK");
		header("Content-Type: application/force-download");
		header('Content-Disposition: attachment; filename="' . basename($tmp_dest) . '"');
		header("Content-Transfer-Encoding: binary");
		@readfile($tmp_dest);
		jexit();
	}

	/**
	 * Method to create a Zip-Archive of a folder
	 *
	 * @param   string  $destination  Destination of the Zip-Archive
	 * @param   string  $folder       The Folder to Archive
	 * @param   array   $exclude      Array of excludes
	 *
	 * @since 4.5.0
	 * @access protected
	 * @return boolean True on Success
	 */
	protected function createZipFromFolder($destination, $folder, $exclude = array())
	{
		$destination = JPath::clean($destination);
		$folder = JPath::clean($folder);

		$zip = new ZipArchive;

		$res = $zip->open($destination, ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE);
		if ($res !== true)
		{
			throw new RuntimeException('Error on creating archive (ZipArchive Error-Code: ' . $res . ') - ' . $destination);
		}
		else
		{
			$files = JFolder::files($folder, '.', true, true, $exclude, array('.*~'));

			foreach ($files as $_file)
			{
				$localName = substr($_file, (strlen($folder) + 1));
				$zip->addFile($_file, $localName);
			}
			$zip->close();

			return file_exists($destination);
		}
	}
}
