<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  Controller
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

// No direct access
defined('_JEXEC') or die;

// Load dependencies
jimport('joomla.controller.base');
jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');

/**
 * JYAML Templatetemplaterename Controller
 *
 * @package     JYAML
 * @subpackage  Controller
 * @since       4.5
 */
class JYAMLControllerAdminTemplaterename extends JControllerBase
{

	/**
	 * Execute the controller.
	 *
	 * @return void
	 */
	public function execute()
	{
		// Check for request forgeries
		JSession::checkToken() or die(JText::_('JINVALID_TOKEN'));

		$response = new \Joomla\Registry\Registry(array('success' => false, 'message' => ''));

		try
		{
			if ($this->renameTemplate())
			{
				$response->set('success', true);
			}
		}
		catch (Exception $e)
		{
			$response->set('success', false);
			$response->set('message', $e->getMessage());
		}

		// Send json response
		echo json_encode($response->jsonSerialize());

		// Close the application.
		JFactory::getApplication()->close();
	}

	/**
	 * Method to rename a jyaml supported template
	 *
	 * @access private
	 * @return bool
	 *
	 * @throws  RuntimeException
	 */
	private function renameTemplate()
	{
		$db = JFactory::getDbo();
		$input = $this->getInput();
		$inputFilter = JFilterInput::getInstance();

		$data = new \Joomla\Registry\Registry($input->post->get('jyaml_rename_template', null, 'array'));

		$fromName = $inputFilter->clean($data->get('from', ''), 'cmd');
		$toName = $inputFilter->clean($data->get('to', ''), 'cmd');
		$toName = preg_replace('#\.\.[^A-Za-z0-9\.\_\- ]#', '', $toName);

		$clientId = $inputFilter->clean($data->get('clientid', 0), 'int');
		$client = JApplicationHelper::getClientInfo($clientId);

		$clientPath = null;
		if ($client && $client->path)
		{
			$clientPath = $client->path;
		}

		$templateFolder = $clientPath . '/templates/' . $fromName;
		$templateFolderNew = $clientPath . '/templates/' . $toName;

		$check = ($clientPath !== null) ? true : false;

		if ($fromName == $toName)
		{
			$check = false;
		}
		if (!JFolder::exists($templateFolder))
		{
			$check = false;
		}
		if (JFolder::exists($templateFolderNew))
		{
			$check = false;
			throw new RuntimeException(JText::sprintf('JYAML__ERROR_TEMPLATE_ALREADY_EXISTS', $toName));
		}

		// Get langauge files
		$languageFiles = (array) JFolder::files($clientPath . '/templates/' . $fromName . '/language', '.ini$|.ini.example$', true, true);
		$languageFilesCore = (array) JFolder::files($clientPath . '/language', '.tpl_' . $fromName . '.ini$', true, true);
		$languageFiles = array_merge($languageFiles, $languageFilesCore);

		// Check is writable for all required files/folders
		if ($check == true)
		{
			if ($languageFiles)
			{
				foreach ($languageFiles as $_file)
				{
					$filename = basename($_file);
					$newFile = str_replace('.tpl_' . $fromName . '.', '.tpl_' . $toName . '.', $filename);
					$newFile = dirname($_file) . DIRECTORY_SEPARATOR . $newFile;

					if (!JYAML::_isWritable($newFile))
					{
						$_path = str_replace(JPATH_ROOT, '', $newFile);
						throw new RuntimeException(JText::sprintf('JYAML__ERROR_NOT_WRITABLE', $_path));
					}
				}
			}

			$checkPath = JPath::clean($clientPath . '/templates');
			if (!JYAML::_isWritable($checkPath))
			{
				$_path = str_replace(JPATH_ROOT, '', $checkPath);
				throw new RuntimeException(JText::sprintf('JYAML__ERROR_NOT_WRITABLE', $_path));
			}

			$checkPath = JPath::clean($clientPath . '/templates/' . $fromName . '/language');
			if (!JYAML::_isWritable($checkPath))
			{
				$_path = str_replace(JPATH_ROOT, '', $checkPath);
				throw new RuntimeException(JText::sprintf('JYAML__ERROR_NOT_WRITABLE', $_path));
			}

			$checkPath = JPath::clean($clientPath . '/language');
			if (!JYAML::_isWritable($checkPath))
			{
				$_path = str_replace(JPATH_ROOT, '', $checkPath);
				throw new RuntimeException(JText::sprintf('JYAML__ERROR_NOT_WRITABLE', $_path));
			}

			if ($languageFiles)
			{
				foreach ($languageFiles as $_file)
				{
					$filename = basename($_file);
					$newFile = str_replace('.tpl_' . $fromName . '.', '.tpl_' . $toName . '.', $filename);
					$newFile = dirname($_file) . DIRECTORY_SEPARATOR . $newFile;

					JFile::move($_file, $newFile);
				}
			}

			// Rename template folder
			if (JFile::move($templateFolder, $templateFolderNew))
			{
				// Rename in database
				$db = JFactory::getDbo();
				$query = $db->getQuery(true);

				$query->clear();
				$query->select('*');
				$query->from($db->qn('#__extensions'));
				$query->where($db->qn('name') . ' = ' . $db->q($fromName));
				$query->where($db->qn('element') . ' = ' . $db->q($fromName));
				$query->where($db->qn('type') . ' = ' . $db->q('template'));
				$db->setQuery($query);
				$result = $db->loadObject();

				if ($result)
				{
					$manifest_cache = json_decode($result->manifest_cache);
					$manifest_cache->name = $toName;
					$manifest_cache = json_encode($manifest_cache);

					$query->clear();
					$query->update($db->qn('#__template_styles'));
					$query->set($db->qn('template') . ' = ' . $db->q($toName));
					$query->where($db->qn('template') . ' = ' . $db->q($fromName));
					$db->setQuery($query);
					$db->execute();

					$query->clear();
					$query->update($db->qn('#__extensions'));
					$query->set($db->qn('name') . ' = ' . $db->q($toName));
					$query->set($db->qn('element') . ' = ' . $db->q($toName));
					$query->set($db->qn('manifest_cache') . ' = ' . $db->q($manifest_cache));
					$query->where($db->qn('name') . ' = ' . $db->q($fromName));
					$query->where($db->qn('element') . ' = ' . $db->q($fromName));
					$query->where($db->qn('type') . ' = ' . $db->q('template'));
					$db->setQuery($query);
					$db->execute();

					// Rename name in templateDetails.xml
					$xmlFile = $templateFolderNew . '/templateDetails.xml';
					$tmplXML = file_get_contents($xmlFile);
					$tmplXML = str_ireplace('<name>' . $fromName . '</name>', '<name>' . $toName . '</name>', $tmplXML);
					JFile::write($xmlFile, $tmplXML);

					// Replace layout overrides from extension params
					// @TODO read params and change with php and write to db (for multi-database support)
					$sqlReplace = 'replace(`params`, \'layout":"' . $fromName . ':\', \'layout":"' . $toName . ':\')';
					$query->clear();
					$query->update($db->qn('#__extensions'));
					$query->set($db->qn('params') . ' = ' . $sqlReplace);
					$db->setQuery($query);
					$db->execute();

					// Replace layout overrides from module params
					// @TODO read params and change with php and write to db (for multi-database support)
					$sqlReplace = 'replace(`params`, \'layout":"' . $fromName . ':\', \'layout":"' . $toName . ':\')';
					$query->clear();
					$query->update($db->qn('#__modules'));
					$query->set($db->qn('params') . ' = ' . $sqlReplace);
					$db->setQuery($query);
					$db->execute();
				}
			}
		}

		return true;
	}
}
