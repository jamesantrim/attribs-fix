<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  Controller
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

// No direct access
defined('_JEXEC') or die;

// Load dependencies
jimport('joomla.controller.base');
jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.archive');

/**
 * JYAML Templateupgrade Controller
 *
 * @package     JYAML
 * @subpackage  Controller
 * @since       4.5
 */
class JYAMLControllerAdminTemplateupgrade extends JControllerBase
{
	protected $prevStep = null;

	protected $step = 0;

	protected $template = '';

	protected $client_id = 0;

	protected $to_version = '';

	protected $to_date = '';

	protected $tpl_file = '';

	protected $response = null;

	/**
	 * Execute the controller.
	 *
	 * @return void
	 */
	public function execute()
	{
		// Check for request forgeries
		JSession::checkToken() or die(JText::_('JINVALID_TOKEN'));

		$defaultResponse = array(
			'success' => false,
			'message' => '',
			'body' => '',
			'footer' => '',
			'blockClose' => false,
			'forceBlockReload' => false
		);
		$this->response = new \Joomla\Registry\Registry($defaultResponse);

		try
		{
			$app = JFactory::getApplication();

			$input = $this->getInput();
			$inputFilter = JFilterInput::getInstance();
			$data = new \Joomla\Registry\Registry($input->post->get('jyaml_upgrade', null, 'array'));

			// Initialise variables
			$this->step = $inputFilter->clean($data->get('step', '0'), 'string');
			if ($this->step != 'download_backup')
			{
				$this->step = (int) $this->step;

				if ($this->step > 0)
				{
					$this->response->set('forceBlockReload', true);
				}
			}

			$this->template = $inputFilter->clean($data->get('template', ''), 'cmd');
			$this->client_id = $inputFilter->clean($data->get('client_id', '0'), 'int');
			$this->to_version = $inputFilter->clean($data->get('to_version', ''), 'string');
			$this->to_date = $inputFilter->clean($data->get('to_date', ''), 'string');
			$this->tpl_file = $inputFilter->clean($data->get('tpl_file', ''), 'string');

			$this->prevStep = $app->getUserState('lib_jyaml.upgrade.currentStep', null);
			$app->setUserState('lib_jyaml.upgrade.previousStep', $this->prevStep);
			$app->setUserState('lib_jyaml.upgrade.currentStep', $this->step);

			$stepMethod = 'getStep_' . $this->step;
			if (method_exists($this, $stepMethod))
			{
				if ($this->$stepMethod())
				{
					$this->response->set('success', true);
				}
			}
			else
			{
				throw new RuntimeException('Method not found: ' . $stepMethod);
			}
		}
		catch (Exception $e)
		{
			$this->response->set('success', false);
			$this->response->set('message', $e->getMessage());
		}

		// Send json response
		echo json_encode($this->response->jsonSerialize());

		// Close the application.
		JFactory::getApplication()->close();
	}

	/**
	 * Get the absolute path of the affected template
	 *
	 * @return string Absolute template path
	 */
	private function getTemplatePath()
	{
		static $path;

		if ($path === null)
		{
			$client = JApplicationHelper::getClientInfo($this->client_id);
			$clientPath = $client->path;
			$path = $clientPath . '/templates/' . $this->template;
		}

		return $path;
	}

	/**
	 * Method to download a backup archive of a specific template
	 *
	 * @access private
	 *
	 * @return void
	 */
	private function getStep_download_backup()
	{
		$templatePath = $this->getTemplatePath();

		$config = JFactory::getConfig();
		$tmp_dest = $config->get('tmp_path') . '/tpl_' . $this->template . '_backup.zip';

		if (JFolder::exists($templatePath))
		{
			$zip = new ZipArchive;

			if (JFile::exists($tmp_dest))
			{
				JFile::delete($tmp_dest);
			}

			$res = $zip->open($tmp_dest, ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE);
			if ($res === true)
			{
				$files = JFolder::files($templatePath, '.', true, true);

				// Descriptor iterator
				$count = 0;

				foreach ($files as $_file)
				{
					$relFile = substr($_file, strlen($templatePath . '/'));
					$zip->addFile($_file, $relFile);

					if (($count++) == 200)
					{
						// Fix for the file descriptor limit
						$zip->close();
						if ($zip = new ZipArchive)
						{
							$zip->open($tmp_dest);
							$count = 0;
						}
					}
				}
				$zip->close();

				header("HTTP/1.1 200 OK");
				header("Content-Type: application/force-download");
				header('Content-Disposition: attachment; filename="' . basename($tmp_dest) . '"');
				header("Content-Transfer-Encoding: binary");
				@readfile($tmp_dest);
				jexit();
			}
			else
			{
				die('Can not create/open zip archive - ZipArchive Error-Code: ' . $res);
			}
		}
	}

	/**
	 * Method to download a backup archive of a specific template.
	 * And does set JSON response.
	 *
	 * @access privte
	 * @return true on success
	 */
	private function getStep_0()
	{
		$app = JFactory::getApplication();

		$body = '';
		$footer = '';

		$client = JApplicationHelper::getClientInfo($this->client_id);
		$libUpdatePath = JPath::clean(JYAML_LIB_PATH . '/update/downloads/template');

		$libTemplatePackageFile = '';
		$libTemplatePackageFiles = array();

		$templatePackageSearchFiles = array(
			'tpl_' . $client->name . '_' . $this->template . '.zip',
		);
		if ($client->name == 'site')
		{
			// Apply core template only for client=site (frontend)
			// Admin templates are not included in the core framework
			$templatePackageSearchFiles[] = 'tpl_jyaml.zip';
		}

		foreach ($templatePackageSearchFiles as $_filename)
		{
			$_path = JPath::clean(JYAML_LIB_PATH . '/update/' . $_filename);
			if (JFile::exists($_path))
			{
				$libTemplatePackageFiles[] = $_path;
			}
		}
		if (empty($libTemplatePackageFiles))
		{
			$body = '<p>' . JText::_('JYAML_UPGRADE_NO_NEW_VERSION_FOUND') . '</p>';
			$this->response->set('footer', '');
			$this->response->set('body', $body);
			return true;
		}

		// Cleanup the jyaml library update folder
		if (JFolder::exists($libUpdatePath))
		{
			JFolder::delete($libUpdatePath);
		}

		if (count($libTemplatePackageFiles) == 1)
		{
			$libTemplatePackageFile = array_shift($libTemplatePackageFiles);
		}
		else
		{
			if (!$this->tpl_file)
			{
				$body .= '<div class="alert alert-info alert-block">' . JText::_('JYAML_UPGRADE_SELECT_TPL_FILE_DESC') . '</div>';
			}

			$body .= '<div class="form-inline">';
			$body .= '<label for="jyaml_upgrade_select_update_file">' . JText::_('JYAML_UPGRADE_SELECT_TPL_FILE') . '</label>';
			$body .= '<select id="jyaml_upgrade_select_update_file">';

			$body .= '<option value="">' . JText::_('JSELECT') . '</option>';
			foreach ($libTemplatePackageFiles as $_updateFile)
			{
				$_isCore = false;
				$_value = basename($_updateFile);
				$_value = JFile::stripExt($_value);
				$_text = $_value;
				$_isCore = (($_text === 'tpl_jyaml') ? true : false);
				$_text = preg_replace('#^tpl_' . $client->name . '_#', 'tpl_', $_text);
				$_text .= ' ' . ($_isCore ? '(Core)' : '(Custom)');

				$_selected = (($_value == $this->tpl_file) ? ' selected="selected"' : '');
				$body .= '<option' . $_selected . ' value="' . $_value . '">' . $_text . '</option>';
			}
			$body .= '</select>';
			$body .= '</div>';

			$this->response->set('footer', $footer);
			$this->response->set('body', $body);

			if (!$this->tpl_file)
			{
				return true;
			}
			else
			{
				$body .= '<hr />';
			}
		}

		if ($this->tpl_file)
		{
			$libTemplatePackageFile = JPath::clean(JYAML_LIB_PATH . '/update/' . $this->tpl_file . '.zip');
		}

		// Store current template in library update folder
		if (JFile::exists($libTemplatePackageFile))
		{
			jimport('joomla.installer.helper');
			$package = JInstallerHelper::unpack(JPath::clean($libTemplatePackageFile));

			if ($package && isset($package['type']) && $package['type'] == 'template')
			{
				JFolder::copy($package['extractdir'], $libUpdatePath, '', true);

				if (JFolder::exists($package['extractdir']))
				{
					JFolder::delete($package['extractdir']);
				}

				// Rename the language files
				$languageFiles = JFolder::files($libUpdatePath . '/language', ' . ini$|.ini.example$', true, true);
				$fromName = 'jyaml';
				$toName = $this->template;

				if ($languageFiles && is_array($languageFiles))
				{
					foreach ($languageFiles as $_file)
					{
						$filename = basename($_file);
						$newFile = JPath::clean(str_replace(' . tpl_' . $fromName . '.', '.tpl_' . $toName . '.', $filename));
						$newFile = JPath::clean(dirname($_file) . '/' . $newFile);

						if ($_file != $newFile)
						{
							JFile::move($_file, $newFile);
						}
					}
				}
			}
		}

		$templatePath = $this->getTemplatePath();

		$fromVersion = null;
		$_templateDetailsXml = $templatePath . '/templateDetails.xml';
		$templateXml = null;
		if (JFile::exists($_templateDetailsXml))
		{
			if (($xml = simplexml_load_file($_templateDetailsXml)))
			{
				$templateXml = $xml;
				$fromVersion = (string) $xml->version;
			}
		}

		$newVersion = false;
		$templateDetailsXml = $libUpdatePath . '/templateDetails.xml';

		if (JFile::exists($templateDetailsXml))
		{
			if (($xml = simplexml_load_file($templateDetailsXml)))
			{
				$currentVersionXML = str_replace(' ', '.', (string) $templateXml->version);
				$toVersionXML = str_replace(' ', '.', (string) $xml->version);

				$app->setUserState('lib_jyaml.upgrade.scripts', null);

				if (JYAMLHelper::version_compare($templateXml->version, $templateXml->creationDate, $xml->version, $xml->creationDate, '<') == 1)
				{
					$updateXmlFiles = $this->getXMLUpgradeFiles($currentVersionXML, $toVersionXML);

					if (!empty($updateXmlFiles))
					{
						$updateScripts = array();
						foreach ($updateXmlFiles as $_xml)
						{
							$script = (string) $_xml->script;
							if ($script)
							{
								$updateScripts[] = $script;
							}
						}

						$app->setUserState('lib_jyaml.upgrade.scripts', $updateScripts);
					}

					$body .= '<p>' . JText::_('JYAML_CURRENT_TMPL_VERSION') . ': <span class="label">'
							. $templateXml->version . ' (' . $templateXml->creationDate . ')</span></p>';
					$body .= '<hr /><p>' . JText::_('JYAML_UPGRADE_NEW_VERSION_AVAILABLE')
							. ': <span class="label label-success">' . $xml->version . ' (' . $xml->creationDate . ')</span></p><hr />';

					$bloglink = '<a href="http://www.jyaml.de/en/blog/" target="_blank">http://www.jyaml.de/en/blog/</a>';
					$body .= '<p>' . JText::sprintf('JYAML_UPGRADE_INFO_TXT', $bloglink) . '</p>';

					$body .= '<div class="alert alert-block">';
					$body .= '<p>' . JText::sprintf('JYAML_UPGRADE_INFO_BACKUP_TXT') . '</p>';
					$body .= '<button class="btn btn-small btn-warning" type="button" id="jyamltemplateupgradeDownloadBackup">'
							. JText::_('JYAML_UPGRADE_BACKUP_DOWNLOAD_BTN') . '</button>';
					$body .= '</div>';

					$footer .= '<button class="btn pull-left" data-dismiss="modal" aria-hidden="true">' . JText::_('JYAML_CLOSE') . '</button>';
					$footer .= '<button class="btn btn-primary" id="jyamltemplateupgradeContinueStep" data-nextstep="' . ($this->step + 1) . '">'
							. JText::_('JYAML_UPGRADE_CONTINUE_TXT') . ': ' . JText::_('JYAML_UPGRADE_STEPNUM' . ($this->step + 1)) . '</button>';

					$app->setUserState('lib_jyaml.upgrade.fromVersion', $currentVersionXML);
					$app->setUserState('lib_jyaml.upgrade.toVersion', $toVersionXML);
					$newVersion = true;
				}
			}
		}

		if (!$newVersion)
		{
			$body = '<p>' . JText::_('JYAML_UPGRADE_NO_NEW_VERSION_FOUND') . '</p>';
			$footer = '';
		}

		$this->response->set('footer', $footer);
		$this->response->set('body', $body);

		return true;
	}

	/**
	 * Read upgrade xml files and overwrite necessary Files.
	 * The file definitions are stored in /libraries/jyaml/update/*.xml.
	 * And does set JSON response.
	 *
	 * @access private
	 *
	 * @return true on success
	 */
	private function getStep_1()
	{
		$app = JFactory::getApplication();

		$body = '';
		$footer = '';

		$owFilesSuccess = array();
		$owFilesFailed = array();
		$backupFileList = array();

		$libUpdate = JYAML_LIB_PATH . '/update';
		$libUpdatePath = $libUpdate . '/downloads/template';
		$templatePath = $this->getTemplatePath();

		$fromVersion = $app->getUserState('lib_jyaml.upgrade.fromVersion');
		$toVersion = $app->getUserState('lib_jyaml.upgrade.toVersion');

		$fromVersion = str_replace(' ', '.', $fromVersion);
		$toVersion = str_replace(' ', '.', $toVersion);

		if (version_compare($fromVersion, $toVersion, '='))
		{
			// Do not upgrade twice (new build on same version should not have updates for the template)
			$overwriteFiles = array();
			$app->setUserState('lib_jyaml.upgrade.scripts', null);
		}
		else
		{
			$overwriteFiles = $this->upgradeGetIncrmentalList($fromVersion, $toVersion);
		}

		if ($overwriteFiles && isset($overwriteFiles['necessary']))
		{
			$files = isset($overwriteFiles['necessary']['files']) ? $overwriteFiles['necessary']['files'] : array();
			$folders = isset($overwriteFiles['necessary']['folders']) ? $overwriteFiles['necessary']['folders'] : array();

			foreach ($files as $file)
			{
				$_src = JPath::clean($libUpdatePath . '/' . $file);
				$_dest = JPath::clean($templatePath . '/' . $file);

				// Source does not exists. Do nothing with it, no errors.
				if (!JFile::exists($_src))
				{
					continue;
				}

				// Check is already copied as new file
				if (in_array($_dest, $overwriteFiles))
				{
					continue;
				}

				// Make backup
				$doCopy = true;
				$backupDest = $_dest . '.bak-' . $fromVersion;
				if (JFile::exists($_dest) && !JFile::exists($backupDest))
				{
					$doCopy = JFile::copy($_dest, $backupDest);
				}
				$backupFileList[$_dest] = basename($backupDest);

				$_destFolder = dirname($_dest);
				if (!JFolder::exists($_destFolder))
				{
					JFolder::create($_destFolder);
				}

				if ($doCopy && JFile::copy($_src, $_dest))
				{
					$owFilesSuccess[] = $_dest;
				}
				else
				{
					$owFilesFailed[] = $_dest;
				}
			}

			foreach ($folders as $folder)
			{
				$_src = JPath::clean($libUpdatePath . '/' . $folder);
				$_dest = JPath::clean($templatePath . '/' . $folder);

				// Source does not exists. Do nothing with it, no errors.
				if (!JFolder::exists($_src))
				{
					continue;
				}

				// Make backup
				$doCopy = true;
				$backupDest = $_dest . '.bak-' . $fromVersion;
				if (JFolder::exists($_dest) && !JFolder::exists($backupDest))
				{
					$doCopy = JFolder::copy($_dest, $backupDest, '', true);
				}
				$backupFileList[$_dest] = basename($backupDest);

				if ($doCopy)
				{
					if (JFolder::copy($_src, $_dest, '', true))
					{
						$owFilesSuccess[] = $_dest;
					}
					else
					{
						$owFilesFailed[] = $_dest;
					}
				}
			}
		}

		$app->setUserState('lib_jyaml.upgrade.owFilesSuccess', $owFilesSuccess);
		$app->setUserState('lib_jyaml.upgrade.owFilesFailed', $owFilesFailed);

		if (!empty($owFilesFailed))
		{
			$body .= '<div class="alert alert-block">' . JText::_('JYAML_UPGRADE_OVERWRITE_FILES_FAILED') . '</div>';
			$body .= '<small><ul>';
			foreach ($owFilesFailed as $_file)
			{
				$body .= '<li>' . JPath::clean(substr($_file, strlen(JPATH_ROOT))) . '</li>';
			}
			$body .= '</ul></small>';
			$footer .= '<button class="btn btn-danger" id="jyamltemplateupgradeContinueStep" data-nextstep="' . $this->step . '">'
					. JText::_('JYAML_UPGRADE_TRY_AGAIN') . '</button>';
		}
		else
		{
			$this->response->set('blockClose', true);
			$footer .= '<button class="btn btn-primary" id="jyamltemplateupgradeContinueStep" data-nextstep="' . ($this->step + 1) . '">'
					. JText::_('JYAML_UPGRADE_CONTINUE_TXT') . ': ' . JText::_('JYAML_UPGRADE_STEPNUM' . ($this->step + 1)) . '</button>';
		}

		if (!empty($owFilesSuccess))
		{
			$body .= '<div class="alert alert-block alert-success">' . JText::_('JYAML_UPGRADE_OVERWRITE_FILES_SUCCESS') . '</div>';
			$body .= '<small><ul>';
			foreach ($owFilesSuccess as $_file)
			{
				$body .= '<li>' . JPath::clean(substr($_file, strlen(JPATH_ROOT))) . (isset($backupFileList[$_file])
						? ' <br /><small class="muted"> -&gt; Backup: .' . DIRECTORY_SEPARATOR . $backupFileList[$_file] : '') . '</small></li>';
			}
			$body .= '</ul></small>';
		}
		elseif (empty($owFilesSuccess) && empty($owFilesFailed))
		{
			$body .= '<div class="alert alert-block alert-success">' . JText::_('JYAML_UPGRADE_COPY_NO_OVERWRITE_FILES_CONTINUE') . '</div>';
		}

		$this->response->set('footer', $footer);
		$this->response->set('body', $body);
		return true;
	}

	/**
	 * Copy all new files are not exists in the template folder
	 * And does set JSON response.
	 *
	 * @access private
	 *
	 * @return true on success
	 */
	private function getStep_2()
	{
		$app = JFactory::getApplication();

		$body = '';
		$footer = '';

		$libUpdatePath = JYAML_LIB_PATH . '/update/downloads/template';
		$templatePath = $this->getTemplatePath();

		$newFilesSuccess = array();
		$newFilesFailed = array();

		if (JFolder::exists($libUpdatePath) && JFolder::exists($templatePath))
		{
			$files = JFolder::files($libUpdatePath, '.', true, true);

			foreach ($files as $_src)
			{
				$_dest = JPath::clean($templatePath . '/' . substr($_src, strlen($libUpdatePath . '/')));
				$_destFolder = dirname($_dest);

				if (!JFolder::exists($_destFolder))
				{
					JFolder::create($_destFolder);
				}

				if (!JFile::exists($_dest))
				{
					if ( JFile::copy($_src, $_dest) )
					{
						$newFilesSuccess[] = $_dest;
					}
					else
					{
						$newFilesFailed[] = $_dest;
					}
				}
			}
		}

		$updateScripts = $app->getUserState('lib_jyaml.upgrade.scripts', null);
		$hasUpdateScripts = !empty($updateScripts);

		$app->setUserState('lib_jyaml.upgrade.newFilesSuccess', $newFilesSuccess);
		$app->setUserState('lib_jyaml.upgrade.newFilesFailed', $newFilesFailed);

		if (!empty($newFilesFailed))
		{
			$body .= '<div class="alert alert-block">' . JText::_('JYAML_UPGRADE_COPY_NEW_FILES_FAILED') . '</div>';
			$body .= '<ul>';
			foreach ($newFilesFailed as $_file)
			{
				$body .= '<li>' . JPath::clean(substr($_file, strlen(JPATH_ROOT))) . '</li>';
			}
			$body .= '</ul>';
			$footer .= '<button class="btn btn-danger" id="jyamltemplateupgradeContinueStep" data-nextstep="' . $this->step . '">'
					. JText::_('JYAML_UPGRADE_TRY_AGAIN') . '</button>';
		}
		else
		{
			$nextStep = ($this->step + ($hasUpdateScripts ? 1 : 2));
			$this->response->set('blockClose', true);
			$footer .= '<button class="btn btn-primary" id="jyamltemplateupgradeContinueStep" data-nextstep="' . $nextStep . '">'
					. JText::_('JYAML_UPGRADE_CONTINUE_TXT') . ': ' . JText::_('JYAML_UPGRADE_STEPNUM' . $nextStep) . '</button>';
		}

		if (!empty($newFilesSuccess))
		{
			$body .= '<div class="alert alert-block alert-success">' . JText::_('JYAML_UPGRADE_COPY_NEW_FILES_SUCCESS') . '</div>';
			$body .= '<small><ul>';
			foreach ($newFilesSuccess as $_file)
			{
				$body .= '<li>' . JPath::clean(substr($_file, strlen(JPATH_ROOT))) . '</li>';
			}
			$body .= '</ul></small>';
		}
		elseif (empty($newFilesSuccess) && empty($newFilesFailed))
		{
			$body .= '<div class="alert alert-block alert-success">' . JText::_('JYAML_UPGRADE_COPY_NO_NEW_FILES_CONTINUE') . '</div>';
		}

		$this->response->set('footer', $footer);
		$this->response->set('body', $body);
		return true;
	}

	/**
	 * Migration Step / Update Scripts
	 * And does set JSON response.
	 *
	 * @access private
	 *
	 * @return true on success
	 */
	private function getStep_3()
	{
		$app = JFactory::getApplication();
		$body = '';
		$footer = '';

		$updateScripts = $app->getUserState('lib_jyaml.upgrade.scripts', null);

		$template = $this->template;
		$clientId = $this->client_id;

		if (is_array($updateScripts) && !empty($updateScripts) && $template != '' && $clientId !== null)
		{
			$excludeFiles = array();
			$_newFiles = $app->getUserState('lib_jyaml.upgrade.newFilesSuccess');
			if (is_array($_newFiles) && !empty($_newFiles))
			{
				$excludeFiles = array_merge($_newFiles);
			}

			$_owFiles = $app->getUserState('lib_jyaml.upgrade.owFilesSuccess');
			if (is_array($_owFiles) && !empty($_owFiles))
			{
				$excludeFiles = array_merge($_owFiles);
			}
			$excludeFiles = array_unique($excludeFiles);

			require_once JYAML_LIB_PATH . '/update/script/update.php';
			foreach ($updateScripts as $k => $scriptfile)
			{
				$updateScript = JYAMLUpdateScript::getInstance($scriptfile);
				$updateScript->setExcludeFiles($excludeFiles);

				$body .= '<p><strong>' . $updateScript->getTitleText() . '</strong></p>';
				$body .= '<p><small>' . $updateScript->getDescriptionText() . '</small></p>';

				// Test and check for errors
				$updateScript->run($template, $clientId, true);
				$errors = $updateScript->getErrorLog();

				if (empty($errors))
				{
					// Now migrate
					$updateScript->run($template, $clientId, false);

					$logsString = $updateScript->getLog(true);
					$body .= '<p><strong>Log:</strong></p>';

					$logsString = trim($logsString);
					if (empty($logsString))
					{
						$logsString = JText::_('JYAML_UPDATE_SCRIPT_MIGRATE_EMPTY_LOG');
					}
					$body .= '<pre class="pre-scrollable">' . $logsString . '</pre>';

					$this->response->set('blockClose', true);
					$footer .= '<button class="btn btn-primary" id="jyamltemplateupgradeContinueStep" data-nextstep="' . ($this->step + 1) . '">'
							. JText::_('JYAML_UPGRADE_CONTINUE_TXT') . ': ' . JText::_('JYAML_UPGRADE_STEPNUM' . ($this->step + 1)) . '</button>';
				}
				else
				{
					$body .= '<div class="alert alert-block">' . JText::_('JYAML_UPDATE_SCRIPT_MIGRATE_ERRORS_DETECTED') . '</div>';

					$body .= '<small><ul>';
					foreach ($errors as $error)
					{
						$body .= '<li>' . JPath::clean(substr($error, strlen(JPATH_ROOT))) . '</li>';
					}
					$body .= '</ul></small>';

					$footer .= '<button class="btn btn-danger" id="jyamltemplateupgradeContinueStep" data-nextstep="' . $this->step . '">'
							. JText::_('JYAML_UPGRADE_TRY_AGAIN') . '</button>';
				}

				if ($k > 0)
				{
					$body .= '<p> -------------------- <br /> -------------------- </p>';
				}

			}
		}
		else
		{
			$body .= '<div class="alert alert-block alert-success">' . JText::_('JYAML_UPGRADE_MIGRATION_NOT_REQUIRED') . '</div>';

			$this->response->set('blockClose', true);
			$footer .= '<button class="btn btn-primary" id="jyamltemplateupgradeContinueStep" data-nextstep="' . ($this->step + 1) . '">'
					. JText::_('JYAML_UPGRADE_CONTINUE_TXT') . ': ' . JText::_('JYAML_UPGRADE_STEPNUM' . ($this->step + 1)) . '</button>';
		}

		$this->response->set('footer', $footer);
		$this->response->set('body', $body);
		return true;
	}

	/**
	 * Finish the upgrade - Write new templateDetails.xml
	 * And does set JSON response.
	 *
	 * @access private
	 *
	 * @return true on success
	 */
	private function getStep_4()
	{
		$db = JFactory::getDbo();

		$body = '';
		$footer = '';

		$template = $this->template;
		$version = $this->to_version;
		$creationDate = date('d F Y', $this->to_date);

		$templatePath = $this->getTemplatePath();
		$libUpdatePath = JYAML_LIB_PATH . '/update/downloads/template';
		$detailsXMLPath = $templatePath . '/templateDetails.xml';

		if (!JFile::exists($libUpdatePath . '/templateDetails.xml'))
		{
			// Fallback already done
			$body .= '<p class="jyaml_upgrade_finished">' . JText::_('JYAML_UPGRADE_FINISHED') . '</p>';
			$footer .= '<button class="btn btn-success" id="jyamltemplateupgradeReloadPage">' . JText::_('JYAML_UPGRADE_RELOAD_PAGE') . '</button>';

			$this->response->set('blockClose', false);
			$this->response->set('footer', $footer);
			$this->response->set('body', $body);
			return true;
		}

		$xmlContent = file_get_contents($detailsXMLPath);
		$xmlLibContent = file_get_contents($libUpdatePath . '/templateDetails.xml');

		if (preg_match('#<positions>(.*)</positions>#Uis', $xmlContent, $matches))
		{
			if (isset($matches[1]) && $matches[1])
			{
				$xmlLibContent = preg_replace('#<positions>(.*)</positions>#Uis', '<positions>' . $matches[1] . '</positions>', $xmlLibContent, 1);
			}
		}

		$xmlContent = $xmlLibContent;

		$ok = true;
		if (JFile::write($detailsXMLPath, $xmlContent))
		{
			$body .= '<div class="alert alert-block alert-success">' . JText::_('JYAML_UPGRADE_TEMPLATE_DETAILS_XML_SUCCESS') . '</div>';
		}
		else
		{
			$body .= '<div class="alert alert-block>' . JText::_('JYAML_UPGRADE_TEMPLATE_DETAILS_XML_FAILED') . '</div>';
			$ok = false;
		}

		$query = $db->getQuery(true);
		$query->select('*');
		$query->from($db->qn('#__extensions'));
		$query->where($db->qn('element') . ' = ' . $db->q($template));
		$query->where($db->qn('type') . ' = ' . $db->q('template'));
		$db->setQuery($query);

		$result = $db->loadObject();

		if ($result = $db->loadObject())
		{
			$format = 'JSON';
			$manifest = new \Joomla\Registry\Registry;
			$manifest->loadString($result->manifest_cache, $format);
			$manifest->set('creationDate', $creationDate);
			$manifest->set('version', $version);
			$manifest = $manifest->toString($format);

			$query->clear();
			$query->update($db->qn('#__extensions'));
			$query->set($db->qn('manifest_cache') . ' = ' . $db->q($manifest));
			$query->where($db->qn('extension_id') . ' = ' . $db->q($result->extension_id));
			$db->setQuery($query);
			$db->execute();

			$body .= '<div class="alert alert-block alert-success">' . JText::_('JYAML_UPGRADE_TEMPLATE_MANIFEST_SUCCESS') . '</div>';
		}
		else
		{
			$body .= '<div class="alert alert-block>' . JText::_('JYAML_UPGRADE_TEMPLATE_MANIFEST_FAILED') . '</div>';
			$ok = false;
		}

		if (!$ok)
		{
			$footer .= '<button class="btn btn-danger" id="jyamltemplateupgradeContinueStep" data-nextstep="' . $this->step . '">'
					. JText::_('JYAML_UPGRADE_TRY_AGAIN') . '</button>';
		}
		else
		{
			$body .= '<p><span class="label label-success">' . JText::_('JYAML_UPGRADE_FINISHED') . '</span></p>';

			$this->response->set('blockClose', true);
			$footer .= '<button class="btn btn-success" id="jyamltemplateupgradeReloadPage">' . JText::_('JYAML_UPGRADE_RELOAD_PAGE') . '</button>';
		}
		$body .= '</div>';

		// Cleanup the jyaml library update folder
		if ($ok && JFolder::exists($libUpdatePath))
		{
			JFolder::delete($libUpdatePath);
		}

		$this->response->set('footer', $footer);
		$this->response->set('body', $body);
		return true;
	}

	/**
	 * Get affected XML upgrade files by versions
	 *
	 * @param   string  $fromVersion  From Version
	 * @param   string  $toVersion    To Version
	 *
	 * @return Array of XML files
	 */
	private function getXMLUpgradeFiles($fromVersion, $toVersion)
	{
		static $xmlUpgradeFiles = array();

		$hash = $fromVersion . '-' . $toVersion;

		if (!isset($xmlUpgradeFiles[$hash]))
		{
			$xmlUpgradeFiles[$hash] = array();
			$fromVersion = str_ireplace('.stable', '', $fromVersion);
			$toVersion = str_ireplace('.stable', '', $toVersion);

			$serachPath = JYAML_LIB_PATH . '/update';
			if (JFolder::exists($serachPath))
			{
				$xmlFiles = JFolder::files($serachPath, '.xml$');
				asort($xmlFiles);

				foreach ($xmlFiles as $_xmlFile)
				{
					$name = JFile::stripExt($_xmlFile);
					$tmp = explode('-', $name);

					if (count($tmp) == 2)
					{
						$from = str_ireplace('.stable', '', $tmp[0]);
						$to = str_ireplace('.stable', '', $tmp[1]);
					}
					else
					{
						continue;
					}

					if (version_compare($to, $fromVersion, '>=') && version_compare($to, $toVersion, '<='))
					{
						if (version_compare($from, $fromVersion, '>=') || (version_compare($from, $fromVersion, '<=') && version_compare($to, $toVersion, '>=')))
						{
							$_xml = simplexml_load_file(JPath::clean($serachPath . '/' . $_xmlFile));
							$_script = (string) $_xml->script;

							if ($_script)
							{
								$_xml->script = JPath::clean($serachPath . '/script/' . $_script);
							}

							$xmlUpgradeFiles[$hash][] = $_xml;
						}
					}
				}
			}
		}

		return $xmlUpgradeFiles[$hash];
	}

	/**
	 * Compare to Versions and get all Files and Folders are required to overwrite
	 *
	 * @param   string  $fromVersion  From Version
	 * @param   string  $toVersion    From Version
	 *
	 * @access private
	 *
	 * @return array Filelist
	 */
	private function upgradeGetIncrmentalList($fromVersion, $toVersion)
	{
		$result = array(
				'necessary' => array(
						'folders' => array(),
						'files' => array()
				)
		);

		$xmlUpgradeFiles = self::getXMLUpgradeFiles($fromVersion, $toVersion);

		if (!empty($xmlUpgradeFiles))
		{
			foreach ($xmlUpgradeFiles as $_xml)
			{
				$_folders = (array) $_xml->necessary->folders;
				if (isset($_folders['folder']) && is_array($_folders['folder']))
				{
					$_folders = $_folders['folder'];
				}

				foreach ($_folders as $_folder)
				{
					if (!in_array($_folder, $result['necessary']['folders']))
					{
						$result['necessary']['folders'][] = (string) $_folder;
					}
				}

				$_files = (array) $_xml->necessary->files;
				if (isset($_files['file']) && is_array($_files['file']))
				{
					$_files = $_files['file'];
				}
				foreach ($_files as $_file)
				{
					if (!in_array($_file, $result['necessary']['files']))
					{
						$result['necessary']['files'][] = (string) $_file;
					}
				}
			}
		}

		return $result;
	}

}
