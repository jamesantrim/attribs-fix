<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  JYAML.Admin
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

defined('_JEXEC') or die;

jimport('joomla.form.formfield');

/**
 * JYAML Help Form Field
 *
 * @package     JYAML
 * @subpackage  JYAML.Admin
 * @since       4.0.0
 */
class JFormFieldJyamlhelp extends JFormField
{
	/**
	 * The form field type.
	 */
	protected $type = 'Jyamlhelp';

	/**
	 * Method to instantiate the form field object.
	 *
	 * @param   JForm  $form  The form to attach to the form field object.
	 *
	 * @since   11.1
	 */
	public function __construct($form = null)
	{
		$document = JFactory::getDocument();

		JHtml::_('jquery.framework');
		$document->addScript(JURI::root(true) . '/libraries/jyaml/assets/js/fields/jyamlhelp.js');
		$document->addStylesheet(JURI::root(true) . '/libraries/jyaml/assets/css/fields/jyaml.css');

		parent::__construct($form);
	}

	/**
	 * Get the field label markup.
	 *
	 * @return string Form field Label
	 */
	protected function getLabel()
	{
		return '';
	}

	/**
	 * Get the field input markup
	 *
	 * @return string Form field input
	 */
	protected function getInput()
	{
		// Initialize some field attributes.
		$key = (string) $this->element['id'];

		$html = '<div class="jyaml_help_link">' . JYAMLHelper::getHelpLink($key) . '</div>';

		return $html;
	}
}
