<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  JYAML.Admin
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

defined('_JEXEC') or die;

jimport('joomla.form.formfield');
JFormHelper::loadFieldClass('jyamltemplatepositions');

/**
 * JYAML Scripts Form Field
 *
 * @package     JYAML
 * @subpackage  JYAML.Admin
 * @since       4.0.0
 */
class JFormFieldJyamlmenuitemcontent extends JFormFieldJyamltemplatepositions
{
	/**
	 * The form field type.
	 */
	protected $type = 'JFormFieldJyamlmenuitemcontent';

	/**
	 * Get the field input markup
	 *
	 * @return string Form field input
	 */
	protected function getInput()
	{
		$this->hideTemplateTag = true;
		$this->clientId = 0;
		$this->template = $this->getUsedTemplateName();

		return parent::getInput();
	}

	/**
	 * Get positions
	 *
	 * @return Array of positions
	 */
	protected function getPositions()
	{
		if (is_object($this->value) && isset($this->value->menuitemcontent))
		{
			$this->value = $this->value->menuitemcontent;
		}
		if (is_array($this->value) && isset($this->value['menuitemcontent']))
		{
			$this->value = $this->value['menuitemcontent'];
		}
		return (array) $this->value;
	}

	/**
	 * Get used template name
	 *
	 * @return string Template Name
	 */
	protected function getUsedTemplateName()
	{
		$name = 'system';

		$menuTemplateStyleId = (int) $this->form->getValue('template_style_id');
		$menuLanguage = $this->form->getValue('language');

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$_fields = array(
				$db->qn('id'),
				$db->qn('home'),
				$db->qn('template'),
				$db->qn('params')
		);
		$query->select(implode(', ', $_fields));
		$query->from($db->qn('#__template_styles'));
		$query->where($db->qn('client_id') . ' = ' . $db->q(0));

		if ($menuTemplateStyleId)
		{
			$query->where($db->qn('id') . ' = ' . $db->q($menuTemplateStyleId));
		}
		else
		{
			if ($menuLanguage && $menuLanguage != '*')
			{
				$query->where($db->qn('home') . ' IN(' . $db->q($menuLanguage) . ', ' . $db->q(1) . ')');
				$query->order($db->escape('home') . ' ' . $db->escape('DESC'));
			}
			else
			{
				$query->where($db->qn('home') . ' = ' . $db->q(1));
			}
		}

		$db->setQuery($query, 0, 1);

		if ($template = $db->loadObject())
		{
			$template->params = new \Joomla\Registry\Registry($template->params);

			if ($template->params->get('_is_jyaml'))
			{
				$name = $template->template;
			}
		}

		return $name;
	}
}
