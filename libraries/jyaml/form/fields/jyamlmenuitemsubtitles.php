<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  JYAML.Admin
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

defined('_JEXEC') or die;

jimport('joomla.form.formfield');
JFormHelper::loadFieldClass('text');

/**
 * JYAML Scripts Form Field
 *
 * @package     JYAML
 * @subpackage  JYAML.Admin
 * @since       4.0.0
 */
class JFormFieldJyamlmenuitemsubtitles extends JFormFieldText
{
	/**
	 * The form field type.
	 */
	protected $type = 'JFormFieldJyamlmenuitemsubtitles';

	/**
	 * Method to instantiate the form field object.
	 *
	 * @param   JForm  $form  The form to attach to the form field object.
	 *
	 * @since   11.1
	 */
	public function __construct($form = null)
	{
		$document = JFactory::getDocument();

		JHtml::_('jquery.framework');
		JHtml::_('jquery.ui', array('core', 'sortable'));

		$document->addScript(JURI::root(true) . '/libraries/jyaml/assets/js/fields/jyaml.js');
		$document->addStylesheet(JURI::root(true) . '/libraries/jyaml/assets/css/fields/jyaml.css');

		parent::__construct($form);
	}

	/**
	 * Get the field input markup
	 *
	 * @return string Form field input
	 */
	protected function getInput()
	{
		$this->name = $this->name . '[]';

		$origValue = $this->value;
		if (is_array($origValue))
		{
			$subtitles = $origValue;
		}
		else
		{
			$subtitles = array();
		}

		$dummyIdentifier = '::' . md5('__DUMMY__') . '::';
		$subtitles[] = $dummyIdentifier;

		$html = '';
		$html .= '<div class="jyamltemplatesubtitles_form">';

		$html .= '<div class="jyaml-move-group">';

		foreach ($subtitles as $_subtitle)
		{
			$isDummy = ($_subtitle == $dummyIdentifier);

			$html .= '<div class="jyaml-group-entry' . ($isDummy ? '-dummy' : '') . '"' . ($isDummy ? ' style="display:none"' : '') . '>';

			$html .= '<div class="input-prepend input-append">';

			$html .= '<span class="btn jyaml-group-move-btn" title="' . JText::_('JYAML_POSITION_MOVE')
					. '"><i class="icon-move"></i></span>';

			$this->value = ($isDummy ? '' : $_subtitle);

			$attributes = 'data-dummy-init="1"';
			if ($isDummy)
			{
				$attributes .= ' disabled="disabled" style="display:none"';
			}

			$html .= '<input type="text" name="' . $this->name . '" id="' . $this->id . '"' . ' value="'
					. htmlspecialchars($this->value, ENT_COMPAT, 'UTF-8') . '" ' . $attributes . '/>';

			$html .= '<button class="btn btn-danger jyaml-group-entry-remove" data-confirm="'
					. JText::_('JYAML_MENUITEMSUBTITLE_CONFIRM_REMOVE') . '" type="button" title="'
					. JText::_('JYAML_MENUITEMSUBTITLE_REMOVE') . '"><i class="icon-remove"></i></button>';

			$html .= '</div>';

			$html .= '</div>';
		}

		$html .= '</div>';

		$html .= '<button class="btn btn-success jyaml-group-entry-add" type="button"><i class="icon-plus"></i> '
				. JText::_('JYAML_MENUITEMSUBTITLE_ADD') . '</button>';

		$html .= '</div>';

		return $html;
	}

}
