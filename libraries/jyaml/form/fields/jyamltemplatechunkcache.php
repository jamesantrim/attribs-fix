<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  JYAML.Admin
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

defined('JPATH_BASE') or die;

jimport('joomla.form.formfield');
JFormHelper::loadFieldClass('radio');

/**
 * Form Field class for the Joomla Framework.
 *
 * @package     JYAML
 * @subpackage  JYAML.Admin
 * @since       4.0.0
 */
class JFormFieldJyamltemplatechunkcache extends JFormFieldRadio
{
	/**
	 * The form field type.
	 */
	protected $type = 'Jyamltemplatechunkcache';

	/**
	 * Method to instantiate the form field object.
	 *
	 * @param   JForm  $form  The form to attach to the form field object.
	 *
	 * @since   11.1
	 */
	public function __construct($form = null)
	{
		$document = JFactory::getDocument();

		JHtml::_('jquery.framework');
		$document->addScript(JURI::root(true) . '/libraries/jyaml/assets/js/spin.js');
		$document->addScript(JURI::root(true) . '/libraries/jyaml/assets/js/jquery.spin.js');
		$document->addScript(JURI::root(true) . '/libraries/jyaml/assets/js/fields/jyamltemplatechunkcache.js');

		parent::__construct($form);
	}

	/**
	 * Get the field input markup
	 *
	 * @return string Form field input
	 */
	protected function getInput()
	{
		$template = $this->form->getValue('template');
		$clientId = $this->form->getValue('client_id');

		$html = parent::getInput();

		$button = '<span class="btn btn-inverse jyamltemplatechunkcache" data-template="' . $template . '" data-clientid="' . $clientId . '">'
			. JText::_('JYAML_CHUNKER_CLEAN_CACHE') . '</span>';

		$html = str_replace('</fieldset>', $button . '</fieldset>', $html);

		return $html;
	}
}
