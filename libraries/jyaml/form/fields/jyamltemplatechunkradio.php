<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  JYAML.Admin
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

defined('JPATH_BASE') or die;

jimport('joomla.form.formfield');
JFormHelper::loadFieldClass('radio');

/**
 * Form Field class for the Joomla Framework.
 *
 * @package     JYAML
 * @subpackage  JYAML.Admin
 * @since       4.0.0
 */
class JFormFieldJyamltemplatechunkradio extends JFormFieldRadio
{
	/**
	 * The form field type.
	 */
	protected $type = 'Jyamltemplatechunkradio';

	/**
	 * Method to instantiate the form field object.
	 *
	 * @param   JForm  $form  The form to attach to the form field object.
	 *
	 * @since   11.1
	 */
	public function __construct($form = null)
	{
		try
		{
			$chunker = new JsCssChunker(JURI::root());
			$canLoadFiles = $chunker->check();
			$isCacheDirWritable = is_writable(JPATH_ROOT . DIRECTORY_SEPARATOR . 'cache');
		}
		catch (Exception $e)
		{
			$canLoadFiles = false;
			$isCacheDirWritable = false;
		}


		$this->chunkerCanLoadFiles = $canLoadFiles;
		$this->chunkerIsCacheDirWritable = $isCacheDirWritable;
	}

	/**
	 * Get the field label markup.
	 *
	 * @return string Field Label
	 */
	protected function getLabel()
	{
		return parent::getLabel();
	}

	/**
	 * Get the field input markup
	 *
	 * @return string Field input
	 */
	protected function getInput()
	{
		if (!$this->chunkerIsCacheDirWritable || !$this->chunkerCanLoadFiles)
		{
			if (!$this->chunkerIsCacheDirWritable)
			{
				$html[] = '<p class="jyaml-option-warning">' . JText::_('JYAML_CHUNKER_WARNING_CACHE_DIR_NOT_WRITABLE') . '</p>';
			}

			if (!$this->chunkerCanLoadFiles)
			{
				$html[] = '<p class="jyaml-option-warning">' . JText::_('JYAML_CHUNKER_WARNING_LOAD_METHODS_NOT_AVAILABLE') . '</p>';
			}

			$html[] = '<input type="hidden" name="' . $this->name . '" value="0" />';
		}
		else
		{
			// Initialize some field attributes.
			$class = $this->element['class'] ? ' class="radio ' . (string) $this->element['class'] . '"' : ' class="radio"';

			// Start the radio field output.
			$html[] = '<fieldset id="' . $this->id . '"' . $class . '>';

			// Get the field options.
			$options = $this->getOptions();

			// Build the radio field output.
			foreach ($options as $i => $option)
			{
				// Initialize some option attributes.
				$checked  = ((string) $option->value == (string) $this->value) ? ' checked="checked"' : '';
				$class    = !empty($option->class) ? ' class="' . $option->class . '"' : '';
				$disabled = !empty($option->disable) ? ' disabled="disabled"' : '';

				// Initialize some JavaScript option attributes.
				$onclick  = !empty($option->onclick) ? ' onclick="' . $option->onclick . '"' : '';

				$html[] = '<input type="radio" id="' . $this->id . $i . '" name="' . $this->name . '"' . ' value="'
						. htmlspecialchars($option->value, ENT_COMPAT, 'UTF-8') . '"' . $checked . $class . $onclick . $disabled . '/>';
				$html[] = '<label for="' . $this->id . $i . '"' . $class . '>'
						. JText::alt($option->text, preg_replace('/[^a-zA-Z0-9_\-]/', '_', $this->fieldname)) . '</label>';
			}

			// End the radio field output.
			$html[] = '</fieldset>';
		}

		return implode($html);
	}
}
