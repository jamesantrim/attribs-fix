<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  JYAML.Admin
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

defined('JPATH_BASE') or die;

jimport('joomla.form.formfield');
JFormHelper::loadFieldClass('list');

/**
 * Form Field class for the Joomla Framework.
 *
 * @package     JYAML
 * @subpackage  JYAML.Admin
 * @since       4.0.0
 */
class JFormFieldJyamltemplatecolumnorder extends JFormFieldList
{
	/**
	 * The form field type.
	 */
	protected $type = 'Jyamltemplatecolumnorder';

	/**
	 * Method to instantiate the form field object.
	 *
	 * @param   JForm  $form  The form to attach to the form field object.
	 *
	 * @since   11.1
	 */
	public function __construct($form = null)
	{
		JHtml::_('jquery.framework');
		JFactory::getDocument()->addScript(
			JURI::root(true) . '/libraries/jyaml/assets/js/fields/jyamltemplatecolumnorder.js'
		);

		parent::__construct($form);
	}

	/**
	 * Get the field input markup
	 *
	 * @return string Form field input
	 */
	protected function getInput()
	{
		$this->element['class'] = trim($this->element['class'] . ' jyamltemplatecolumnorder');

		$html = parent::getInput();

		$cols = array();
		$value = $this->value;

		if ($value && $value != 'disable_columns' && stripos($value, '_') !== false)
		{
			$tmp = explode('_', $value);
			$arr = str_split($tmp[1]);

			foreach ($arr as $_col)
			{
				$cols[$_col] = true;
			}
		}

		foreach (array(1, 3, 2) as $_col)
		{
			if (!isset($cols[$_col]))
			{
				$cols[$_col] = false;
			}
		}

		$html .= '<div id="' . $this->id . '_columnlabels"' . ($value == '' ? ' style="display:none"' : '') . '>';
		foreach ($cols as $col => $enabled)
		{
			$class = $enabled ? 'success' : 'important';
			$html .= '<span class="label label-' . $class . ' jyaml-col-' . $col . '">col' . $col . '</span> ';
		}
		$html .= '</div>';

		return $html;
	}
}
