<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  Admin
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

defined('JPATH_BASE') or die;

jimport('joomla.form.formfield');

/**
 * Form Field class for the Joomla Framework.
 *
 * @package     JYAML
 * @subpackage  Admin
 * @since       4.5.0
 */
class JFormFieldJyamltemplatecreatepackage extends JFormField
{
	/**
	 * The form field type.
	 */
	protected $type = 'Jyamltemplatecreatepackage';

	/**
	 * Method to instantiate the form field object.
	 *
	 * @param   JForm  $form  The form to attach to the form field object.
	 *
	 * @since   11.1
	 */
	public function __construct($form = null)
	{
		$document = JFactory::getDocument();

		JHtml::_('jquery.framework');
		$document->addScript(JURI::root(true) . '/libraries/jyaml/assets/js/spin.js');
		$document->addScript(JURI::root(true) . '/libraries/jyaml/assets/js/jquery.spin.js');
		$document->addScript(JURI::root(true) . '/libraries/jyaml/assets/js/jquery.block-ui.js');
		$document->addScript(JURI::root(true) . '/libraries/jyaml/assets/js/fields/jyamltemplatecreatepackage.js');

		parent::__construct($form);
	}

	/**
	 * Get the field input markup.
	 *
	 * @return string Form field input
	 */
	protected function getInput()
	{
		$html = '';

		if (!isset($this->scopeClientId))
		{
			$this->clientId = $this->form->getValue('client_id');
		}
		else
		{
			$this->clientId = $this->scopeClientId;
		}

		$client = JApplicationHelper::getClientInfo($this->clientId);

		if (!isset($this->scopeTemplate))
		{
			$this->template = $this->form->getValue('template');
		}
		else
		{
			$this->template = $this->scopeTemplate;
		}

		$clientPath = $client->path;
		$this->templatePath = $clientPath . '/templates/' . $this->template;

		$available = extension_loaded('zlib');

		if ($available)
		{
			$html .= '<button class="btn btn-info" data-toggle="modal" data-target="#jyamltemplatecreatepackageModal">'
				. JText::_('JYAML_TMPL_CREATE_PACKAGE_BTN') . '</button>';
		}
		else
		{
			$html .= '<div class="control-group error"><span class="help-inline">'
				. JText::_('JYAML_TMPL_CREATE_NOTICE_ZLIB_EXTENSION_REQUIRED') . '</span></div>';
		}

		JFactory::getDocument()->addScriptDeclaration('JYAML.createModals.push(' . json_encode($this->getModalHtml()) . ');');

		return $html;
	}

	/**
	 * Get the modal markup
	 *
	 * @return string modal html
	 */
	private function getModalHtml()
	{
		$jinput = JFactory::getApplication()->input;
		$headline = JText::sprintf('JYAML_CREATE_PACKAGE_FRAME_TITLE', $this->template);

		$html = '';
		$html .= '<div class="modal hide fade" id="jyamltemplatecreatepackageModal" tabindex="-1"
			role="dialog" aria-labelledby="jyamltemplatecreatepackageModalLabel" aria-hidden="true">';
		$html .= '<div class="modal-header">';
		$html .= '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
		$html .= '<h3 id="jyamltemplatecreatepackageModalLabel">' . $headline . '</h3>';
		$html .= '</div>';
		$html .= '<div class="modal-body" style="overflow-y:auto">';
		$html .= '<form action="' . JRoute::_('') . '" method="post" class="form-horizontal" id="jyamltemplatecreatepackageForm">';
		$html .= '<fieldset>';
		$html .= '<legend>' . JText::_('JYAML_CREATE_PACKAGE_FRAME_FIELDSET_OPTIONS') . '</legend>';
		$html .= '<label class="checkbox" for="jyaml_cp_iem">';
		$html .= '<input type="checkbox" name="jyaml_create_package[install_example_modules]" id="jyaml_cp_iem" value="1" />';
		$html .= ' ' . JText::_('JYAML_CREATE_PACKAGE_INSTALL_EXAMPLE_MODULES');
		$html .= '</label>';
		$html .= '<label class="checkbox" for="jyaml_cp_icc">';
		$html .= '<input checked="checked" type="checkbox" name="jyaml_create_package[include_current_configuration]" id="jyaml_cp_icc" value="1" />';
		$html .= ' ' . JText::_('JYAML_CREATE_PACKAGE_INCLUDE_CURRENT_CONFIGURATION');
		$html .= '</label>';
		$html .= '</fieldset>';

		$smallImage = $this->templatePath . '/template_thumbnail.png';
		$bigImage = $this->templatePath . '/template_preview.png';
		$smallImage = JURI::root() . JPath::clean(substr($smallImage, strlen(JPATH_ROOT)), '/');
		$bigImage = JURI::root() . JPath::clean(substr($bigImage, strlen(JPATH_ROOT)), '/');

		$html .= '<fieldset>';
		$html .= '<legend>&nbsp;</legend>';

		$html .= '<div class="row-fluid">';
		$html .= '<ul class="thumbnails">';
		$html .= '<li class="span4">';
		$html .= '<p><span class="label label-info">' . basename($smallImage) . '</span></p>';
		$html .= '<a href="' . $smallImage . '" target="_blank" class="thumbnail"><img src="' . $smallImage . '" /></a>';
		$html .= '</li>';
		$html .= '<li class="span6">';
		$html .= '<p><span class="label label-info">' . basename($bigImage) . '</span></p>';
		$html .= '<a href="' . $bigImage . '" target="_blank" class="thumbnail"><img src="' . $bigImage . '" /></a>';
		$html .= '</li>';
		$html .= '</ul>';
		$html .= '</div>';

		$html .= '</fieldset>';

		$html .= '<input type="hidden" name="jyamladmincontroller" value="templatecreatepackage" />';
		$html .= '<input type="hidden" name="jyaml_create_package[style_id]" value="' . $jinput->get('id', 0, 'int') . '" />';
		$html .= '<input type="hidden" name="jyaml_create_package[template]" value="' . $this->template . '" />';
		$html .= '<input type="hidden" name="jyaml_create_package[client_id]" value="' . $this->clientId . '" />';
		$html .= '<input type="hidden" name="jyaml_create_package_download" value="0" />';
		$html .= '<input type="hidden" name="' . JSession::getFormToken() . '" value="1" />';
		$html .= '</form>';

		$html .= '</div>';
		$html .= '<div class="modal-footer">';
		$html .= '<button class="btn" data-dismiss="modal" aria-hidden="true">' . JText::_('JYAML_CLOSE') . '</button>';
		$html .= '<button class="btn btn-primary" id="jyaml_createpackage_btn">' . JText::_('JYAML_CREATE_PACKAGE_SUBMIT_BTN') . '</button>';

		$html .= '</div>';
		$html .= '</div>';

		return $html;
	}
}
