<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  JYAML.Admin
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

defined('JPATH_BASE') or die;

jimport('joomla.form.formfield');
JFormHelper::loadFieldClass('text');

/**
 * Form Field class for the Joomla Framework.
 *
 * @package  JYAML
 * @since    4.5.0
 */
class JFormFieldJyamltemplategetip extends JFormFieldText
{
	/**
	 * The form field type.
	 */
	protected $type = 'Jyamltemplategetip';

	/**
	 * Method to instantiate the form field object.
	 *
	 * @param   JForm  $form  The form to attach to the form field object.
	 *
	 * @since   11.1
	 */
	public function __construct($form = null)
	{
		$document = JFactory::getDocument();

		JHtml::_('jquery.framework');
		$document->addScript(JURI::root(true) . '/libraries/jyaml/assets/js/fields/jyamltemplategetip.js');

		parent::__construct($form);
	}

	/**
	 * Get the field input markup
	 *
	 * @return string Input html markup
	 */
	protected function getInput()
	{
		$this->element['class'] = trim($this->element['class'] . ' input-small');

		$html = '<div class="input-append">';
		$html .= parent::getInput();
		$html .= '<span class="btn jyamltemplategetip" data-value="' . $_SERVER['REMOTE_ADDR'] . '" data-value-target="#'
				. $this->id . '">' . JText::_('JYAML_ADMIN_DEBUG_LOAD_MY_IP_ADDRESS_BUTTON') . '</span>';
		$html .= '</div>';

		return $html;
	}
}
