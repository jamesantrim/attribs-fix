<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  JYAML.Admin
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

defined('JPATH_BASE') or die;

jimport('joomla.form.formfield');
JFormHelper::loadFieldClass('list');

/**
 * Form Field class for the Joomla Framework.
 *
 * @package     JYAML
 * @subpackage  JYAML.Admin
 * @since       4.0.0
 */
class JFormFieldJyamltemplatehtmlfilelist extends JFormFieldList
{
	/**
	 * The form field type.
	 */
	protected $type = 'Jyamltemplatehtmlfilelist';

	/**
	 * Get the field label markup.
	 *
	 * @return Form field Label
	 */
	protected function getLabel()
	{
		$this->translateDescription = false;

		$origDesc = $this->description;
		$this->description = JText::sprintf($this->description, '[tmpl]');

		$label = parent::getLabel();

		$this->description = $origDesc;

		return $label;
	}

	/**
	 * Get the field input markup
	 *
	 * @return string Form field input
	 */
	protected function getInput()
	{
		$this->translateLabel = false;
		$this->translateDescription = false;

		$this->_optionScope = array();
		$origId = $this->id;
		$origName = $this->name;
		$origValue = $this->value;
		$origLabel = JText::_((string) $this->element['label']);
		$origDesc = $this->description;

		$html = '';
		$filelist = $this->getFilelist();

		$i = 1;
		$count = count($filelist);
		foreach ($filelist as $folder => $options)
		{
			$this->_optionScope = $options;
			$this->id = $origId . '_' . $folder;
			$this->name = $origName . '[' . $folder . ']';
			$this->value = ((is_array($origValue) && isset($origValue[$folder])) ? $origValue[$folder] : $origValue);

			$this->element['label'] = '<div><span class="label">' . $folder . '.php &gt;</span></div>';
			$this->description = '';

			$label = parent::getLabel();
			$input = '<div class="input-prepend"><span class="add-on">/html/' . $folder . '/</span>' . parent::getInput() . '</div>';

			$html .= '<div>';
			$html .= $label;
			$html .= $input;
			$html .= '</div>' . ($i == $count ? '' : '<br />');

			++$i;
		}

		// Reset
		$this->_optionScope = array();
		$this->id = $origId;
		$this->name = $origName;
		$this->value = $origValue;
		$this->element['label'] = $origLabel;
		$this->description = $origDesc;

		return $html;
	}

	/**
	 * Get the field options.
	 *
	 * @return array option scope
	 */
	protected function getOptions()
	{
		return $this->_optionScope;
	}

	/**
	 * Get the html file option list
	 *
	 * @return array File list
	 */
	protected function getFilelist()
	{
		$tmplFiles = array();
		$files = array(
			'index' => array(),
			'component' => array(),
			'error' => array(),
			'offline' => array()
		);

		$template = $this->form->getValue('template');

		$clientId = $this->form->getValue('client_id');
		$client = JApplicationHelper::getClientInfo($clientId);
		$clientPath = $client->path;

		if ($clientId == 1)
		{
			// Admin templates don't require an offline template
			unset($files['offline']);
		}

		$templatePath = $clientPath . '/templates/' . $template;

		$formParams = $this->form->getValue('params');
		$params = new \Joomla\Registry\Registry;
		$params->loadObject($formParams);

		if (JFolder::exists($templatePath))
		{
			// Search tmpl files
			$tmplFiles = (array) JFolder::files($templatePath, '.php$', false, false);

			foreach ($tmplFiles as $_file)
			{
				$_htmlFolder = strtolower(JFile::stripExt($_file));
				$_htmlPath = $templatePath . '/html/' . $_htmlFolder;

				if (JFolder::exists($_htmlPath))
				{
					$files[$_htmlFolder][]  = JHtml::_('select.optgroup', '/html/' . $_htmlFolder . '/');
					$_files = (array) JFolder::files($_htmlPath, '.php$', false, false);

					if (in_array('default.php', $_files))
					{
						$files[$_htmlFolder][] = JHtml::_('select.option', 'default', 'default.php', 'value', 'text');

						foreach ($_files as $_file2)
						{
							if (strtolower($_file2) != 'default.php')
							{
								$value = JFile::stripExt($_file2);
								$files[$_htmlFolder][] = JHtml::_('select.option', $value, $_file2, 'value', 'text');
							}
						}
					}

					$files[$_htmlFolder][]  = JHtml::_('select.optgroup', '/html/' . $_htmlFolder . '/');
				}
			}
		}

		return $files;
	}
}
