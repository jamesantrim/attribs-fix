<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  JYAML.Admin
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

defined('JPATH_BASE') or die;

jimport('joomla.form.formfield');
JFormHelper::loadFieldClass('radio');

/**
 * Form Field class for the Joomla Framework.
 *
 * @package     JYAML
 * @subpackage  JYAML.Admin
 * @since       4.0.0
 */
class JFormFieldJyamltemplatehtmltidyradio extends JFormFieldRadio
{
	/**
	 * The form field type.
	 */
	protected $type = 'Jyamltemplatehtmltidyradio';

	/**
	 * Get the field input markup
	 *
	 * @return string Form field input
	 */
	protected function getInput()
	{
		$htmlTidyAvailable = function_exists('tidy_parse_string');

		if (!$htmlTidyAvailable)
		{
			return '<strong>' . JText::_('JYAML_ADVANCED_HTML_TIDY_WARNING_NOT_AVAILABLE') . '</strong>';
		}
		else
		{
			return parent::getInput();
		}
	}
}
