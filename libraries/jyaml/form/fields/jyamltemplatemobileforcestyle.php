<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  JYAML.Admin
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

defined('JPATH_BASE') or die;

jimport('joomla.form.formfield');
JFormHelper::loadFieldClass('radio');

/**
 * Form Field class for the Joomla Framework.
 *
 * @package     JYAML
 * @subpackage  JYAML.Admin
 * @since       4.0.0
 */
class JFormFieldJyamltemplatemobileforcestyle extends JFormFieldRadio
{
	/**
	 * The form field type.
	 */
	protected $type = 'Jyamltemplatemobileforcestyle';

	/**
	 * Get the field label markup.
	 *
	 * @return Form field Label
	 */
	protected function getLabel()
	{
		if (($_msg = $this->getMessage()))
		{
			// Get the label text from the XML element, defaulting to the element name.
			$text = $this->element['label'] ? (string) $this->element['label'] : (string) $this->element['name'];
			$text = $this->translateLabel ? JText::_($text) : $text;

			// Do not translate again in parent
			$this->translateLabel = false;

			$this->element['label'] = $text . ': <span class="readonly">' . $_msg . '</span>';
			return '<div class="jyaml-fullwidth-label">' . parent::getLabel() . '</div>';
		}

		return parent::getLabel();
	}

	/**
	 * Get the field input markup
	 *
	 * @return string Form field input
	 */
	protected function getInput()
	{
		if (!$this->getMessage())
		{
			return parent::getInput();
		}

		return '';
	}

	/**
	 * Get message
	 *
	 * @return string Message
	 */
	protected function getMessage()
	{
		static $msg;

		if ($msg !== null)
		{
			return $msg;
		}

		$home = $this->form->getValue('home');
		$msg = false;

		if ($home)
		{
			$msg = JText::_('JYAML_MOBILE_FORCETHISTEMPLATESTYLE_NOTE_NOTDEFAULT');
		}
		else
		{
			$styleId = $this->form->getValue('id');
			$clientId = $this->form->getValue('client_id');

			// Find jyaml mobile styles
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$_fields = array(
					$db->qn('id'),
					$db->qn('title')
			);
			$query->select(implode(', ', $_fields));
			$query->from($db->qn('#__template_styles'));
			$query->where($db->qn('client_id') . ' = ' . $db->q($clientId));
			$query->where($db->qn('params') . ' LIKE ' . $db->q('%"_is_jyaml":"1"%'));
			$query->where($db->qn('params') . ' LIKE ' . $db->q('%"mobileForceThisTemplateStyle":"1"%'));
			$query->where($db->qn('params') . ' LIKE ' . $db->q('%"mobileDetector":"1"%'));
			$query->where($db->qn('home') . ' = ' . $db->q(0));
			$query->where($db->qn('id') . ' != ' . $db->q($styleId));
			$db->setQuery($query);

			if ($_style = $db->loadObject())
			{
				$msg = '<span class="jyaml-inline-warning">'
									. JText::sprintf('JYAML_MOBILE_FORCETHISTEMPLATESTYLE_NOTE_ONCE', $_style->title, $_style->id) . '<span>';
			}
		}

		return $msg;
	}
}
