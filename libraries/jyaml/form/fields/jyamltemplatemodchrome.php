<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  JYAML.Admin
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

defined('JPATH_BASE') or die;

jimport('joomla.form.formfield');
JFormHelper::loadFieldClass('jyamltemplatepositions');

/**
 * Form Field class for the Joomla Framework.
 *
 * @package     JYAML
 * @subpackage  JYAML.Admin
 * @since       4.0.0
 */
class JFormFieldJyamltemplatemodchrome extends JFormFieldJyamltemplatepositions
{
	/**
	 * The form field type.
	 */
	protected $type = 'Jyamltemplatemodchrome';

	/**
	 * Get the field label markup.
	 *
	 * @param   boolean  $showLabel  Show Label
	 *
	 * @return Form field Label
	 */
	protected function getLabel($showLabel=false)
	{
		return parent::getLabel(true);
	}

	/**
	 * Get the field input markup
	 *
	 * @return string Form field input
	 */
	protected function getInput()
	{
		$html = '';
		$excludes = array();
		$isStandalone = false;

		$scope = isset($this->form->jyamlParentScope) ? $this->form->jyamlParentScope : new \Joomla\Registry\Registry;
		$scopeValue = $scope->get('value', '');
		$substyleExclude = (array) $scope->get('substyleExclude', array());

		if (!isset($this->form->template) || !isset($this->form->clientId))
		{
			$result = $this->detectDefaultTemplate();

			if (false === $result)
			{
				return '<p class="jyaml-warning">' . JText::_('JYAML_EXT_MOD_LAYOUT_MODCHROME_STANDALONE_WARNING') . '</p>';
			}
			else
			{
				$isStandalone = true;

				$_style = new \Joomla\Registry\Registry($this->value);

				$scope->set('name', $this->name);
				$scope->set('fieldname', $this->fieldname);
				$scope->set('selectedStyle', $_style->get('style', ''));
				$scope->set('k', '');
				$scope->set('disabled', false);
				$scope->set('data', $_style->get('__attributes'));
				$scope->set('value', $_style->get('style', ''));

				$this->form->jyamlParentScope = $scope;

				$this->value = $_style->get('style', '');
				$this->name .= '[style]';
			}
		}

		$this->clientId = $this->form->clientId;
		$this->template = $this->form->template;

		$exclude = (string) $this->element['exclude'];
		if ($exclude != '')
		{
			$exclude = (array) explode(',', $exclude);
			foreach ($exclude as $_v)
			{
				$_v = trim($_v . '');
				if ($_v != '')
				{
					$excludes[] = trim($_v);
				}
			}
		}
		if ($scopeValue != '')
		{
			$excludes[] = $scopeValue;
		}
		if (count($substyleExclude))
		{
			$excludes = array_merge($substyleExclude, $excludes);
		}

		$k = $scope->get('k');
		$data = $scope->get('data');

		$params = null;
		if (is_object($data) && isset($data->params))
		{
			$params = $data->params;
		}
		elseif (is_array($data) && isset($data['params']))
		{
			$params = $data['params'];
		}

		$params = new \Joomla\Registry\Registry($params);
		$selectedSubStyle = $params->get($this->fieldname, '');
		if (!$selectedSubStyle)
		{
			$selectedSubStyle = $this->value;
		}

		$html .= '<div class="jyaml-group-entry">';
		$html .= '<div class="jyamltemplatepositions_form">';

		$html .= $this->getStyleSelect($selectedSubStyle, $scope->get('disabled', true), $k, $excludes, true);
		$html .= $this->getModChromeAttributesHtml($k, $selectedSubStyle, $data, $excludes, true, $isStandalone, $scope->get('disabled', false));

		$html .= '</div>';
		$html .= '</div>';

		return $html;
	}

	/**
	 * Detect Default JYAML Template
	 *
	 * @return True on success
	 */
	protected function detectDefaultTemplate()
	{
		$db = JFactory::getDbo();
		$jinput = JFactory::getApplication()->input;

		$option = $jinput->get('option', '');
		$id = $jinput->get('id', 0, 'int');

		$isModule = ($option == 'com_modules' || $option == 'com_advancedmodules');

		if ($isModule)
		{
			$_clientId = (int) $this->form->getValue('client_id', null, 0);

			// Get the default template by given client_id
			$query = $db->getQuery(true);
			$query->select('*');
			$query->from($db->qn('#__template_styles'));
			$query->where($db->qn('client_id') . ' = ' . $db->q($_clientId));
			$query->where($db->qn('home') . ' = ' . $db->q(1));
			$query->where($db->qn('params') . ' LIKE ' . $db->q('%"_is_jyaml":"1"%'));
			$db->setQuery($query);

			if (($template = $db->loadObject()))
			{
				$this->form->template = $template->template;
				$this->form->clientId = $_clientId;

				return true;
			}
		}

		return false;
	}
}
