<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  JYAML.Admin
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

defined('_JEXEC') or die;

jimport('joomla.form.formfield');
jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.file');


/**
 * JYAML Template Positions Form Field
 *
 * @package     JYAML
 * @subpackage  JYAML.Admin
 * @since       4.0.0
 */
class JFormFieldJyamltemplatepositions extends JFormField
{
	/**
	 * The form field type.
	 */
	protected $type = 'Jyamltemplatepositions';

	/**
	 * Method to instantiate the form field object.
	 *
	 * @param   JForm  $form  The form to attach to the form field object.
	 *
	 * @since   11.1
	 */
	public function __construct($form = null)
	{
		$document = JFactory::getDocument();

		JHtml::_('jquery.framework');
		JHtml::_('jquery.ui', array('core', 'sortable'));

		$document->addScript(JURI::root(true) . '/libraries/jyaml/assets/js/fields/jyaml.js');
		$document->addScript(JURI::root(true) . '/libraries/jyaml/assets/js/fields/jyamltemplatepositions.js');
		$document->addStylesheet(JURI::root(true) . '/libraries/jyaml/assets/css/fields/jyaml.css');
		$document->addStylesheet(JURI::root(true) . '/libraries/jyaml/assets/css/fields/jyamltemplatepositions.css');

		$this->hideTemplateTag = false;

		parent::__construct($form);
	}

	/**
	 * Get the field label markup.
	 *
	 * @return Form field Label
	 */
	protected function getLabel()
	{
		$origLabelClass = $this->labelClass;
		$this->labelClass .= trim($this->labelClass . ' jyaml-popover');
		$html = parent::getLabel();
		$this->labelClass = $origLabelClass;

		$usedFiles = (string) $this->element['used_files'];
		if ($usedFiles)
		{
			$usedFiles = explode(',', $usedFiles);

			$html .= '<div style="display:none;" id="' . $this->id . '_jyaml-popover-content" data-title="'
					. JText::_('JYAML_POSITION_USED_IN_FILES_TITLE') . '">';
			$html .= '<ul class="muted">';
			foreach ($usedFiles as $file)
			{
				$html .= '<li><small>' . $file . '</small></li>';
			}
			$html .= '</ul>';
			$html .= '</div>';
		}

		return $html;
	}

	/**
	 * Get the field input markup
	 *
	 * @return string Form field input
	 */
	protected function getInput()
	{
		$this->clientId = (int) $this->form->getValue('client_id');
		$this->template = (string) trim($this->form->getValue('template'));

		$html = '';
		if (!$this->hideTemplateTag)
		{
			$html .= '<p><span class="muted">Template-Tag:</span> <code>&lt;jdoc:include type=&quot;jyaml&quot; name=&quot;'
					. $this->element['label'] . '&quot; /&gt;</code></p>';
		}
		$html .= '<div class="jyamltemplatepositions_form">';

		$dynamicPositions = array();
		$dynamicModules = array();

		$positions = $this->getPositions();
		array_push($positions, array('type' => 'dummy'));

		$regexName = preg_quote($this->name, '/');
		$groupRegex = '(' . $regexName . '\[)([0-9]*|dummy)(\].*)';

		$html .= '<div class="jyaml-move-group" data-group-regex="' . $groupRegex . '">';

		$i = 0;
		foreach ($positions as $position)
		{
			$jdoc = new \Joomla\Registry\Registry($position);
			$_type = $jdoc->get('type');

			if ($_type == '')
			{
				continue;
			}

			$_name = $jdoc->get('name');
			$_style = $jdoc->get('style');

			$isDummy = $_type == 'dummy';
			$k = $isDummy ? $_type : $i;

			$html .= '<div class="jyaml-group-entry' . ($isDummy ? '-dummy' : '') . ' jyaml-group-entry-bordered"'
					. ($isDummy ? ' style="display:none"' : '') . '>';
			$html .= '<span class="btn btn-mini jyaml-group-move-btn" title="' . JText::_('JYAML_POSITION_MOVE')
					. '"><i class="icon-move"></i></span>';

			$html .= $this->getTypeSelect($_type, $isDummy, $k);
			$html .= $this->getModulePositionSelect($_name, ($_type != 'modules'), $k);
			$html .= $this->getModuleNameSelect($_name, ($_type != 'module'), $k);

			$disabled = (substr($_type, 0, 6) != 'module');
			$html .= $this->getStyleSelect($_style, $disabled, $k);

			$html .= '<button class="btn btn-danger btn-mini jyaml-group-entry-remove" data-confirm="'
				. JText::_('JYAML_POSITION_CONFIRM_REMOVE') . '" type="button" title="'
				. JText::_('JYAML_REMOVE') . '"><i class="icon-remove"></i></button>';

			$html .= '<div class="jyaml_position_modchrome_options">';
			$html .= $this->getModChromeAttributesHtml($k, $_style, $jdoc->get('__attributes'), array(), false, false, $disabled);
			$html .= '</div>';

			$html .= '<div class="jyamltemplate-advanced-container">';
			$html .= $this->getAdvancedHtml($k, $isDummy, $jdoc);
			$html .= '</div>';

			$html .= '</div>';

			++$i;
		}
		$html .= '</div>';

		$html .= '<button class="btn btn-success jyaml-group-entry-add" type="button"><i class="icon-plus"></i> '
			. JText::_('JYAML_POSITION_ADD') . '</button>';

		$html .= '</div>';

		return $html;
	}

	/**
	 * Get positions
	 *
	 * @return Array of positions
	 */
	protected function getPositions()
	{
		return (array) $this->value;
	}

	/**
	 * Get advanced options html
	 *
	 * @param   int      $k         Key
	 * @param   boolean  $disabled  Disabled
	 * @param   object   $jdoc      Jdoc options
	 *
	 * @return string HTML
	 */
	protected function getAdvancedHtml($k=0, $disabled=false, $jdoc = null)
	{
		$html = '<hr />';
		$_option = '__outerHTML';
		$_default = '{$position}';

		$name = $this->name . '[' . $k . '][' . $_option . ']';
		$id = $this->name . '_' . $_option . '_' . $k;
		$id = str_replace(array('[', ']'), '', $id);

		$html .= '<label for="' . $id . '">' . JText::_('JYAML_POSITION_OUTER_HTML') . ': <small class="jyaml_pos_info">'
				. JText::sprintf('JYAML_POSITION_OUTER_HTML_DESC', htmlentities('<div class="example-outer-html">{$position}</div>'))
				. '</small></label> ';

		$attributes = 'autocomplete="off" rows="3" cols="50" style="width:90%;" name="' . $name . '" id="' . $id . '"';
		if ($disabled)
		{
			$attributes .= ' data-dummy-init="1" disabled="disabled" style="display:none"';
		}

		$html .= '<textarea ' . $attributes . '>' . htmlspecialchars($jdoc->get($_option, $_default)) . '</textarea>';

		return $html;
	}

	/**
	 * Get the position types select
	 *
	 * @param   string   $selected  Selected value
	 * @param   boolean  $disabled  Disabled
	 * @param   int      $k         Key
	 *
	 * @return string Input HTML
	 */
	protected function getTypeSelect($selected='', $disabled=false, $k=0)
	{
		$name = $this->name . '[' . $k . '][type]';
		$id = $this->name . '_type_' . $k;
		$id = str_replace(array('[', ']'), '', $id);

		$options = array();
		$options[]  = JHtml::_('select.optgroup', JText::_('JYAML_POSITION_TYPE_SELECT'));
		$options[] = JHtml::_('select.option', '', '--' . JText::_('JYAML_SELECT') . '--', 'value', 'text');
		$options[] = JHtml::_('select.option', 'modules', 'Module Position', 'value', 'text');
		$options[] = JHtml::_('select.option', 'module', 'Module', 'value', 'text');
		$options[] = JHtml::_('select.option', 'component', 'Component', 'value', 'text');
		$options[] = JHtml::_('select.option', 'message', 'Message', 'value', 'text');
		$options[] = JHtml::_('select.optgroup', JText::_('JYAML_POSITION_TYPE_SELECT'));

		$select = '<label for="' . $id . '" class="jyaml-hide">' . JText::_('JYAML_POSITION_TYPE_SELECT') . ':</label> ';

		$attributes = 'autocomplete="off" class="jyamltemplateposition-type" data-remove-group="is_empty"';
		if ($disabled)
		{
			$attributes .= ' data-dummy-init="1" disabled="disabled" style="display:none"';
		}

		$select .= JHtml::_('select.genericlist', $options, $name, $attributes, 'value', 'text', $selected, $id);

		return $select;
	}

	/**
	 * Get the module postions select
	 *
	 * @param   string   $selected  Selected
	 * @param   boolean  $disabled  Disabled
	 * @param   string   $k         Key
	 *
	 * @return string Input HTML list
	 */
	protected function getModulePositionSelect($selected, $disabled, $k)
	{
		$name = $this->name . '[' . $k . '][name]';
		$id = $this->name . '_name_' . $k;
		$id = str_replace(array('[', ']'), '', $id);

		$allPositionOptions = JYAMLHelper::getAllModulePositions($this->template, $this->clientId, true);

		$options = array();
		$options[]  = JHtml::_('select.optgroup', JText::_('JYAML_POSITION_MODULEPOSITION_SELECT'));
		$options[] = JHtml::_('select.option', '', '--' . JText::_('JYAML_SELECT') . '--', 'value', 'text');

		if (empty($allPositionOptions[$selected]))
		{
			// Add on top if selected position not in list
			$options = array_merge(array(JHtml::_('select.option', $selected, $selected)), $allPositionOptions);
		}
		if ($allPositionOptions)
		{
			$options = array_merge($options, $allPositionOptions);
		}

		$options[]  = JHtml::_('select.optgroup', JText::_('JYAML_POSITION_MODULEPOSITION_SELECT'));

		$attributes = 'autocomplete="off" class="jyamltemplateposition-name jyaml-custom-select-input" data-type-custom-text="' . JText::_('JYAML_CUSTOM_VALUE_INPUT')
			. '" data-position-type="modules" data-remove-group="is_empty"';

		if ($disabled)
		{
			$attributes .= ' disabled="disabled" style="display:none"';
		}

		$select = '<label for="' . $id . '" class="jyaml-hide">' . JText::_('JYAML_POSITION_MODULEPOSITION_SELECT') . ':</label> ';
		$select .= JHtml::_('select.genericlist', $options, $name, $attributes, 'value', 'text', $selected, $id);

		return $select;
	}

	/**
	 * Get the module name select
	 *
	 * @param   string   $selected  Selected
	 * @param   boolean  $disabled  Disabled
	 * @param   string   $k         Key
	 *
	 * @return string Input HTML list
	 */
	protected function getModuleNameSelect($selected, $disabled, $k)
	{
		$name = $this->name . '[' . $k . '][name]';
		$id = $this->name . '_name_' . $k;
		$id = str_replace(array('[', ']'), '', $id);

		$allModuleNameOptions = JYAMLHelper::getAllModules($this->template, $this->clientId, true);

		$options = array();
		$options[] = JHtml::_('select.optgroup', JText::_('JYAML_POSITION_MODULE_SELECT'));
		$options[] = JHtml::_('select.option', '', '--' . JText::_('JYAML_SELECT') . '--', 'value', 'text');

		if ($allModuleNameOptions)
		{
			$options = array_merge($options, $allModuleNameOptions);
		}

		$options[]  = JHtml::_('select.optgroup', JText::_('JYAML_POSITION_MODULE_SELECT'));

		$attributes = 'autocomplete="off" class="jyamltemplateposition-name" data-position-type="module" data-remove-group="is_empty"';
		if ($disabled)
		{
			$attributes .= ' disabled="disabled" style="display:none"';
		}

		$select = '<label for="' . $id . '" class="jyaml-hide">' . JText::_('JYAML_POSITION_MODULE_SELECT') . ':</label> ';
		$select .= JHtml::_('select.genericlist', $options, $name, $attributes, 'value', 'text', $selected, $id);

		return $select;
	}

	/**
	 * Get the style select
	 *
	 * @param   string   $selected    Selected
	 * @param   boolean  $disabled    Disabled
	 * @param   string   $k           Key
	 * @param   array    $exclude     Exclude styles
	 * @param   boolean  $isSubstyle  Is substyle
	 *
	 * @return string Style input HTML list
	 */
	protected function getStyleSelect($selected, $disabled, $k, $exclude=array(), $isSubstyle=false)
	{
		if ($isSubstyle)
		{
			$name = $this->name;
			$id = $this->name;
		}
		else
		{
			$name = $this->name . '[' . $k . '][style]';
			$id = $this->name . '_style_' . $k;
		}
		$id = str_replace(array('[', ']'), '', $id);

		$options = JYAMLHelper::getAllStyleOptions($this->template, $this->clientId);

		if (count($exclude))
		{

			foreach ($options as $_k => $_option)
			{
				$_value = $_option->value;

				if ($_value == '' || in_array($_value, $exclude))
				{
					unset($options[$_k]);
				}
			}
		}

		$select = '<label for="' . $id . '" class="jyaml-hide">' . JText::_('JYAML_POSITION_STYLE_SELECT') . ':</label> ';

		$attributes = 'autocomplete="off" class="jyamltemplateposition-style"';
		if ($disabled)
		{
			$attributes .= ' disabled="disabled" style="display:none"';
		}

		if ($isSubstyle)
		{
			$noneOpt = array(JHtml::_('select.option', '', JText::_('JYAML_USE_TEMPLATE_SETTINGS'), 'value', 'text'));
			$options = array_merge($noneOpt, $options);
		}

		$select .= JHtml::_('select.genericlist', $options, $name, $attributes, 'value', 'text', $selected, $id);

		return $select;
	}

	/**
	 * Get params html of the modChrome attributes
	 *
	 * @param   string   $k              Key
	 * @param   string   $selectedStyle  Style
	 * @param   string   $data           Data
	 * @param   array    $exclude        Exclude styles
	 * @param   boolean  $isSubstyle     Is substyle
	 * @param   boolean  $isStandalone   Is standalone (has no parent style)
	 * @param   boolean  $forceDisabled  Disabled
	 *
	 * @return string Html
	 */
	protected function getModChromeAttributesHtml($k, $selectedStyle='', $data=null, $exclude=array(), $isSubstyle=false,
		$isStandalone=false, $forceDisabled=false)
	{
		static $substyleExclude = null;
		static $allStylesWithValidValues = array();
		static $dummyHtml = null;

		$html = '';
		$dummyFieldnameToken = '____TOKEN__DUMMY__POSITION____';

		$_fieldname = $this->fieldname;
		$_name = $this->name;
		if ($isSubstyle)
		{
			$_fieldname = $this->form->jyamlParentScope->get('fieldname');
			$_name = $this->form->jyamlParentScope->get('name');
		}

		if ($k === 'dummy' && $dummyHtml !== null)
		{
			$html = $dummyHtml;
			$html = str_replace($dummyFieldnameToken, $_fieldname, $html);
			return $html;
		}

		$tmp = explode('[' . $_fieldname . ']', $_name);
		$formControl = $tmp[0];

		$_origFieldname = $_fieldname;
		if ($k === 'dummy')
		{
			$_fieldname = $dummyFieldnameToken;
		}

		$allStyles = JYAMLHelper::getAllStyleOptions($this->template, $this->clientId);

		// Initialise Forms (required to get substyle excludes)
		if ($substyleExclude === null)
		{
			$substyleExclude = array();

			foreach ($allStyles as $_option)
			{
				$value = $_option->value;
				if ($value == '<OPTGROUP>' || $value == '</OPTGROUP>' || $value == '' || in_array($value, $exclude))
				{
					continue;
				}

				$allStylesWithValidValues[] = $_option;

				try
				{
					$form = $this->loadModChromeStylesForm($formControl, $value);

					if ($form && $form->hasChromeSubstyleField)
					{
						$substyleExclude[] = $value;
					}
				}
				catch (Exception $e)
				{
					$html = $e->getMessage();
				}
			}
		}

		foreach ($allStylesWithValidValues as $_option)
		{
			$value = $_option->value;
			$disabled = true;

			if (!$forceDisabled && $value != '' && $value == $selectedStyle)
			{
				$disabled = false;
			}
			if ($disabled && $k !== 'dummy' && !$isStandalone)
			{
				continue;
			}

			$html .= '<div' . ($disabled ? ' style="display:none"' : '') . ' class="jyaml_position_chrome_style form-horizontal" data-style="' . $value . '">';

			try
			{
				$form = $this->loadModChromeStylesForm($formControl, $value);

				if ($form)
				{
					$form->bind($data);

					$parentScope = array(
						'name' => $this->name,
						'fieldname' => $this->fieldname,
						'k' => $k,
						'selectedStyle' => $selectedStyle,
						'disabled' => $disabled,
						'data' => $data,
						'value' => $value,
						'substyleExclude' => $substyleExclude
					);
					$form->jyamlParentScope = new \Joomla\Registry\Registry($parentScope);

					$_title = '';
					if ($form->xmlDescription)
					{
						$_title .= JText::_($form->xmlDescription);
					}

					$html .= '<hr /><p class="hasTip" title="' . $_title . '">';
					$html .= '<strong>' . JText::_('JYAML_POSITION_MODCHROME_OPTIONS') . ': </strong>';
					$html .= '<span class="label">' . $value . '</span>';
					$html .= '</p>';

					$fieldSets = $form->getFieldsets('params');

					foreach ($fieldSets as $name => $fieldSet)
					{
						foreach ($form->getFieldset($name) as $field)
						{
							$_fieldtype = (string) $field->element['type'];
							if ($isSubstyle && ($_fieldtype == 'jyamltemplatemodchrome' || $field->fieldname == 'subStyle'))
							{
								continue;
							}

							$field->disabled = $disabled;

							if ($isStandalone)
							{
								$field->formControl .= '[' . $_fieldname . '][__attributes]';
							}
							else
							{
								$field->formControl .= '[' . $_fieldname . '][' . $k . '][__attributes]';
							}

							$field->name = $field->getName($field->fieldname);
							$field->id = $field->getId('', $field->fieldname);

							$field->element['disabled'] = "false";
							if ($disabled)
							{
								$field->element['disabled'] = "true";
							}

							$html .= '<div class="control-group">';

							$html .= '<div class="control-label">';
							$html .= $field->getLabel();
							$html .= '</div>';

							$html .= '<div class="controls">';
							$html .= $field->getInput();
							$html .= '</div>';

							$html .= '</div>';
						}
					}
				}
			}
			catch (Exception $e)
			{
				$html = $e->getMessage();
			}

			$html .= '</div>';
		}

		if ($dummyHtml === null && $k === 'dummy')
		{
			$dummyHtml = $html;
		}

		if ($k === 'dummy')
		{
			$html = str_replace($dummyFieldnameToken, $_origFieldname, $html);
		}

		return $html;
	}

	/**
	 * Get the form (jform) of a modChrome xml definition
	 *
	 * @param   string  $formControl  Formname prefix
	 * @param   string  $style        Style
	 *
	 * @return bool|JForm
	 */
	private function loadModChromeStylesForm($formControl, $style='none')
	{
		static $forms = array();
		static $xmls = array();

		$sig = $style;

		if (!isset($forms[$sig]))
		{
			$client = JApplicationHelper::getClientInfo($this->clientId);
			$pathLib = JPath::clean(JYAML_LIB_PATH . '/html/modChrome');
			$pathTmpl = JPath::clean($client->path . '/templates/' . $this->template . '/html/modChrome');

			$stylesXML = JYAMLHelper::getModChromeStylesList($this->template, $this->clientId, true);

			if ($stylesXML)
			{
				foreach ($stylesXML as $_loc => $_styles)
				{
					$path = ($_loc == 'lib' ? $pathLib : $pathTmpl);

					foreach ($_styles as $_styleXML)
					{
						$xmlFile = JPath::clean($path . '/' . $_styleXML . '.xml');
						if (!isset($xmls[$xmlFile]))
						{
							$xmls[$xmlFile] = simplexml_load_file($xmlFile, 'SimpleXMLElement', LIBXML_NOCDATA);
						}

						if (isset($xmls[$xmlFile]) && $xmls[$xmlFile])
						{
							$_form = JForm::getInstance(
									'jyaml.framework.modchrome.' . $_styleXML,
									$xmls[$xmlFile]->asXML(),
									array('control' => $formControl),
									true,
									'//config'
							);

							$_form->xmlDescription = (string) $xmls[$xmlFile]->description;
							$_form->clientId = $this->clientId;
							$_form->template = $this->template;

							// Check has substyle
							$subStyleField = $xmls[$xmlFile]->xpath("//field[@type='jyamltemplatemodchrome']");
							if (empty($subStyleField))
							{
								$subStyleField = $xmls[$xmlFile]->xpath("//field[@name='subStyle']");
							}
							$_form->hasChromeSubstyleField = !empty($subStyleField);

							$forms[$sig][$_styleXML] = $_form;
						}
					}
				}
			}
		}

		if (isset($forms[$sig]))
		{
			return isset($forms[$sig][$style]) ? (clone $forms[$sig][$style]) : false;
		}

		return false;
	}

}
