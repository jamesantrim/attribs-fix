<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  JYAML.Admin
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

defined('JPATH_BASE') or die;

jimport('joomla.form.formfield');
JFormHelper::loadFieldClass('text');

/**
 * Form Field class for the Joomla Framework.
 *
 * @package     JYAML
 * @subpackage  JYAML.Admin
 * @since       4.0.0
 */
class JFormFieldJyamltemplaterename extends JFormFieldText
{
	/**
	 * The form field type.
	 */
	protected $type = 'Jyamltemplaterename';

	/**
	 * Method to instantiate the form field object.
	 *
	 * @param   JForm  $form  The form to attach to the form field object.
	 *
	 * @since   11.1
	 */
	public function __construct($form = null)
	{
		$document = JFactory::getDocument();

		JHtml::_('jquery.framework');
		$document->addScript(JURI::root(true) . '/libraries/jyaml/assets/js/spin.js');
		$document->addScript(JURI::root(true) . '/libraries/jyaml/assets/js/jquery.spin.js');
		$document->addScript(JURI::root(true) . '/libraries/jyaml/assets/js/jquery.block-ui.js');
		$document->addScript(JURI::root(true) . '/libraries/jyaml/assets/js/fields/jyaml.js');
		$document->addScript(JURI::root(true) . '/libraries/jyaml/assets/js/fields/jyamltemplaterename.js');

		parent::__construct($form);
	}

	/**
	 * Get the field label markup.
	 *
	 * @return Form field Label
	 */
	protected function getInput()
	{
		$template = $this->form->getValue('template');
		$clientId = $this->form->getValue('client_id');
		$this->value = $template;

		$this->element['class'] = trim($this->element['class'] . ' input-medium');

		$html = '<div class="input-append">';
		$html .= parent::getInput();
		$html .= '<span class="btn jyamltemplaterename" data-template="' . $template . '" data-clientid="' . $clientId . '" data-id="'
				. $this->id . '">' . JText::_('JYAML_ADMIN_TEMPLATE_RENAME_BUTTON') . '</span>';
		$html .= '</div>';

		return $html;
	}
}
