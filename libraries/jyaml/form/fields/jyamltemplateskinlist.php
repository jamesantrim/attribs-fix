<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  JYAML.Admin
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

defined('JPATH_BASE') or die;

jimport('joomla.form.formfield');
JFormHelper::loadFieldClass('list');

/**
 * Form Field class for the Joomla Framework.
 *
 * @package     JYAML
 * @subpackage  JYAML.Admin
 * @since       4.0.0
 */
class JFormFieldJyamltemplateskinlist extends JFormFieldList
{
	/**
	 * The form field type.
	 */
	protected $type = 'Jyamltemplateskinlist';

	/**
	 * Get the field input markup
	 *
	 * @return string Form field input
	 */
	protected function getInput()
	{
		$html = parent::getInput();

		return $html;
	}

	/**
	 * Get the field options.
	 *
	 * @return array Options
	 */
	protected function getOptions()
	{
		// Initialize variables.
		$options  = array();
		$template = $this->form->getValue('template');

		$clientId = $this->form->getValue('client_id');
		$client = JApplicationHelper::getClientInfo($clientId);
		$clientPath = $client->path;

		$formParams = $this->form->getValue('params');
		$params = new \Joomla\Registry\Registry;
		$params->loadObject($formParams);

		// Path to theme directory
		$path = $clientPath . '/templates/' . $template . '/css';
		$files = array();

		// Get the Template html files
		if (JFolder::exists($path))
		{
			$excludeFiles = array();
			$files = JFolder::files($path, '^skin\.(.*)\.css$', false, false, $excludeFiles);
		}

		// Fill the options array
		$options[] = JHtml::_('select.option', '', '- ' . JText::_('JYAML_SKIN_NONE') . ' -', 'value', 'text');

		foreach ($files as $name)
		{
			$value = JFile::stripExt($name);
			$value = explode('.', $value);

			if (isset($value[1]))
			{
				$skinname = $value[1];
				$options[] = JHtml::_('select.option', $skinname, $skinname, 'value', 'text');
			}
		}

		reset($options);

		return $options;
	}
}
