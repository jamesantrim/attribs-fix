<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  JYAML.Admin
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

defined('_JEXEC') or die;

jimport('joomla.form.formfield');
jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.file');

/**
 * JYAML Stylesheets Form Field
 *
 * @package     JYAML
 * @subpackage  JYAML.Admin
 * @since       4.0.0
 */
class JFormFieldJyamltemplatestylesheets extends JFormField
{
	/**
	 * The form field type
	 */
	protected $type = 'JFormFieldJyamltemplatestylesheets';

	/**
	 * Method to instantiate the form field object.
	 *
	 * @param   JForm  $form  The form to attach to the form field object.
	 *
	 * @since   11.1
	 */
	public function __construct($form = null)
	{
		$document = JFactory::getDocument();

		JHtml::_('jquery.framework');
		JHtml::_('jquery.ui', array('core', 'sortable'));

		$document->addScript(JURI::root(true) . '/libraries/jyaml/assets/js/fields/jyaml.js');
		$document->addStylesheet(JURI::root(true) . '/libraries/jyaml/assets/css/fields/jyaml.css');

		parent::__construct($form);
	}

	/**
	 * Get the field input markup
	 *
	 * @return string Form field input
	 */
	protected function getInput()
	{
		$clientId = $this->form->getValue('client_id');
		$client = JApplicationHelper::getClientInfo($clientId);

		$this->clientPath   = $client->path;
		$this->template     = $this->form->getValue('template');
		$this->templatePath = $this->clientPath . '/templates/' . $this->template;

		$formParams = $this->form->getValue('params');
		$params = new \Joomla\Registry\Registry;
		$params->loadObject($formParams);

		$stylesheets = (array) $this->value;

		// Dummy for new script
		$dummy = array('file' => '', 'media' => 'all', 'IEconditionalComment' => '', 'dummy' => '1');
		$stylesheets = array_merge($stylesheets, array($dummy));

		$html = '';
		$html .= '<div clas="jyamltemplatestylesheets_form">';

		$regexName = preg_quote($this->name, '/');
		$groupRegex = '(' . $regexName . '\[)([0-9]*|dummy)(\].*)';

		$html .= '<div class="jyaml-move-group" data-group-regex="' . $groupRegex . '">';

		if (!empty($stylesheets))
		{
			$i = 0;
			foreach ($stylesheets as $stylesheet)
			{
				$stylesheet = new \Joomla\Registry\Registry($stylesheet);

				$file = $stylesheet->get('file', '');
				$media = $stylesheet->get('media', 'all');
				$IEcc = $stylesheet->get('IEconditionalComment', '');
				$isDummy = ($stylesheet->get('dummy', false) ? true : false);

				if (!$isDummy && empty($file))
				{
					continue;
				}

				$k = $isDummy ? 'dummy' : $i;

				$html .= '<div class="jyaml-group-entry' . ($isDummy ? '-dummy' : '') . ' jyaml-group-entry-bordered"'
						. ($isDummy ? ' style="display:none"' : '') . '>';
				$html .= '<span class="btn btn-mini jyaml-group-move-btn" title="' . JText::_('JYAML_STYLESHEET_MOVE')
						. '"><i class="icon-move"></i></span>';

				$html .= $this->getStylesheetSelect($file, $isDummy, $k);
				$html .= $this->getMediaSelect($media, $k);

				$html .= '<button class="btn btn-danger btn-mini jyaml-group-entry-remove" data-confirm="'
						. JText::_('JYAML_STYLESHEET_CONFIRM_REMOVE') . '" type="button" title="'
								. JText::_('JYAML_STYLESHEET_REMOVE') . '"><i class="icon-remove"></i></button>';

				$html .= '<hr /><div class="jyamltemplate-advanced-container">';
				$html .= $this->getIEConditionalCommentHtml($IEcc, $isDummy, $k);
				$html .= '</div>';

				$html .= '</div>';

				++$i;
			}
		}
		$html .= '</div>';

		$html .= '<button class="btn btn-success jyaml-group-entry-add" type="button"><i class="icon-plus"></i> '
				. JText::_('JYAML_STYLESHEET_ADD') . '</button>';

		$html .= '</div>';

		return $html;
	}

	/**
	 * Get Stylesheet File select
	 *
	 * @param   string   $selected  Selected
	 * @param   boolean  $disabled  Disabled
	 * @param   int      $k         Key
	 *
	 * @return string Html input
	 */
	private function getStylesheetSelect($selected = '', $disabled = false, $k = 0)
	{
		static $optionsCache;

		$name = $this->name . '[' . $k . '][file]';
		$id = $this->name . '_file_' . $k;
		$id = str_replace(array('[', ']'), '', $id);

		$options = array();
		if ($optionsCache == null)
		{
			$forceAutoload = array(
				'{$source.theme}editor.css',
				'{$source.theme}template.css',
				'{$source.theme}system.error.css'
			);

			$optionsCache = array();

			$themePath = $this->clientPath . '/templates/' . $this->template . '/css';
			$yamlPath  = JYAML_LIB_PATH . '/yaml';

			$themeFiles = array();
			if (JFolder::exists($themePath))
			{
				$themeFiles = JFolder::files($themePath, '.css$', true, true);
				sort($themeFiles);
			}

			$yamlFiles = array();
			if (JFolder::exists($yamlPath))
			{
				$yamlFiles = JFolder::files($yamlPath, '.css$', true, true);
				sort($yamlFiles);
			}

			$autoloadFilesProcessed = array();
			$autoloadFiles = JYAML::getAutoLoadFiles(
				$this->clientPath . '/templates/' . $this->template, 'stylesheets', false, true
			);

			$options[] = JHtml::_('select.option', '', '--' . JText::_('JYAML_SELECT') . '--', 'value', 'text');

			$options[]  = JHTML::_('select.optgroup', JText::sprintf('JYAML_STYLESHEET_THEME_FILES', '/templates/' . $this->template . '/css/'));
			foreach ($themeFiles as $file)
			{
				if (preg_match('#skin\.(.*)\.css#Uis', basename($file)))
				{
					// Exclude skin files (skin.*.css)
					continue;
				}

				$file = JPath::clean($file, DIRECTORY_SEPARATOR);
				$themePath = JPath::clean($themePath, DIRECTORY_SEPARATOR);
				$_disabled = false;

				$value = str_replace($themePath . DIRECTORY_SEPARATOR, '{$source.theme}', $file);
				$value = str_replace(DIRECTORY_SEPARATOR, '/', $value);

				$text = str_replace($themePath . DIRECTORY_SEPARATOR, '', $file);
				$text = str_replace(DIRECTORY_SEPARATOR, '/', $text);

				$valueAlsoCheck = $value;
				if (strpos($value, '.min.css'))
				{
					$valueAlsoCheck = preg_replace('/.min.css$/', '.css', $valueAlsoCheck);
				}
				else
				{
					$valueAlsoCheck = preg_replace('/.css$/', '.min.css', $valueAlsoCheck);
				}

				// Rtl stylesheets always autoloaded
				$isRTL = (preg_match('#[_-]rtl.css$#Uis', $value) || preg_match('#[_-]rtl.css$#Uis', $valueAlsoCheck));

				if (in_array($value, $autoloadFiles) || in_array($valueAlsoCheck, $autoloadFiles) || $isRTL || in_array($value, $forceAutoload))
				{
					$_disabled = true;
					$text = $text . ' - (autoload)';
				}

				$autoloadFilesProcessed[] = $value;
				$options[] = JHtml::_('select.option', $value, $text, 'value', 'text', $_disabled);
			}
			$options[]  = JHTML::_('select.optgroup', JText::sprintf('JYAML_STYLESHEET_THEME_FILES', '/templates/' . $this->template . '/css/'));

			$options[]  = JHTML::_('select.optgroup', JText::sprintf('JYAML_STYLESHEET_YAML_FILES', '/templates/' . $this->template . '/yaml/'));
			foreach ($yamlFiles as $file)
			{
				$file = JPath::clean($file, DIRECTORY_SEPARATOR);
				$yamlPath = JPath::clean($yamlPath, DIRECTORY_SEPARATOR);

				$_disabled = false;

				$value = str_replace($yamlPath . DIRECTORY_SEPARATOR, '{$source.yaml}', $file);
				$value = str_replace(DIRECTORY_SEPARATOR, '/', $value);

				$text = str_replace($yamlPath . DIRECTORY_SEPARATOR, '', $file);
				$text = str_replace(DIRECTORY_SEPARATOR, '/', $text);

				$valueAlsoCheck = $value;
				if (strpos($value, '.min.css'))
				{
					$valueAlsoCheck = preg_replace('/.min.css$/', '.css', $valueAlsoCheck);
				}
				else
				{
					$valueAlsoCheck = preg_replace('/.css$/', '.min.css', $valueAlsoCheck);
				}

				// Rtl stylesheets always autoloaded
				$isRTL = (preg_match('#[_-]rtl.css$#Uis', $value) || preg_match('#[_-]rtl.css$#Uis', $valueAlsoCheck));

				if (in_array($value, $autoloadFiles) || in_array($valueAlsoCheck, $autoloadFiles)|| $isRTL || in_array($value, $forceAutoload))
				{
					$_disabled = true;
					$text = $text . ' - (autoload)';
				}

				$autoloadFilesProcessed[] = $value;
				$options[] = JHtml::_('select.option', $value, $text, 'value', 'text', $_disabled);
			}
			$options[]  = JHTML::_('select.optgroup', JText::sprintf('JYAML_STYLESHEET_YAML_FILES', '/templates/' . $this->template . '/yaml/'));

			$otherFiles = array();
			foreach ($autoloadFiles as $_file)
			{
				if (!in_array($_file, $autoloadFilesProcessed))
				{
					$otherFiles[] = $_file;
				}
			}
			if (!empty($otherFiles))
			{
				$options[]  = JHTML::_('select.optgroup', JText::sprintf('JYAML_OTHER_AUTOLOAD_FILES'));
				foreach ($otherFiles as $file)
				{
					$options[] = JHtml::_('select.option', $file, $file . ' - (autoload)', 'value', 'text', true);
				}
				$options[]  = JHTML::_('select.optgroup', JText::sprintf('JYAML_OTHER_AUTOLOAD_FILES'));
			}

			$optionsCache = $options;
		}

		$options = $optionsCache;

		if (preg_match('#^(http|https|\/\/)#i', $selected))
		{
			$options[]  = JHTML::_('select.optgroup', JText::_('JYAML_EXTERNAL'));
			$options[] = JHtml::_('select.option', $selected, $selected, 'value', 'text');
			$options[] = JHTML::_('select.optgroup', JText::_('JYAML_EXTERNAL'));
		}

		$select = '<label for="' . $id . '" class="jyaml-hide">' . JText::_('JYAML_STYLESHEET_FILE') . ':</label> ';

		$urlRegex = '^(http:\/\/|https:\/\/|\/\/).*';
		$attributes = 'class="jyamltemplatestylesheet-name jyaml-custom-select-input" data-remove-group="is_empty" data-type-custom-text="'
				. JText::_('JYAML_TYPE_EXTERNAL_URL') . '" data-type-custom-notrim="1" data-type-custom-regex="' . $urlRegex . '"';

		if ($disabled)
		{
			$attributes .= ' data-dummy-init="1" disabled="disabled" style="display:none"';
		}

		$select .= JHtml::_('select.genericlist', $options, $name, $attributes, 'value', 'text', $selected, $id);

		return $select;
	}

	/**
	 * Get Stylesheet Media select
	 *
	 * @param   string   $selected  Selected
	 * @param   boolean  $disabled  Disabled
	 * @param   int      $k         Key
	 *
	 * @return string Html input
	 */
	private function getMediaSelect($selected = 'all', $disabled = false, $k = 0)
	{
		static $options;

		$name = $this->name . '[' . $k . '][media]';
		$id = $this->name . '_media_' . $k;
		$id = str_replace(array('[', ']'), '', $id);

		if ($options == null)
		{
			$options = array();

			$mediaOptions = array(
				'all' => '',
				'screen' => '',
				'projection' => '',
				'screen, procection' => '',
				'print' => '',
				'only screen and (min-device-width : 320px) and (max-device-width : 480px)' => 'Smartphones [portrait and landscape]',
				'only screen and (min-width : 321px)' => 'Smartphones [landscape]',
				'only screen and (max-width : 320px)' => 'Smartphones [portrait]',
				'only screen and (min-device-width : 768px) and (max-device-width : 1024px)' => 'iPads [portrait and landscape]',
				'only screen and (min-device-width : 768px) and (max-device-width : 1024px) and (orientation : landscape)' => 'iPads [landscape]',
				'only screen and (min-device-width : 768px) and (max-device-width : 1024px) and (orientation : portrait)' => 'iPads [portrait]',
				'only screen and (min-width : 1224px)' => 'Desktops and Laptops',
				'only screen and (min-width : 1824px)' => 'Large screens',
				'only screen and (-webkit-min-device-pixel-ratio : 1.5), only screen and (min-device-pixel-ratio : 1.5)' => 'iPhone 4',
				'only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min--moz-device-pixel-ratio: 2), '
					. 'only screen and (-o-min-device-pixel-ratio: 2/1), only screen and (min-device-pixel-ratio: 2), '
					. 'only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx)' => 'High pixel ratio [Retina]'
			);

			if ($selected != "" && !array_key_exists($selected, $mediaOptions))
			{
				$mediaOptions[$selected] = '';
			}

			$options[]  = JHTML::_('select.optgroup', JText::_('JYAML_STYLESHEET_MEDIA'));
			foreach ($mediaOptions as $media => $desc)
			{
				$mediaLabel = $media;
				if (strlen($media) > 100)
				{
					// Truncate
					$mediaLabel = substr($mediaLabel, 0, 97) . '...';
				}

				$options[] = JHtml::_('select.option', $media, (!empty($desc) ? '(' . $desc . ') - ' : '') . $mediaLabel, 'value', 'text');
			}
			$options[] = JHTML::_('select.optgroup', JText::_('JYAML_STYLESHEET_MEDIA'));
		}

		$attributes = 'class="jyamltemplatestylesheet-media jyaml-custom-select-input"';
		if ($disabled)
		{
			$attributes .= ' data-dummy-init="1" disabled="disabled" style="display:none"';
		}

		$select = '<label for="' . $id . '" class="jyaml-hide">' . JText::_('JYAML_STYLESHEET_MEDIA') . ': </label>';
		$select .= JHtml::_('select.genericlist', $options, $name, $attributes, 'value', 'text', $selected, $id);

		return $select;
	}

	/**
	 * Get IECC HTML
	 *
	 * @param   string   $value     Current value
	 * @param   boolean  $disabled  Disabled
	 * @param   int   $k         Key
	 *
	 * @return string Html input
	 */
	private function getIEConditionalCommentHtml($value = '', $disabled = false, $k = 0)
	{
		$name = $this->name . '[' . $k . '][IEconditionalComment]';
		$id = $this->name . '_IEconditionalComment_' . $k;
		$id = str_replace(array('[', ']'), '', $id);

		$html = '';

		$attributes = 'class="input-small"';
		if ($disabled)
		{
			$attributes .= ' data-dummy-init="1" disabled="disabled" style="display:none"';
		}

		$html .= '<label for="' . $id . '">' . JText::_('JYAML_CONDITIONAL_COMMENT_IE') . '</label> ';
		$html .= '&lt;!--[<input type="text" value="' . $value . '" name="' . $name . '" id="'
				. $id . '" ' . $attributes . ' />]&gt;&nbsp;<small>['
						. JText::_('JYAML_STYLESHEET') . ']</small> &lt;![endif]--&gt;';

		return $html;
	}
}
