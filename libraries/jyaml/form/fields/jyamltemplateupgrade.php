<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  Admin
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

defined('JPATH_BASE') or die;

jimport('joomla.form.formfield');

/**
 * Form Field class for the Joomla Framework.
 *
 * @package     JYAML
 * @subpackage  Admin
 * @since       4.0.0
 */
class JFormFieldJyamltemplateupgrade extends JFormField
{
	/**
	 * The form field type.
	 */
	protected $type = 'Jyamltemplateupgrade';

	/**
	 * Method to instantiate the form field object.
	 *
	 * @param   JForm  $form  The form to attach to the form field object.
	 *
	 * @since   11.1
	 */
	public function __construct($form = null)
	{
		JHtml::_('behavior.keepalive');

		$document = JFactory::getDocument();

		JHtml::_('jquery.framework');
		$document->addScript(JURI::root(true) . '/libraries/jyaml/assets/js/spin.js');
		$document->addScript(JURI::root(true) . '/libraries/jyaml/assets/js/jquery.spin.js');
		$document->addScript(JURI::root(true) . '/libraries/jyaml/assets/js/jquery.block-ui.js');
		$document->addScript(JURI::root(true) . '/libraries/jyaml/assets/js/fields/jyamltemplateupgrade.js');

		parent::__construct($form);
	}

	/**
	 * Get the field label markup.
	 *
	 * @return Form field Label
	 */
	protected function getLabel()
	{
		return '';
	}

	/**
	 * Get the field input markup.
	 *
	 * @return string Form field input
	 */
	protected function getInput()
	{
		$db = JFactory::getDbo();

		$version = 'unknown';
		$versionNum = array();

		$versionLib = 'unknown';
		$versionLibNum = array();

		if (!isset($this->scopeClientId))
		{
			$clientId = $this->form->getValue('client_id');
		}
		else
		{
			$clientId = $this->scopeClientId;
		}

		$client = JApplicationHelper::getClientInfo($clientId);

		if (!isset($this->scopeTemplate))
		{
			$this->template = $this->form->getValue('template');
		}
		else
		{
			$this->template = $this->scopeTemplate;
		}

		$this->clientPath   = $client->path;
		$this->templatePath = $this->clientPath . '/templates/' . $this->template;
		$xmlFile = $this->templatePath . '/templateDetails.xml';

		if (file_exists($xmlFile))
		{
			if (($xml = simplexml_load_file($xmlFile)))
			{
				$v = (string) $xml->version;
				$vstr = str_ireplace('.stable', '', $v);
				$c = (string) $xml->creationDate;

				$versionNum = array(
					'version' => $vstr, 'creationDate' => strtotime($c)
				);

				$version = $v . ' <small>[' . $c . ']</small>';
			}
		}

		$query = $db->getQuery(true);
		$query->select('*');
		$query->from($db->qn('#__extensions'));
		$query->where($db->qn('element') . ' = ' . $db->q('jyaml'));
		$query->where($db->qn('type') . ' = ' . $db->q('library'));
		$db->setQuery($query);

		if ($result = $db->loadObject())
		{
			$manifest = new \Joomla\Registry\Registry;
			$manifest->loadString($result->manifest_cache, 'JSON');

			$v = (string) $manifest->get('version');
			$vstr = str_ireplace('.stable', '', $v);
			$c = (string) $manifest->get('creationDate');

			$versionLibNum = array(
				'version' => $vstr, 'creationDate' => strtotime($c)
			);

			$versionLib = $v . ' <small>[' . $c . ']</small>';
		}

		$html = '';

		$tmplIsOld = false;
		if (version_compare(implode('.', $versionLibNum), implode('.', $versionNum), '>') == 1 && $versionLib != 'unknown')
		{
			$tmplIsOld = true;
		}

		$html .= '<p>' . JText::_('JYAML_CURRENT_LIB_VERSION') . ': ' . $versionLib . '</p>';
		$html .= '<p>' . JText::_('JYAML_CURRENT_TMPL_VERSION') . ': <span class="' . ($tmplIsOld ? 'jyaml-tmpl-version-old' : 'jyaml-tmpl-version-ok')
									. '">' . $version . '</span></p>';

		// Force to hide the upgrade button on development versions (token vars)
		if (strpos($versionLib, '@@') !== false || strpos($version, '@@') !== false)
		{
			$tmplIsOld = false;
		}

		if ($tmplIsOld)
		{
			$btnLink = '<button class="btn btn-warning jyamltemplateupgradeModalBtn" type="button">'
					. JText::_('JYAML_TMPL_UPGRADE_BTN') . '</button>';

			$html .= '<div class="jyaml-btn-upgrade">' . $btnLink . '</div>';

			$_msg = JText::sprintf('JYAML_TMPL_UPGRADE_MESSAGE', $btnLink);

			$modalJson = json_encode($this->getModalHtml($versionLibNum['version'], $versionLibNum['creationDate']));
			JFactory::getApplication()->enqueueMessage($_msg, 'warning');
			JFactory::getDocument()->addScriptDeclaration('JYAML.createModals.push(' . $modalJson . ');');
		}
		else
		{
			$html .= '<p>' . JText::_('JYAML_TMPL_NO_UPGRADE_NOTE') . '</p>';
			$_vTxt = empty($versionLibNum['version']) ? 'unknown' : $versionLibNum['version'];
			$html .= '<p>' . JText::sprintf('JYAML_TMPL_UPGRADE_NOTE', $_vTxt) . '</p>';
		}

		return $html;
	}

	/**
	 * Get the modal markup
	 *
	 * @param   string  $toVersion  To Version
	 * @param   string  $toDate     To Date
	 *
	 * @return string modal html
	 */
	private function getModalHtml($toVersion, $toDate)
	{
		$headline = JText::sprintf('JYAML_TMPL_UPGRADE_FRAME_TITLE', $this->template);
		$headline = JYAMLHelper::getHelpLink('config:upgrade') . ' ' . $headline;

		$html = '';
		$html .= '<div class="modal hide fade" id="jyamltemplateupgradeModal" tabindex="-1"
			role="dialog" aria-labelledby="jyamltemplateupgradeModalLabel" aria-hidden="true">';

		$html .= '<div class="modal-header">';
		$html .= '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
		$html .= '<h3 id="jyamltemplateupgradeModalLabel">' . $headline . '</h3>';
		$html .= '</div>';

		$html .= '<form action="' . JRoute::_('') . '" method="post" id="jyamltemplateupgradeInitForm" style="display:none;">';
		$html .= '<input type="hidden" name="jyamladmincontroller" value="templateupgrade" />';
		$html .= '<input type="hidden" name="jyaml_upgrade[template]" value="' . $this->template . '" />';
		$html .= '<input type="hidden" name="jyaml_upgrade[client_id]" value="' . $this->clientId . '" />';
		$html .= '<input type="hidden" name="jyaml_upgrade[to_version]" value="' . $toVersion . '" />';
		$html .= '<input type="hidden" name="jyaml_upgrade[to_date]" value="' . $toDate . '" />';
		$html .= '<input type="hidden" name="jyaml_upgrade[tpl_file]" value="" />';
		$html .= '<input type="hidden" name="jyaml_upgrade[step]" value="0" />';
		$html .= '<input type="hidden" name="' . JSession::getFormToken() . '" value="1" />';
		$html .= '</form>';

		$html .= '<div id="jyamltemplateupgradeInitFooter" style="display:none;">';
		$html .= '<button class="btn" data-dismiss="modal" aria-hidden="true">' . JText::_('JYAML_CLOSE') . '</button>';
		$html .= '</div>';

		$html .= '<div class="modal-body" style="overflow-y:auto"></div>';
		$html .= '<div class="modal-footer"></div>';

		$html .= '</div>';

		return $html;
	}
}
