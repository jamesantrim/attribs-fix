<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  Helper
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 *
 */

defined('_JEXEC') or die;

/**
 * JYAMLHelper class
 *
 * @package     JYAML
 * @subpackage  Helper
 * @since       4.5.0
 */
class JYAMLHelper
{
	/**
	 * Find and get all dynamic template positions
	 *
	 * @param   string   $templateName  Tempate name
	 * @param   int      $clientId      Client Id (0=site, 1=admin)
	 * @param   boolean  $absolute      Determine to return files with absolute path, defaults false
	 * @param   boolean  $includeFiles  Determines to include affected html files on each position, defaults true
	 *
	 * @access public
	 * @static
	 *
	 * @return Array of positions
	 */
	static public function getDynamicPositions($templateName, $clientId, $absolute=false, $includeFiles = true)
	{
		static $cache = array();

		$clientId = (int) $clientId;
		$templateName = trim((string) $templateName);
		$hash = $clientId . '.' . ($absolute ? '1' : '0') . '.' . ($includeFiles ? '1' : '0') . '.' . $templateName;

		if (!isset($cache[$hash]))
		{
			$positions = array(
				'col1_content' => array(),
				'col2_content' => array(),
				'col3_content' => array()
			);
			if (!$includeFiles)
			{
				$positions = array_keys($positions);
			}

			$client = JApplicationHelper::getClientInfo($clientId);
			$htmlPath = $client->path . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . $templateName;

			$files = self::getHtmlFiles($templateName, $clientId, $absolute);

			foreach ($files as $file)
			{
				if ($absolute)
				{
					$contents = file_get_contents($file);
				}
				else
				{
					$contents = file_get_contents($htmlPath . DIRECTORY_SEPARATOR . $file);
				}

				if (preg_match_all('#<jdoc:include\ type="jyaml" (.*)\/>#iU', $contents, $matches))
				{
					$count = count($matches[0]);

					for ($i = 0; $i < $count; $i++)
					{
						$attribs = JUtility::parseAttributes($matches[1][$i]);

						$name = isset($attribs['name']) ? $attribs['name'] : '';

						if ($name)
						{
							if ($includeFiles)
							{
								if (!isset($positions[$name]))
								{
									$positions[$name] = array();
								}

								if (!in_array($file, $positions[$name]))
								{
									$positions[$name][] = $file;
								}
							}
							elseif (!in_array($name, $positions))
							{
								$positions[] = $name;
							}
						}
					}
				}
			}

			$cache[$hash] = $positions;
		}

		return isset($cache[$hash]) ? (array) $cache[$hash] : array();
	}

	/**
	 * Get html template file list
	 *
	 * @param   string   $templateName  Tempate name
	 * @param   int      $clientId      Client Id (0=site, 1=admin)
	 * @param   boolean  $absolute      Determine to return files with absolute path, defaults false
	 *
	 * @access public
	 * @static
	 *
	 * @return Array of files
	 */
	static public function getHtmlFiles($templateName, $clientId, $absolute=false)
	{
		static $cache = array();

		$clientId = (int) $clientId;
		$templateName = trim((string) $templateName);
		$hash = $clientId . '.' . ($absolute ? '1' : '0') . '.' . $templateName;

		if (!isset($cache[$hash]))
		{
			$files = array();

			$client = JApplicationHelper::getClientInfo($clientId);
			$htmlPath = $client->path . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . $templateName;

			if (JFolder::exists($htmlPath))
			{
				$files = JFolder::files($htmlPath, '.php$', true, true);

				foreach ($files as $k => $v)
				{
					$v = JPath::clean($v);

					if ($absolute)
					{
						$files[$k] = $v;
					}
					else
					{
						$tmp = explode($htmlPath . DIRECTORY_SEPARATOR, $v);
						unset($tmp[0]);
						$files[$k] = implode(DIRECTORY_SEPARATOR, $tmp);
					}
				}
			}

			$cache[$hash] = $files;
		}

		return isset($cache[$hash]) ? (array) $cache[$hash] : array();
	}

	/**
	 * Get all module positions from database and current templateDetails.xml
	 *
	 * @param   string   $templateName  Tempate name
	 * @param   int      $clientId      Client Id (0=site, 1=admin)
	 * @param   boolean  $asOptions     Determine to return as jhtml select options, defaults true
	 *
	 * @access public
	 * @static
	 *
	 * @return Array of options
	 */
	static public function getAllModulePositions($templateName, $clientId, $asOptions=true)
	{
		static $cache;

		$clientId = (int) $clientId;
		$templateName = trim((string) $templateName);
		$hash = $clientId . '.' . ($asOptions ? '1' : '0') . '.' . $templateName;

		if (!isset($cache[$hash]))
		{
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$client = JApplicationHelper::getClientInfo($clientId);

			// Template assignment filter
			$query->select('DISTINCT(' . $db->qn('template') . ')');
			$query->from($db->qn('#__template_styles'));
			$query->where($db->qn('client_id') . ' = ' . $db->q($clientId));

			$db->setQuery($query);
			$templates = $db->loadColumn();

			if (empty($templates))
			{
				return array();
			}

			$query->clear();
			$query->select('DISTINCT(' . $db->qn('position') . ')');
			$query->from($db->qn('#__modules'));
			$query->where($db->qn('client_id') . ' = ' . $db->q($clientId));
			$query->where($db->qn('position') . ' != ' . $db->q(''));

			$db->setQuery($query);
			$positions = $db->loadColumn();

			if (empty($positions))
			{
				return array();
			}

			// Load the positions from the current template.
			$path = JPath::clean($client->path . '/templates/' . $templateName . '/templateDetails.xml');

			if (file_exists($path))
			{
				$xml = simplexml_load_file($path);
				if (isset($xml->positions[0]))
				{
					foreach ($xml->positions[0] as $position)
					{
						$positions[] = (string) $position;
					}
				}
			}

			$positions = array_unique($positions);
			sort($positions);

			if ($asOptions)
			{
				foreach ($positions as $position)
				{
					$cache[$hash][$position]  = JHtml::_('select.option', $position, $position);
				}
			}
			else
			{
				$cache[$hash] = $positions;
			}
		}

		return isset($cache[$hash]) ? (array) $cache[$hash] : array();
	}

	/**
	 * Get all module names
	 *
	 * @param   string   $templateName  Tempate name
	 * @param   int      $clientId      Client Id (0=site, 1=admin)
	 * @param   boolean  $asOptions     Determine to return as jhtml select options, defaults true
	 *
	 * @access public
	 * @static
	 *
	 * @return Array of options
	 */
	static public function getAllModules($templateName, $clientId, $asOptions=true)
	{
		static $cache;

		$clientId = (int) $clientId;
		$templateName = trim((string) $templateName);
		$hash = $clientId . '.' . ($asOptions ? '1' : '0') . '.' . $templateName;

		if (!isset($cache[$hash]))
		{
			$options = array();

			$db = JFactory::getDbo();
			$query = $db->getQuery(true);

			// Template assignment filter
			$query->select('DISTINCT(' . $db->qn('name') . ')');
			$query->from($db->qn('#__extensions'));
			$query->where($db->qn('client_id') . ' = ' . $db->q($clientId));
			$query->where($db->qn('type') . ' = ' . $db->q('module'));

			$db->setQuery($query);
			$modules = $db->loadColumn();

			if (empty($modules))
			{
				return $options;
			}

			$modules = array_unique($modules);
			sort($modules);

			if ($asOptions)
			{
				foreach ($modules as $module)
				{
					$cache[$hash][$module]  = JHtml::_('select.option', $module, $module);
				}
			}
			else
			{
				$cache[$hash] = $modules;
			}
		}

		return isset($cache[$hash]) ? (array) $cache[$hash] : array();
	}

	/**
	 * Get all chrome styles as option array
	 *
	 * @param   string  $templateName  Tempate name
	 * @param   int     $clientId      Client Id (0=site, 1=admin)
	 *
	 * @access public
	 * @static
	 *
	 * @return Array of options
	 */
	static public function getAllStyleOptions($templateName, $clientId)
	{
		static $cache;

		$clientId = (int) $clientId;
		$templateName = trim((string) $templateName);
		$hash = $clientId . '.' . $templateName;

		if (!isset($cache[$hash]))
		{
			$client = JApplicationHelper::getClientInfo($clientId);

			$options = array();
			$_styles = self::getModChromeStylesList($templateName, $clientId, false);
			$_styles['lib'] = isset($_styles['lib']) ? (array) $_styles['lib'] : array();
			$_styles['tmpl'] = isset($_styles['tmpl']) ? (array) $_styles['tmpl'] : array();

			$searchPaths = array(
				'JYAML_POSITION_STYLE_SELECT_CORE' => $client->path . '/templates/system/html/modules.php',
				'JYAML_POSITION_STYLE_SELECT_LIB' => $_styles['lib'],
				'JYAML_POSITION_STYLE_SELECT_TEMPLATE' => $_styles['tmpl']
			);

			$excludeStyles = array(
				'horz',
				'table',
				'outline'
			);

			foreach ($searchPaths as $group => $file)
			{
				if (is_array($file))
				{
					$options[]  = JHtml::_('select.optgroup', JText::_($group));

					foreach ($file as $style)
					{
						$options[]  = JHtml::_('select.option', $style, $style);
					}

					$options[]  = JHtml::_('select.optgroup', JText::_($group));
				}
				elseif (file_exists($file) && ($contents = file_get_contents($file)))
				{
					if (preg_match_all("/function modChrome_(.*)\(/Uis", $contents, $matches))
					{
						if (isset($matches[1]) && is_array($matches[1]))
						{
							$options[]  = JHtml::_('select.optgroup', JText::_($group));

							foreach ($matches[1] as $style)
							{
								if (!in_array($style, $excludeStyles))
								{
									// Exclude internal functions with underscore prefix
									if (substr($style, 0, 1) == '_')
									{
										continue;
									}

									$options[]  = JHtml::_('select.option', $style, $style);
								}
							}

							$options[]  = JHtml::_('select.optgroup', JText::_($group));
						}
					}
				}
			}

			$cache[$hash] = $options;
		}

		return isset($cache[$hash]) ? (array) $cache[$hash] : array();
	}

	/**
	 * Get JYAML Help link
	 *
	 * @param   string  $key  Help-Key
	 *
	 * @return string HTML of the help link
	 */
	public static function getHelpLink($key='')
	{
		if (!trim($key))
		{
			return '';
		}

		$url = 'http://help.jyaml.com/view/' . trim($key);
		$onclick = "Joomla.popupWindow('" . $url . "', '" . JText::_('JYAML_HELP', true) . "', 700, 500, 1); return false;";

		$html = '<button class="btn btn-info btn-small" onclick="' . $onclick . '" type="button">'
				. JText::_('JYAML_HELP') . '</button>';

		return $html;
	}

	/**
	 * Get a list of all modChrome styles in the template (xml or php based search)
	 *
	 * @param   string   $templateName  Tempate name
	 * @param   int      $clientId      Client Id (0=site, 1=admin)
	 * @param   boolean  $useXml        Use XML based search, defaults false
	 *
	 * @access public
	 * @static
	 *
	 * @return Array of styles
	 */
	static public function getModChromeStylesList($templateName, $clientId, $useXml=false)
	{
		static $cache;

		$clientId = (int) $clientId;
		$templateName = trim((string) $templateName);
		$hash = $clientId . '.' . $templateName;

		if (!isset($cache[$hash]))
		{
			$client = JApplicationHelper::getClientInfo($clientId);
			$pathLib = JPath::clean(JYAML_LIB_PATH . '/html/modChrome');
			$pathTmpl = JPath::clean($client->path . '/templates/' . $templateName . '/html/modChrome');

			$types = array('php', 'xml');

			foreach ($types as $_type)
			{
				$styles[$_type] = array('tmpl' => array(), 'lib' => array());

				if (JFolder::exists($pathLib) && ($files = JFolder::files($pathLib, '.' . $_type . '$', false, false)))
				{
					foreach ($files as $_file)
					{
						$styleName = JFile::stripExt($_file);
						$styles[$_type]['lib'][] = $styleName;
					}
				}

				if (JFolder::exists($pathTmpl) && ($files = JFolder::files($pathTmpl, '.' . $_type . '$', false, false)))
				{
					foreach ($files as $_file)
					{
						$styleName = JFile::stripExt($_file);
						$styles[$_type]['tmpl'][] = JFile::stripExt($_file);

						$_key = array_search($styleName, $styles[$_type]['lib']);
						if ($_key !== false)
						{
							unset($styles[$_type]['lib'][$_key]);
						}
					}
				}
			}

			$cache[$hash] = $styles;
		}

		$cStyles = isset($cache[$hash]) ? (array) $cache[$hash] : array();

		return ($useXml ? $cStyles['xml'] : $cStyles['php']);
	}

	/**
	 * Extended version_compare with date
	 *
	 * @param   string  $v1        Version1
	 * @param   string  $d1        Date1
	 * @param   string  $v2        Version2
	 * @param   string  $d2        Date2
	 * @param   string  $operator  Operator (see: version_compage)
	 *
	 * @return mixed Result of version_compare
	 */
	public static function version_compare($v1, $d1, $v2, $d2, $operator = null)
	{
		$v1 = str_replace(' ', '.', (string) $v1) . '.' . strtotime((string) $d1);
		$v1 = str_ireplace('.stable', '', $v1);
		$v2 = str_replace(' ', '.', (string) $v2) . '.' . strtotime((string) $d2);
		$v2 = str_ireplace('.stable', '', $v2);

		return version_compare($v1, $v2, $operator);
	}

	/**
	 * Get the contents of specific file/url
	 *
	 * @param   string   $file     Absolute Path or Url to the file
	 * @param   integer  $timeout  Timout to wait for response
	 *
	 * @return string Contents from File
	 */
	public static function getFileContents($file, $timeout=5)
	{
		static $loadMethod;
		static $phpSafeMode;
		static $phpOpenBasedir;

		if (empty($loadMethod))
		{
			// Check PHP settings
			$safeMode = strtolower(ini_get('safe_mode'));
			$phpSafeMode = (($safeMode == '0' || $safeMode == 'off') ? false : true);
			$phpOpenBasedir = (ini_get('open_basedir') == '' ? false : true);

			@ini_set('allow_url_fopen', '1');
			$allow_url_fopen = ini_get('allow_url_fopen');

			if (empty($loadMethod) && function_exists('curl_init') && function_exists('curl_exec'))
			{
				$loadMethod = 'CURL';
			}

			if (empty($loadMethod) && function_exists('file_get_contents') && $allow_url_fopen)
			{
				$loadMethod = 'FILEGETCONTENTS';
			}

			if (empty($loadMethod) && function_exists('fsockopen'))
			{
				$connnection = @fsockopen($file, 80, $errno, $error, 4);
				if ($connnection && @is_resource($connnection))
				{
					$loadMethod = 'FSOCKOPEN';
				}
			}
		}

		$content = '';

		@ini_set('default_socket_timeout', $timeout);

		$origLoadMethod = $loadMethod;

		// Force file_get_contents if file exists on local filesystem
		if (!preg_match('#^(http|https)://#Uis', $file) && file_exists($file) && is_readable($file))
		{
			$loadMethod = 'FILEGETCONTENTS';
		}

		switch ($loadMethod)
		{
			case 'FILEGETCONTENTS':
				$content = @file_get_contents($file);
				break;

			case 'FSOCKOPEN':
				$errno = 0;
				$errstr = '';

				$uri = parse_url($file);
				$fileHost = @$uri['host'];
				$filePath = @$uri['path'];

				$fp = @fsockopen($fileHost, 80, $errno, $errstr, $timeout);

				if ($fp && $fileHost && $filePath)
				{
					@fputs($fp, "GET / " . $filePath . " HTTP/1.1\r\n");
					@fputs($fp, "HOST: " . $fileHost . "\r\n");
					@fputs($fp, "User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1) Gecko/20061010 Firefox/2.0\r\n");
					@fputs($fp, "Connection: close\r\n\r\n");
					@stream_set_timeout($fp, $timeout);
					@stream_set_blocking($fp, 1);

					$response = '';
					while (!@feof($fp))
					{
						$response .= @fgets($fp);
					}
					fclose($fp);

					if ($response)
					{
						// Dplit headers from content
						$response = explode("\r\n\r\n", $response);

						// Remove headers from response
						array_shift($response);

						// Get contents only as string
						$content = trim(implode("\r\n\r\n", $response));
					}
				}
				else
				{
					throw new RuntimeException('fsockopen - Error on load file - ' . $file);
				}
				break;

			case 'CURL':
				$ch = @curl_init();
				if ($ch)
				{
					curl_setopt($ch, CURLOPT_HEADER, 0);
					curl_setopt($ch, CURLOPT_FAILONERROR, 1);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
					curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
					curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
					curl_setopt($ch, CURLOPT_URL, $file);

					// Do not verify the SSL certificate
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

					if ($phpSafeMode || $phpOpenBasedir)
					{
						// Follow location/redirect does not work if safe_mode enabled or open_basedir is set
						curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
						$data = self::curlExecFollow($ch);
					}
					else
					{
						curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
						$data = curl_exec($ch);
					}

					$info = curl_getinfo($ch);
					$http_code = @$info['http_code'];

					if ($http_code == '200')
					{
						$content = $data;
					}
					else
					{
						throw new RuntimeException('cURL - Error load file (http-code: ' . $http_code . ') - ' . $file);
					}

					if (curl_errno($ch))
					{
						throw new RuntimeException('cURL - Error: ' . curl_error($ch));
					}

					curl_close($ch);
				}
				break;
		}

		$loadMethod = $origLoadMethod;

		return $content;
	}

	/**
	 * Wrapper for curl_exec when CURLOPT_FOLLOWLOCATION is not possible
	 * {@link http://www.php.net/manual/de/function.curl-setopt.php#102121}
	 *
	 * @param   resource  $ch            Curl Ressource
	 * @param   integer   &$maxredirect  Maximum amount of redirects (defaults 5 or libcurl limit)
	 *
	 * @access private
	 * @return string Contents of curl_exec
	 */
	private static function curlExecFollow($ch, &$maxredirect=null)
	{
		$mr = ($maxredirect === null ? 5 : (int) $maxredirect);

		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
		if ($mr > 0)
		{
			$newurl = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
			$rch = curl_copy_handle($ch);

			curl_setopt($rch, CURLOPT_HEADER, true);
			curl_setopt($rch, CURLOPT_NOBODY, true);
			curl_setopt($rch, CURLOPT_FORBID_REUSE, false);
			curl_setopt($rch, CURLOPT_RETURNTRANSFER, true);

			do
			{
				curl_setopt($rch, CURLOPT_URL, $newurl);
				$header = curl_exec($rch);

				if (curl_errno($rch))
				{
					$code = 0;
				}
				else
				{
					$code = curl_getinfo($rch, CURLINFO_HTTP_CODE);

					if ($code == 301 || $code == 302)
					{
						preg_match('/Location:(.*?)\n/', $header, $matches);
						$newurl = trim(array_pop($matches));
					}
					else
					{
						$code = 0;
					}
				}
			}
			while ($code && --$mr);

			curl_close($rch);
			if (!$mr)
			{
				if ($maxredirect === null)
				{
					throw new RuntimeException('Too many redirects. When following redirects, libcurl hit the maximum amount.');
				}
				else
				{
					$maxredirect = 0;
				}

				return false;
			}

			curl_setopt($ch, CURLOPT_URL, $newurl);
		}

		return curl_exec($ch);
	}
}
