/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

// Only define the JYAML namespace if not defined.
if (typeof(JYAML) === 'undefined') {
	var JYAML = {};
}

/**
 * Accessible Accordion based on Mootools Fx.Accordion
 *
 * HTML Markup Example:
 * --------------------
		<div class="jyaml-accordion">
			<div class="accordion_content ym-clearfix">
					<div class="accordion-container ym-clearfix">
						<h5 class="accordionhead" id="optional_unique_id_for_anchor"><span>Item 1</span></h5>
						<div class="accordionbody">
							<div class="accordionbody-content ym-clearfix">Accordion Content 1</div>
						</div>
					</div>
					<div class="accordion-container ym-clearfix">
						<h5 class="accordionhead" id="optional_unique_id_for_anchor"><span>Item 2</span></h5>
						<div class="accordionbody">
							<div class="accordionbody-content ym-clearfix">Accordion Content 2</div>
						</div>
					</div>
				</div>
			</div>
		</div>
 * --------------------
 *
 */

(function($){
	$.fn.JYAMLAccordion = function(config){
		var defaults = {
			// Tag or valid Query Selector of the Elements to be treated as the Accordion Head
			headSelector: '.accordionhead',
			// Tag or valid Query Selector of the Elements to be treated as the Accordion Body
			bodySelector: '.accordionbody',

			// Classname to apply to the current accordion item
			currentClassContainer: 'current-container',
			currentClassHead: 'current-head',
			currentClassBody: 'current-body',

			// Text to indicate for screenreaders which tab is the current one
			currentInfoText: 'current tab: ',
			// Definition where to insert the Info Text. Can be either "before" or "after"
			currentInfoPosition: 'before',
			// Class to apply to the span wrapping the CurrentInfoText
			currentInfoClass: 'current-info',

			// Syncs the heights of the tab contents
			syncHeights: false,
			// Set the Method name of the plugin you want to use to sync the tab contents.
			// Defaults to the SyncHeight plugin (prefix for call is always 'JYAML.'):
			syncHeightMethodName: 'syncHeight',

			// Save the selected tab into a cookie so it stays selected after a reload.
			// This requires that the wrapping div needs to have an ID (so we know which content we're saving)
			saveState: false,

			// Initial item to show, if saveState=true this will be overwritten
			display: 0,

			// Accordion orientation
			orientation: 'vertical',
			// Effect duration in ms
			duration: 500,
			// transition: jQuery easing (see: http://jqueryui.com/demos/effect/#easing)
			transition: 'swing',
			// Opacity/Fade effect
			opacity: true,
			// If set to true, it will be possible to close all items. Otherwise, one will remain open at all time.
			alwaysHide: false,
			// If multipleOpen true, it will be able to open more then one accordion item at the same time.
			// The option alwaysHide is always enabled and saveState works only for the last opened item.
			// This only work with orientation=vertical.
			multipleOpen: false,

			// The trigger to open items. 'click' and 'hover' are valid values
			trigger: 'click'

			/*
			 * Events:
			 *
			 * onBeforeCreateAccordion: function(el, i){},
			 * onAfterCreateAccordion: function(el, i){},
			 * onComplete: function(el, i){}, // only for legacy use onAfterCreateAccordion instead
			 *
			 * onActive: function(el, toggler, element){},
			 * onBackground: function(el, toggler, element){},
			 *
			 */
		};
		var options = {};
		$.extend(options, defaults, config);

		// Check easing function exists to prevent errors
		if (typeof $.easing[options.transition] != 'function')
		{
			options.transition = defaults.transition;
		}

		// Vertical detection shorthand
		options.vertical = (options.orientation != 'horizontal' ? true : false);

		if (options.multipleOpen){
			if (options.trigger == 'click' && options.vertical){
				options.alwaysHide = true;
			} else {
				options.multipleOpen = false;
			}
		}

		// Bind events
		for (var option in options){
			if (typeof options[option] != 'function' || !(/^on[A-Z]/).test(option)) continue;
			this.on(option, options[option]);
			delete options[option];
		}

		// keyCode to direction
		var keyCodes = {
			37 : -1, // LEFT
			38 : -1, // UP
			39 : +1, // RIGHT
			40 : +1  // DOWN
		};

		// add js class to body for css adjustments, if javascript enabled
		$('body').addClass('js');

		var p = this;

		p.extend({
			createAccordions: function(accordionIdx, el){
				el.headEls = [];
				el.linkEls = [];
				el.containerEls = [];
				el.rootContEl = null;
				el.contentEls = [];
				el.acCurrent = -1;
				el.isAnimating = false;

				el.rootContEl = el.find('> .accordion_content');
				el.containerEls = el.rootContEl.find('> .accordion-container');
				el.headEls = el.containerEls.find(options.headSelector);
				el.contentEls = el.containerEls.find(options.bodySelector);

				if (el.headEls.length != el.contentEls.length){
					// throw 'JYAML Accordion: Heading element count does not match Content element count';
					return;
				}

				el.rootContEl[0].totalWidth = el.rootContEl.width();
				el.totalHeadWidth = 0; /* required for horizontal orientation */

				if (!options.vertical){
					el.rootContEl.wrapInner('<div style="width:100%;overflow:hidden;">'
							+ '<div style="margin-right: -' + el.rootContEl[0].totalWidth + 'px;" class="ym-clearfix" /></div>');
				}

				var hoverTimeout = null;
				var stopHover = false;

				el.headEls.each(function(index, head){
					var $head = $(head),
							id = $head.attr('id');

					if (!id){
						id = p.getUniqueId('accessibleaccordioncontent', accordionIdx, index);
						$head.attr('id', id);
					}
					var link = $('<a />', {
						'href': '#' + id
					});
					$head.wrapInner(link);
					link = $head.find('[href=' + link.attr('href') + ']');
					el.linkEls.push(link);

					$head.on({
						'click': function(event, isHover, parentEvent){
							// if (el.isAnimating){return false;}

							if (typeof parentEvent != 'undefined'){
								event = parentEvent;
							} else if (options.trigger == 'hover') {
								// Cancel, if hover defined and real click is triggered.
								return false;
							}

							link.focus();
							p.openAccordion(event, el, index, false, isHover);
							return false;
						}
					});

					if (options.trigger == 'hover')
					{
						el.containerEls.eq(index).on('mouseenter mouseleave', function(event){
							$head.trigger('click', [true, event]);
						});
					}

					$contentEl = $(el.contentEls[index]);
					$contentEl.css('overflow', 'hidden');
					$contentEl.data('origHeight', $(el.contentEls[index]).outerHeight());
					$contentEl.wrapInner('<div class="accordionHorizontalFxHelper" style="padding:0;margin:0;border:0;" />');

					if (options.opacity){
						$contentEl.css('opacity', 0.1);
					}

					if (!options.vertical){
						el.totalHeadWidth += $head.outerWidth(true);
					}

					el.contentEls[index].isOpen = false;
				});
				el.rootContEl.addClass('accordionamount' + el.linkEls.length);

				// Accessibility
				el.on({
					'keyup': function(event){
						if(keyCodes[event.keyCode]) {
							var index = el.acCurrent + keyCodes[event.keyCode];
							if (index < 0){
								index = el.headEls.length -1;
							} else if((index + 1) > el.headEls.length){
								index = 0;
							}

							if (!el.contentEls[index].isOpen){
								p.openAccordion(event, el, index, false, false);
							}
							el.linkEls[index].focus();
						}
					}
				});

				// Initial display
				el.contentEls.hide();

				el.initialWidth = 0;
				el.containerWidth = 0;
				if (!options.vertical){
					// set max width for items
					el.containerWidth = el.rootContEl[0].totalWidth - el.totalHeadWidth;
					el.initialWidth = 1;
					el.containerWidth = (el.containerWidth > 0 ? el.containerWidth : 1);
					el.contentEls.width(el.initialWidth).find('.accordionHorizontalFxHelper').width(el.containerWidth);
					el.contentEls.show();
				}


				// Sync Height
				p.syncHeights(el);
				$(window).load(function(){
					// 2nd syncheight for final heights
					p.syncHeights(el);
				});

				// From cookie is available
				if (options.saveState && $.cookie){
					var savedState = $.cookie('accessibleaccordion_' + el.attr('id') + '_active');
					if (savedState !== null && savedState >= 0){
						options.display = savedState;
					}
				}
				// Goto anchor if given
				if (window.location.hash){
					el.headEls.each(function(index, head){
						if (window.location.hash == ('#' + $(head).attr('id'))){
							options.display = index;
						}
					});
				}
				if (options.display >= 0){
					var initialElement = el.contentEls.eq(options.display);
					p.openAccordion(null, el, options.display, true);
				}
			},

			openAccordion: function(event, el, i, noFx, isHover){
				if (typeof el.headEls[i] == 'undefined'){
					return;
				}

				if (isHover === true && i == el.acCurrent){
					var eventType = event ? event.type : 'click';

					if (options.alwaysHide && (eventType == 'mouseout' || eventType == 'mouseleave')){
						var item = el.contentEls[i];
						if (item.isOpen)
						{
							var $item = $(item);
							var $head = el.headEls.eq(i);
							var $container = el.containerEls.eq(i);

							p._closeItem(el, i, noFx, isHover, item, $item, $head, $container);
						}
					}
				} else {
					var opener = null;
					el.contentEls.each(function(index, item){
						var $item = $(item);
						var $head = el.headEls.eq(index);
						var $container = el.containerEls.eq(index);

						if (options.multipleOpen){
							if (index == i){
								var wasOpen = item.isOpen;

								// Close
								p._closeItem(el, i, noFx, isHover, item, $item, $head, $container);

								if (wasOpen){
								// Continue
									return;
								}
							}
						} else {
							if ((item.isOpen || (item.isOpen && options.alwaysHide))){
								// Close
								p._closeItem(el, i, noFx, isHover, item, $item, $head, $container);

								if (options.alwaysHide){
									// Continue
									return;
								}
							}
						}
						if (index == i && !item.isOpen){
							// Open
							// Hold arguments to call later, after all hiding is already started
							opener = [el, i, noFx, isHover, item, $item, $head, $container];
						}
					});

					// Call _openItem after all (hiding) is started.
					// This reduces the possibility (mainly for horizontal orientation) to become not more as 100% on width/height
					if (opener !== null && $.isArray(opener)){
						p._openItem.apply(this, opener);
					}
				}

				var hasOpenItems = false;
				el.contentEls.each(function(index, item){
					if (item.isOpen){
						hasOpenItems = true;
						// break;
						return false;
					}
				});

				var beforeCurrent = el.acCurrent;
				el.acCurrent = -1;
				el.rootContEl.addClass('accordion-all-items-closed');
				el.rootContEl.removeClass('accordion-has-open-item');
				if (hasOpenItems){
					el.acCurrent = i;
					el.rootContEl.removeClass('accordion-all-items-closed');
					el.rootContEl.addClass('accordion-has-open-item');
				} else if (el.linkEls[beforeCurrent]) {
					if (event.clientX != 0) {
						el.linkEls[beforeCurrent].blur();
					}
				}
				if (options.saveState && $.cookie){
					$.cookie('accessibleaccordion_' + el.attr('id') + '_active', el.acCurrent);
				}
			},

			_openItem: function(el, index, noFx, isHover, item, $item, $head, $container){
				item.isOpen = true;

				el.rootContEl.addClass('accordion-open-item-' + index);
				$container.addClass(options.currentClassContainer);
				$item.addClass(options.currentClassBody);
				$head.addClass(options.currentClassHead);

				el.linkEls[index]
					.not(':has(span.' + options.currentInfoClass + ')')
					[(options.currentInfoPosition == 'after' ? 'append' : 'prepend')]
					('<span class="' + options.currentInfoClass + '">' + options.currentInfoText + '</span>');

				if (noFx === true){
					$item.css('opacity', '');

					if (!options.vertical){
						var containerWidth = el.rootContEl[0].totalWidth - el.totalHeadWidth;
						var toWidth = (containerWidth > 0 ? containerWidth : 0);

						if (options.horizontalAllOpen && el.containerWidth)
						{
							toWidth = el.containerWidth;
						}

						$item.width(toWidth);
					}

					$item.show();
				} else {
					$item.stop(true, false);
					var toStyle = {};

					$item.css('display', 'block');
					if (options.vertical){
						var toHeight = $item.outerHeight();

						if ($item.outerHeight() >= (toHeight - 3))
						{
							$item.height(0);
						}
						toStyle = {height: toHeight};
					} else {
						var containerWidth = el.rootContEl[0].totalWidth - el.totalHeadWidth;
						var toWidth = (containerWidth > 0 ? containerWidth : 0);

						if (options.horizontalAllOpen && el.containerWidth)
						{
							toWidth = el.containerWidth;
						} else if(el.acCurrent >= 0) {
							/*
							var restWidth = 0;
							el.containerEls.each(function(){
								restWidth += $(this).width();
							});
							restWidth = (el.rootContEl[0].totalWidth - restWidth);
							if (restWidth > 0){
								// reduce space on fast switch
								$item.width(restWidth);
							}
							*/
						}

						toStyle = {width: toWidth};
					}
					if (options.opacity){
						toStyle.opacity = 1;
					}

					el.isAnimating = true;
					$item.animate(toStyle, {
						queue: false,
						duration: options.duration,
						easing: options.transition,
						complete: function(){
							if (options.vertical){
								$item.css('height', '');
							} else {
								// $item.css('height', '');
							}
							$item.css({
								'display': 'block',
								'opacity': ''
							});
							el.isAnimating = false;
						}
					});
				}

				el.trigger('onActive', [el, $head, $item]);
			},

			_closeItem: function(el, index, noFx, isHover, item, $item, $head, $container){
				item.isOpen = false;

				el.rootContEl.removeClass('accordion-open-item-' + index);
				$container.removeClass(options.currentClassContainer);
				$item.removeClass(options.currentClassBody);
				$head.removeClass(options.currentClassHead);

				el.linkEls[index].find('span.' + options.currentInfoClass).remove();

				if (noFx === true){
					$item.hide();
					if (!options.vertical){
						var toWidth = 1;
						if (options.horizontalAllOpen && el.initialWidth)
						{
							toWidth = el.initialWidth;
						}
						$item.width(toWidth);
					}
				} else {
					$item.stop(true, false);
					var toStyle = {};

					if (options.vertical){
						toStyle = {height: '0'};
					} else {
						toStyle = {width: '1px'};

						if (options.horizontalAllOpen && el.initialWidth)
						{
							toStyle['width'] = el.initialWidth;
						}
					}
					if (options.opacity){
						toStyle.opacity = 0.1;
					}
					$item.animate(toStyle, {
						queue: false,
						duration: options.duration,
						easing: options.transition,
						complete: function(){
							if (options.vertical){
								$item.css('height', '');
								$item.css('display', 'none');
							} else {
								var toWidth = '1px';
								if (options.horizontalAllOpen && el.initialWidth)
								{
									toWidth = el.initialWidth + 'px';
								} else {
									$item.css('display', 'none');
								}
								$item.css('width', toWidth);
							}
						}
					});
				}

				el.trigger('onBackground', [el, $head, $item]);
			},
			syncHeights: function(el)
			{
				var selector = options.bodySelector + ' > div';
				if (!options.vertical){
					options.syncHeights = true;
					selector += ', ' + options.headSelector + ' > a';
				}

				if (options.syncHeights && typeof JYAML[options.syncHeightMethodName] !== 'undefined') {
					new JYAML[options.syncHeightMethodName](selector, {
						rowSelector: '' + p.selector,
						updateOnResize: false,
						includeHidden: true
					});
				}
			},
			updateSize: function(el){
				if (!options.vertical){
					el.contentEls.stop(true, true);
					el.rootContEl[0].totalWidth = el.rootContEl.width();
					el.totalHeadWidth = 0;
					el.headEls.each(function(index, head){
						el.totalHeadWidth += $(head).outerWidth(true);
					});

					var containerWidth = el.rootContEl[0].totalWidth - el.totalHeadWidth;
					containerWidth = (containerWidth > 0 ? containerWidth : 0);
					el.contentEls.find('.accordionHorizontalFxHelper').width(containerWidth);
					el.contentEls.eq(el.acCurrent).width(containerWidth);
				}

				p.syncHeights(el);
			},

			// We assume there could be multiple sets of tabs on a page, so,
			// the unique id for each invididual tab's heading is identified
			// with params q and r (e.g., id="accessibleslider-0-2-gk2k2kwl")
			getUniqueId: function(p, q, r){
				if (r===undefined) {r='';} else {r='-'+r;}
				return p + q + r;
			}
		});

		return p.each(function(i, el) {
			var el = $(el);
			if (!el.data('jyaml-accordion')) { // prevent double creation
				el.data('jyaml-accordion', this);
				el.trigger('onBeforeCreateAccordion', [el, i]);
				p.createAccordions(i, el);
				el.trigger('onAfterCreateAccordion', [el, i]);

				// update some things on resize (fluid layout)
				$(window).resize(function(){
					p.updateSize(el);
				});

				el.trigger('onComplete', [el, i]);
			}
		});
	};
	JYAML.Accordion = function(selector, options){
		return $(selector).JYAMLAccordion(options);
	};
})(jQuery);
