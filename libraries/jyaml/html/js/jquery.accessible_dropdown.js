/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

// Only define the JYAML namespace if not defined.
//noinspection JSUnusedAssignment
if (typeof(JYAML) === 'undefined') {
	var JYAML = {};
}

/**
 * Accessible JYAML Dropdown
 */
(function ($) {
	JYAML.DropdownMenusGlobal = (function () {
		this.menus = [];
		this.initHoverDone = false; // to check the mouse cursor is currently over a item

		this.registerMenu = function (m) {
			this.menus.push(m);
		};

		this.hideAllOtherMenus = function (activeMenu) {
			activeMenu = typeof(activeMenu) == 'object' ? activeMenu[0] : null;
			$.each(this.menus, function (index, menu) {
				if (menu[0] != activeMenu) {
					menu.hideSubMenus();
				}
			});
		}
	});
	var myJYAMLDropdownMenusGlobal = new JYAML.DropdownMenusGlobal();

	$.fn.JYAMLDropdown = function (config) {
		var defaults = {
			// submenu selector
			subMenuSelector: 'ul, ol',
			// link elements
			linksSelector: 'a, div.menuitem-content',
			// id of the div that will be generated to hold the submenus for absolute
			// positioning
			subMenusContainerId: 'dropdownSubMenusContainer',

			// hover class for dropitem - options.subMenuSelector.
			// for old browsers (like IE<7)
			hoverClass: 'hover',

			// horizontal or vertical
			orientation: 'horizontal',

			// submenu direction (x=left or right, y=up or down).
			// will be automaticly detected by position defined in css
			direction: {x: 'right', y: 'down'},
			// if true the submenu becomes 100% of the width
			linearSubMenu: false,
			// reverse the direction of the submenu (no the root items), if its out of the browsers viewport
			reverseDirectionOutOfViewport: true,

			// hide delay on mouseout in milliseconds
			hideDelay: 750,
			// determine to hide all submenus are not in this menu assigned on hover/focus
			hideSubMenusOnAllMenus: true,
			// transparency of submenus (0.00-1) - affected on effect fade or null
			opacity: 0.90,
			// effect: ['slide'], ['fade'], ['slide', 'fade'] or null
			// use fullslide instead of slide to slide in 'both' directions
			effects: ['slide', 'fade'],

			// effect: duration of the effect in milliseconds
			showDuration: 750,
			hideDuration: 250,
			// effect: jQuery easing onShowMenu
			showTransition: 'swing',
			// effect: jQuery easing onHideMenu
			hideTransition: 'swing',

			// stretch the first submensus width of his parent of smaller
			stretchWidthFistSubMenu: true,

			// if true the submenu position is move to left/right instead of direction change (dont work if linearSubMenu=true)
			moveHorizPosOutOfViewport: false

			/*
			 * Events:
			 *
			 * onBeforeInitialize: function(event){},
			 * onAfterInitialize: function(event){},
			 *
			 * onBeforeSubMenuInitialize: function(event, item){},
			 * onAfterSubMenuInitialize: function(event, item){},
			 *
			 * onBeforeHideChildSubMenus: function(event){},
			 * onAfterHideChildSubMenus: function(event){},
			 *
			 * onBeforeHideAllSubMenus: function(event){},
			 * onAfterHideAllSubMenus: function(event){},
			 *
			 * onAddEffectOnShowSubMenu: function(event, effect, dropitem){},
			 * onAddEffectOnHideSubMenu: function(event, effect, dropitem){},
			 *
			 */

			/*
			 * Custom Effects Example:
			 * -----------------------
			 onAddEffectOnShowSubMenu: function(event, effect, dropitem){
			 switch (effect){
			 case 'myfade': // e.g. effects: ['slide', 'myfade']
			 if(this.initShow) {
			 // Do some things that required an init value or something else
			 this.childMenuElement.css('opacity', 0.01); // required for IE on opacity effects
			 }
			 // Apply CSS transitions to the dropitem
			 $.extend(dropitem.effectStyles, {'opacity': 0.75});
			 break;
			 }
			 },
			 onAddEffectOnHideSubMenu: function(event, effect, dropitem){
			 switch (effect){
			 case 'myfade': // e.g. effects: ['slide', 'myfade']
			 $.extend(dropitem.effectStyles, {'opacity': 0});
			 break;
			 }
			 }
			 * -----------------------
			 */
		};
		var options = {};
		$.extend(true, options, defaults, config);

		// Check easing function exists to prevent errors
		if (typeof $.easing[options.transition] != 'function') {
			options.transition = defaults.transition;
		}

		// initialize directions
		options.direction.x = options.direction.x.toLowerCase();
		options.direction.y = options.direction.y.toLowerCase();

		// Bind events
		for (var option in options) {
			if (typeof options[option] != 'function' || !(/^on[A-Z]/).test(option)) {
				continue;
			}
			this.on(option, options[option]);
			delete options[option];
		}

		var isTouchDevice = (('ontouchstart' in window) || (navigator.MaxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0));

		var keyMap = {
			9: 'tab',
			37: 'left',
			38: 'up',
			39: 'right',
			40: 'down'
		};
		var getKeyMap = function (keyCode) {
			if (typeof(keyMap[keyCode]) != 'undefined') {
				return keyMap[keyCode];
			}
			return keyCode;
		};

		// add js class to body for css adjustments, if javascript enabled
		$('body').addClass('js');

		var p = this;

		p.extend({
			subMenusHideTimeout: null,
			subMenus: $([]),
			currentTouchItem: null,

			createMenu: function (i, el) {
				var $el = $(el),
					parentContainerClass = el.parent().attr('class'),
					subMenusContainerOuter = $('#' + options.subMenusContainerId);

				// Create helper container, if not exists in DOM
				if (!subMenusContainerOuter.length) {
					subMenusContainerOuter = $(document.createElement('div'))
						.attr('id', options.subMenusContainerId)
						.appendTo('body');
				}

				// Create subcontainer helper from parent with class to hold css styles for better fallback support without js
				var subMenusContainer = $(document.createElement('div'))
					.attr('class', parentContainerClass)
					.appendTo(subMenusContainerOuter);

				// parse links by linksSelector (before inject into helper container)
				var links = $el.find(options.linksSelector);
				var linksFiltered = $([]);

				links.each(function (index, item) {
					var $item = $(item),
						parentLinks = $item.parent().parents('li').children(options.linksSelector);

					if (!parentLinks.length) {
						$item.data('isRootMenu', true);
					} else {
						if (options.linearSubMenu) {
							return true; // (continue) only root item links
						}

						$item.data('isRootMenu', false).attr('tabindex', '-1');
					}
					$item.data('parentLinks', parentLinks);

					// store child menu
					var childMenus = $item.closest('li', el).children(options.subMenuSelector);
					if (childMenus.length) {
						var childMenu = childMenus.first();
						var coordinates = childMenu.offset();
						coordinates.width = childMenu.outerWidth();

						$item.data('childMenu', childMenu).data('childMenuCoordinates', coordinates);
					} else {
						$item.data('childMenu', null).data('childMenuCoordinates', {});
					}

					linksFiltered = linksFiltered.add(item);
				});
				links = linksFiltered;
				linksFiltered = null; // free memory

				// insert all childs/dropitems by subMenuSelector into helper subcontainer
				var linearSelector = (options.linearSubMenu ? '> li > ' : '');
				$el.find(linearSelector + options.subMenuSelector).each(function (index, item) {
					var $item = $(item);
					$item.on('click', function (e) {
						e.stopPropagation();
					});

					var helperClasses = ['dropdownHelperContainer'];
					// add additional helper classes for more layouting
					$($item.attr('class').split(' ')).each(function (i, c) {
						helperClasses.push($.trim(c) + '-Helper');
					});

					$(document.createElement('div')).attr('class', helperClasses.join(' '))
						.wrapInner($item)
						.appendTo(subMenusContainer);
				});

				// parse links by linksSelector (after inject into helper container)
				links.each(function (index, item) {
					var $item = $(item);

					if ($item.data('childMenu') !== null) {
						// change the childMenu from submenu self into parent helper container
						var childMenu = $item.data('childMenu').parent('div');
						$item.data('childMenu', childMenu);
						// add to allSubMenus array
						p.subMenus = p.subMenus.add(childMenu);

						// store the parentSubMenus
						var parentSubmenus = $([]);
						$($item.data('parentLinks')).each(function (i, pL) {
							parentSubmenus = parentSubmenus.add($(pL).data('childMenu'));
						});
						$item.data('parentSubMenus', parentSubmenus).data('subMenusContainer', subMenusContainer);

						// WAI-ARIA
						$item.attr('aria-haspopup', 'true');
						if (!$item.attr('id')) {
							Date.now = Date.now || function () {
									return +new Date;
								}; // Date.now with fallback for older Browsers (like IE8 and older)
							$item.attr('id', 'aria-dip_' + (Date.now().toString(36)));
						}

						// create a JYAML.DropdownDropitem
						new Dropitem(item, p);
						/////////////////////////////////
					} else if ($item.data('isRootMenu')) {
						$item.on({
							'mouseenter': function () {
								p.hideSubMenus();
								myJYAMLDropdownMenusGlobal.hideAllOtherMenus(p);
							},
							'focus': function () {
								p.hideSubMenus();
								myJYAMLDropdownMenusGlobal.hideAllOtherMenus(p);
							},
							'keydown': function (e) {
								e.key = getKeyMap(e.keyCode);

								// accessible features
								if (e.key == 'up' || e.key == 'down' || e.key == 'left' || e.key == 'right') {
									e.preventDefault();
								}

								var parent = $item.parent('li'),
									prev = parent.prev('li'),
									next = parent.next('li');

								if ((options.orientation == 'horizontal' && e.key == 'left') || (options.orientation === 'vertical' && e.key == 'up')) {
									if (prev.length) {
										prev.find(options.linksSelector).first().focus();
									} else {
										// if first item focus the last item now
										parent.parent().children('li:last').find(options.linksSelector).first().focus();
									}
								} else if ((options.orientation == 'horizontal' && e.key == 'right') || (options.orientation == 'vertical' && e.key == 'down')) {
									if (next.length) {
										next.find(options.linksSelector).first().focus();
									} else {
										// if last item focus the first item now
										parent.parent().children('li:first').find(options.linksSelector).first().focus();
									}
								}
							}
						});
					}
				});

				// hide submenus onclick document
				$(document).on('click', function () {
					// Note: stopPropagation attached on this.options.subMenuSelector
					p.hideSubMenus();
				}).trigger('mousemove');

				if (options.hideSubMenusOnAllMenus) {
					myJYAMLDropdownMenusGlobal.registerMenu(p);
				}
			},
			hideSubMenus: function () {
				this.trigger('onBeforeHideAllSubMenus');

				clearTimeout(this.subMenusHideTimeout);
				this.subMenus.trigger('hide');

				this.trigger('onAfterHideAllSubMenus');
			}
		});

		var Dropitem = (function (item, context) {
			var dp = this;

			this.inViewportRange = (function (element, position) {
				if (!this.options.reverseDirectionOutOfViewport) {
					// if the feature disabled return always true(=in range)
					return true;
				}
				var $el = $(element),
					$w = $(window);

				switch (position) {
					case 'bottom':
						return (($el.offset().top + $el.outerHeight()) <= ($w.height() + $w.scrollTop()));
					case 'top':
						return ($el.offset().top >= $w.scrollTop());
					case 'right':
						return (($el.offset().left + $el.outerWidth()) <= ($w.width() + $w.scrollLeft()));
					case 'left':
						return ($el.offset().left >= $w.scrollLeft());
					default:
						return (this.inViewportRange(element, 'right') && this.inViewportRange(element, 'left') && this.inViewportRange(element, 'bottom') && this.inViewportRange(element, 'top'));
				}
			});
			this.getSubpixels = (function () {
				// Fix rounding deviations (subpixel rendering)
				var cssleft = this.childMenu.css('left'),
					tmp = (typeof(cssleft) == 'string' ? cssleft.split('.') : 0),
					subpixels = 0;

				if (tmp.length == 2) {
					subpixels = 1 - parseFloat('.' + tmp[1]);
				}

				return subpixels;
			});
			this.setSubMenuDimensions = (function () {
				if (!this.width || !this.height || this.options.linearSubMenu) {
					var displayState = this.childMenu.css('display');
					this.childMenu.css('display', 'block');

					if (this.options.linearSubMenu && this.isRootMenu && this.options.orientation == 'horizontal') {
						var fullWidth = $(context).outerWidth();
						var subpixels = this.getSubpixels(this.childMenu);
						this.childMenuElement.css('width', fullWidth + subpixels);
						this.childMenu.css('width', fullWidth + subpixels);
					}

					var newWidth = this.childMenuElement.outerWidth();
					this.width = newWidth ? newWidth : (this.width || null);
					this.childMenuElement.css('width', this.width);

					var newHeight = this.childMenuElement.outerHeight();
					this.height = newHeight ? newHeight : (this.height || null);
					this.childMenuElement.css('width', '');

					this.stretchWidthFistSubMenu();

					this.childMenu.css('width', this.width);
					this.childMenu.css('height', this.height);
					this.childMenuElement.css('width', this.width);

					this.childMenu.css('display', displayState);
				}
			});
			this.stretchWidthFistSubMenu = (function () {
				if (!this.options.stretchWidthFistSubMenu || !this.isRootMenu || this.stretchWidthComplete) {
					return;
				}

				var parentWidth = this.item.width(),
					doStretch = true;

				var elements = this.childMenu.find(this.options.linksSelector).filter(function () {
					return $(this).parent('.menuitem-content').length ? false : true;
				});

				elements.each(function () {
					if ($(this).width() > parentWidth) {
						doStretch = false;
						return false; // break each
					}
				});

				if (doStretch) {
					elements.css('width', parentWidth);
					this.childMenu.css('width', 'auto');
					this.width = this.childMenu.children().first().width() + this.getSubpixels(this.childMenu);
					elements.css('width', '');
				}
				this.stretchWidthComplete = true;
			});
			this.setSubMenuPosition = (function () {
				if (this.onExecHide) {
					// do not set the position if the menu is currently on hiding
					return;
				}

				// inherit direction from parent menu
				if (!this.holdDirection && this.parentSubMenu && !this.isRootMenu && !this.parentSubMenu.isRootMenu) {
					this.options.direction.x = this.parentSubMenu.options.direction.x;
					this.options.direction.y = this.parentSubMenu.options.direction.y;
				}

				if (this.isRootMenu && this.options.orientation == 'horizontal') {
					if (this.options.direction.y == 'down') {
						this.childMenu.css({
							'top': this.item.offset().top + this.item.outerHeight()
						});
						this.childMenu.data('acPos', 'down');
					} else if (this.options.direction.y == 'up') {
						this.childMenu.css({
							'top': (this.item.offset().top - this.height)
						});
						this.childMenu.data('acPos', 'up');
					}
					if (this.options.direction.x == 'right') {
						this.childMenu.css({
							'left': this.item.offset().left
						});
					} else if (this.options.direction.x == 'left') {
						this.childMenu.css({
							'left': this.item.offset().left + this.item.outerWidth() - this.width
						});
					}

					if (this.options.linearSubMenu) {
						this.childMenu.css({
							'left': context.offset().left
						});
					} else {
						if (this.options.moveHorizPosOutOfViewport) {
							if (!this.inViewportRange(this.childMenu, 'right')) {
								var diff = this.childMenu.offset().left + this.childMenu.outerWidth() - ($(window).width() + $(window).scrollLeft);
								var left = this.childMenu.offset().left - diff;
								if (left < 0) {
									left = 0;
								}
								this.childMenu.css({
									'left': left
								});
							} else if (!this.inViewportRange(this.childMenu, 'left')) {
								// TODO: check if required for RTL read direction?
							}
						}
					}

				} else if (!this.isRootMenu || this.options.orientation == 'vertical') {
					if (this.options.direction.x == 'left') {
						this.childMenu.css({
							'top': this.item.offset().top,
							'left': this.item.offset().left - this.width
						});
						this.childMenu.data('acPos', 'left');

						if (!this.inViewportRange(this.childMenu, 'left')) {
							// reverse direction if outside of viewport
							this.options.direction.x = 'right';
							this.holdDirection = true;
							this.childMenu.css({
								'top': this.item.offset().top,
								'left': this.item.offset().left + this.item.outerWidth()
							});
							//this.fallbackDirection.x = 'left';
							this.childMenu.data('acPos', 'right');
						}
					} else if (this.options.direction.x == 'right') {
						this.childMenu.css({
							'top': this.item.offset().top,
							'left': this.item.offset().left + this.item.outerWidth()
						});
						this.childMenu.data('acPos', 'right');

						if (!this.inViewportRange(this.childMenu, 'right')) {
							// reverse direction if outside of viewport
							this.options.direction.x = 'left';
							this.childMenu.css({
								'top': this.item.offset().top,
								'left': this.item.offset().left - this.width
							});
							//this.fallbackDirection.x = 'right';
							this.childMenu.data('acPos', 'left');
							this.holdDirection = true;
						}
					}

					if (this.options.direction.y == 'up') {
						this.childMenu.css({
							'top': (this.item.offset().top + this.item.outerHeight() - this.height)
						});
					}
				}

				// this.fireEvent('afterPositionSubMenu');
			});
			this.cancelHideSubMenus = (function () {
				clearTimeout(context.subMenusHideTimeout);
			});
			this.hideChildSubMenus = (function () {
				context.trigger('onBeforeHideChildSubMenus');

				if (!this.item.data('childSubMenus')) {
					var childSubmenus = context.subMenus.filter(function () {
						return ($.inArray(this, dp.item.data('parentSubMenus')) == -1) && this != dp.childMenu[0];
					});
					this.item.data('childSubMenus', childSubmenus);
				}
				this.parentSubMenus.trigger('show');
				this.item.data('childSubMenus').trigger('hide');

				context.trigger('onAfterHideChildSubMenus');
			});
			this.showSubMenu = (function () {
				if (this.childMenu.isOpen) {
					return;
				}

				this.onExecHide = false;
				this.item.trigger('onBeforeShowSubMenu');

				// foreground the menu of all other menus
				this.item.data('subMenusContainer').css('z-index', 5001);

				// set width/height
				this.setSubMenuDimensions();

				// set position always new on show
				this.setSubMenuPosition();

				// add hover class
				this.item.addClass(this.options.hoverClass);
				this.item.parent('li').addClass(this.options.hoverClass);

				if (this.options.effects && this.options.effects.length) {
					dp.effectStyles = {};

					// set show transaction
					$.each(this.options.effects, function (i, effect) {
						effect = '' + $.trim(effect).toLowerCase();

						switch (effect) {
							case 'slide':
							case 'fullslide':
								if (dp.isRootMenu && dp.options.orientation == 'horizontal') {
									if (dp.options.direction.y == 'down') {
										if (dp.initShow) {
											dp.childMenuElement.css('margin-top', -dp.height);
										}
										$.extend(dp.effectStyles, {'margin-top': 0});
									} else if (dp.options.direction.y == 'up') {
										if (dp.initShow) {
											dp.childMenuElement.css('margin-top', dp.height);
										}
										$.extend(dp.effectStyles, {'margin-top': 0});
									}
									if (effect == 'fullslide') {
										if (dp.options.direction.x == 'left') {
											if (dp.initShow) {
												dp.childMenuElement.css('margin-left', dp.width);
											}
											$.extend(dp.effectStyles, {'margin-left': 0});
										} else if (dp.options.direction.x == 'right') {
											if (dp.initShow) {
												dp.childMenuElement.css('margin-left', -dp.width);
											}
											$.extend(dp.effectStyles, {'margin-left': 0});
										}
									}
								} else if (!dp.isRootMenu || dp.options.orientation == 'vertical') {
									if (dp.options.direction.x == 'left') {
										if (dp.initShow) {
											dp.childMenuElement.css('margin-left', dp.width);
										}
										$.extend(dp.effectStyles, {'margin-left': 0});
									} else if (dp.options.direction.x == 'right') {
										if (dp.initShow) {
											dp.childMenuElement.css('margin-left', -dp.width);
										}
										$.extend(dp.effectStyles, {'margin-left': 0});
									}
									if (effect == 'fullslide') {
										if (dp.options.direction.y == 'down') {
											if (dp.initShow) {
												dp.childMenuElement.css('margin-top', -dp.height);
											}
											$.extend(dp.effectStyles, {'margin-top': 0});
										} else if (dp.options.direction.y == 'up') {
											if (dp.initShow) {
												dp.childMenuElement.css('margin-top', dp.height);
											}
											$.extend(dp.effectStyles, {'margin-top': 0});
										}
									}
									if (dp.options.orientation == 'vertical' && dp.options.direction.y == 'up') {
										// nothing else
									}
								}
								break;
							case 'fade':
								if (dp.initShow) {
									// it is important to use 0.01, because IE dont show the submenu on first time if its 0
									dp.childMenuElement.css('opacity', 0.01);
								}
								$.extend(dp.effectStyles, {'opacity': dp.options.opacity});
								break;
						}

						context.trigger('onAddEffectOnShowSubMenu', [effect, dp]) || {};
					});

					this.childMenu.css({
						'display': 'block',
						'visibility': 'visible'
					});
					this.childMenuElement.css({
						'display': 'block',
						'visibility': 'visible'
					}).stop(true, false).animate(dp.effectStyles, {
						duration: dp.options.showDuration,
						easing: dp.options.showTransition,
						step: function () {
							dp.setSubMenuPosition();
						},
						complete: function () {
							dp.setSubMenuPosition();
						}
					});

				} else {
					this.childMenu.css({
						'display': 'block',
						'visibility': 'visible'
					});
					this.childMenuElement.css({
						'display': 'block',
						'visibility': 'visible'
					});

					this.item.trigger('onAfterShowSubMenu');
				}

				// WAI-ARIA
				this.childMenu.attr('aria-hidden', 'false');

				if (this.initShow) {
					this.initShow = false;
				}
				this.childMenu.isOpen = true;
			});
			this.hideSubMenu = (function () {
				if (!this.childMenu.isOpen) {
					return;
				}

				this.item.trigger('onBeforeHideSubMenu');
				this.onExecHide = true;

				// on hide set the menu on same level of all other menus
				// only foreground currently open menus (showSubMenu)
				if (this.isRootMenu) {
					this.item.data('subMenusContainer').css('z-index', 5000);
				}

				if (this.options.effects && this.options.effects.length) {
					dp.effectStyles = {};

					$.each(this.options.effects, function (i, effect) {
						effect = '' + $.trim(effect).toLowerCase();

						switch (effect) {
							case 'slide':
							case 'fullslide':
								if (dp.isRootMenu && dp.options.orientation == 'horizontal') {
									if (dp.options.direction.y == 'down') {
										$.extend(dp.effectStyles, {'margin-top': -dp.height});
									} else if (dp.options.direction.y == 'up') {
										$.extend(dp.effectStyles, {'margin-top': dp.height});
									}
									if (effect == 'fullslide') {
										if (dp.options.direction.x == 'left') {
											$.extend(dp.effectStyles, {'margin-left': dp.width});
										} else if (dp.options.direction.x == 'right') {
											$.extend(dp.effectStyles, {'margin-left': -dp.width});
										}
									}
								} else if (!dp.isRootMenu || dp.options.orientation == 'vertical') {
									if (dp.options.direction.x == 'left') {
										$.extend(dp.effectStyles, {'margin-left': dp.width});
									} else if (dp.options.direction.x == 'right') {
										$.extend(dp.effectStyles, {'margin-left': -dp.width});
									}
									if (effect == 'fullslide') {
										if (dp.options.direction.y == 'down') {
											$.extend(dp.effectStyles, {'margin-top': -dp.height});
										} else if (dp.options.direction.y == 'up') {
											$.extend(dp.effectStyles, {'margin-top': dp.height});
										}
									}

									if (dp.options.orientation == 'vertical' && dp.options.direction.y == 'up') {
										// nothing else
									}
								}
								break;
							case 'fade':
								$.extend(dp.effectStyles, {'opacity': 0});
								break;
						}

						context.trigger('onAddEffectOnHideSubMenu', [effect, dp]) || {};
					});

					this.childMenuElement.stop(true, false).animate(dp.effectStyles, {
						duration: dp.options.hideDuration,
						easing: dp.options.hideTransition,
						step: function () {
							dp.setSubMenuPosition();
						},
						complete: function () {
							dp.childMenu.css('display', 'none');
							dp.onHideSubMenu();
						}
					});
				} else {
					this.childMenu.css('display', 'none');
					this.onHideSubMenu();
				}

				// remove hover class
				this.item.removeClass(this.options.hoverClass);
				this.item.parent('li').removeClass(this.options.hoverClass);

				this.childMenu.isOpen = false;
			});
			this.onHideSubMenu = (function () {
				// WAI-ARIA
				this.childMenu.attr('aria-hidden', 'true');

				this.onExecHide = false;
				this.item.trigger('onAfterHideSubMenu');
			});
			this.hideSubMenus = (function () {
				context.trigger('onBeforeHideAllSubMenus');

				clearTimeout(context.subMenusHideTimeout);
				context.subMenusHideTimeout = setTimeout(function () {
					clearTimeout(this.subMenusHideTimeout);
					context.subMenus.trigger('hide');
				}, this.options.hideDelay);

				context.trigger('onAfterHideAllSubMenus');
			});

			this.item = $(item);

			this.options = {};
			$.extend(true, this.options, options);

			this.initShow = true;
			this.holdDirection = false;
			this.onExecHide = false;

			// initialize elements
			this.isRootMenu = this.item.data('isRootMenu');
			this.childMenu = this.item.data('childMenu');
			this.childMenuElement = this.childMenu.children().first();
			this.parentSubMenus = this.item.data('parentSubMenus');
			this.parentLinks = this.item.data('parentLinks');

			this.parentSubMenu = null;
			if (this.parentSubMenus) {
				this.parentSubMenu = this.parentSubMenus.last();
			}
			if (this.parentSubMenu) {
				this.parentSubMenu = this.parentSubMenu.data('class');
			}

			if (this.childMenu.children('div').hasClass('dropitem-forceLinear')) {
				this.options.linearSubMenu = true;
			}

			// WAI-ARIA
			this.childMenu.attr('aria-hidden', 'true');
			this.childMenu.attr('aria-labelledby', this.item.attr('id'));

			// Store this on bind for retrieve class recursive
			this.childMenu.data('class', this);

			// Set default/starting status to closed
			this.childMenu.isOpen = false;

			this.item.trigger('onBeforeSubMenuInitialize', [item]);

			// add show/hide Event shortcut
			this.childMenu.on({
				'hide': function () {
					dp.hideSubMenu();
				},
				'show': function () {
					dp.showSubMenu();
				}
			});

			var coChild = this.item.data('childMenuCoordinates');
			var coItem = this.item.offset();

			// Set default position
			this.childMenu.css({
				'top': coChild.top,
				'left': coChild.left,
				'width': coChild.width,
				//'height': coChild.height,
				'bottom': 'auto',
				'right': 'auto'
			});

			// dectect directions by original css position
			if (coChild.left < coItem.left) {
				this.options.direction.x = 'left';
				this.childMenu.data('acPos', 'left');
			} else {
				this.options.direction.x = 'right';
				this.childMenu.data('acPos', 'right');
			}

			if (coChild.top < coItem.top) {
				this.options.direction.y = 'up';
				this.childMenu.data('acPos', 'up');
			} else {
				this.options.direction.y = 'down';
				this.childMenu.data('acPos', 'down');
			}

			// set default styles
			this.childMenu.css({
				'display': 'block',
				'visibility': 'hidden',
				'overflow': 'hidden'
			});
			this.childMenuElement.css({
				'display': 'block',
				'visibility': 'hidden',
				'opacity': this.options.opacity
			});

			// set/init dimensions
			this.setSubMenuDimensions();

			var loadResizeTimer = null;
			$(window).bind('load resize', function (e) {
				// Wait of all images and fonts are loaded (or on reisze) and re-calculate sizes
				var delay = e.type == 'resize' ? 500 : 1;
				clearTimeout(loadResizeTimer);
				loadResizeTimer = setTimeout(function () {
					dp.width = null;
					dp.stretchWidthComplete = false;
					dp.setSubMenuDimensions();
				}, delay);
			});

			// and now we hide it absolute to ignore scrollbars in the browser
			this.childMenu.css('display', 'none');

			// required to foreground the menu of all other menus (defaults for all)
			this.item.data('subMenusContainer').css('z-index', 5000);

			// events for links without child menu
			var linksWithoutChildMenu = $(this.childMenu).find(this.options.linksSelector).filter(function () {
				return !$(this).data('childMenu');
			});

			linksWithoutChildMenu.on({
				'mouseenter': function () {
					dp.cancelHideSubMenus();
					dp.hideChildSubMenus();
					myJYAMLDropdownMenusGlobal.hideAllOtherMenus(context);
				},
				'focus': function () {
					dp.cancelHideSubMenus();
					dp.hideChildSubMenus();
					myJYAMLDropdownMenusGlobal.hideAllOtherMenus(context);
				},
				'mouseleave': function () {
					dp.cancelHideSubMenus();
					if (dp.isRootMenu) {
						// this.hideSubMenus();
					}
				},
				'blur': function () {
					if ($(this).prop('nodeName') == 'DIV') {
						// allow tabbing if the item not a link
						return;
					}
					dp.cancelHideSubMenus();
					// dp.hideSubMenus();
				},
				'keydown': function (e) {
					e.key = getKeyMap(e.keyCode);
					var $item = $(this);

					// accessible features
					if (e.key == 'up' || e.key == 'down' || e.key == 'left' || e.key == 'right') {
						e.preventDefault();
					}

					if (e.key == 'tab' && $item.prop('nodeName') != 'DIV' && !$item.parent('li').children(dp.options.subMenuSelector).length) {
						// fallback on press tab or shift+tab to prevent focus lost on current submenu
						e.preventDefault();
						e.key = (e.shiftKey ? 'left' : 'right');
					}

					var parent = $item.parent('li'),
						prev = parent.prev('li'),
						next = parent.next('li'),
						parentLinks = $item.data('parentLinks'),
						menuPosition = dp.childMenu.data('acPos'),
						jumpToParent = false,
						parentLink = null;

					if (parentLinks && parentLinks.length) {
						parentLink = parentLinks.first();
					}

					if (parentLink && parentLink.length && parentLink.data('isRootMenu')) {
						jumpToParent = true;
					} else {
						// if reverse direction jump to parent
						if ((e.key == 'left' && menuPosition == 'right') || (e.key == 'right' && menuPosition == 'left')) {
							jumpToParent = true;
						} else if ((e.key == 'up' && menuPosition == 'down') || (e.key == 'down' && menuPosition == 'up')) {
							jumpToParent = true;
						}
					}

					if (e.key == 'left' || e.key == 'up') {
						if (prev.length) {
							prev.find(dp.options.linksSelector).first().focus();
						} else {
							if (jumpToParent) {
								dp.item.focus();
							} else {
								// if first item focus the last item now
								parent.parent().find('* ' + dp.options.linksSelector.replace(',', ',* ')).last().focus();
							}
						}
					} else if (e.key == 'right' || e.key == 'down') {
						if (next.length) {
							next.find(dp.options.linksSelector).first().focus();
						} else {
							if (jumpToParent) {
								dp.item.focus();
							} else {
								// if last item focus the first item now
								parent.parent().find('* ' + dp.options.linksSelector.replace(',', ',* ')).first().focus();
							}
						}
					}
				}
			});

			// events for the parent item
			this.childMenu.on({
				'mouseenter': function () {
					dp.cancelHideSubMenus();
					// foreground the menu of all other menus
					dp.item.data('subMenusContainer').css('z-index', 5001);
				},
				'mouseleave': function () {
					// on hide set the menu on same level of all other menus
					// only foreground currently open menus
					dp.item.data('subMenusContainer').css('z-index', 5000);

					dp.hideSubMenus();
				}
			});

			// events for the item
			this.item.on({
				'click': function (e) {
					// do not hide submenus on click
					e.stopPropagation();
				},
				'MSPointerDown touchstart': function(e){
					if (isTouchDevice) {
						if (this != p.currentTouchItem) {
							e.stopPropagation();
							e.preventDefault();
							$(this).trigger('mouseenter');
							p.currentTouchItem = this;
						}
					}
				},
				'mousemove': function () {
					// trigger showSubMenu if the mouse cursor over the item on pageload
					if (myJYAMLDropdownMenusGlobal.initHoverDone === false) {
						dp.showSubMenu();
						myJYAMLDropdownMenusGlobal.initHoverDone = true;
					}
				},
				'mouseenter': function () {
					myJYAMLDropdownMenusGlobal.initHoverDone = true; // disable mousemove on pageload if mouseenter before was executed
					dp.cancelHideSubMenus();
					dp.hideChildSubMenus();
					dp.showSubMenu();
					myJYAMLDropdownMenusGlobal.hideAllOtherMenus(context);
				},
				'focus': function () {
					dp.cancelHideSubMenus();
					dp.hideChildSubMenus();
					dp.showSubMenu();
					myJYAMLDropdownMenusGlobal.hideAllOtherMenus(context);
				},
				'mouseleave': function () {
					dp.cancelHideSubMenus();
					if (dp.isRootMenu) {
						dp.hideSubMenus();
					}
				},
				'blur': function () {
					if (dp.item.prop('nodeName') == 'DIV') {
						// allow tabbing if the item not a link
						return;
					}

					dp.cancelHideSubMenus();
					//dp.hideSubMenus();
				},
				'keydown': function (e) {
					e.key = getKeyMap(e.keyCode);

					// accessible features
					if (e.key == 'up' || e.key == 'down' || e.key == 'left' || e.key == 'right') {
						e.preventDefault();
					}

					if (e.key == 'tab' && !dp.isRootMenu && dp.item.prop('nodeName') != 'DIV' && !dp.item.parent('li').find(dp.options.subMenuSelector).first()) {
						// fallback on press tab or shift+tab to prevent focus lost on current submenu
						e.preventDefault();
						e.key = (e.shiftKey ? 'left' : 'right');
					}

					var menuPosition = dp.childMenu.data('acPos');

					if (menuPosition == e.key) {
						var focusElement = null;

						if (e.key == 'down' || e.key == 'right') {
							focusElement = dp.childMenu.find('* ' + dp.options.linksSelector.replace(',', ',* ')).first();
						} else if (e.key == 'up' || e.key == 'left') {
							focusElement = dp.childMenu.children('* ' + dp.options.linksSelector.replace(',', ',* ')).last();
						}

						if (focusElement && focusElement.length) {
							focusElement.focus();
						}
					} else {
						var jumpToParent = false,
							parent = dp.item.parent('li'),
							prev = parent.prev('li'),
							next = parent.next('li'),
							stop = false;

						if (dp.parentSubMenu && dp.parentSubMenu.isRootMenu) {
							jumpToParent = true;
						} else {
							// if reverse direction jump to parent
							if ((e.key == 'left' && menuPosition == 'right') || (e.key == 'right' && menuPosition == 'left')) {
								jumpToParent = true;
							} else if ((e.key == 'up' && menuPosition == 'down') || (e.key == 'down' && menuPosition == 'up')) {
								jumpToParent = true;
							}
						}

						if (dp.isRootMenu && (e.key == 'up' || e.key == 'down') && dp.options.orientation == 'horizontal') {
							stop = true;
						}
						if (dp.isRootMenu && (e.key == 'left' || e.key == 'right') && dp.options.orientation == 'vertical') {
							stop = true;
						}

						if ((e.key == 'left' || e.key == 'up') && !stop) {
							if (prev.length) {
								prev.find(dp.options.linksSelector).first().focus();
							} else {
								if (jumpToParent) {
									dp.parentSubMenu.item.focus();
								} else {
									// if first item focus the last item now
									parent.parent().find('* ' + dp.options.linksSelector.replace(',', ',* ')).last().focus();
								}
							}
						} else if ((e.key == 'right' || e.key == 'down') && !stop) {
							if (next.length) {
								next.find(dp.options.linksSelector).first().focus();
							} else {
								if (jumpToParent) {
									dp.parentSubMenu.item.focus();
								} else {
									// if last item focus the first item now
									parent.parent().find('* ' + dp.options.linksSelector.replace(',', ',* ')).first().focus();
								}
							}
						}
					}
				}
			});

			context.trigger('onAfterSubMenuInitialize', [item]);
		});

		return p.each(function (i, el) {
			el = $(el);
			if (!el.data('jyaml-dropdown')) { // prevent double creation
				el.data('jyaml-dropdown', this);
				el.trigger('onBeforeInitialize', [el, i]);
				p.createMenu(i, el);
				el.trigger('onAfterInitialize', [el, i]);
			}
		});
	};
	JYAML.Dropdown = function (selector, options) {
		return $(selector).JYAMLDropdown(options);
	};
})(jQuery);
