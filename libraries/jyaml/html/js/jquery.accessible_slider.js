/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

// Only define the JYAML namespace if not defined.
if (typeof(JYAML) === 'undefined') {
	var JYAML = {};
}

/**
 * Accessible Content Slider
 *
 * HTML Markup Example:
 * --------------------
		<div class="jyaml-slider">
			<div class="slider_inner">
				<div class="slider_content ym-clearfix">
					<div class="slider-item" id="optional_unique_id_for_anchor">
						<div class="slider-item-content ym-clearfix">Slider 1 Contents</div>
						<div class="slider-item-content ym-clearfix">Slider 2 Contents</div>
					</div>
				</div>
			</div>
		</div>
 * --------------------
 *
 */

(function($){
	$.fn.JYAMLSlider = function(config){
		var defaults = {
			// effect: duration of the effect in milliseconds
			duration: 1000,
			// transition: jQuery easing (see: http://jqueryui.com/demos/effect/#easing)
			transition: 'swing',
			// Opacity/Fade effect
			opacity: false,

			// show/hide prev/next navigation
			navPrevNext: true,
			// prev/next text
			navPrevText: '&lt;',
			navNextText: '&gt;',
			// allow/disallow loop of the slide items
			navPrevNextLoop: false,

			// show/hide pagination items
			pagination: true,

			// auto slideshow
			autoSlideshow: true, // Note: Does pause automaticly on mouseover or has focus elements
			// interval to next slide
			autoSlideshowInterval: 5000,
			// show/hide play/stop buttons
			autoSlideshowControlButtons: true,
			// play/stop button text
			autoSlideshowPlayText: 'Play',
			autoSlideshowStopText: 'Stop',
			// show/hide progressbar
			autoSlideshowProgressbar: true,

			// equal heights of each content item
			syncHeights: true,
			// set the Method name of the plugin you want to use to sync the tab contents.
			// Defaults to the SyncHeight plugin (prefix for call is always 'JYAML.'):
			syncHeightMethodName: 'syncHeight'

			/*
			 * Events:
			 *
			 * onBeforeInitialize: function(event, el, i){},
			 * onAfterInitialize: function(event, el, i){},
			 *
			 * onBeforeGotoSlide: function(event, el, i, focus, forceLoop){},
			 * onAfterGotoSlide: function(event, el, i, focus, forceLoop){},
			 *
			 * onBeforeCreateNavPrevNext: function(event, el){},
			 * onAfterCreateNavPrevNext: function(event, el, prev, next){},
			 * onBeforeCreatePagination: function(event, el){},
			 * onAfterCreatePagination: function(event, el, items){},
			 *
			 * onBeforeCreateSlideshowButtons: function(event, el){},
			 * onAfterCreateSlideshowButtons: function(event, el, play, stop){},
			 * onBeforeStopSlideshow: function(event, el){},
			 * onAfterStopSlideshow: function(event, el){},
			 * onBeforeStartSlideshow: function(event, el){},
			 * onAfterStartSlideshow: function(event, el){},
			 *
			 * onBeforeCreateSlideshowProgressbar: function(event, el){},
			 * onAfterCreateSlideshowProgressbar: function(event, el, bar){},
			 * onBeforeStartProgressbar: function(event, el){},
			 * onAfterStartProgressbar: function(event, el){},
			 * onBeforeStopProgressbar: function(event, el){},
			 * onAfterStopProgressbar: function(event, el){},
			 *
			 * onUpdateSize: function(event, el){},
			 *
			 */
		};
		var options = {};
		$.extend(options, defaults, config);

		// Check easing function exists to prevent errors
		if (typeof $.easing[options.transition] != 'function')
		{
			options.transition = defaults.transition;
		}

		// Bind events
		for (var option in options){
			if (typeof options[option] != 'function' || !(/^on[A-Z]/).test(option)) continue;
			this.on(option, options[option]);
			delete options[option];
		}

		// keyCode to direction
		var keyCodes = {
			37 : -1, // LEFT
			38 : -1, // UP
			39 : +1, // RIGHT
			40 : +1  // DOWN
		};

		// add js class to body for css adjustments, if javascript enabled
		$('body').addClass('js');

		var p = this;

		p.extend({
			syncHeights: function(el)
			{
				if(options.syncHeights && typeof JYAML[options.syncHeightMethodName] !== 'undefined') {
					new JYAML[options.syncHeightMethodName]('.syncheight', {
						rowSelector: '' + p.selector,
						updateOnResize: false
					});
				}
			},
			createSliders: function(slideIdx, el){
				p.syncHeights();

				el.lastFocus = null;
				el.pause = false;
				el.stop = false;
				el.stopEls = [];

				el.inner = el.find('.slider_inner').eq(0);
				el.content = el.find('.slider_content').eq(0);

				// force to disable scrollbars
				el.content.css('overflow', 'hidden');

				el.slider = $('<div />', {'class': 'fxslider'});
				el.slider.current = 0;
				el.slider.items = el.find('.slider-item');

				el.content.append(el.slider);
				el.slider.append(el.slider.items);

				el.slider.widthStep = el.content.outerWidth();

				el.itemIds = [];
				el.itemHeadings = [];
				el.slider.items.each(function(index, item){
					var $item = $(item);
					item.content = $item.find('.slider-item-content').eq(0);
					item.content.attr('tabindex', '0');

					var id = $item.attr('id');
					if (!id){
						id = p.getUniqueId('accessibleslidecontent', slideIdx, index);
						$item.attr('id', id);
					}
					var headText = index+1;


					if (options.opacity && index > 0){
						$item.css('opacity', 0.1);
					}

					// prepare content items
					$item.css({
						'overflow': 'hidden',
						'width': el.slider.widthStep,
						'float': 'left'
					});
					$item.attr('id', id);

					// enable accessible features
					item.content.on({
						'focus': function(e){
							e.stopPropagation();
							el.lastFocus = e.target;
							p.stopProgressbar(el);
							p.gotoSlide(el, index);
						},
						'blur': function(e){
							p.startProgressbar(el);
							p.startSlideshow(el);
						},
						'keyup': function(e){
							if(keyCodes[e.keyCode]) {
								p.stopProgressbar(el);
								var gotoSlider = index + keyCodes[e.keyCode];
								p.gotoSlide(el, gotoSlider, true);
							}
						}
					});
					el.stopEls.push(el.content);

					// enable accessible features of links within the item content
					var cLinks = $item.find('a[href]');
					if(cLinks && cLinks.length) {
						item.content.attr('tabindex', '-1');

						cLinks.on({
							'focus': function(e){
								el.lastFocus = e.target;
								p.gotoSlide(el, index);
							},
							'blur': function(e){
								p.startProgressbar(el);
								p.startSlideshow(el);
							},
							'keyup': function(e){
								if(keyCodes[e.keyCode]) {
									var gotoSlider = index + keyCodes[e.keyCode];
									p.gotoSlide(el, gotoSlider, true);
								}
							}
						});
					}

					el.itemIds.push(id);
					el.itemHeadings.push(headText);
					item.index = index;
				});

				el.slider.css({
					'float': 'left',
					'width': (el.slider.widthStep * el.slider.items.length)
				});

				// create prev/next buttons, if enabled
				if(options.navPrevNext){
					el.trigger('onBeforeCreateNavPrevNext', [el]);
					p.createNavPrevNext(el);
					el.trigger('onAfterCreateNavPrevNext', [el, el.slider.prev, el.slider.next]);
				}

				// create pagination, if enabled
				if(options.pagination){
					el.trigger('onBeforeCreatePagination', [el]);
					p.createPagination(el);
					el.trigger('onAfterCreatePagination', [el, el.pagination.items]);
				}

				// prepare autoSlideshow, if enabled
				if(options.autoSlideshow) {
					if(options.autoSlideshowInterval < options.duration) {
						// Fallback: Make sure the slideshow interval is not faster as the slide effect speed
						options.autoSlideshowInterval = options.duration + 1000;
					}

					// create play/stop buttons, if enabled
					if(options.autoSlideshowControlButtons){
						el.trigger('onBeforeCreateSlideshowButtons', [el]);
						p.createSlideshowButtons(el);
						el.trigger('onAfterCreateSlideshowButtons', [el, el.autoslide.play, el.autoslide.stop]);
					}

					// create progressbar, if enabled
					if(options.autoSlideshowProgressbar) {
						el.trigger('onBeforeCreateSlideshowProgressbar', [el]);
						p.createSlideshowProgressbar(el);
						el.trigger('onAfterCreateSlideshowProgressbar', [el, el.autoslideprogress.bar]);
					}

					// activate the auto slideshow
					p.intervalSlideshow(el, options.autoSlideshowInterval);

					// pause/stop auto slideshow on hover control elements or the content items itself
					$(el.stopEls).each(function(index, stopEl){
						stopEl.on({
							'mouseover': function(e){
								p.stopSlideshow(el);
							},
							'mouseout': function(e){
								p.startSlideshow(el);
							}
						});
					});
				}

				// Touch support
				p.touchSupport(el);

				// Goto anchor if given
				if (window.location.hash){
					var anchorItem = $(el).find(window.location.hash);

					if (anchorItem && anchorItem.length == 1)
					{
						var anchorIndex = $.inArray(anchorItem.attr('id'), el.itemIds);
						if (anchorIndex != -1)
						{
							p.gotoSlide(el, anchorIndex, true);
						}
					}
				}
			},

			createSlideshowButtons: function(el){
				el.autoslide = $('<div />', {
					'class': 'autoslide'
				});

				var ppC = $('<div />', {
					'class': 'autoslide_content ym-clearfix'
				}).appendTo(el.autoslide);

				el.autoslide.play = $('<a />', {
					'class': 'play',
					'style': 'display: none',
					'title': options.autoSlideshowPlayText,
					'html': '<span>' + options.autoSlideshowPlayText + '</span>'
				}).on({
					'click': function(e){
						el.stop = false;
						el.autoslide.play.css('display', 'none');
						el.autoslide.stop.css('display', '');
						p.startSlideshow(el);
					}
				}).appendTo(ppC);

				el.autoslide.stop = $('<a />', {
					'class': 'stop',
					'title': options.autoSlideshowStopText,
					'html': '<span>' + options.autoSlideshowStopText + '</span>'
				}).on({
					'click': function(e){
						el.stop = true;

						el.autoslide.play.css('display', '');
						el.autoslide.stop.css('display', 'none');
						p.stopSlideshow(el);
					}
				}).appendTo(ppC);
				el.autoslide.appendTo(el);
			},

			createSlideshowProgressbar: function(el){
				el.autoslideprogress = $('<div />', {
					'class': 'autoslide-loadingbar',
					'html': '<div class="loadingbar"><div class="bar" style="width:0px;"></div></div>'
				}).appendTo(el);

				el.autoslideprogress.bar = el.autoslideprogress.find('.bar').eq(0);
			},

			slideshowTimer: null,
			intervalSlideshow: function(el, interval){
				p.startProgressbar(el);

				p.slideshowTimer = setInterval(function(){
					p.stopProgressbar(el);

					// For Accessibility - Stop the slideshow if some element has focus within the slider contents
					var hasFocus = el.content.has(':focus').length;

					if(el.pause===false && !hasFocus){
						p.gotoSlide(el, (el.slider.current+1), false, true);
						p.startProgressbar(el);
					}

				}, interval);
			},

			stopSlideshow: function(el){
				el.trigger('onBeforeStopSlideshow', [el]);
				el.pause = true;
				p.stopProgressbar(el);
				if(p.slideshowTimer) { clearInterval(p.slideshowTimer); }
				el.trigger('onAfterStopSlideshow', [el]);
			},

			startSlideshow: function(el){
				if(el.stop) { return; }

				el.trigger('onBeforeStartSlideshow', [el]);
				el.pause = false;
				p.startProgressbar(el);
				if(p.slideshowTimer) {
					clearInterval(p.slideshowTimer);
					p.intervalSlideshow(el, options.autoSlideshowInterval);
				}
				el.trigger('onAfterStartSlideshow', [el]);
			},

			startProgressbar: function(el, update){
				if(el.stop || !options.autoSlideshow) { return; }

				el.trigger('onBeforeStartProgressbar', [el]);

				if(options.autoSlideshowProgressbar){
					var hasFocus = el.content.has(':focus').length;

					if(!hasFocus) {
						p.stopProgressbar(el);
						el.autoslideprogress.bar.animate({ width: '100%'}, { queue: false, duration: options.autoSlideshowInterval, easing: 'linear' });
					}
				}

				el.trigger('onAfterStartProgressbar', [el]);
			},

			stopProgressbar: function(el){
				if(options.autoSlideshow && options.autoSlideshowProgressbar){
					el.trigger('onBeforeStopProgressbar', [el]);
					el.autoslideprogress.bar.stop(true, false);
					el.autoslideprogress.bar.css('width', 0);
					el.trigger('onAfterStopProgressbar', [el]);
				}
			},

			updateSize: function(el){
				el.slider.stop(true, true);

				// calculate new sizes
				el.slider.widthStep = el.content.outerWidth();
				el.slider.items.css('width', el.slider.widthStep);

				el.slider.css({
					'width': (el.slider.widthStep * el.slider.items.length),
					'margin-left': -(el.slider.widthStep * el.slider.current) // slide position correction without effect
				});

				p.syncHeights();
				el.trigger('onUpdateSize', [el]);
			},

			gotoSlide: function(el, i, focus, forceLoop){
				// reset
				p.stopProgressbar(el);

				// autofocus fix for some browsers
				setTimeout(function(){
					$(el.content).scrollLeft(0).scrollTop(0); // reset browser scroll
				}, 1);

				// do nothing if the current item already is the open item
				if(i==el.slider.current) { return; }

				// accessible features
				var mI = (el.slider.items.length - 1);
				if(i>mI) {
					if(options.navPrevNext && !options.navPrevNextLoop && forceLoop!==true) {
						if(el.lastFocus) { el.lastFocus.focus(); }
						return;
					}
					i = 0;
				} else if(i<0) {
					if(options.navPrevNext && !options.navPrevNextLoop && forceLoop!==true) {
						if(el.lastFocus) { el.lastFocus.focus(); }
						return;
					}
					i = mI;
				}

				el.trigger('onBeforeGotoSlide', [el, i, focus, forceLoop]);

				// get the position of the item
				var widthStep = el.slider.widthStep * i;

				/* Acceleration on fast switch (equalize speed on transition) */
				var diff = (Math.abs(Math.abs(parseInt(el.slider.css('margin-left')))-widthStep)) * (Math.abs(el.slider.current - i));
				var slideDuration = options.duration;
				if(diff>0 && diff < el.slider.widthStep) {
					slideDuration = options.duration / el.slider.widthStep * diff;
					slideDuration = Math.abs(slideDuration);
				}

				if (options.opacity){
					var items = $(el.slider.items);

					items.eq(el.slider.current).stop(true, false).animate({'opacity': 0.1}, {
						queue: false,
						duration: slideDuration,
						easing: options.transition
					});
					items.eq(i).stop(true, false).animate({'opacity': 1}, {
						queue: false,
						duration: slideDuration,
						easing: options.transition,
						complete: function(){
							$(this).css('opacity', '');
						}
					});
				}

				el.slider.stop(true, false);
				el.slider.animate({'margin-left': -widthStep}, {queue: false, duration: slideDuration, easing: options.transition });

				// force focus if required
				if(focus===true) {
					el.slider.items[i].content.focus();

					// reset browser/element scroll after focus
					$(el.content).scrollLeft(0).scrollTop(0);

					p.stopProgressbar(el);
				}

				// update pagination, if enabled
				if(options.pagination) {
					$(el.pagination.items).each(function(i, item){
						item.removeClass('current');
					});
					el.slider.items[i].paginationItem.addClass('current');
				}

				// update prev/next button, if enabled
				if(options.navPrevNext && !options.navPrevNextLoop) {
					el.slider.next.removeClass('pager-disabled').removeClass('page-next-disabled');
					el.slider.prev.removeClass('pager-disabled').removeClass('page-prev-disabled');

					if(i==0) {
						el.slider.prev.addClass('pager-disabled').addClass('page-prev-disabled');
					}
					if(i==mI){
						el.slider.next.addClass('pager-disabled').addClass('page-next-disabled');
					}
				}

				// set current
				el.slider.current = i;

				el.trigger('onAfterGotoSlide', [el, i, focus, forceLoop]);
			},

			createPagination: function(el){
				el.pagination = $('<div />', {
					'class': 'pagination'
				});
				el.pagination.items = [];

				var pagC = $('<div />', {
					'class': 'pagination_content ym-clearfix'
				}).appendTo(el.pagination);

				el.slider.items.each(function(i, item){
					var num = i+1;

					var pEl = new $('<a />', {
						'href': '#' + $(item).attr('id'),
						'class': 'pagination-item' + (i==0 ? ' current' : ''),
						'title': el.itemHeadings[i],
						'tabindex': '-1',
						'html': '<span>' + num + '</span>'
					}).on({
						'click': function(e){
							p.gotoSlide(el, i);
							return false;
						}
					}).appendTo(pagC);

					item.paginationItem = pEl;
					el.pagination.items.push(pEl);
					el.stopEls.push(pEl);
				});

				el.pagination.appendTo(el);
			},

			createNavPrevNext: function(el){
				var sliderItems = $(el.slider.items);

				el.slider.prev = $('<a />', {
					'href': '#',
					'class': 'pager page-prev' + (!options.navPrevNextLoop ? ' pager-disabled page-prev-disabled' : ''),
					'tabindex': '-1',
					'html': '<span>' + options.navPrevText + '</span>'
				}).on({
					'click': function(e){
						e.target.blur();
						p.gotoSlide(el, (el.slider.current-1));
						return false;
					}
				}).appendTo(el.inner);
				el.stopEls.push(el.slider.prev);

				el.slider.next = $('<a />', {
					'href': '#',
					'class': 'pager page-next' + (sliderItems.length<2 ? ' pager-disabled page-next-disabled' : ''),
					'tabindex': '-1',
					'html': '<span>' + options.navNextText + '</span>'
				}).on({
					'click': function(e){
						e.target.blur();
						p.gotoSlide(el, (el.slider.current+1));
						return false;
					}
				}).appendTo(el.inner);
				el.stopEls.push(el.slider.next);
			},

			touchSupport: function(el)
			{
				var touch_isMoving = false,
						touch_startX = 0,
						touch_startY = 0,
						touch_sliderCurrentPos = 0,
						touch_sliderNewPos = 0,
						touch_slideshowStartTimeout = null,
						element = el[0];

				if (typeof element.addEventListener == 'undefined'){
					/*
					 * No touch support for very old browsers and IE<=8
					 *
					 * I don not use "if ('ontouchstart' in document.documentElement)"
					 * to tigger plugin enabled support
					 */
					return;
				}

				// Scroll only when a quarter (1/4) of the total width has been reached
				var requiredMovePx = (el.slider.widthStep / 4);

				var onTouchStart = (function(event){
					if (event.touches.length == 1) {
						touch_sliderCurrentPos = parseInt($(el.slider).css('margin-left'));
						touch_sliderNewPos = parseInt($(el.slider).css('margin-left'));
						touch_startX = event.touches[0].pageX;
						touch_startY = event.touches[0].pageY;

						touch_isMoving = true;
						p.stopSlideshow(el);

						// Bind on touch start
						element.addEventListener('touchmove', onTouchMove, false);
						element.addEventListener('touchend', onTouchEnd, false);
						element.addEventListener('touchcancel', onTouchEnd, false);
					}
				});

				var onTouchMove = (function(event){
					// Detect vertical scrolling
					var onScroll = Math.abs(touch_startX - event.touches[0].pageX) < Math.abs(event.touches[0].pageY - touch_startY);

					if(touch_isMoving && !onScroll) {
						event.preventDefault();
						var x = event.touches[0].pageX,
								diffX = touch_startX - x;
						// var y = event.touches[0].pageY;
						// var diffY = touch_startY - y;

						var isFirst = (diffX <= 0 && el.slider.current == 0);
						var isLast = (diffX >= 0 && (el.slider.current+1) == el.slider.items.length);

						if (isFirst || isLast){
							var maxFirstLastOuter = Math.abs((requiredMovePx / 2));
							if (Math.abs(diffX) > maxFirstLastOuter)
							{
								diffX = diffX > 0 ? maxFirstLastOuter : (maxFirstLastOuter*-1);
							}
						}

						// Stop animation/effect
						el.slider.stop(true, false);

						touch_sliderNewPos = (touch_sliderCurrentPos + (diffX*-1));
						$(el.slider).css('margin-left', touch_sliderNewPos);
					}
				});

				var onTouchEnd = (function(event){
					if(touch_isMoving) {
						var movedPx = (touch_sliderCurrentPos - touch_sliderNewPos);

						if (Math.abs(movedPx) > requiredMovePx) {
							if (movedPx > 0) {
								// Next
								p.gotoSlide(el, (el.slider.current+1), false, false);
							} else if(movedPx < 0) {
								// Previous
								p.gotoSlide(el, (el.slider.current-1), false, false);
							}
						} else {
							// Cancel
							var jumpToIdx = el.slider.current;
							el.slider.current = -1; // Reset to force execution
							p.gotoSlide(el, jumpToIdx, false, false);
						}

						touch_isMoving = false;

						// Restart slideshow (delayed for lagging browsers)
						if (touch_slideshowStartTimeout) {
							clearTimeout(touch_slideshowStartTimeout);
						}
						var startDelay = ((options.autoSlideshowInterval < 1000) ? 1000 : options.autoSlideshowInterval);
						if (startDelay < options.duration)
						{
							startDelay = ((options.duration < 1000) ? 1000 : options.duration);
						}
						touch_slideshowStartTimeout = setTimeout(function(){
							$(el).find(':focus').blur(); /* Prevent intermidiate stopping of auto-slideshow on autofocusing links on touchstart */
							p.startSlideshow(el); /* Now restart the auto-slideshow */
						}, startDelay);

						// Remove events after done to not prevent scrolling
						element.removeEventListener('touchmove', onTouchMove, false);
						element.removeEventListener('touchend', onTouchEnd, false);
						element.removeEventListener('touchcancel', onTouchEnd, false);
					}
				});

				element.addEventListener('touchstart', onTouchStart, false);
			},

			// We assume there could be multiple sets of tabs on a page, so,
			// the unique id for each invididual tab's heading is identified
			// with params q and r (e.g., id="accessibleslider-0-2-gk2k2kwl")
			getUniqueId: function(p, q, r){
				if (r===undefined) {r='';} else {r='-'+r;}
				return p + q + r;
			}
		});

		return p.each(function(i, el) {
			var el = $(el);
			if(!el.data('jyaml-slider')) { // prevent double creation
				el.data('jyaml-slider', this);
				el.trigger('onBeforeInitialize', [el, i]);
				p.createSliders(i, el);
				el.trigger('onAfterInitialize', [el, i]);

				// update some things on resize (fluid layout)
				$(window).resize(function(){
					p.updateSize(el);
				});
			}
		});
	};
	JYAML.Slider = function(selector, options){
		return $(selector).JYAMLSlider(options);
	};
})(jQuery);
