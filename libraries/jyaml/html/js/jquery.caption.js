/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

// Only define the JYAML namespace if not defined.
if (typeof(JYAML) === 'undefined') {
	var JYAML = {};
}

/**
 * jQuery version of caption.js from Joomla
 */
(function($){
	$.fn.JYAMLCaption = function(config){
		var defaults = {};
		var options = {};
		$.extend(options, defaults, config);

		var p = this;
		return p.each(function(i, el) {
			var $el = $(el),
			caption = $el.attr('title');

			if (caption == '' || !caption)
			{
				return;
			}

			var	container = $('<span style="display:' + $el.css('display') + '" />'),
			text = $('<span style="display:block; width:auto;" />'),
			width = $el.attr('width'),
			align = $el.attr('align');

			if (!width) {
				width = $el.width();
			}
			if (!align) {
				align = $el.css('float');
			}
			if ('' == align || !align) {
				align = '';
			}

			var className = p.selector.replace('.', '_');
			container.addClass(className)

			if (align != '') {
				container.addClass(align).css('float', align);
			}

			text.html('<span class="caption_text">' + caption + '</span>');
			text.addClass(className);
			$el.wrap(container);
			text.insertAfter($el);
		});

		return $this;
	};
})(jQuery);
