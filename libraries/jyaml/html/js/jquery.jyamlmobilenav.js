/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

// Only define the JYAML namespace if not defined.
if (typeof(JYAML) === 'undefined') {
	var JYAML = {};
}

/**
 * JYAML Mobile Navigation Plugin for jQuery
 */
(function($){
	$.fn.JYAMLMobileNav = function(config){
		var defaults = {
				// The width threshold in px to show the mobile nav
				width_threshold: 768,

				// Append current link text
				show_active_text: true,
				// CSS Selector to find the text of the current active link text
				// (only affected is show_active_text=true)
				active_selector: '.current',

				// Title text
				title_text: 'Menu',

				// CSS selector where to show the mobile nav
				insert_selector: 'body',
				// 'prepend' or 'append' to insert_selector
				insert_method: 'prepend',

				// Container class
				container_class: 'jyaml-mobilenav',

				// Body class to add, if the menu is visible for further CSS adjustments
				body_class: 'jyaml-mobilenav-active',

				customEffect: null
				/*
				customEffect: function(toggler, menu, el, nav){
					if (toggler == 'close'){
						menu.hide();
						el.trigger('mobilenav.afterClose', nav);
					} else {
						menu.show();
						el.trigger('mobilenav.afterOpen', nav);
					}
				}
				*/

				/*
				 * Events:
				 *
				 * mobilenav.init
				 * mobilenav.show (nav),
				 * mobilenav.hide (nav),
				 * mobilenav.beforeOpen (nav),
				 * mobilenav.afterOpen (nav),
				 * mobilenav.beforeClose (nav),
				 * mobilenav.afterClose (nav)
				 *
				 * Example:
				 * jQuery('#nav > ul.menu')
				 *     .on('mobilenav.init', function(e){[function]})
				 *     .on('mobilenav.show', function(e, nav){[options]})
				 *     .JYAMLMobileNav({[options]});
				 */
		};
		var options = {};
		$.extend(options, defaults, config);

		if (options.width_threshold < 0){
			// Prevent invalid with values
			options.width_threshold = defaults.width_threshold;
		}
		if (options.insert_method != 'append'){
			// Prevent invalid method value
			options.insert_method = 'prepend';
		}

		// add js class to body for css adjustments, if javascript enabled
		var $body = $('body');
		$body.addClass('js');

		var p = this;

		p.extend({
			create: function(i, el)
			{
				var rel = el,
						preset = $(el.data('jyaml-mobilenav-element'));

				if (preset.length){
					var _rel = $(el.data('jyaml-mobilenav-rel'));
					if (_rel.length){
						rel = _rel;
					}
					el = preset;
				}

				var activeItemTxt = (options.show_active_text ? $.trim($(options.active_selector, el).last().text()) : ''),
						html = '', nav, menu;

				html += '<div class="' + options.container_class + '" style="display:none;">'
					+ '<div class="mobilenav-title"><strong class="icon-list">'
					+ ' <span class="mobilenav-menu-title">' + options.title_text + '</span>';
				if (activeItemTxt){
					html += '<span class="mobilenav-active"> - ' + activeItemTxt + '</span>';
				}
				html += '</strong></div>'
					+ '<div class="mobilenav-menu" style="display:none;">'
					+ p.getListHtml(el)
					+ '</div>'
					+ '</div>';

				nav = $(html);
				menu = $('>.mobilenav-menu', nav);
				$(options.insert_selector)[options.insert_method](nav);

				p.showHide(rel, nav);
				$(window).resize(function(){
					p.showHide(rel, nav);
				});
				$('>.mobilenav-title', nav).on('click', function(){
					if (menu.is(':visible')){
						el.trigger('mobilenav.beforeClose', nav);
						$('span.mobilenav-active', this).show();

						if (typeof(options.customEffect) === 'function'){
							options.customEffect('close', menu, el, nav);
						} else {
							menu.slideUp({
								complete: function () {
									el.trigger('mobilenav.afterClose', nav);
								}
							});
						}

						$body.removeClass('jyaml-mobilenav-open');
					} else {
						el.trigger('mobilenav.beforeOpen', nav);
						$('span.mobilenav-active', this).hide();

						if (typeof(options.customEffect) === 'function'){
							options.customEffect('open', menu, el, nav);
						} else {
							menu.slideDown({
								complete: function () {
									el.trigger('mobilenav.afterOpen', nav);
								}
							});
						}

						$body.addClass('jyaml-mobilenav-open');
					}
				});

				el.trigger('mobilenav.init', nav);
			},
			getListHtml: function(el)
			{
				el = $(el).clone();
				el.find('[id]').removeAttr('id'); // remove all id's to prevent id duplicates
				return $('<div>').append(el).html(); // return outer html
			},
			getViewportWidth: function(){
				var e = window;
				var a = 'inner';
				if (!('innerWidth' in window)){
					a = 'client';
					e = document.documentElement || document.body;
				}
				return e[ a+'Width' ];
			},
			showHide: function(el, nav){
				if (p.getViewportWidth() < options.width_threshold) {
					if (!$body.hasClass(options.body_class)) {
						nav.show();
						el.hide();
						el.trigger('mobilenav.show', nav);
						$body.addClass(options.body_class);
					}
				} else {
					if ($body.hasClass(options.body_class)) {
						nav.hide();
						el.show();
						el.trigger('mobilenav.hide', nav);
						$('body').removeClass(options.body_class);
					}
				}
			}
		});

		return p.each(function(i, el) {
			var el = $(el);
			if(!el.data('jyaml-mobilenav')) { // prevent double creation
				el.data('jyaml-mobilenav', this);
				p.create(i, el);
			}
		});
	};
	JYAML.MobileNav = function(selector, options){
		return $(selector).JYAMLMobileNav(options);
	};
})(jQuery);
