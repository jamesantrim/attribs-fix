/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

/*
 * Inspired from Dirk Gander (jQuery plugin: syncHeight)
 * See: http://blog.ginader.de/dev/syncheight/
 *
 * Usage:
 *   All elements:
 *     window.addEvent('load', function(){
 *       new JYAML.syncHeight('p', {updateOnResize:true});
 *     });
 *
 *   Group elements (e.g. rows and columns on floating divs):
 *     window.addEvent('load', function(){
 *       new JYAML.syncHeight('.col', {rowSelector:'.row', updateOnResize:true});
 *     });
 *
 */

// @TODO FIXME: syncheight - Bug in IE8: updateSize does loop because on $.css() the resize event is always triggered

// Only define the JYAML namespace if not defined.
if (typeof(JYAML) === 'undefined') {
	var JYAML = {};
}

(function($) {
	var getHeightProperty = function() {
		var browser_id = 0,
				property = [
					// To avoid content overflow in synchronised boxes on font scaling, we
					// use 'min-height' property for modern browsers ...
					['min-height','0px'],
					// and 'height' property for Internet Explorer.
					['height','1%']
				],
				bMatch = /(msie) ([\w.]+)/.exec(navigator.userAgent.toLowerCase()) || [],
				browser = bMatch[1] || "",
				browserVersion = bMatch[2] || "0";

		// check for IE6 ...
		if(browser == 'msie' && browserVersion < 7){
			browser_id = 1;
		}

		return {'name': property[browser_id][0], 'autoheightVal': property[browser_id][1]};
	};

	var onUpdate = false;

	$.getSyncedHeight = function(selector, options) {
		var val, max = 0, heightProperty = getHeightProperty();

		// get maximum element height ...
		$(selector).each(function() {
			// fallback to auto height before height check ...
			$(this).css(heightProperty.name, heightProperty.autoheightVal);

			if (options.includeHidden && $(this).is(':hidden')){
				val = $(this).actual('outerHeight');
			} else {
				val = $(this).outerHeight();
			}

			if(val > max){
				max = val;
			}
		});
		return max;
	};

	$.fn.syncHeight = function(config) {
		var defaults = {
			updateOnResize: false,	// re-sync element heights after a browser resize event (useful in flexible layouts)
			height: false,
			rowSelector: null,
			includeHidden: false
		};
		var options = {};
		$.extend(options, defaults, config);

		var e = this;

		// Sync height's per row (per parent selector)
		if(options.rowSelector) {
			$(options.rowSelector).each(function(){
				options.rowSelector = null;
				$(this).find(e.selector).syncHeight(options);
			});

			return this;
		}

		var max = 0,
				heightPropertyName = getHeightProperty().name;

		if(typeof(options.height) === "number") {
			max = options.height;
		} else {
			max = $.getSyncedHeight(this, options);
		}

		// set synchronized element height ...
		$(this).each(function(){
			$(this).css(heightPropertyName, max+'px');
		});

		// optional sync refresh on resize event ...
		if (options.updateOnResize === true) {
			$(window).resize(function(){
				options.updateOnResize = false;
				$(e).syncHeight(options);
			});
		}

		return this;
	};
	JYAML.syncHeight = function(selector, options){
		return $(selector).syncHeight(options);
	};
})(jQuery);

/*! Copyright 2011, Ben Lin (http://dreamerslab.com/)
* Licensed under the MIT License (LICENSE.txt).
*
* Version: 1.0.6
*
* Requires: jQuery 1.2.3 ~ 1.7.1
*/
;( function( $ ){
	$.fn.extend({
		actual : function( method, options ){
			var $hidden, $target, configs, css, tmp, actual, fix, restore;

			// check if the jQuery method exist
			if( !this[ method ]){
				throw '$.actual => The jQuery method "' + method + '" you called does not exist';
			}

			configs = $.extend({
				absolute : false,
				clone : false,
				includeMargin : undefined
			}, options );

			$target = this;

			if( configs.clone === true ){
				fix = function(){
					// this is useful with css3pie
					$target = $target.filter( ':first' ).clone().css({
						position : 'absolute',
						top : -1000
					}).appendTo( 'body' );
				};

				restore = function(){
					// remove DOM element after getting the width
					$target.remove();
				};
			}else{
				fix = function(){
					// get all hidden parents
					$hidden = $target.parents().andSelf().filter( ':hidden' );

					css = configs.absolute === true ?
						{ position : 'absolute', visibility: 'hidden', display: 'block' } :
						{ visibility: 'hidden', display: 'block' };

					tmp = [];

					// save the origin style props
					// set the hidden el css to be got the actual value later
					$hidden.each( function(){
						var _tmp = {}, name;
						for( name in css ){
							// save current style
							_tmp[ name ] = this.style[ name ];
							// set current style to proper css style
							this.style[ name ] = css[ name ];
						}
						tmp.push( _tmp );
					});
				};

				restore = function(){
					// restore origin style values
					$hidden.each( function( i ){
						var _tmp = tmp[ i ], name;
						for( name in css ){
							this.style[ name ] = _tmp[ name ];
						}
					});
				};
			}

			fix();
			// get the actual value with user specific methed
			// it can be 'width', 'height', 'outerWidth', 'innerWidth'... etc
			// configs.includeMargin only works for 'outerWidth' and 'outerHeight'
			actual = /(outer)/g.test( method ) ?
				$target[ method ]( configs.includeMargin ) :
				$target[ method ]();

			restore();
			// IMPORTANT, this plugin only return the value of the first element
			return actual;
		}
	});
})( jQuery );
