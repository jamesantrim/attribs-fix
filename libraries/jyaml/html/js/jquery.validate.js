/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  Validator
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 *
 */

/*
 * This validator class provides handling for YAML's form markup.
 */

// Only define the JYAML namespace if not defined.
if (typeof(JYAML) === 'undefined') {
	var JYAML = {};
}

/*
 * This validator class provides handling for YAML's form markup.
 * The jQuery validator is based on: http://bassistance.de/jquery-plugins/jquery-plugin-validation/
 */
(function($){
	$.fn.JYAMLValidator = function(config){
		var options = {};
		var defaults = {
			// If this class is found on parent elements the validator concat/join all children error messages
			jyaml_validate_group_class: 'validate-group',

			// Invalid/Error Class to add on the element container
			jyaml_element_container_invalid_class: 'ym-error',

			// Valid Class to add on the element container
			jyaml_element_container_valid_class: 'ym-success',

			// All other options, see: http://docs.jquery.com/Plugins/Validation/validate#options

			errorPlacement: function(error, element) {
				var group = getGroup(element[0]);

				if(group) {
					error.prependTo(group);
				} else {
					error.prependTo(element.parent());
				}
			},

			highlight: function(element, errorClass, validClass) {
				var group = getGroup(element),
						$el = $(element),
						container = null;

				if (group) {
					container = $(group);
				} else {
					container = $el.closest('div[class^=ym-fbox]');
				}

				//$el.addClass(errorClass).removeClass(validClass);
				container
					.removeClass(options.jyaml_element_container_valid_class)
					.addClass(options.jyaml_element_container_invalid_class);
				//$(element.form).find("label[for=" + element.id + "]").addClass(errorClass);
			},
			unhighlight: function(element, errorClass, validClass) {
				var group = getGroup(element),
						$el = $(element),
						container = null,
						timeout = 0;

				if (group) {
					container = $(group);
					timeout = 150;
				} else {
					container = $el.closest('div[class^=ym-fbox]');
				}

				//$el.removeClass(errorClass).addClass(validClass);

				setTimeout(function(){
					var hasVisibleLabels = false;
					if (group) {
						hasVisibleLabels = container.find(options.errorElement + '.' + options.errorClass).is(':visible');
					}
					if (!hasVisibleLabels)
					{
						container
							.addClass(options.jyaml_element_container_valid_class)
							.removeClass(options.jyaml_element_container_invalid_class);

						//$(element.form).find("label[for=" + element.id + "]").removeClass(errorClass);
					}
				}, timeout);
			},

			validClass: 'ym-valid',
			errorClass: 'ym-message',
			errorElement: 'p',
			groups: {}
		};
		$.extend(options, defaults, config);

		// find groups
		$('.' + options.jyaml_validate_group_class, this).each(function(){
			var groupNames = [];
			$(':input', this).each(function(){
				var name = $(this).attr('name');
				if (name)
				{
					groupNames.push(name);
				}
			});
			if (groupNames.length)
			{
				var groupName = groupNames.join('_');
				options.groups[groupName] = groupNames.join(' ');
			}
		});

		var getGroup = (function(element){
			var group = $.data(element, 'jyaml_validate_group');

			if (group === undefined)
			{
				var groupEl = $(element).closest('.' + options.jyaml_validate_group_class, element.form);

				if(groupEl.length) {
					group = groupEl;
				} else {
					group = null;
				}

				$.data(element, 'jyaml_validate_group', group);
			}

			return group;
		});

		return $(this).validate(options);
	};
	JYAML.validator = function(selector, options){
		return $(selector).each(function(){
			$(this).JYAMLValidator(options);
		});
	};
})(jQuery);

jQuery(document).ready(function($){
	// Default form validator
	$('.jyaml-form-validate:not(.ym-form), .form-validate').validate();
});
