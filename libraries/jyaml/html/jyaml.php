<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  JHtml
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

defined('_JEXEC') or die;

/**
 * JYAML Html Helper for JHtml
 *
 * @package     JYAML
 * @subpackage  JHtml
 * @since       4.0.0
 */
abstract class JHtmlJyaml
{
	/**
	 * @var array
	 */
	protected static $loaded = array();

	/**
	 * Method loaded on initialize JYAML
	 * Provide JHtml replacements
	 *
	 * @return void
	 *
	 * @since   4.5.0
	 */
	public static function init()
	{
		foreach (get_class_methods('JHtmlBootstrap') as $_method)
		{
			JHtml::register('jhtml.bootstrap.' . $_method, array('JHtmlJyaml', 'jhtml_bootstrap_' . $_method));
		}
	}

	public static function __callStatic($name, $arguments)
	{
		if (strpos($name, 'jhtml_bootstrap_') !== false)
		{
			$jyaml      = JYAML::getDocument();
			$isAdmin    = JFactory::getApplication()->isAdmin();
			$isJYAML    = $jyaml ? $jyaml->get('isJYAML') : false;
			$method     = substr($name, 16);
			$callMethod = 'JHtmlBootstrap::' . $method;

			if (is_callable($callMethod))
			{
				if ($isAdmin || ! $isJYAML)
				{
					return call_user_func_array($callMethod, $arguments);
				}
				else
				{
					if ($jyaml)
					{
						$loadJuiBootstrap = $jyaml->params->get('load_jui_bootstrap', 0);

						if ($loadJuiBootstrap)
						{
							if ($loadJuiBootstrap == 2)
							{
								JHtmlBootstrap::loadCss();
							}
							return call_user_func_array($callMethod, $arguments);
						}
					}
				}
			}
		}

		return '';
	}

	/**
	 * Replacement for JHtml::_('behavior.keepalive')
	 * Simple intervaled AJAX request without require an JS-Framework
	 *
	 * @return void
	 *
	 * @since   4.5.0
	 */
	public static function jhtml_behavior_keepalive()
	{
		static $loaded;

		// Load once
		if ($loaded === true)
		{
			return;
		}

		$jyaml = JYAML::getDocument();

		$config   = JFactory::getConfig();
		$lifetime = ($config->get('lifetime') * 60000);

		// Refresh time is 1 minute less than the liftime assined in the configuration.php file.
		$refreshTime = ($lifetime <= 60000) ? 30000 : $lifetime - 60000;

		// The longest refresh period is one hour to prevent integer overflow.
		if ($refreshTime > 3600000 || $refreshTime <= 0)
		{
			$refreshTime = 3600000;
		}

		$script = 'var keepalive=(function(){';
		$script .= 'var r;';
		$script .= 'try{';
		$script .= 'r=window.XMLHttpRequest?new XMLHttpRequest():new ActiveXObject("Microsoft.XMLHTTP")';
		$script .= '}catch(e){}';
		$script .= 'if(r){r.open("GET","./",true);r.send(null)}';
		$script .= '});';
		$script .= 'window.setInterval(keepalive, ' . $refreshTime . ');';

		$jyaml->addScriptDeclaration($script);

		$loaded = true;
	}

	/**
	 * Method to load jQuery
	 *
	 * If debugging mode is on an uncompressed version of jQuery is included for easier debugging.
	 *
	 * @param   boolean $noConflict True to load jQuery in noConflict mode [optional]
	 * @param   mixed   $debug      Is debugging mode on? [optional]
	 *
	 * @return void
	 *
	 * @since   4.5.0
	 */
	public static function jquery($noConflict = true, $debug = null) {
		// Since Joomla!3.0 jQuery is included
		JHtml::_('jquery.framework', $noConflict, $debug);
	}

	/**
	 * Method to load jQuery Cookie
	 *
	 * @return void
	 */
	public static function jqueryCookie()
	{
		static $loaded;

		// Load once
		if ($loaded === true)
		{
			return;
		}

		self::jquery();

		$jyaml = JYAML::getDocument();
		$jyaml->addScript($jyaml->getUrl('libraries', 'html/js/jquery.cookie.js'));

		$loaded = true;
	}

	/**
	 * Loads and executes the syncheight js plugin
	 *
	 * If set $rowSelector all childs within this synced on each $selector.
	 * If $rowSelector NOT set all elements are found in $selector will be synced.
	 *
	 * @param   string $selector    CSS selector
	 * @param   string $rowSelector CSS selector
	 * @param   bool   $onResize    Determine to refresh the height on resize
	 *
	 * @access public
	 * @static
	 * @return void
	 */
	public static function syncheight($selector = '', $rowSelector = '', $onResize = true) {
		static $loaded;
		static $loadedSelector;

		$jyaml = JYAML::getDocument();

		// Load once
		if ( ! $loaded)
		{
			self::jquery();

			$jyaml->addScript($jyaml->getUrl('libraries', 'html/js/jquery.syncheight.js'));
			$loaded = true;
		}

		$selectorHash = (string) $selector . (string) $rowSelector;

		if ($selector && ! isset($loadedSelector[$selectorHash]))
		{
			$options   = array();
			$options[] = "rowSelector:" . ($rowSelector ? "'" . $rowSelector . "'" : 'null');
			$options[] = "updateOnResize:" . ($onResize ? 'true' : 'false');

			$script = "new JYAML.syncHeight('" . $selector . "', {" . implode(', ', $options) . "});";

			$script = "jQuery(window).load(function(){" . $script . "});";
			$jyaml->addScriptDeclaration($script);

			$loadedSelector[$selectorHash] = true;
		}
	}

	/**
	 * Loads the Dropdown Script
	 *
	 * @param   string $selector CSS selector
	 * @param   array  $options  Slider options (see: /libraries/jyaml/html/js/jquery.accessible_dropdown.js)
	 *
	 * @access public
	 * @static
	 * @return void
	 */
	public static function dropdown($selector = '', $options = array()) {
		static $loaded;
		static $loadedSelector;

		$jyaml = JYAML::getDocument();

		// Load once
		if ( ! $loaded)
		{
			self::jquery();
			self::easing();

			$jyaml->addScript($jyaml->getUrl('libraries', 'html/js/jquery.accessible_dropdown.js'));
			$loaded = true;
		}

		$selectorHash = (string) $selector;

		if ($selector && ! isset($loadedSelector[$selectorHash]))
		{
			if ( ! is_array($options))
			{
				$options = array();
			}

			$script = "new JYAML.Dropdown('" . $selector . "', {" . implode(', ', $options) . "});";
			$script = "jQuery(document).ready(function($){" . $script . "});";
			$jyaml->addScriptDeclaration($script);

			$loadedSelector[$selectorHash] = true;
		}
	}

	/**
	 * Loads the Accessible Tabs Script
	 *
	 * @param   string $selector CSS selector
	 * @param   array  $options  Tab options (see: /libraries/jyaml/html/js/jquery.accessible_tabs.js)
	 *
	 * @access public
	 * @static
	 * @return void
	 */
	public static function tabs($selector = '', $options = array()) {
		static $loaded;
		static $loadedSelector;

		$jyaml = JYAML::getDocument();

		// Load once
		if ( ! $loaded)
		{
			self::jquery();
			self::jqueryCookie();
			self::syncheight();
			self::easing();

			$jyaml->addScript($jyaml->getUrl('libraries', 'html/js/jquery.accessible_tabs.js'));
			$loaded = true;
		}

		$selectorHash = (string) $selector;

		if ($selector && ! isset($loadedSelector[$selectorHash]))
		{
			if ( ! is_array($options))
			{
				$options = array();
			}

			$script = "new JYAML.Tabs('" . $selector . "', {" . implode(', ', $options) . "});";
			$script = "jQuery(document).ready(function($){" . $script . "});";
			$jyaml->addScriptDeclaration($script);

			$loadedSelector[$selectorHash] = true;
		}
	}

	/**
	 * Loads the Accessible Content Slider Script
	 *
	 * @param   string $selector CSS selector
	 * @param   array  $options  Slider options (see: /libraries/jyaml/html/js/jquery.accessible_slider.js)
	 *
	 * @access public
	 * @static
	 * @return void
	 */
	public static function slider($selector = '', $options = array()) {
		static $loaded;
		static $loadedSelector;

		$jyaml = JYAML::getDocument();

		// Load once
		if ( ! $loaded)
		{
			self::jquery();
			self::syncheight();
			self::easing();

			$jyaml->addScript($jyaml->getUrl('libraries', 'html/js/jquery.accessible_slider.js'));
			$loaded = true;
		}

		$selectorHash = (string) $selector;

		if ($selector && ! isset($loadedSelector[$selectorHash]))
		{
			if ( ! is_array($options))
			{
				$options = array();
			}

			$script = "new JYAML.Slider('" . $selector . "', {" . implode(', ', $options) . "});";
			$script = "jQuery(document).ready(function($){" . $script . "});";
			$jyaml->addScriptDeclaration($script);

			$loadedSelector[$selectorHash] = true;
		}
	}

	/**
	 * Loads the Accessible Accordion Script
	 *
	 * @param   string $selector CSS selector
	 * @param   array  $options  Tab options (see: /libraries/jyaml/html/js/jquery.accessible_accordion.js)
	 *
	 * @access public
	 * @static
	 * @return void
	 */
	public static function accordion($selector = '', $options = array()) {
		static $loaded;
		static $loadedSelector;

		$jyaml = JYAML::getDocument();

		// Load once
		if ( ! $loaded)
		{
			self::jquery();
			self::jqueryCookie();
			self::syncheight();
			self::easing();

			$jyaml->addScript($jyaml->getUrl('libraries', 'html/js/jquery.accessible_accordion.js'));
			$loaded = true;
		}

		$selectorHash = (string) $selector;

		if ($selector && ! isset($loadedSelector[$selectorHash]))
		{
			if ( ! is_array($options))
			{
				$options = array();
			}

			$script = "new JYAML.Accordion('" . $selector . "', {" . implode(', ', $options) . "});";
			$script = "jQuery(document).ready(function($){" . $script . "});";
			$jyaml->addScriptDeclaration($script);

			$loadedSelector[$selectorHash] = true;
		}
	}

	/**
	 * Method to load jQuery easing effects (required for special transitions/animations)
	 *
	 * @return void
	 *
	 * @since   4.5.0
	 */
	public static function easing()
	{
		static $loaded;

		// Load once
		if ($loaded === true)
		{
			return;
		}

		$jyaml = JYAML::getDocument();
		$jyaml->addScript($jyaml->getUrl('libraries', 'html/js/jquery.easing.js'));

		$loaded = true;
	}

	/**
	 * Loads and executes the js validator for forms with the class 'jyaml-form-validate'
	 *
	 * @param   string $selector CSS selector
	 * @param   array  $options  Validator options (see: /libraries/jyaml/html/js/jquery.validator.js)
	 *
	 * @access public
	 * @static
	 * @return void
	 */
	public static function validator($selector = 'form.ym-form.jyaml-form-validate', $options = array()) {
		static $loaded;
		static $loadedSelector;

		$jyaml = JYAML::getDocument();

		// Load once
		if ( ! $loaded)
		{
			self::jquery();
			$jyaml->addScript($jyaml->getUrl('libraries', 'html/js/jquery.validate/jquery.validate.js'));
			$jyaml->addScript($jyaml->getUrl('libraries', 'html/js/jquery.validate/additional-methods.js'));
			$jyaml->addScript($jyaml->getUrl('libraries', 'html/js/jquery.validate.js'));

			$lang     = JFactory::getLanguage();
			$langTag  = $lang->getTag();
			$langTags = explode('-', $langTag);

			$mainLangPath    = JYAML_LIB_PATH . '/html/js/jquery.validate/localization';
			$searchLangFiles = array(
				'messages_' . $langTags[0] . '.js',
				'messages_' . implode('_', $langTags) . '.js'
			);

			foreach ($searchLangFiles as $_file)
			{
				if (file_exists($mainLangPath . '/' . $_file))
				{
					$jyaml->addScript($jyaml->getUrl('libraries', 'html/js/jquery.validate/localization/' . $_file));
					break;
				}
			}

			$loaded = true;
		}

		$selectorHash = (string) $selector;

		if ($selector && ! isset($loadedSelector[$selectorHash]))
		{
			if ( ! is_array($options))
			{
				$options = array();
			}

			$script = "new JYAML.validator('" . $selector . "', {" . implode(', ', $options) . "});";
			$script = "jQuery(document).ready(function($){" . $script . "});";
			$jyaml->addScriptDeclaration($script);

			$loadedSelector[$selectorHash] = true;
		}
	}

	/**
	 * Method to load Mobile Nav
	 *
	 * @param   string $selector CSS selector
	 * @param   array  $options  Mobile Nav options (see: /libraries/jyaml/html/js/jquery.jyamlmobilenav.js)
	 *
	 * @since  4.5.0
	 *
	 * @access public
	 * @static
	 *
	 * @return void
	 */
	public static function mobilenav($selector = '', $options = array()) {
		static $loaded;
		static $loadedSelector;

		$jyaml = JYAML::getDocument();

		// Load once
		if ( ! $loaded)
		{
			self::jquery();
			$jyaml->addScript($jyaml->getUrl('libraries', 'html/js/jquery.jyamlmobilenav.js'));

			$loaded = true;
		}

		$selectorHash = (string) $selector;

		if ($selector && ! isset($loadedSelector[$selectorHash]))
		{
			if ( ! is_array($options))
			{
				$options = array();
			}

			$script = "new JYAML.MobileNav('" . $selector . "', {" . implode(', ', $options) . "});";
			$script = "jQuery(document).ready(function($){" . $script . "});";
			$jyaml->addScriptDeclaration($script);

			$loadedSelector[$selectorHash] = true;
		}
	}
}
