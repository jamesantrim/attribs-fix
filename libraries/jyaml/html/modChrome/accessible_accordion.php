<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  JYAML.Template
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

// No direct access.
defined('_JEXEC') or die;

/**
 * Chrome Style: This style does output a html markup for Accessible accordion
 * Available and Used attributes:
 *   - headlineTag: Headline Tag (defaults: h3)
 *   - syncHeight: Sync Height of contents (defaults: 0)
 *   - saveState: Save current open item as cookie (defaults: 0)
 *   - display: The index of the element to show at start (defaults: 0). Use -1 to close all by default
 *   - opacityEffect: Opacity effect on show/hide (defaults: 1)
 *   - alwaysHide: If set to true, it will be possible to close all contents (defaults: 0)
 *   - trigger: The event that triggers a change in element display (click or hover). (defaults: click)
 *   - orientation: Accordion orientation [horizintal, vertical] (defaults: vertical)
 *   - duration: Effect Duration in ms (defaults: 500)
 *   - transition: Effect Transition (jQuery easing, see: http://jqueryui.com/demos/effect/#easing)
 *   - display: Initial item to show, if saveState=true this will be overwritten
 *
 * @param   object  $module    Module reference object
 * @param   object  &$params   Module params reference object \Joomla\Registry\Registry
 * @param   array   &$attribs  Array of additional jdoc attributes
 *
 * @return void
 */

function ModChrome_Accessible_accordion($module, &$params, &$attribs)
{
	static $modules;
	static $moduleIdCount = array();

	$jyaml = JYAML::getDocument();

	if (!isset($moduleIdCount[$module->id]))
	{
		$moduleIdCount[$module->id] = 0;
	}
	++$moduleIdCount[$module->id];

	if (!isset($modules[$module->position]))
	{
		// Get active modules with consideration of param layout_countemptymodules
		$mods = $jyaml->getRegisteredModules($module->position);

		$modules[$module->position]['count'] = count($mods);
		reset($mods);
		$modules[$module->position]['first'] = current($mods);
		$modules[$module->position]['last'] = end($mods);
	}

	$attr = new \Joomla\Registry\Registry($attribs);

	$class = '';
	$containerClass = '';
	$orientation = $attr->get('orientation', 'vertical');

	if ($orientation != 'vertical')
	{
		$class .= ' jyaml-accordion-horizontal';
	}

	if (isset($modules[$module->position]))
	{
		if ($modules[$module->position]['first']->id == $module->id)
		{
			$containerClass .= ' accordion-container-first';
			$mCount = $modules[$module->position]['count'];
			$uniqueId = 'accordion-' . md5($module->position . '::' . $mCount);

			$options = array();
			$options[] = "syncHeights: " . (int) $attr->get('syncHeight', 0);
			$options[] = "saveState: " . (int) $attr->get('saveState', 0);
			$options[] = "currentInfoText: ' " . JText::_('JYAML_TABS_CURRENT_TEXT') . " '";
			$options[] = "currentInfoPosition: '" . ($jyaml->direction == 'rtl' ? 'after' : 'before') . "'";
			$options[] = "orientation: '" . $orientation . "'";
			$options[] = "duration: " . (int) $attr->get('duration', 500);
			$options[] = "transition: '" . $attr->get('transition', 'swing') . "'";
			$options[] = "display: " . (int) $attr->get('display', 0);
			$options[] = "opacity: " . (int) $attr->get('opacityEffect', 1);
			$options[] = "alwaysHide: " . (int) $attr->get('alwaysHide', 0);
			$options[] = "multipleOpen: " . (int) $attr->get('multipleOpen', 0);
			$options[] = "trigger: '" . $attr->get('trigger', 'click') . "'";

			JHtml::_('jyaml.accordion', '#' . $uniqueId, $options);
			$jyaml->addStylesheet($jyaml->getUrl('css', 'screen.accordion.css'));

			echo '<div class="jyaml-accordion' . $class . '" id="' . $uniqueId . '">' . "\n";
			echo '<div class="accordion_content ym-clearfix">' . "\n";
		}
		if ($modules[$module->position]['last']->id == $module->id)
		{
			$containerClass .= ' accordion-container-last';
		}

		// Create a consistent ID per modul (required for save state)
		$accordionIdUnique = 'accordion-' . md5($module->id . '-' . count($moduleIdCount[$module->id]));

		$headlineTag = $attr->get('headerLevel', 'h3');
		?>
		<div class="accordion-container<?php echo $containerClass; ?> ym-clearfix">
			<?php echo '<' . $headlineTag . ' class="module-heading accordionhead" id="' . $accordionIdUnique . '">'; ?>
				<span><?php echo $module->title; ?></span>
			<?php echo '</' . $headlineTag . '>'; ?>
			<div class="accordionbody">
				<div class="accordionbody-content ym-clearfix">
					<?php
							$module->showtitle = 0;
							$subStyle = 'modChrome_' . (isset($attribs['subStyle']) ? $attribs['subStyle'] : 'none');
							JYAMLModuleHelper::renderSubstyle($subStyle, $module, $params, $attribs);
					?>
				</div>
			</div>
		</div>
		<?php

		if ($modules[$module->position]['last']->id == $module->id)
		{
			echo '</div>' . "\n";
			echo '</div>' . "\n";
		}
	}
}
