<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  JYAML.Template
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

// No direct access.
defined('_JEXEC') or die;

/**
 * Chrome Style: This style does output a html markup for Accessible Content Slider
 * Available and Used attributes: *
 *   - duration: Effect Duration in ms (defaults: 1000)
 *   - transition: Effect Transition (jQuery easing, see: http://jqueryui.com/demos/effect/#easing)
 *   - opacityEffect: Opacity effect on show/hide (defaults: 0)
 *   - nav_prevnext: Prev/Next Buttons (defaults: 1)
 *   - nav_prevnext_loop: Allow/Disallow Prev/Next Loop (defaults: 0)
 *   - pagination: Pagination Buttons (defaults: 1)
 *   - auto_slideshow: Auto Slideshow (defaults: 1)
 *   - auto_slideshow_interval: Interval in msfor Auto Slideshow (defaults: 5000)
 *   - auto_slideshow_buttons: Play/Stop Buttons for Auto Slideshow (defaults: 1)
 *   - auto_slideshow_progressbar: Graphical progressbar for Auto Slideshow (defaults: 1)
 *   - syncheights: Equalize heigts for each module (defaults: 1)
 *   - subStyle: modChrome Style for each Modules (defaults: jyaml) - (additional attributes definable)
 *
 * @param   object  $module    Module reference object
 * @param   object  &$params   Module params reference object \Joomla\Registry\Registry
 * @param   array   &$attribs  Array of additional jdoc attributes
 *
 * @return void
 */
function ModChrome_Accessible_slider($module, &$params, &$attribs)
{
	static $modules;
	static $moduleIdCount = array();
	static $curCol = 1;
	static $curRow = 1;
	static $itemCount = 0;
	static $slideItemRowCount = 0;

	$jyaml = JYAML::getDocument();

	if (!isset($moduleIdCount[$module->id]))
	{
		$moduleIdCount[$module->id] = 0;
	}
	++$moduleIdCount[$module->id];

	if (!isset($modules[$module->position]))
	{
		// Get active modules with consideration of param layout_countemptymodules
		$mods = $jyaml->getRegisteredModules($module->position);

		$modules[$module->position]['count'] = count($mods);
		reset($mods);
		$modules[$module->position]['first'] = current($mods);
		$modules[$module->position]['last'] = end($mods);
	}

	// Detect defined column width's from screen.basemod.css (template) and base.css (yaml core)
	$definedColWidths = (array) $jyaml->getDefinedColumnWidtsFromCss();

	$attr = new \Joomla\Registry\Registry($attribs);

	$visibleColumns = (int) $attr->get('cols', 1);
	$visibleRows = (int) $attr->get('rows', 1);

	if (!$visibleColumns)
	{
		$visibleColumns = 1;
	}
	if (!$visibleRows)
	{
		$visibleRows = 1;
	}

	$width = false;
	if ($visibleColumns > 1)
	{
		$width = (100 / $visibleColumns);
		$width = round($width, 2, PHP_ROUND_HALF_DOWN);
	}

	if (isset($modules[$module->position]))
	{
		if ($modules[$module->position]['first']->id == $module->id)
		{
			$class = '';
			$iClass = '';

			$mCount = $modules[$module->position]['count'];
			$uniqueId = 'slider-' . md5($module->position . '::' . $mCount);

			$options = array();
			$options[] = "duration: " . (int) $attr->get('duration', 1000);
			$options[] = "transition: '" . $attr->get('transition', 'swing') . "'";
			$options[] = "opacity: " . (int) $attr->get('opacityEffect', 0);
			$options[] = "navPrevNext: " . (int) $attr->get('nav_prevnext', 1);
			$options[] = "navPrevNextLoop: " . (int) $attr->get('nav_prevnext_loop', 0);
			$options[] = "pagination: " . (int) $attr->get('pagination', 1);
			$options[] = "autoSlideshow: " . (int) $attr->get('auto_slideshow', 1);
			$options[] = "autoSlideshowInterval: " . (int) $attr->get('auto_slideshow_interval', 5000);
			$options[] = "autoSlideshowControlButtons: " . (int) $attr->get('auto_slideshow_buttons', 1);
			$options[] = "autoSlideshowProgressbar: " . (int) $attr->get('auto_slideshow_progressbar', 1);
			$options[] = "syncHeights: " . (int) $attr->get('syncheights', 0);

			JHtml::_('jyaml.slider', '#' . $uniqueId, $options);
			$jyaml->addStylesheet($jyaml->getUrl('css', 'screen.slider.css'));

			if ($attr->get('nav_prevnext', 1))
			{
				$class .= ' hasNavPrevNext';
			}

			echo '<div class="jyaml-slider' . $class . '" id="' . $uniqueId . '">' . "\n";
			echo '<div class="slider_inner' . $iClass . '">' . "\n";
			echo '<div class="slider_content ym-clearfix">' . "\n";
		}

		$rowClasses = array();
		$rowClasses[] = 'ym-grid ym-equalize slider-row';

		$colClasses = array();
		$colClasses[] = 'slider-col';

		$styles = array();
		if ($width)
		{
			if (in_array($width, $definedColWidths))
			{
				$colClasses[] = 'ym-g' . $width;
			}
			else
			{
				$styles[] = 'width: ' . $width . '%';
			}

			if ($curCol == $visibleColumns || ($modules[$module->position]['last']->id == $module->id && $curCol > 1))
			{
				if ($curCol == $visibleColumns)
				{
					$colClasses[] = 'ym-gr';
				}
				else
				{
					$colClasses[] = 'ym-gl';
				}
				$colClasses[] = 'slider-col-last';
			}
			else
			{
				$colClasses[] = 'ym-gl';

				if ($curCol == 1 || $modules[$module->position]['last']->id == $module->id)
				{
					$colClasses[] = 'slider-col-first';
				}
			}
		}
		else
		{
			$colClasses[] = 'slider-col-once';
		}

		if ($visibleRows > 1)
		{
			if ($curRow == 1)
			{
				$rowClasses[] = 'slider-row-first';
			}
			elseif ($curRow == $visibleRows || $modules[$module->position]['last']->id == $module->id)
			{
				$rowClasses[] = 'slider-row-last';
			}
		}
		else
		{
			$rowClasses[] = 'slider-row-once';
		}

		?>
		<?php if ($curRow == 1 && $curCol == 1) : ?>
			<?php
				// Create a consistent ID per modul (required for save state)
				$sliderIdUnique = 'slider-' . md5($module->id . '-' . count($moduleIdCount[$module->id]));
			?>
			<div class="slider-item slider-item<?php echo $itemCount; ?>" id="<?php echo $sliderIdUnique; ?>">
			<div class="slider-item-content ym-clearfix">
		<?php endif; ?>
			<?php if ($curCol == 1) : ?>
				<?php $slideItemRowCount = 0; ?>
				<div class="<?php echo implode(' ', $rowClasses); ?> ym-clearfix">
			<?php endif; ?>
					<div class="<?php echo implode(' ', $colClasses); ?>"<?php echo $styles ? ' style="' . implode('; ', $styles) . '"' : ''; ?>>
						<div class="slider-col-content syncheight ym-clearfix">
							<?php
								$subStyle = 'modChrome_' . (isset($attribs['subStyle']) ? $attribs['subStyle'] : 'jyaml');
								JYAMLModuleHelper::renderSubstyle($subStyle, $module, $params, $attribs);
							?>
						</div>
					</div>
					<?php ++ $slideItemRowCount; ?>
			<?php if ($curCol == $visibleColumns || $modules[$module->position]['last']->id == $module->id) : ?>
				<?php $_dummys = $visibleColumns - $slideItemRowCount; ?>
				<?php for ($_i = 0; $_i < $_dummys; $_i++): /* fill empty row columns for robust layout */ ?>
					<?php $_emptyClass = str_replace(array('-first', '-last'), '-empty', implode(' ', $colClasses)); ?>
					<div class="<?php echo $_emptyClass; ?>"<?php echo $styles ? ' style="' . implode('; ', $styles) . '"' : ''; ?>>
						<div class="slider-col-content syncheight ym-clearfix"> &nbsp; </div>
					</div>
				<?php endfor; ?>
				</div>
			<?php endif; ?>
		<?php if (($curRow == $visibleRows && $curCol == $visibleColumns) || $modules[$module->position]['last']->id == $module->id) : ?>
			</div>
			</div>
			<?php ++$itemCount; ?>
		<?php endif; ?>
		<?php

		if ($modules[$module->position]['last']->id == $module->id)
		{
			echo '</div>' . "\n";
			echo '</div>' . "\n";
			echo '</div>' . "\n";
		}

		++$curCol;

		if ($curCol > $visibleColumns)
		{
			$curCol = 1;
			++$curRow;
		}

		if ($curRow > $visibleRows)
		{
			$curRow = 1;
		}
	}
}
