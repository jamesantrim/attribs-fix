<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  JYAML.Template
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

// No direct access.
defined('_JEXEC') or die;

/**
 * Chrome Style: This style does output a html markup for Accessible tabs
 * Available and Used attributes:
 *   - headlineTag: Headline Tag (defaults: h3)
 *   - showPagination: Show pagination buttons (defaults: true)
 *   - saveState: Save current open item as cookie (defaults: false)
 *   - position: Detetermines the position of the tab navigation [top, bottom] (defaults: top)
 *   - syncheights: Sync heights of tab contents (defaults: false)
 *   - orientation: Tablist orientation [horizintal, vertical] (defaults: horizontal)
 *   - effects: Apply Effects ('show', 'fadeIn', 'slideDown') (defaults: show)
 *
 * @param   object  $module    Module reference object
 * @param   object  &$params   Module params reference object \Joomla\Registry\Registry
 * @param   array   &$attribs  Array of additional jdoc attributes
 *
 * @return void
 */
function ModChrome_Accessible_tabs($module, &$params, &$attribs)
{
	static $modules;
	static $moduleIdCount = array();

	$jyaml = JYAML::getDocument();

	if (!isset($moduleIdCount[$module->id]))
	{
		$moduleIdCount[$module->id] = 0;
	}
	++$moduleIdCount[$module->id];

	if (!isset($modules[$module->position]))
	{
		// Get active modules with consideration of param layout_countemptymodules
		$mods = $jyaml->getRegisteredModules($module->position);

		$modules[$module->position]['columnCount'] = 0;
		$modules[$module->position]['count'] = count($mods);
		reset($mods);
		$modules[$module->position]['first'] = current($mods);
		$modules[$module->position]['last'] = end($mods);
	}
	++$modules[$module->position]['columnCount'];

	$attr = new \Joomla\Registry\Registry($attribs);
	$headlineTag = $attr->get('headerLevel', 'h3');
	$orientation = $attr->get('orientation', 'horizontal');
	$position = $attr->get('position', 'top');

	$class = '';

	if (isset($modules[$module->position]) && $modules[$module->position]['count'] > 0)
	{
		if ($modules[$module->position]['first']->id == $module->id)
		{
			$mCount = $modules[$module->position]['count'];

			// Create a consistent ID per modul (required for save state)
			$uniqueId = 'tabs-' . md5($module->id . '-' . count($moduleIdCount[$module->id]));

			$options = array();
			$options[] = "tabhead: '.module-heading'";
			$options[] = "tabbody: '.tabbody'";
			$options[] = "syncheights: " . (int) $attr->get('syncheights', 0);
			$options[] = "autoAnchor: true";
			$options[] = "currentInfoText: ' " . JText::_('JYAML_TABS_CURRENT_TEXT') . " '";
			$options[] = "currentInfoPosition: '" . ($jyaml->direction == 'rtl' ? 'append' : 'prepend') . "'";
			$options[] = "pagination: " . (int) $attr->get('showPagination', 0);
			$options[] = "saveState: " . (int) $attr->get('saveState', 0);
			$options[] = "fx: '" . $attr->get('effects', 'show') . "'";
			$options[] = "fxspeed: " . (int) $attr->get('effect_duration', 500);
			$options[] = "position: '" . $position . "'";
			$options[] = "wrapInnerNavLinks: '<span>'";

			JHtml::_('jyaml.tabs', '#' . $uniqueId, $options);
			$jyaml->addStylesheet($jyaml->getUrl('css', 'screen.tabs.css'));

			if ($orientation == 'vertical')
			{
				$class .= ' jyaml-tabs-vertical';
			}

			if ($position == 'bottom')
			{
				$class .= ' jyaml-tabs-list-bottom';
			}

			echo '<div id="' . $uniqueId . '" class="jyaml-tabs' . $class . ' ym-clearfix">' . "\n";
		}

		// Create a consistent ID per modul (required for save state)
		$tagIdUnique = 'tab-' . md5($module->id . '-' . count($moduleIdCount[$module->id]));

		?>
		<?php echo '<' . $headlineTag . ' class="module-heading" id="' . $tagIdUnique . '">'; ?>
			<?php echo $module->title; ?>
		<?php echo '</' . $headlineTag . '>'; ?>
		<div class="tabbody">
			<div class="tabbody-content ym-clearfix">
				<?php
					$module->showtitle = 0;
					$subStyle = 'modChrome_' . (isset($attribs['subStyle']) ? $attribs['subStyle'] : 'none');
					JYAMLModuleHelper::renderSubstyle($subStyle, $module, $params, $attribs);
				?>
			</div>
		</div>
		<?php

		if ($modules[$module->position]['last']->id == $module->id)
		{
			echo '</div>' . "\n";
		}
	}
}
