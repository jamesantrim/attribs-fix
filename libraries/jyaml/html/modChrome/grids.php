<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  JYAML.Template
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

// No direct access.
defined('_JEXEC') or die;

/**
 * Chrome Style: This style does output YAML Grids
 * Available and Used attributes:
 *   - headlineTagl: Headline Tag (defaults: h3)
 *   - subStyle: Chrome Style for each column (defaults jyaml)
 *   - equalize: Equal height columns
 *
 * @param   object  $module    Module reference object
 * @param   object  &$params   Module params reference object \Joomla\Registry\Registry
 * @param   array   &$attribs  Array of additional jdoc attributes
 *
 * @return void
 */
function ModChrome_grids($module, &$params, &$attribs)
{
	static $modules;

	if (!isset($modules[$module->position]))
	{
		$jyaml = JYAML::getDocument();

		// Get active modules with consideration of param layout_countemptymodules
		$mods = $jyaml->getRegisteredModules($module->position);

		$modules[$module->position]['columnCount'] = 0;
		$modules[$module->position]['count'] = count($mods);
		reset($mods);
		$modules[$module->position]['first'] = current($mods);
		$modules[$module->position]['last'] = end($mods);
	}
	++$modules[$module->position]['columnCount'];

	$attr = new \Joomla\Registry\Registry($attribs);
	$equalize = $attr->get('equalize', 0);
	$subStyle = $attr->get('subStyle', 'jyaml');
	$subStyleCall = 'modChrome_' . $subStyle;

	if (isset($modules[$module->position]) && $modules[$module->position]['count'] > 1)
	{
		if ($modules[$module->position]['first']->id == $module->id)
		{
			$class = ' cols-' . $modules[$module->position]['count'];
			if ($equalize)
			{
				$class .= ' ym-equalize';
			}

			echo '<div class="ym-grid' . $class . '">' . "\n";
		}

		ModChrome__Grids_col($module, $params, $attribs, $modules[$module->position]);

		if ($modules[$module->position]['last']->id == $module->id)
		{
			echo '</div>' . "\n";
		}
	}

	// Fallback if only one module in position (then no subcolumns required)
	else
	{
		// 1col
		JYAMLModuleHelper::renderSubstyle($subStyleCall, $module, $params, $attribs);
	}
}

/**
 * Helper function to parse subtemplate columns
 * Do not use this _grids_col as style. It wont work.
 *
 * @param   object  $module    Module reference object
 * @param   object  &$params   Module params reference object \Joomla\Registry\Registry
 * @param   array   &$attribs  Array of additional jdoc attributes
 * @param   array   $modules   list of all active modules
 *
 * @return void
 */
function ModChrome__Grids_col($module, &$params, &$attribs, $modules)
{
	$jyaml = JYAML::getDocument();

	$attr = new \Joomla\Registry\Registry($attribs);
	$subStyle = $attr->get('subStyle', 'jyaml');
	$subStyleCall = 'modChrome_' . $subStyle;

	// Detect defined column width's from screen.basemod.css (template) and base.css (yaml core)
	$definedColWidths = (array) $jyaml->getDefinedColumnWidtsFromCss();

	$subcClass = 'ym-gbox';
	$cAlign = 'ym-gl';

	if ($modules['first']->id == $module->id)
	{
		$subcClass = 'ym-gbox ym-gbox-left';
	}
	elseif ($modules['last']->id == $module->id)
	{
		$subcClass = 'ym-gbox ym-gbox-right';
		$cAlign = 'ym-gr';
	}

	// Auto width
	$width = (100 / $modules['count']);

	$mClass = $params->get('moduleclass_sfx', '');
	$mClasses = explode(' ', $mClass);

	foreach ($mClasses as $i => $class)
	{
		$class = trim($class);

		if (strpos($class, '%') !== false)
		{
			$width = str_replace('%', '', $class);
			unset($mClasses[$i]);
		}
		elseif (strpos($class, 'px') !== false)
		{
			$width = str_replace('px', '', $class);
			unset($mClasses[$i]);
		}
		elseif (strpos($class, 'em') !== false)
		{
			$width = str_replace('em', '', $class);
			unset($mClasses[$i]);
		}
	}

	$mClass = implode(' ', $mClasses);

	// Set without n% data for module inner rendering
	$params->set('moduleclass_sfx', $mClass);

	$width  = floor($width);
	$cClass = 'ym-g' . $width . ' ' . $cAlign . ' item ' . 'column-' . $modules['columnCount'];

	$colWidthStyle = '';
	if (!in_array($width, $definedColWidths))
	{
		$_styleWidth = (100 / $modules['count']);
		if (($_dotPos = strpos($_styleWidth, '.')) !== false)
		{
			$_styleWidth = substr($_styleWidth, 0, $_dotPos + 1) . substr($_styleWidth, $_dotPos + 1, 3);
		}

		// Set width inline style if the width class (.ym-g[n]) is not defined in screen.basemod.css
		$colWidthStyle = ' style="width:' . $_styleWidth . '%"';
	}

	echo '  <div class="' . $cClass . '"' . $colWidthStyle . '>' . "\n";
	echo '    <div class="' . $subcClass . '">' . "\n";
	echo '      ' . JYAMLModuleHelper::renderSubstyle($subStyleCall, $module, $params, $attribs) . "\n";
	echo '    </div>' . "\n";
	echo '  </div>' . "\n";
}
