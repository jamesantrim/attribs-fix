<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  JYAML.Template
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

/*
 * TODO: description how it works now
 */

// No direct access.
defined('_JEXEC') or die;

/**
 * Chrome Style: This style is similar xhtml. But additional you can define the headerLevel
 * Available and Used attributes:
 *   - headlineTag: Headline Tag (defaults: h3)
 *
 * @param   object  $module    Module reference object
 * @param   object  &$params   Module params reference object \Joomla\Registry\Registry
 * @param   array   &$attribs  Array of additional jdoc attributes
 *
 * @return void
 */
function ModChrome_jyaml($module, &$params, &$attribs)
{
	$attr = new \Joomla\Registry\Registry($attribs);
	$headlineTag = $attr->get('headlineTag', 'h3');
	$sfx = htmlspecialchars($params->get('moduleclass_sfx', ''));
	$classSfx = $sfx ? ' modulebox' . $sfx : '';

	$headerClasss = trim($params->get('header_class', ''));
	$headerClasss = $headerClasss ? ' ' . $headerClasss : '';
	$moduleTag = $params->get('module_tag', 'div');

	if (!empty($module->content))
	{
		echo '<' . $moduleTag . ' class="module-box' . $classSfx . '">';

		if ($module->showtitle != 0)
		{
			echo '<' . $headlineTag . ' class="module-heading' . $headerClasss . '">'; ?><?php
			echo $module->title;
			echo '</' . $headlineTag . '>';
		}

		echo $module->content;

		echo '</' . $moduleTag . '>';
	}
}
