<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  JYAML.Template
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

// No direct access.
defined('_JEXEC') or die;

/**
 * Chrome Style: This style does output only Links
 * Available and Used attributes:
 *   - spacer: Spacer for links (defaults blank/space)
 *   - headlineTag: Headline Tag (defaults: h3)
 *
 * @param   object  $module    Module reference object
 * @param   object  &$params   Module params reference object \Joomla\Registry\Registry
 * @param   array   &$attribs  Array of additional jdoc attributes
 *
 * @return void
 */
function ModChrome_rawlinks($module, &$params, &$attribs)
{
	$attr = new \Joomla\Registry\Registry($attribs);

	$spacer = $attr->get('spacer', '|');
	$headlineTag = $attr->get('headlineTag', 'h3');
	$sfx = htmlspecialchars($params->get('moduleclass_sfx', ''));
	$classSfx = $sfx ? ' rawlinks' . $sfx : '';

	$headerClasss = trim($params->get('header_class', ''));
	$headerClasss = $headerClasss ? ' ' . $headerClasss : '';
	$moduleTag = $params->get('module_tag', 'div');

	if (!empty($spacer))
	{
		$spacer = '<span class="rawlink-spacer">' . $spacer . '</span>';
	}

	$contents = array();

	preg_match_all('#<a.*>.*</a>#Uis', $module->content, $matches);

	if ($matches && isset($matches[0]) && count($matches[0]))
	{
		$contents = $matches[0];
	}

	if (!empty($contents))
	{
		echo '<' . $moduleTag . ' class="raw-links' . $classSfx . '">';

		if ($module->showtitle != 0)
		{
			echo '<' . $headlineTag . ' class="module-heading">';
			echo $module->title;
			echo '</' . $headlineTag . '> ';
		}

		echo implode(' ' . $spacer . ' ', $contents);

		echo '</' . $moduleTag . '>';
	}
}
