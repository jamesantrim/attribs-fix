<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  Admin
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 *
 */

defined('_JEXEC') or die;

/**
 * JYAMLAdmin class
 * Provides methods for admin actions
 *
 * @package     JYAML
 * @subpackage  Admin
 * @since       4.0.0
 */
class JYAMLAdmin extends JObject
{
	public $log = array();

	/**
	 * Get the JYAML library version
	 *
	 * @param   boolean  $includeDate  Include date
	 * @param   boolean  $asHash       Return as md5 hash value
	 *
	 * @static
	 * @access public
	 *
	 * @return string Version number
	 */
	public static function getVersion($includeDate = false, $asHash = false)
	{
		return JYAML::getVersion($includeDate, $asHash);
	}

	/**
	 * Load some scripts and stylesheets required for admin page
	 *
	 * @deprecated Dont use this in future versions
	 * @static
	 * @access public
	 * @return void
	 */
	public static function loadFormAssets()
	{
		/*
		 * Removed assets from here.
		 * As fallback only load MooTools for form fields that require loadFormAssets to avoid JavaScript errors.
		 */
		JHtml::_('behavior.framework');
	}

	/**
	 * Method to get a Reference object of this class (instance)
	 *
	 * @access public
	 *
	 * @return JYAMLAdmin
	 */
	public static function getInstance()
	{
		static $instance;

		if ($instance == null)
		{
			self::loadLanguage();
			$instance = new JYAMLAdmin;
		}

		return $instance;
	}

	/**
	 * Execute an JYAML admin controller
	 *
	 * @param   string  $controller  Controller name
	 *
	 * @return void
	 */
	public static function executeController($controller)
	{
		$path = JYAML_LIB_PATH . '/controller/admin/' . $controller . '.php';
		$class = 'JYAMLControllerAdmin' . ucfirst(strtolower($controller));

		if (JFile::exists($path) && !class_exists($class))
		{
			require_once $path;
		}

		if (class_exists($class))
		{
			$instance = new $class;
			$instance->execute();
		}
	}

	/**
	 * Run Admin Hanlder, chack forgereies and execute methods
	 *
	 * @static
	 * @access public
	 * @return void
	 */
	public static function run()
	{
		$admin = self::getInstance();
		$post = $_POST;

		// Allow only post
		if (self::input()->getMethod() != 'POST' || empty($post))
		{
			return;
		}

		// Set basic properties in object
		$admin->set('option', self::input()->get('option', ''));
		$admin->set('task', self::input()->get('task', ''));
		$admin->set('post', $post);

		// Save template params
		if ($admin->get('option') == 'com_templates' && ($admin->get('task') == 'style.apply' || $admin->get('task') == 'style.save'))
		{
			$admin->_prepareTemplateParamsOnSave();
			$admin->addLog('Prepared template params for save');
		}
	}

	/**
	 * Get JInput
	 *
	 * @static
	 * @return JFactory::getApplication()->input
	 */
	protected static function input()
	{
		return JFactory::getApplication()->input;
	}

	/**
	 * Method to prepare jform post data
	 *
	 * @access private
	 * @return void
	 */
	private function _prepareTemplateParamsOnSave()
	{
		$inputFilter = JFilterInput::getInstance();

		// Save all positions in templateDetails.xml
		$positions = array();
		$post = $this->get('post');

		$form = isset($post['jform']) ? $post['jform'] : array();

		$template = isset($form['template']) ? $inputFilter->clean($form['template'], 'cmd') : '';
		$styleId  = self::input()->get('id', 0, 'int');
		$clientId = isset($form['client_id']) ? $inputFilter->clean($form['client_id'], 'int') : null;

		$isJYAML = false;
		if (isset($form['params']) && isset($form['params']['_is_jyaml']) && $form['params']['_is_jyaml'])
		{
			$isJYAML = true;
		}

		if ($template != "" && $clientId !== null && $isJYAML)
		{
			// Check for request forgeries
			JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

			// Get positions from current submit
			if (!empty($form) && is_array($form))
			{
				if (isset($form['params']) && is_array($form['params']))
				{
					if (isset($form['params']['positions']) && is_array($form['params']['positions']))
					{
						foreach ($form['params']['positions'] as $_namespace => $_positions)
						{
							if (!empty($_positions) && is_array($_positions))
							{
								foreach ($_positions as $_position)
								{
									$type = isset($_position['type']) ? $inputFilter->clean($_position['type'], 'string') : '';
									$name = isset($_position['name']) ? $inputFilter->clean($_position['name'], 'string') : '';

									if ($type == 'modules' && $name != '' && !in_array($name, $positions))
									{
										$positions[] = $name;
									}
								}
							}
						}
					}
				}
			}

			// Get positions from all other styles assigned to this template
			$db = JFactory::getDbo();

			$query = $db->getQuery(true);
			$query->select($db->qn('params'));
			$query->from($db->qn('#__template_styles'));

			// Ignore the current style
			$query->where($db->qn('id') . ' != ' . $db->q((int) $styleId));
			$query->where($db->qn('client_id') . ' = ' . $db->q((int) $clientId));
			$query->where($db->qn('template') . ' = ' . $db->q((string) $template));

			$db->setQuery($query);

			$styleParams = array();

			if ($_styleParamsResult = $db->loadColumn())
			{
				$styleParams = (array) $_styleParamsResult;
			}

			foreach ($styleParams as $json)
			{
				$temp = new \Joomla\Registry\Registry;
				$temp->loadString($json, 'JSON');
				$styleParam = $temp;

				$_allPositions = $styleParam->get('positions');
				if (is_object($_allPositions))
				{
					$_allPositions = \Joomla\Utilities\ArrayHelper::fromObject($_allPositions);
				}
				$_allPositions = (array) $_allPositions;

				foreach ($_allPositions as $_namespace => $_positions)
				{
					if (!empty($_positions) && is_array($_positions))
					{
						foreach ($_positions as $_position)
						{
							$type = isset($_position['type']) ? $inputFilter->clean($_position['type'], 'string') : '';
							$name = isset($_position['name']) ? $inputFilter->clean($_position['name'], 'string') : '';

							if ($type == 'modules' && $name != '' && !in_array($name, $positions))
							{
								$positions[] = $name;
							}
						}
					}
				}
			}

			$client = JApplicationHelper::getClientInfo($clientId);
			$clientPath = $client->path;
			$templatePath = $clientPath . '/templates/' . $template;

			// Get all positions of type=modules from index files
			$indexFiles = JFolder::files($templatePath . '/html/index', '.php$', false, true);
			if ($indexFiles)
			{
				foreach ($indexFiles as $_indexFile)
				{
					$_contents = file_get_contents($_indexFile);

					if (preg_match_all('#<jdoc:include\ type="modules" (.*)\/>#iU', $_contents, $matches))
					{
						$count = count($matches[1]);

						for ($i = 0; $i < $count; $i++)
						{
							$_attribs = JUtility::parseAttributes($matches[1][$i]);
							$_name    = isset($_attribs['name']) ? trim($_attribs['name']) : null;

							if ($_name)
							{
								$positions[] = $_name;
							}
						}
					}
				}
			}

			$positions = array_unique($positions);

			// Save positions in templateDetails.xml
			if (!empty($positions))
			{
				$xmlFile = JPath::clean($templatePath . '/templateDetails.xml');

				if (file_exists($xmlFile))
				{
					$xmlContent = file_get_contents($xmlFile);

					$xmlPositions = "\n";
					foreach ($positions as $_position)
					{
						$xmlPositions .= "\t\t" . '<position>' . $_position . '</position>' . "\n";
					}

					$xmlContent = preg_replace(
						'#<positions>(.*)</positions>#Uis',
						'<positions>' . $xmlPositions . '  </positions>',
						$xmlContent,
						1
					);

					// Convert line endings to unix
					$xmlContent = str_replace(array("\r\n", "\r"), "\n", $xmlContent);

					@JFile::write($xmlFile, $xmlContent);
				}
			}
		}
	}

	/**
	 * Extend JForm data and fields
	 *
	 * @param   object  $form  JForm Object
	 * @param   mixed   $data  JForm Data
	 *
	 * @access public
	 * @static
	 * @return bool
	 */
	public static function onContentPrepareForm($form, $data)
	{
		if (!($form instanceof JForm))
		{
			return false;
		}

		$formName = $form->getName();

		if (empty($formName))
		{
			return false;
		}

		$admin = self::getInstance();
		$admin->addLog('Start: onContentPrepareForm');

		$requestMethod = self::input()->getMethod();
		$lang = JFactory::getLanguage();

		$templatePath = '';
		$templateName = '';
		$clientId = null;

		$files = array();
		$customFiles = array();
		$pluginFiles = array();

		// Support for NoNumber AdvancedModule
		$moduleComponent = 'com_modules';
		$tmp = explode('.', $formName);
		if (isset($tmp[0]) && $tmp[0] == 'com_advancedmodules')
		{
			$tmp[0] = 'com_modules';
			$formName = implode('.', $tmp);
			$moduleComponent = 'com_advancedmodules';
		}

		$admin->addLog('Using form name: ' . $formName);

		switch ($formName)
		{
			case 'com_templates.style':
				$JYAMLSupportEnabled = $form->getFieldAttribute('support', 'value', 'false', 'jyaml');

				if ($JYAMLSupportEnabled == 'true' && $formName)
				{
					$files[] = JYAML_LIB_PATH . '/config/' . $formName . '.xml';

					// Remove params from templateDetails.xml (remove dependencies check)
					$form->removeGroup('params');
					$form->removeGroup('jyaml');

					$templateName = '';
					$clientId = null;

					// Try to get the template name form form data
					if (!empty($data) && !empty($data->template))
					{
						$templateName = $data->template;
						$clientId = (!empty($data->client_id) ? $data->client_id : 0);
					}

					// Try to get the template name from database if not set in form data
					if (empty($templateName))
					{
						$option = self::input()->get('option', '', 'cmd');
						$styleId = self::input()->get('id', 0, 'int');

						if ($option == 'com_templates' && $styleId)
						{
							$db = JFactory::getDbo();
							$query = $db->getQuery(true);
							$query->select($db->qn('template') . ', ' . $db->qn('client_id'));
							$query->from($db->qn('#__template_styles'));
							$query->where($db->qn('id') . ' = ' . $db->q((int) $styleId));
							$db->setQuery($query);

							if (($_template = $db->loadObject()))
							{
								$templateName = $_template->template;
								$clientId = $_template->client_id;
							}
						}
					}

					// Custom template params definition file
					if ($templateName && $clientId !== null)
					{
						$client = JApplicationHelper::getClientInfo($clientId);

						if ($client && $client->path)
						{
							$clientPath = $client->path;
						}
						else
						{
							// Fallback
							$clientPath = JPATH_ROOT;
						}

						$templatePath = $clientPath . '/templates/' . $templateName;
						$customFiles[] = $templatePath . '/custom.xml';

						// Load the language template file
						$lang->load('core', $templatePath, null, false, false);

						// Load custom language file for end-user changes
						$lang->load('custom', $templatePath, null, false, false);

						// Find plugin jform xml overrides
						self::applyPluginToPrepareForm($formName, $pluginFiles);
					}

					if (!empty($data) && empty($data->params))
					{
						// Show message to click save once on first load
						JFactory::getApplication()->enqueueMessage(JText::_('JYAML__NOTICE_SAVE_TEMPLATE_FIRST_TIME'), 'notice');

						// Copy default images into Joomla images path
						$_sourceImagePath = JPATH_ROOT . '/libraries/jyaml/assets/images/defaults';
						$_destImagePath = JPATH_ROOT . '/images/jyaml';
						JFolder::copy($_sourceImagePath, $_destImagePath, '', true, true);

						// Load default param values
						if (!empty($data->xml) && !empty($data->xml->defaultparams))
						{
							$defaultparams = $data->xml->defaultparams;
							$defaultData = array();
							$childCount = count($defaultparams->children());

							if ($childCount == 1 && isset($defaultparams->json))
							{
								$_params = new \Joomla\Registry\Registry;
								$_params->loadString((string) $defaultparams->json, 'JSON');
								$defaultData = $_params->toArray();
							}
							elseif($childCount == 1 && isset($defaultparams->xml))
							{
								$_params = new \Joomla\Registry\Registry;
								$_params->loadString((string) $defaultparams->json, 'XML');
								$defaultData = $_params->toArray();
							}
							else
							{
								foreach ($defaultparams->children() as $key => $node)
								{
									$defaultData[$key] = array();
									$childrens = $node->children();

									if (!empty($childrens))
									{
										foreach ($childrens as $child)
										{
											if ($child->attributes())
											{
												$attributes = array();
												foreach ($child->attributes() as $k => $v)
												{
													$attributes[$k] = (string) $v;
												}
												$defaultData[$key][$child->getName()][] = $attributes;
											}
										}
									}
									else
									{
										$value = (string) $node;
										$value = trim($value);

										if ($value)
										{
											$defaultData[$node->getName()] = $value;
										}
									}
								}
							}

							if (!empty($defaultData))
							{
								if (isset($defaultData['_is_jyaml']))
								{
									unset($defaultData['_is_jyaml']);
								}

								$data->params = $defaultData;
							}
						}
					}
				}
				break;

			case 'com_plugins.plugin':
				$_plugin = '';

				if (isset($data->type) && $data->type == 'plugin' && !empty($data->element))
				{
					$_plugin = $data->element;
				}
				// Else try get plugin name from database by request
				elseif (empty($_plugin) && $requestMethod == 'POST' && self::input()->get('option', '', 'cmd') == 'com_plugins'
						&& ($_pluginId = self::input()->get('extension_id', 0, 'int')))
				{
					$_table = JTable::getInstance('extension');

					if ($_table->load($_pluginId) && !empty($_table->element))
					{
						$_plugin = $_table->element;
					}
				}

				if ($_plugin)
				{
					$files[] = JYAML_LIB_PATH . '/config/' . $formName . '.xml';
					$files[] = JYAML_LIB_PATH . '/config/' . $formName . '.' . $_plugin . '.xml';

					$customFiles[] = JYAML_LIB_PATH . '/config/custom/' . $formName . '.' . $_plugin . '.xml';

					// Find plugin jform xml overrides
					self::applyPluginToPrepareForm($formName, $pluginFiles);
					self::applyPluginToPrepareForm($formName . '.' . $_plugin, $pluginFiles);
				}
				break;

			case 'com_modules.module':
				$_module = '';

				// Try to get module name from data if loaded
				if (!empty($data->module))
				{
					$_module = $data->module;
				}
				// Else try get module name from database by request
				elseif (empty($_module) && $requestMethod == 'POST' && self::input()->get('option', '', 'cmd') == $moduleComponent
									&& ($_moduleId = self::input()->get('id', 0, 'int')))
				{
					$_table = JTable::getInstance('module');
					$_table->load($_moduleId);

					if ($_table->load($_moduleId) && !empty($_table->module))
					{
						$_module = $_table->module;
					}
				}

				if ($_module)
				{
					$files[] = JYAML_LIB_PATH . '/config/' . $formName . '.xml';
					$files[] = JYAML_LIB_PATH . '/config/' . $formName . '.' . $_module . '.xml';

					$customFiles[] = JYAML_LIB_PATH . '/config/custom/' . $formName . '.' . $_module . '.xml';

					// Find plugin jform xml overrides
					self::applyPluginToPrepareForm($formName, $pluginFiles);
					self::applyPluginToPrepareForm($formName . '.' . $_module, $pluginFiles);

				}
				break;

			default:
				$lang = JFactory::getLanguage();
				$lang->load('custom', JYAML_LIB_PATH, null, false, false);

				$files[] = JPath::clean(JYAML_LIB_PATH . '/config/' . $formName . '.xml');
				$customFiles[] = JPath::clean(JYAML_LIB_PATH . '/config/custom/' . $formName . '.xml');

				// Find plugin jform xml overrides
				self::applyPluginToPrepareForm($formName, $pluginFiles);
				break;
		}

		// Load xml files
		$allFiles = array('core' => $files, 'custom' => $customFiles, 'plugin' => $pluginFiles);

		foreach ($allFiles as $_key => $_files)
		{
			foreach ($_files as $_file)
			{
				if (file_exists($_file))
				{
					$form->loadFile($_file, true, '/config');
					$admin->addLog('Load JForm XML file (' . $_key . '): ' . $_file);
				}
				else
				{
					$admin->addLog('JForm XML file that is not in use (' . $_key . '): ' . $_file);
				}
			}
		}

		if ($formName == 'com_templates.style' && $templatePath)
		{
			$isJ32gt = version_compare(JVERSION, '3.2.0', '>=');

			$_positions = JYAMLHelper::getDynamicPositions($templateName, $clientId);

			$_data = '<?xml version="1.0" encoding="utf-8"?>' . "\n";
			$_data .= '<config>';
			$_data .= '<fields name="params">';
			$_data .= '<fieldset name="positions">';
			$_data .= '<fields name="positions">';
			if ($isJ32gt)
			{
				$_data .= '<fieldset name="positions">';
			}

			// Add positions are configured but not available in any html file
			if ($data && is_object($data))
			{
				$_params = $data->get('params');
				$positionsValues = (isset($_params['positions']) ? (array) $_params['positions'] : array());

				if (!empty($positionsValues))
				{
					foreach ($positionsValues as $_ns => $_pos)
					{
						if (!isset($_positions[$_ns]) && !empty($_pos))
						{
							$_positions[$_ns] = array();
						}
					}
				}
			}

			foreach ($_positions as $_position => $_usedFiles)
			{
				$_data .= '<field type="jyamltemplatepositions" name="' . $_position . '" label="' . $_position
					. '" used_files="' . implode(',', $_usedFiles) . '" />';

				$_data .= '<field type="spacer" class="jyaml-param-spacer" hr="true" />';
			}
			if ($isJ32gt)
			{
				$_data .= '</fieldset>';
			}
			$_data .= '</fields>';
			$_data .= '</fieldset>';
			$_data .= '</fields>';
			$_data .= '</config>';

			$form->removeField('positions', 'params');
			$form->load($_data, true, '/config');
		}

		return true;
	}

	/**
	 * Helper to apply JYAML plugin xml overrides of jform
	 *
	 * @param   string  $formName  The name of the form
	 * @param   array   &$arr      The Array to push the file
	 *
	 * @access protected
	 * @static
	 * @return true
	 */
	protected static function applyPluginToPrepareForm($formName, &$arr)
	{
		static $pluginsImported;

		// Find jyaml plugins for jform xml overrides
		$files = array();
		$folder = JPATH_ROOT . '/plugins/jyaml';

		if (JFolder::exists($folder))
		{
			// Force to init plugin group to (load language, etc.)
			if (!$pluginsImported)
			{
				JPluginHelper::importPlugin('jyaml');
				$pluginsImported = true;
			}

			$plugins = JPluginHelper::getPlugin('jyaml');

			foreach ($plugins as $_plugin)
			{
				if (JPluginHelper::isEnabled('jyaml', $_plugin->name))
				{
					$files = JFolder::files($folder . '/' . $_plugin->name, '^' . $formName . '.xml$', true, true);

					foreach ($files as $_file)
					{
						$arr[] = $_file;
					}
				}
			}
		}

		return true;
	}

	/**
	 * Function to load JYAML Libraray language files
	 *
	 * @since 4.5.0
	 *
	 * @static
	 * @return void
	 */
	public static function loadLanguage()
	{
		$lang = JFactory::getLanguage();
		$lang->load('lib_jyaml', JYAML_LIB_PATH) || $lang->load('lib_jyaml', JPATH_ADMINISTRATOR);
	}

	/**
	 * Add an log message.
	 *
	 * @param   string  $log  Log message.
	 *
	 * @access public
	 * @return void
	 */
	public function addLog($log)
	{
		$admin = self::getInstance();

		array_push($admin->log, $log);
	}

	/**
	 * Add an log message.
	 *
	 * @access public
	 * @return array Log Messages
	 */
	public function getLog()
	{
		return $this->log;
	}
}
