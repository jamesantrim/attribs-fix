<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  Mobile
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

defined('_JEXEC') or die;

/**
 * JYAMLMobile class
 * This is a PHP Class for simple mobile device detection
 *
 * Inspired from Mobile Detect: http://www.opensource.org/licenses/mit-license.php The MIT License
 *
 * @package     JYAML
 * @subpackage  Mobile
 * @since       4.0.0
 */
class JYAMLMobile extends JObject
{

	/**
	 * Detection State on initialize
	 *
	 * @var boolean
	 */
	protected $enabled = true;

	/**
	 * HTTP_ACCEPT
	 *
	 * @var string
	 */
	protected $accept;

	/**
	 * HTTP_USER_AGENT
	 *
	 * @var string
	 */
	protected $userAgent;

	/**
	 * Is Mobile state
	 *
	 * @var boolean
	 */
	protected $isMobile = false;

	/**
	 * The detected mobile device if known in $devices
	 *
	 * @var string
	 */
	protected $currentMobileDevice = null;

	/**
	 * List of available __call Methods "is[Device]"
	 *
	 * @var array
	 */
	protected $isMobileDevice = array(
		'android'       => null,
		'iphone'        => null,
		'ipad'          => null,
		'blackberry'    => null,
		'opera'         => null,
		'palm'          => null,
		'windows'       => null,
		'generic'       => null
	);

	/**
	 * List of tests (regex) for specific mobile devices
	 *
	 * @var array
	 */
	protected $devices = array(
		'android'       => "android",
		'blackberry'    => "blackberry",
		'iphone'        => "(iphone|ipod)",
		'ipad'          => "ipad",
		'opera'         => "opera mini",
		'palm'          => "(avantgo|blazer|elaine|hiptop|palm|plucker|xiino)",
		'windows'       => "windows ce; (iemobile|ppc|smartphone)",
		'generic'       => "(kindle|mobile|mmp|midp|o2|pda|pocket|psp|symbian|smartphone|treo|up.browser|up.link|vodafone|wap)",
	);

	/**
	 * Class constructor
	 *
	 * @param   boolean  $enabled  Set this to false to disable detection on initialize
	 *
	 * @access public
	 */
	public function __construct($enabled=true)
	{
		$this->enabled   = $enabled;
		$this->userAgent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
		$this->accept    = isset($_SERVER['HTTP_ACCEPT']) ? $_SERVER['HTTP_ACCEPT'] : '';

		$this->checkIsMobile();
	}

	/**
	 * Disable the detector (after call isMobile returns always false)
	 *
	 * @return void
	 */
	public function disable()
	{
		$this->enabled = false;
	}

	/**
	 * Check is Mobile
	 *
	 * @return void
	 */
	protected function checkIsMobile()
	{
		if (!$this->enabled)
		{
			$this->isMobile = false;
			return;
		}

		if (isset($_SERVER['HTTP_X_WAP_PROFILE']) || isset($_SERVER['HTTP_PROFILE']))
		{
			$this->isMobile = true;
		}
		elseif (strpos($this->accept, 'text/vnd.wap.wml') > 0 || strpos($this->accept, 'application/vnd.wap.xhtml+xml') > 0)
		{
			$this->isMobile = true;
		}
		else
		{
			foreach ($this->devices as $device => $regexp)
			{
				if ($this->isDevice($device))
				{
					$this->isMobile = true;
				}
			}
		}
	}

	/**
	 * Overloads isAndroid() | isIphone | isIpad | isBlackberry() | isOpera() |
	 *           isPalm() | isWindows() | isGeneric()
	 *           through isDevice()
	 *
	 * @param   string  $name       Method (see description)
	 * @param   array   $arguments  Arguments (not used in class, but available for class extends)
	 *
	 * @return boolean
	 */
	public function __call($name, $arguments)
	{
		$device = substr($name, 2);

		if ($name == ("is" . ucfirst($device)))
		{
			return $this->isDevice($device);
		}
		else
		{
			trigger_error("Method $name not defined", E_USER_ERROR);
		}
	}

	/**
	 * Returns true if any type of mobile device detected, including special ones
	 *
	 * @access public
	 * @return boolean
	 */
	public function isMobile()
	{
		$this->checkIsMobile();
		return $this->isMobile;
	}

	/**
	 * Returns the current mobile device name/type
	 *
	 * @access public
	 * @return string a key of {@var self::$devices}
	 */
	public function getCurrentMobileDevice()
	{
		return $this->currentMobileDevice;
	}

	/**
	 * Returns true if a specific mobile device detected
	 * Supported strings look at keys of {@var self::$devices}
	 *
	 * @param   string  $device  Name/Type of the device
	 *
	 * @access protected
	 * @return boolean
	 */
	protected function isDevice($device)
	{
		if (!$this->enabled)
		{
			return false;
		}

		$device = strtolower($device);

		if (!isset($this->devices[$device]))
		{
			trigger_error("Device $device not supported for specific detection", E_USER_ERROR);
			return false;
		}

		if (!isset($this->isMobileDevice[$device]))
		{
			$this->isMobileDevice[$device] = null;
		}
		if ($this->isMobileDevice[$device] !== null)
		{
			return $this->isMobileDevice[$device];
		}

		$val = $this->isMobileDevice[$device];
		$regex = $this->devices[$device];

		$this->isMobileDevice[$device] = ($val === null ? (bool) preg_match("/" . $regex . "/i", $this->userAgent) : $val);

		if ($this->isMobileDevice[$device] === true)
		{
			$this->currentMobileDevice = $device;
		}
		if ($device != 'generic' && $this->isMobileDevice[$device] === true)
		{
			$this->isMobileDevice['generic'] = false;
		}

		return $this->isMobileDevice[$device];
	}
}
