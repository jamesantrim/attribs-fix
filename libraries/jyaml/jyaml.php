<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  Library
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 *
 */

defined('_JEXEC') or die;

// Define the JYAML library path for global usage
define('JYAML_LIB_PATH', dirname(__FILE__));

// Load dependencies
jimport('joomla.utilities.utility');
jimport('joomla.document.document');
jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');
jimport('joomla.application.categories');
jimport('jyaml.helper');
jimport('jyaml.loader');
jimport('jyaml.application.module.helper');

// Register class names in JLoader
JLoader::register('JsCssChunker', JYAML_LIB_PATH . '/vendor/JsCssChunker/src/chunker.php');
JLoader::register('phpImage', JYAML_LIB_PATH . '/vendor/phpImage/src/image.php');
JLoader::register('JYAMLMobile', JYAML_LIB_PATH . '/jyaml.mobile.php');

/**
 * JYAML class
 * Provides an extended interface to parse and display an html document
 *
 * @package     JYAML
 * @subpackage  Library
 * @since       4.0.0
 */
class JYAML extends JObject
{
	/**
	 * State of Object can initialized to prevent loading before onAfterRoute
	 *
	 * @static
	 * @var    boolean
	 * @since  4.5.0
	 */
	static public $readyToStart = false;

	/**
	 * Name of the template
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	public $template = '';

	/**
	 * Template options
	 *
	 * @var \Joomla\Registry\Registry
	 */
	public $params = null;

	/**
	 * Set object Is JYAML
	 *
	 * @var    boolean
	 * @since  4.0.0
	 */
	protected $isJYAML = true;

	/**
	 * Name of the active Layout
	 *
	 * @var    string
	 * @since  4.0.0
	 */
	protected $active_layout = '';

	/**
	 * Skiplinks
	 *
	 * @var    array
	 * @since  4.0.0
	 */
	protected $skiplink = array();

	/**
	 * Names of active extensions
	 *
	 * @var    array
	 * @since  4.0.0
	 */
	protected $active_extensions = array();

	/**
	 * Body CSS Classes
	 *
	 * @var    array
	 * @since  4.0.0
	 */
	protected $body_css_class = array();

	/**
	 * Counting values of specific module positions
	 *
	 * @var    array
	 * @since  4.0.0
	 */
	protected $positions_count = array();

	/**
	 * Current menu item id (Itemid)
	 *
	 * @var    integer
	 * @since  4.0.0
	 */
	protected $currentItemID = null;

	/**
	 * Parent menu Itemid's of $currentItemID, if available
	 *
	 * @var    array
	 * @since  4.0.0
	 */
	protected $parentItemIDs = array();

	/**
	 * Relative and absolute path values for important folders
	 *
	 * @var    array
	 * @since  4.0.0
	 */
	protected $paths = array();

	/**
	 * Names of Modules with empty contents
	 *
	 * @var    array
	 * @since  4.0.0
	 */
	protected $emptyModules = array();

	/**
	 * All enabled module objects by position loaded on current page
	 *
	 * @var    array
	 * @since  4.0.0
	 */
	protected $registeredModules = array();

	/**
	 * Message log entries
	 *
	 * @var    array
	 * @since  4.0.0
	 */
	protected $log = array();

	/**
	 * Error log entries
	 *
	 * @var    array
	 * @since  4.5.0
	 */
	protected $errorLog = array();

	/**
	 * Method call queue
	 *
	 * @var    array
	 * @since  4.0.0
	 */
	protected $methodCallQueue = array();

	/**
	 * Template Tags (jdoc parse list)
	 *
	 * @var    array
	 * @since  4.0.0
	 */
	protected $templateTags = array();

	/**
	 * Get a document object
	 *
	 * This is a wrapper for JDocumentHtml with additional methods
	 * and parameters. only creating it if it does not already exist.
	 * Does return false if called to early (before onAfterRoute)
	 *
	 * @access public
	 * @static
	 * @staticvar $instance Reference of JYAML class
	 *
	 * @return JYAML
	 */
	public static function getDocument()
	{
		static $instance;

		if ( ! self::$readyToStart)
		{
			return false;
		}

		if ($instance == null)
		{
			$instance = new JYAML;
			$instance->linkJDocumentProperties()->initialise();
		}

		return $instance;
	}

	/**
	 * Method to get JDocument public properties as reference
	 *
	 * @access private
	 * @since  4.5.0
	 *
	 * @return JYAML
	 */
	private function linkJDocumentProperties()
	{
		/*
		$document = self::doc();
		$reflect = new ReflectionClass($document);
		$props   = $reflect->getProperties(ReflectionProperty::IS_PUBLIC);

		foreach ($props as $_property)
		{
			$_name = $_property->getName();

			if ($_property->isStatic())
			{
				$this->$_name = &$document::${$_name};
			}
			else
			{
				$this->$_name = &$document->$_name;
			}
		}
		*/

		$this->set('params', new \Joomla\Registry\Registry);

		return $this;
	}

	/**
	 * Initialise JYAML class
	 *
	 * @access private
	 * @return void
	 */
	private function initialise()
	{
		$this->addLog('Initialize JYAML');

		// Load library language
		self::loadLanguage();

		$tmpl = self::app()->getTemplate(true);

		// Check the current template is a jyaml template
		$isTemplateJYAML = $tmpl->params->get('_is_jyaml', 0);

		if ( ! $isTemplateJYAML)
		{
			// Extended check is jyaml support enabled in templateDetails.xml when config is not saved currently
			$templateDetailsXml = $this->getTemplatePath() . DIRECTORY_SEPARATOR . $tmpl->template . DIRECTORY_SEPARATOR . 'templateDetails.xml';

			if (JFile::exists($templateDetailsXml))
			{
				$_xml   = simplexml_load_file($templateDetailsXml);
				$_check = $_xml->xPath("//fieldset[@name='jyaml']/field[@name='support'][@value='true']");

				$isTemplateJYAML = empty($_check) ? false : true;
			}
		}

		if ( ! $isTemplateJYAML)
		{
			$this->isJYAML = false;
			$tmpl->params->get('_is_jyaml', $this->isJYAML);
			$this->addLog('The template "' . $tmpl->template . '" is not a JYAML template.');
			return;
		}
		else
		{
			// Fallback (first load)
			$_tmp = $tmpl->params->toArray();
			if (empty($tmpl->params) || empty($_tmp))
			{
				$this->params = new \Joomla\Registry\Registry;

				// Show message to click save once on first load
				if ( ! self::app()->isAdmin())
				{
					die('JYAML ERROR: ' . JText::_('JYAML__NOTICE_SAVE_TEMPLATE_FIRST_TIME'));
				}
			}
		}

		// Set include path for additional html behaviors
		JHtml::addIncludePath(JYAML_LIB_PATH . '/html');
		JHtml::_('jyaml.init');

		// Set the template and there params in object
		$this->set('template', $tmpl->template);
		$this->set('params', $tmpl->params);

		// Init vars
		$tmplPath    = $this->getTemplatePath(false);
		$tmplPathAbs = $this->getTemplatePath();

		// Register paths
		$this->setPath('root', JURI::base(true));
		$this->setPath('root', JPATH_ROOT);

		$this->setPath('template', $tmplPath);
		$this->setPath('template', $tmplPathAbs);

		$this->setPath('image', $tmplPath . '/images');
		$this->setPath('image', $tmplPathAbs . DIRECTORY_SEPARATOR . 'images');

		$this->setPath('css', $tmplPath . '/css');
		$this->setPath('css', $tmplPathAbs . DIRECTORY_SEPARATOR . 'css');

		$this->setPath('script', $tmplPath . '/script');
		$this->setPath('script', $tmplPathAbs . DIRECTORY_SEPARATOR . 'script');

		$this->setPath('libraries', JURI::root(true) . '/libraries/jyaml');
		$this->setPath('libraries', JYAML_LIB_PATH);

		$this->setPath('yaml', JURI::root(true) . '/libraries/jyaml/yaml');
		$this->setPath('yaml', JYAML_LIB_PATH . DIRECTORY_SEPARATOR . 'yaml');

		// Load custom language file for end-user changes
		$lang = JFactory::getLanguage();

		// $lang->load('core', $this->getPath('template'), null, false, false);
		// $lang->load('custom', $this->getPath('template'), null, false, false);
		$lang->load('tpl_' . $this->get('template'), $this->getPath('template'), null, false, false);
		$lang->load('tpl_' . $this->get('template') . '.custom', $this->getPath('template'), null, false, false);

		// Set Browser and Client inforamtions
		$this->setBrowserEnvironment();

		// Set current and parent item ids
		$this->setItemIds();

		// Register current component as active extension
		$this->addAcitveExtension(self::input()->get('option'));

		// Load jQuery
		if ($this->params->get('loadjquery'))
		{
			JHtml::_('jquery.framework', true, null);
		}

		// Load Bootstrap/JUI components
		if (($loadJuiBootstrap = $this->params->get('load_jui_bootstrap')))
		{
			JHtml::_('bootstrap.framework');

			if ($loadJuiBootstrap == 2)
			{
				$this->addStyleSheet($this->getUrl('css', 'icons.css'));
			}
		}

		// Load MooTools
		if (($loadMooTools = $this->params->get('loadmootools')))
		{
			$mooToolsMore = ($loadMooTools == 2 ? true : false);
			JHtml::_('behavior.framework', $mooToolsMore);
		}

		$this->triggerPlugin('onAfterJYAMLinitialize');
	}

	/**
	 * Dispatch the configured HTML Template file
	 *
	 * @param   string $folder Folder name in /html/ of the template
	 *
	 * @access  public
	 * @return  void
	 */
	public function dispatchTemplate($folder = 'index')
	{
		$loaded = array();

		$templatePath = $this->getTemplatePath();
		$htmlFile     = $this->getHtmlFile($folder);

		// Fallback for if folder not index (e.g. like tmpl=component)
		if ($folder != 'index' && ! file_exists($templatePath . DIRECTORY_SEPARATOR . $htmlFile))
		{
			$htmlFileOrig = $htmlFile;
			$htmlFile     = $this->getHtmlFile($folder, 'default');
			$this->addErrorLog('HTML template not found: ' . $htmlFileOrig . ' :[Fallback]> ' . $htmlFile);
		}

		$hash = md5($templatePath . $htmlFile);

		if ( ! isset($loaded[$hash]))
		{
			$templateFile = $templatePath . DIRECTORY_SEPARATOR . $htmlFile;

			if (file_exists($templateFile))
			{
				// Load files for head (before)
				$this->_setHead('before');

				// Init layout col_x
				$this->_initLayout();

				// Generate body class string
				$this->_generateBodyClassString();

				// Check some things !before render!
				$this->chunkJsCssCheck();

				ob_start();
				include_once $templateFile;
				$_contents = @ob_get_contents();
				$this->set('__html__', $_contents);
				unset($_contents);
				ob_clean();

				// Prepare for debug if enabled
				$this->debugPrepare();

				$this->triggerPlugin('onBeforeJYAMLdispatch');

				// Prepare dynamic positions
				$this->_prepareJdocTypeJYAMLtoReal();

				// Parse jdoc
				$this->_parseTemplate();
				$this->_renderTemplate();

				// Trigger plugin: onAfterJYAMLdispatch
				$this->triggerPlugin('onAfterJYAMLdispatch');

				$loaded[$hash] = true;
				$this->addLog('HTML template rendered: ' . $htmlFile);

				// Fallback: Joomla does not trigger (most times) onAfterRender on error
				if ($this->get('errorDocument'))
				{
					$this->set(
						'__html__',
						$this->optimizeMarkup(
							$this->get('__html__', '')
						)
					);
				}

				// Output template contents
				echo $this->get('__html__', '');

				// Reset html (free memory)
				unset($this->__html__);
			}
			else
			{
				$this->setError('HTML template not found: ' . $htmlFile);
			}
		}

	}

	/**
	 * Parse the document template
	 *
	 * @param   boolean $nested Determine to render in nested mode
	 *
	 * @access private
	 * @return boolean
	 */
	private function _parseTemplate($nested = false)
	{
		$result = false;

		$this->templateTags = array();
		$matches            = array();

		// Head and then messages must be parsed at last to grow up all includes/outputs
		$customTypeOrder = array(
			'jyaml-skiplinks' => array(),
			'head'            => array(),
			'message'         => array()
		);

		if (preg_match_all('#<jdoc:include\ type="([^"]+)" (.*)\/>#iU', $this->get('__html__', ''), $matches))
		{
			// Note: parse order must be reverse for more backward compatibility (except of $customTypeOrder)
			$matches[0] = array_reverse($matches[0]);
			$matches[1] = array_reverse($matches[1]);
			$matches[2] = array_reverse($matches[2]);

			$count = count($matches[1]);

			for ($i = 0; $i < $count; $i ++)
			{
				$attribs = JUtility::parseAttributes($matches[2][$i]);
				$type    = $matches[1][$i];
				$name    = isset($attribs['name']) ? $attribs['name'] : null;

				if (($nested && $type != 'head'))
				{
					$result = true;
				}
				else
				{
					$result = true;
				}

				if (array_key_exists($type, $customTypeOrder))
				{
					$customTypeOrder[$type][$matches[0][$i]] = array(
						'type'    => $type,
						'name'    => $name,
						'attribs' => $attribs
					);
				}
				else
				{
					$this->templateTags[$matches[0][$i]] = array(
						'type'    => $type,
						'name'    => $name,
						'attribs' => $attribs
					);
				}
			}
		}

		// Apply at last custom type ordering to the parse jdocs
		foreach ($customTypeOrder as $_tags)
		{
			$this->templateTags = array_merge($this->templateTags, $_tags);
		}

		if ($nested)
		{
			return $result;
		}

		// Try to find a favicon by checking the template and root folder
		$dirs = array($this->getPath('template') . DIRECTORY_SEPARATOR, JPATH_BASE . DIRECTORY_SEPARATOR);

		foreach ($dirs as $dir)
		{
			$icon = $dir . 'favicon.ico';
			if (file_exists($icon))
			{
				$path = str_replace(JPATH_BASE . DIRECTORY_SEPARATOR, '', $dir);
				$path = JPath::clean($path, '/');
				$this->addFavicon(JURI::base(true) . '/' . $path . 'favicon.ico');
				break;
			}
		}

		return $result;
	}

	/**
	 * Render pre-parsed template
	 *
	 * @param   boolean $nested Determine to render in nested mode
	 *
	 * @access private
	 * @return void
	 */
	private function _renderTemplate($nested = false)
	{
		static $nestedLevel = 0;

		$replace = array();
		$with    = array();

		$replaceHead = '';
		$withHead    = '';

		foreach ($this->templateTags as $jdoc => $args)
		{
			if ($nested && $args['type'] == 'head')
			{
				continue;
			}

			if ($args['type'] != 'head')
			{
				$replace[] = $jdoc;
			}

			if ($args['type'] == 'jyaml-skiplinks')
			{
				$_outer = isset($args['attribs']['outer']) ? $args['attribs']['outer'] : null;
				$_wrap  = isset($args['attribs']['wrap']) ? $args['attribs']['wrap'] : null;
				$with[] = $this->getSkiplinkHtml($_outer, $_wrap);
			}
			else
			{
				if ($args['type'] == 'head')
				{
					// Ignore head (let it render Joomla at last)

					// Fallback: But on error Joomla dont render the head
					if ($this->get('errorDocument'))
					{
						$replaceHead = $jdoc;
						$withHead    = $args;
					}
				}
				else
				{
					$buffer = '';

					if ($args['type'] == 'message')
					{
						if ($this->hasMessages())
						{
							$buffer = $this->getBuffer($args['type'], $args['name'], $args['attribs']);
						}
					}
					else
					{
						$buffer = $this->getBuffer($args['type'], $args['name'], $args['attribs']);
					}

					if ((int) $this->isDebug('tp') && $args['type'] != 'head')
					{
						if ($buffer === '')
						{
							$buffer = '<div class="jyaml-debug-position-empty">empty</div>';
						}

						$outline = '<div class="jyaml-debug-preview-title">' . htmlentities($jdoc) . '</div>';
						$outline .= '<div class="jyaml-debug-preview clearfix">' . $buffer . '</div>';

						$buffer = $outline;
					}

					$with[] = $buffer;
				}
			}
		}

		$html = $this->get('__html__', '');
		$html = str_replace($replace, $with, $html);
		$this->set('__html__', $html);

		// Prepare jdoc from type=jyaml to real positions (replace __html__),
		// If set in a module (like module-content feature in menuitem)
		$this->_prepareJdocTypeJYAMLtoReal();

		// Nested jdoc statements (max 3 levels to prevent loop)
		if ($nestedLevel < 2 && $this->_parseTemplate(true))
		{
			++ $nestedLevel;
			$this->_renderTemplate(true);
		}

		if ($nested)
		{
			return;
		}

		if ($replaceHead && $withHead)
		{
			$html = $this->get('__html__', '');
			$this->onBeforeLoadHead();
			$withHead = $this->getBuffer($withHead['type'], $withHead['name'], $withHead['attribs']);
			$html     = str_replace($replaceHead, $withHead, $html);
			$this->set('__html__', $html);
		}
	}

	/**
	 * Get the contents of a document type
	 * Wrapper for JDocumentHTML::getBuffer
	 *
	 * @param   string $type    The type of renderer
	 * @param   string $name    The name of the element to render
	 * @param   array  $attribs Associative array of remaining attributes.
	 *
	 * @access private
	 * @return string The output of the renderer
	 */
	private function getBuffer($type = null, $name = null, $attribs = array())
	{
		$buffer = '';

		if (($type == 'modules' || $type == 'module') && $name !== null)
		{
			if ($type == 'module')
			{
				$name = '_' . $name;
			}

			$_modules = $this->getRegisteredModules($name);

			foreach ($_modules as $k => $_module)
			{
				$extendedDebug         = false;
				$extendedDebugSubStyle = false;

				$_attribs = $attribs;
				$_params  = new \Joomla\Registry\Registry;
				$_params->loadString($_module->params, 'JSON');
				$_styleOverride = $_params->get('style_override');
				if (isset($_styleOverride->style) && $_styleOverride->style != '')
				{
					if (isset($_attribs['subStyle']))
					{
						$_attribs['subStyle']  = $_styleOverride->style;
						$extendedDebug         = true;
						$extendedDebugSubStyle = true;
					}
					else
					{
						$_attribs['style']     = $_styleOverride->style;
						$extendedDebug         = true;
						$extendedDebugSubStyle = false;
					}

					if (isset($_styleOverride->__attributes) && isset($_styleOverride->__attributes->params))
					{
						$attribsOverride = \Joomla\Utilities\ArrayHelper::fromObject($_styleOverride->__attributes->params);
						$_attribs        = array_merge($_attribs, $attribsOverride);
					}
				}

				$extendedOutline = '';
				if ($extendedDebug && (int) $this->isDebug('tp'))
				{
					$_tmp      = $_attribs;
					$_position = array('type' => $type, 'name' => $name);
					if (isset($_tmp['style']))
					{
						$_position['style'] = $_tmp['style'];
						unset($_tmp['style']);
					}
					$_position['__attributes']['params'] = $_tmp;
					$_jdoc                               = htmlentities($this->convertToJdocString($_position));

					$extendedOutline = '<div class="jyaml-debug-dynamic-positions jyaml-debug-extended-outline">';
					$extendedOutline .= '<div class="jyaml-debug-preview-title">' . JText::_('JYAML_EXT_MOD_LAYOUT_SUBSTYLE') . ': <br />' . $_jdoc . '</div>';
					$extendedOutline .= '${{JYAML_MODCHROME_MODULE_CONTENTS}}';
					$extendedOutline .= '</div>';
					$_attribs['____jyaml_extended_debug_outline_content'] = $extendedOutline;
				}

				if ( ! isset($_module->__rawContents) || $_module->isCountEmpty)
				{
					// Get contents, when countEmptyContents is true
					if ($this->params->get('layout_countemptymodules'))
					{
						/** @var JDocumentRendererModule $_renderer */
						$_renderer = $this->doc()->loadRenderer('module');

						if (isset($_attribs['params']))
						{
							$_attribs['params'] = new \Joomla\Registry\Registry($_attribs['params']);
						}
						else
						{
							$_attribs['params'] = new \Joomla\Registry\Registry;
						}
						$_attribs['params']->set('cachemode', 'itemid');
						$_attribs['params'] = $_attribs['params']->toString();

						$contents = $_renderer->render($_module, $_attribs);
					}
					else
					{
						$contents = JModuleHelper::renderModule($_module, $_attribs);
					}
				}
				else
				{
					// Get contents, when countEmptyContents is false (raw prerendered contents without chrome style)
					$_module->user    = 1;
					$_module->content = isset($_module->__rawContents) ? $_module->__rawContents : '';

					if ($_module->module == 'mod_custom')
					{
						// Fix [#JYAML-56]: Prevent double <div class="custom" /> outline of mod_custom module
						$doc = $this->getHTMLDomDocument($_module->content);

						$bodyNode = $doc->getElementsByTagName('body')->item(0);

						if ($bodyNode && $bodyNode->hasChildNodes())
						{
							$innerHTML = '';
							foreach ($bodyNode->childNodes as $node)
							{
								if ($node instanceof \DOMElement && strpos(trim($node->getAttribute('class')), 'custom') === 0 && $node->hasChildNodes())
								{
									foreach ($node->childNodes as $child)
									{
										$innerHTML .= $child->ownerDocument->saveHtml($child);
									}
								}
							}
							$_module->content = $innerHTML;
						}
					}

					if ($this->params->get('layout_countemptymodules'))
					{
						/** @var JDocumentRendererModule $_renderer */
						$_renderer = $this->doc()->loadRenderer('module');

						if (isset($_attribs['params']))
						{
							$_attribs['params'] = new \Joomla\Registry\Registry($_attribs['params']);
						}
						else
						{
							$_attribs['params'] = new \Joomla\Registry\Registry;
						}
						$_attribs['params']->set('cachemode', 'itemid');
						$_attribs['params'] = $_attribs['params']->toString();

						$contents = $_renderer->render($_module, $_attribs);
					}
					else
					{
						$contents = JModuleHelper::renderModule($_module, $_attribs);
					}

					$this->registeredModules[$name][$k]->content = $contents;
				}

				if ($extendedOutline && ! $extendedDebugSubStyle)
				{
					$contents = str_replace('${{JYAML_MODCHROME_MODULE_CONTENTS}}', $contents, $extendedOutline);
				}

				$contents = trim($contents);

				if ($contents != '')
				{
					$buffer .= $contents;

					$this->execMethodQueue('preRenderScope.mod.' . $_module->position . '.' . $_module->id);

					if ($_module->module != ('mod_' . $_module->position))
					{
						$this->addAcitveExtension($_module->module);
					}
				}
			}
		}
		else
		{
			$buffer = self::doc()->getBuffer($type, $name, $attribs);
		}

		return $buffer;
	}

	/**
	 * Loads HTML with DOMDocument
	 * (Support for unknown tags like HTML5 and fix UTF8 issues)
	 * Note: Only UTF8 supported. If you have other encodings please convert before
	 *
	 * @param $html
	 *
	 * @return DOMDocument
	 */
	public function getHTMLDomDocument($html)
	{
		$dom               = new \DOMDocument();
		$dom->formatOutput = true;

		// Allow unknown tags (HTML5 support)
		libxml_use_internal_errors(true);

		// Load HTML with xml encoding tag to fix UTF8 issues and force UTF-8.
		$prefixHtml      = '<?xml encoding="UTF-8">';
		$removeMetaToken = false;
		if (strpos($html, '</head>') === false)
		{
			$removeMetaToken = '___JYAML_FRAMEWORK_UTF8FIX_METATAG___';
			$prefixHtml .= '<meta id="' . $removeMetaToken . '" http-equiv="content-type" content="text/html; charset=utf-8">';
		}

		// Load HTML with xml encoding tag to fix UTF8 issues.
		$dom->loadHtml($prefixHtml . $html);

		// Then remove the added elements
		/** @var \DomElement[] $removeChilds */
		$removeChilds = array();
		foreach ($dom->childNodes as $item)
		{
			/* @var $item \DOMNode */
			if ($item->nodeType == XML_PI_NODE)
			{
				$removeChilds[] = $item;
				break;
			}
		}
		if ($removeMetaToken)
		{
			$metaNode = $dom->getElementById($removeMetaToken);
			if ($metaNode instanceof \DOMElement)
			{
				$removeChilds[] = $metaNode;
			}
		}
		// Childs must removed after traversing
		foreach ($removeChilds as $node)
		{
			$node->parentNode->removeChild($node);
		}

		// Force UTF8
		$dom->encoding = 'UTF-8';

		// Clean error messages (HTML5 support)
		libxml_clear_errors();

		return $dom;
	}

	/**
	 * Method to do some things before '<jdoc:include type="head" />' is parsed/replaced
	 *
	 * @access public
	 * @return void
	 */
	public function onBeforeLoadHead()
	{
		static $loaded;

		if ( ! $this->isJYAML)
		{
			return;
		}

		if ($loaded)
		{
			return;
		}

		$document = self::doc();

		// Load dynamic css/js template file
		$dynamicFiles = array(
			'Style'  => $this->getPath('css', true, 'css.php'),
			'Script' => $this->getPath('script', true, 'script.php')
		);
		foreach ($dynamicFiles as $_type => $_dynamicFile)
		{
			if (JFile::exists($_dynamicFile))
			{
				ob_start();
				include_once $_dynamicFile;
				$_contents = @ob_get_contents();
				ob_clean();
				$_contents = strip_tags($_contents);

				if ( ! empty($_contents))
				{
					$_addDeclarationMethod = 'add' . $_type . 'Declaration';
					$this->$_addDeclarationMethod($_contents);
				}
				unset($_contents);
			}
		}

		// Load files for head (after)
		$this->_setHead('after');

		// Set skin here, to add these files in the end of head
		$this->_setSkin();

		// Add scripts with parameter noChunk=1 at first.
		if (isset($document->_scripts) && is_array($document->_scripts))
		{
			$_scriptsFirst = array();
			foreach ($document->_scripts as $_src => $attr)
			{
				if (strpos($_src, 'noChunk=1') !== false)
				{
					$_scriptsFirst[$_src] = $attr;
					unset($document->_scripts[$_src]);
				}
			}

			if (count($_scriptsFirst))
			{
				$document->_scripts = array_merge($_scriptsFirst, $document->_scripts);
			}
		}

		// Add stylesheets with parameter noChunk=1 at first.
		if (isset($document->_styleSheets) && is_array($document->_styleSheets))
		{
			$_styleSheetsFirst = array();
			foreach ($document->_styleSheets as $_src => $attr)
			{
				if (strpos($_src, 'noChunk=1') !== false)
				{
					$_styleSheetsFirst[$_src] = $attr;
					unset($document->_styleSheets[$_src]);
				}
			}

			if (count($_styleSheetsFirst))
			{
				$document->_styleSheets = array_merge($_styleSheetsFirst, $document->_styleSheets);
			}
		}

		// Chunk(combine/minifiy) JS and CSS Files in html head, if enabled
		$this->chunkJsCss();

		// Add Favicon
		$_favicon = $this->params->get('favicon', '');
		if ($_favicon)
		{
			// Try to remove the template favicon
			if (isset($document->_links) && ! empty($document->_links))
			{
				$tmplFavicoPath = $this->getUrl('template', 'favicon.ico');
				foreach ($document->_links as $_link => $_attribs)
				{
					if (strpos($_link, $tmplFavicoPath) !== false)
					{
						unset($document->_links[$_link]);
					}
				}
			}

			// Add new favicon
			$this->addFavicon(JURI::base(true) . '/images/' . $_favicon);
		}

		$_globalJsNs = '__TOKEN__JYAML._getGlobalJsFunctions__TOKEN__';
		$document->addScript($_globalJsNs);
		$_getGlobalJsFunctions = $document->_scripts[$_globalJsNs];
		unset($document->_scripts[$_globalJsNs]);
		$document->_scripts = array_merge(array($_globalJsNs => $_getGlobalJsFunctions), $document->_scripts);

		$this->triggerPlugin('onBeforeJYAMLloadHead');

		// Set the new html
		$this->set('__html__', JResponse::getBody());

		$loaded = true;
	}

	/**
	 * Get a cache object (group jyaml_framework, continues enabled)
	 *
	 * @param   string $handler The handler to use
	 * @param   string $storage The storage method
	 *
	 * @since 4.5.0
	 *
	 * @return  JCacheController
	 */
	public function getCache($handler = '', $storage = null)
	{
		static $caches = array();

		$hash = md5($handler . $storage);
		if (isset($caches[$hash]))
		{
			return $caches[$hash];
		}

		$conf    = JFactory::getConfig();
		$handler = ($handler == 'function') ? 'callback' : $handler;

		$options = array(
			'defaultgroup' => 'jyaml_framework',
			'storage'      => $conf->get('cache_handler', (isset($storage) ? $storage : '')),
			'caching'      => true,
			'cachebase'    => $conf->get('cache_path', JPATH_SITE . '/cache')
		);

		$cache         = JCache::getInstance($handler, $options);
		$caches[$hash] = $cache;

		return $caches[$hash];
	}

	/**
	 * Get defined autoload stylesheets and scripts
	 *
	 * @param   string  $templatePath Absolute Path to the JYAML template
	 * @param   string  $type         Type of files (script, stylesheet)
	 * @param   boolean $fullInfo     If false returns only filenames
	 * @param   boolean $recursiv     Recursiv loading (only stylesheets supported)
	 * @param   string  $modeFilter   Fiter by mode (after or blank) (will not work when $recursiv=true)
	 *
	 * @access public
	 * @return array List of files
	 */
	public static function getAutoLoadFiles($templatePath, $type, $fullInfo = true, $recursiv = false, $modeFilter = '')
	{
		$files = array();

		if ( ! class_exists('JYAML_Autoload'))
		{
			$loaderFile = $templatePath . DIRECTORY_SEPARATOR . 'jyaml.autoload.php';

			if (file_exists($loaderFile))
			{
				require_once $loaderFile;
			}
		}

		if (class_exists('JYAML_Autoload'))
		{
			if (constant('JDEBUG'))
			{
				JProfiler::getInstance('Application')->mark('JYAML::getAutoLoadFiles before:(' . $type . '-' . $modeFilter . ')');
			}

			$jyaml    = self::getDocument();
			$cache    = $jyaml->getCache();
			$cacheKey = md5('tpl_' . $jyaml->template . '.function.getAutoLoadFiles.' . serialize(func_get_args()));

			if (false !== ($_data = $cache->get($cacheKey)))
			{
				$files = unserialize($_data);
			}
			else
			{
				$loader = new JYAML_Autoload;

				if ($recursiv && $loader->convertType($type) != 'stylesheets')
				{
					$recursiv = false;
				}

				if ($recursiv)
				{
					$files = $loader->getAutoLoadStylesheetsRecursivly($templatePath);
				}
				else
				{
					$jyaml  = self::getDocument();
					$_files = $loader->getAutoLoadFiles($type, $fullInfo);

					foreach ($_files as $k => $v)
					{
						$_mode = isset($v['mode']) ? $v['mode'] : '';
						$_ifc  = isset($v['ifconfig']) ? $v['ifconfig'] : '';
						if ($_ifc && ! is_array($_ifc))
						{
							$_ifc = array('key' => $_ifc);
						}

						if ($_ifc)
						{
							$_ifcKey = isset($_ifc['key']) ? $_ifc['key'] : '';
							$_ifcDef = isset($_ifc['default']) ? $_ifc['default'] : null;
							$_ifcEq  = isset($_ifc['equal']) ? $_ifc['equal'] : null;

							if ($_ifcKey)
							{
								$_val = $jyaml->params->get($_ifcKey, $_ifcDef);

								if (($_ifcEq === null && ! $_val) || ($_ifcEq !== null && $_val != $_ifcEq))
								{
									continue;
								}
							}

						}

						if ($modeFilter != 'after' && $_mode != 'after')
						{
							$files[$k] = $v;
						}
						elseif ($modeFilter == 'after' && $_mode == 'after')
						{
							$files[$k] = $v;
						}
					}

				}

				$cache->store(serialize($files), $cacheKey);
			}

			if (constant('JDEBUG'))
			{
				JProfiler::getInstance('Application')->mark('JYAML::getAutoLoadFiles after:(' . $type . '-' . $modeFilter . ')');
			}
		}

		return $files;
	}

	/**
	 * Set the page Header
	 *
	 * @param   string $load Autoload Files = before, Additional Files = after
	 *
	 * @access private
	 * @return void
	 */
	private function _setHead($load = 'before')
	{
		$stylesheets = array();
		$scripts     = array();

		if ($load == 'before')
		{
			// Get autoload stylesheets (before)
			foreach (self::getAutoLoadFiles($this->getPath('template'), 'css', true, false, '') as $stylesheet)
			{
				$stylesheets[] = $stylesheet;
			}

			// Get autoload scripts
			foreach (self::getAutoLoadFiles($this->getPath('template'), 'script', true, false, '') as $script)
			{
				$scripts[] = $script;
			}
		}
		else
		{
			// Get autoload stylesheets (after)
			foreach (self::getAutoLoadFiles($this->getPath('template'), 'css', true, false, 'after') as $stylesheet)
			{
				$stylesheets[] = $stylesheet;
			}

			// Get autoload scripts
			foreach (self::getAutoLoadFiles($this->getPath('template'), 'script', true, false, 'after') as $script)
			{
				$scripts[] = $script;
			}

			// Get additional stylesheets
			$_arr = $this->params->get('stylesheets');

			if (is_object($_arr))
			{
				$_arr = \Joomla\Utilities\ArrayHelper::fromObject($_arr);
			}
			$_arr = (array) $_arr;

			foreach ($_arr as $stylesheet)
			{
				$stylesheets[] = $stylesheet;
			}

			// Get additional scripts
			$_arr = $this->params->get('scripts');

			if (is_object($_arr))
			{
				$_arr = \Joomla\Utilities\ArrayHelper::fromObject($_arr);
			}
			$_arr = (array) $_arr;

			foreach ($_arr as $script)
			{
				$scripts[] = $script;
			}
		}

		// Add stylesheets
		foreach ($stylesheets as $stylesheet)
		{
			$stylesheet = new \Joomla\Registry\Registry($stylesheet);

			$file   = $stylesheet->get('file', '');
			$type   = $stylesheet->get('type', 'text/css');
			$media  = $stylesheet->get('media', 'all');
			$IEcc   = $stylesheet->get('IEconditionalComment', '');
			$pos    = $stylesheet->get('position', '');
			$inline = $stylesheet->get('inline', false);

			if ($file)
			{
				if ($inline)
				{
					$this->addStyleDeclaration($file, $type, $IEcc, $pos);
				}
				else
				{
					$this->addStylesheet($file, $type, $media, array(), $IEcc, $pos);
				}

			}
		}

		// Add Scripts
		foreach ($scripts as $script)
		{
			$script = new \Joomla\Registry\Registry($script);

			$file   = $script->get('file', '');
			$type   = $script->get('type', 'text/javascript');
			$IEcc   = $script->get('IEconditionalComment', '');
			$pos    = $script->get('position', '');
			$inline = $script->get('inline', false);

			if ($file)
			{
				if ($inline)
				{
					$this->addScriptDeclaration($file, $type, $IEcc, $pos);
				}
				else
				{
					$this->addScript($file, $type, $IEcc, $pos);
				}
			}
		}

		$this->addLog('Page Header initialized (' . $load . ')');
	}

	/**
	 * Set the skin (add css etc.)
	 *
	 * @access private
	 * @return void
	 */
	private function _setSkin()
	{
		// Add skin stylesheet
		if (($_skin = $this->params->get('skin')))
		{
			$file = '{$source.theme}skin.' . $_skin . '.css';
			$this->addStylesheet($file);
		}
	}

	/**
	 * Replace an path by source variable {$source.yaml|theme}
	 *
	 * @param   string $path Path of file or Url
	 * @param   string $type The type of Path (eg. css, script, yaml)
	 *
	 * @access protected
	 * @return string Replaced Path
	 */
	protected function replacePathBySourceType($path, $type)
	{
		if (strpos($path, '{$source.theme}') !== false)
		{
			$path = str_replace('{$source.theme}', $this->getUrl($type), $path);
		}
		elseif (strpos($path, '{$source.yaml}') !== false)
		{
			$path = str_replace('{$source.yaml}', $this->getUrl('yaml'), $path);
		}
		elseif ( ! preg_match('#^[http|//]#', $path) && ! preg_match('#^' . $this->getUrl('root', '') . '#i', $path))
		{
			$path = JPath::clean($this->getUrl('root') . $path, '/');
		}

		/*
		 * Full SSL support for absolute URLs (its required for strong certificate validation)
		 * Limitation: The target URL requires also an valid certificate
		 */
		if (preg_match('#^http#', $path))
		{
			if ($this->isSSLActive())
			{
				$path = preg_replace('#^http://#', 'https://', $path);
			}
		}

		return $path;
	}

	/**
	 * Method to check SSL is active or not
	 *
	 * @access public
	 * @return boolean
	 */
	public function isSSLActive()
	{
		static $ssl = null;

		if ($ssl === null)
		{
			$uri = JURI::getInstance();
			$ssl = $uri->isSSL();
		}

		return $ssl;
	}

	/**
	 * Adds a linked script to the page
	 *
	 * @param   string $url      URL to the linked script
	 * @param   string $type     Type of script. (default 'text/javascript')
	 * @param   string $IEcc     Contitional Comment contition for Internet Explorer (eg. 'if lte IE 7')
	 * @param   string $position Position
	 *
	 * @access public
	 * @return void
	 */
	public function addScript($url, $type = "text/javascript", $IEcc = '', $position = '')
	{
		if (is_array($type) || is_array($IEcc))
		{
			// B/C new in 3.7.0 second and third parameters are options and attributes
			self::doc()->addScript($url, $type, $IEcc);
			return;
		}

		static $loaded = array();

		if (($_scope = $this->getIsPreRenderScope()))
		{
			$this->addMethodQueue('preRenderScope.' . $_scope, 'addScript', $url, $type, $IEcc, $position);
			return;
		}

		if (in_array($url, $loaded))
		{
			// Prevent dublicate insert
			return;
		}
		else
		{
			$loaded[] = $url;
		}

		$url = $this->replacePathBySourceType($url, 'script');

		if ($position == 'head')
		{
			$position = '';
		}

		if ($IEcc != "" || $position != "")
		{
			$script = '<script type="' . $type . '" src="' . $url . '"></script>';

			if ($IEcc)
			{
				$script = '<!--[' . $IEcc . ']> ' . $script . ' <![endif]-->';
			}

			if ($position)
			{
				$this->addMethodQueue('prepareMarkup', 'addHtmlContentByPosition', $script, $position);
			}
			else
			{
				$this->addCustomTag($script);
			}
		}
		else
		{
			self::doc()->addScript($url, $type);
		}
	}

	/**
	 * Adds a script to the page
	 *
	 * @param   string $content  Script contents
	 * @param   string $type     Scripting mime (default 'text/javascript')
	 * @param   string $IEcc     Contitional Comment contition for Internet Explorer (eg. 'if lte IE 7')
	 * @param   string $position Position
	 *
	 * @access public
	 * @return void
	 */
	public function addScriptDeclaration($content, $type = 'text/javascript', $IEcc = '', $position = '')
	{
		static $loaded = array();

		if (($_scope = $this->getIsPreRenderScope()))
		{
			$this->addMethodQueue('preRenderScope.' . $_scope, 'addScriptDeclaration', $content, $type, $IEcc, $position);
			return;
		}

		$hash = md5($content);
		if (in_array($hash, $loaded))
		{
			// Prevent dublicate insert
			return;
		}
		else
		{
			$loaded[] = $hash;
		}

		if ($position == 'head')
		{
			$position = '';
		}

		if ($IEcc != "" || $position != "")
		{
			$script = '<script type="' . $type . '">' . $content . '</script>';

			if ($IEcc)
			{
				$script = '<!--[' . $IEcc . ']> ' . $script . ' <![endif]-->';
			}

			if ($position)
			{
				$this->addMethodQueue('prepareMarkup', 'addHtmlContentByPosition', $script, $position);
			}
			else
			{
				$this->addCustomTag($script);
			}
		}
		else
		{
			self::doc()->addScriptDeclaration($content, $type);
		}
	}

	/**
	 * Adds a linked stylesheet to the page
	 *
	 * @param   string $url      URL to the linked style sheet
	 * @param   string $type     Mime encoding type (default 'text/css')
	 * @param   string $media    Media type that this stylesheet applies to (default 'all')
	 * @param   array  $attribs  Additional Attributes
	 * @param   string $IEcc     Contitional Comment contition for Internet Explorer (eg. 'if lte IE 7')
	 * @param   string $position Position
	 *
	 * @access public
	 * @return void
	 */
	public function addStyleSheet(
		$url,
		$type = 'text/css',
		$media = 'all',
		$attribs = array(),
		$IEcc = '',
		$position = ''
	) {
		static $loaded = array();

		if (($_scope = $this->getIsPreRenderScope()))
		{
			$this->addMethodQueue('preRenderScope.' . $_scope, 'addStyleSheet', $url, $type, $media, $attribs, $IEcc, $position);
			return;
		}

		if (in_array($url, $loaded))
		{
			// Prevent dublicate insert
			return;
		}
		else
		{
			$loaded[] = $url;
		}

		$url = $this->replacePathBySourceType($url, 'css');

		if ($position == 'head')
		{
			$position = '';
		}

		if ($IEcc != "" || $position != "")
		{
			$style = '<link rel="stylesheet" href="' . $url . '" type="' . $type . '" media="' . $media . '"';
			$style .= ($attribs ? ' ' . \Joomla\Utilities\ArrayHelper::toString($attribs) : '') . ' />';

			if ($IEcc)
			{
				$style = '<!--[' . $IEcc . ']> ' . $style . ' <![endif]-->';
			}

			if ($position)
			{
				$this->addMethodQueue('prepareMarkup', 'addHtmlContentByPosition', $style, $position);
			}
			else
			{
				$this->addCustomTag($style);
			}
		}
		else
		{
			self::doc()->addStyleSheet($url, $type, $media, $attribs);
		}
	}

	/**
	 * Adds a stylesheet declaration to the page
	 *
	 * @param   string $content  Style contents
	 * @param   string $type     Type of stylesheet (default 'text/css')
	 * @param   string $IEcc     Contitional Comment contition for Internet Explorer (eg. 'if lte IE 7')
	 * @param   string $position Position
	 *
	 * @access public
	 * @return void
	 */
	public function addStyleDeclaration($content, $type = 'text/css', $IEcc = '', $position = '')
	{
		static $loaded = array();

		if (($_scope = $this->getIsPreRenderScope()))
		{
			$this->addMethodQueue('preRenderScope.' . $_scope, 'addStyleDeclaration', $content, $type, $IEcc, $position);
			return;
		}

		$hash = md5($content);
		if (in_array($hash, $loaded))
		{
			// Prevent dublicate insert
			return;
		}
		else
		{
			$loaded[] = $hash;
		}

		if ($position == 'head')
		{
			$position = '';
		}

		if ($IEcc != "" || $position != "")
		{
			$style = '<style type="' . $type . '">' . $content . '</style>';

			if ($IEcc)
			{
				$style = '<!--[' . $IEcc . ']> ' . $style . ' <![endif]-->';
			}

			if ($position)
			{
				$this->addMethodQueue('prepareMarkup', 'addHtmlContentByPosition', $style, $position);
			}
			else
			{
				$this->addCustomTag($style);
			}
		}
		else
		{
			self::doc()->addStyleDeclaration($content, $type);
		}
	}

	/**
	 * Wrapper for JDocument addCustomTag
	 *
	 * @param   string $html The html to add to the head
	 *
	 * @access public
	 * @return void
	 */
	public function addCustomTag($html)
	{
		if (($_scope = $this->getIsPreRenderScope()))
		{
			$this->addMethodQueue('preRenderScope.' . $_scope, 'addCustomTag', $html);
			return;
		}

		$document = self::doc();

		if (method_exists($document, 'addCustomTag'))
		{
			$document->addCustomTag($html);
		}
	}

	/**
	 * Method to check the current page is Home
	 *
	 * @param   string $byLangTag   If set you can check an specific lang tag (eg. en-GB, de-DE).
	 *                              If not set (or null) the current language will be automaticly detected
	 *
	 * @access public
	 * @return (int/boolean) false or the menu item id where is home
	 */
	public function isHomepage($byLangTag = null)
	{
		static $result = array();

		$sig = $byLangTag ? (string) $byLangTag : 'all';

		if ( ! isset($result[$sig]))
		{
			if (JFactory::getApplication()->isAdmin())
			{
				// Exclude admin
				return false;
			}

			if ($byLangTag === null)
			{
				$lang      = JFactory::getLanguage();
				$byLangTag = $lang->getTag();
			}

			$menu    = JFactory::getApplication()->getMenu('site');
			$active  = $menu->getActive();
			$default = $menu->getDefault($byLangTag);

			if (empty($active) || empty($default))
			{
				$result[$sig] = false;
			}
			else
			{
				$result[$sig] = ($active->id == $default->id) ? $active->id : false;
			}
		}

		return $result[$sig];
	}

	/**
	 * Method to get the Homepage Link
	 *
	 * @param   string $byLangTag      If set you can check an specific lang tag (eg. en-GB, de-DE).
	 *                                 If not set (or null) the current language will be automaticly detected
	 * @param   bool   $returnItemId   If true, return the menu item id instead of the link
	 *
	 * @access public
	 * @return string Homepage Link
	 */
	public function getHomepageLink($byLangTag = null, $returnItemId = false)
	{
		static $result = array();

		$sig = ($byLangTag ? (string) $byLangTag : 'all') . '_' . ($returnItemId ? '0' : '1');

		if ( ! isset($result[$sig]))
		{
			$menu    = JFactory::getApplication()->getMenu('site');
			$default = $menu->getDefault($byLangTag);

			if ($default && $default->id)
			{
				$result[$sig] = $returnItemId ? $default->id : JRoute::_('index.php?Itemid=' . $default->id);
			}
			else
			{
				$result[$sig] = $returnItemId ? 0 : JURI::base();
			}
		}

		return $result[$sig];
	}

	/**
	 * Method to get Real ItemId (follow alias links)
	 *
	 * @param   string $item Menu Item object
	 *
	 * @access public
	 * @return boolean|integer The Id of the resolved alias link or the original Id
	 */
	public function getRealMenuItemId($item)
	{
		if (JFactory::getApplication()->isAdmin())
		{
			// Exclude admin
			return false;
		}

		$id = null;

		if ($item && isset($item->id))
		{
			$id = $item->id;

			if (isset($item->type) && $item->type == 'alias' && isset($item->params) && is_object($item->params))
			{
				$aliasid = $item->params->get('aliasoptions');
				if ($aliasid)
				{
					$aliasItem = JFactory::getApplication()->getMenu('site')->getItem($aliasid);
					if ($aliasItem)
					{
						$id = $this->getRealMenuItemId($aliasItem);
					}
				}
			}
		}

		return $id;
	}

	/**
	 * Method to get the current header image
	 *
	 * @param   array $additionalParameterMap Get a value of an aditional parameter and returns an array of values
	 *
	 * @access public
	 * @return string or array
	 */
	public function getHeaderImage($additionalParameterMap = array())
	{
		$image = null;

		if (null !== $image)
		{
			return $image;
		}

		$additionalParams = array();
		foreach ($additionalParameterMap as $key)
		{
			$additionalParams[$key] = '';
		}

		$image     = '';
		$_disabled = false;

		$option        = self::input()->get('option');
		$componentName = '';
		if ($option)
		{
			$tmp           = explode('_', $option);
			$componentName = isset($tmp[1]) ? $tmp[1] : '';
		}

		$view = self::input()->get('view');

		// Search article, from category or article category (including parents)
		if ($option && $componentName)
		{
			$_rootCatId = 0;

			// Search from article
			$_articleId = self::input()->get('id', 0, 'int');
			$isArticle  = ($option == 'com_content' && $view == 'article' && $_articleId);
			if ($isArticle)
			{
				$db    = JFactory::getDBO();
				$query = $db->getQuery(true);
				$query->select('*');
				$query->from($db->qn('#__content'));
				$query->where($db->qn('id') . ' = ' . $db->q($_articleId));
				$db->setQuery($query);

				if ($result = $db->loadObject())
				{
					$_rootCatId = (int) $result->catid;
					$_params    = new \Joomla\Registry\Registry($result->attribs);
					$_disabled  = $_params->get('jyaml_header_image_disable', false);

					if ( ! $_disabled)
					{
						$image = $_params->get('jyaml_header_image');

						foreach ($additionalParameterMap as $key)
						{
							$additionalParams[$key] = $_params->get($key);
						}
					}
				}
			}

			if ( ! $_rootCatId && ($view == 'category' || $view == 'categories'))
			{
				$_rootCatId = self::input()->get('id', 0, 'int');
			}

			// Search from category (and inherit from parents)
			if ( ! $image && ! $_disabled && $_rootCatId)
			{
				if ($_categories = JCategories::getInstance(ucfirst($componentName)))
				{
					$_category = $_categories->get($_rootCatId);

					if ($_category && $path = $_category->getPath())
					{
						$path = array_reverse($path);

						foreach ($path as $cat)
						{
							$tmp   = explode(':', $cat);
							$catId = $tmp[0];

							$_category = $_categories->get($catId);

							if ($_category)
							{
								$_params   = $_category->getParams();
								$_disabled = $_params->get('jyaml_header_image_disable', false);
							}

							if ( ! $_disabled)
							{
								$image = $_params->get('jyaml_header_image');

								foreach ($additionalParameterMap as $key)
								{
									$additionalParams[$key] = $_params->get($key);
								}
							}

							if ($_disabled || $image)
							{
								break;
							}
						}
					}
				}
			}
		}

		// Search in current menu item (and inherit from parents)
		if ( ! $image && ! $_disabled)
		{
			$menuItems = array_reverse($this->parentItemIDs);
			$menuItems = array_merge(array($this->currentItemID), $menuItems);

			if ( ! empty($menuItems))
			{
				$menu = JFactory::getApplication()->getMenu('site');
				foreach ($menuItems as $_menuItemid)
				{
					$_menuItem = $menu->getItem($_menuItemid);

					if ($_menuItem)
					{
						$_disabled = $_menuItem->params->get('jyaml_header_image_disable', false);
					}

					if ($_menuItem && ! $_disabled)
					{
						$image = $_menuItem->params->get('jyaml_header_image');

						foreach ($additionalParameterMap as $key)
						{
							$additionalParams[$key] = $_menuItem->params->get($key);
						}
					}

					if ($_disabled || $image)
					{
						break;
					}
				}
			}
		}

		// Use default image from template settings
		if ( ! $image && ! $_disabled)
		{
			$image = $this->params->get('header_image', '');

			foreach ($additionalParameterMap as $key)
			{
				$additionalParams[$key] = $this->params->get($key);
			}
		}

		if ($additionalParameterMap)
		{
			return array_merge($additionalParams, array('jyaml_header_image' => $image));
		}

		return $image;
	}

	/**
	 * Method to check System Messages are available to display
	 *
	 * @access public
	 * @return boolean
	 */
	public function hasMessages()
	{
		$messages = JFactory::getApplication()->getMessageQueue();
		return (empty($messages) ? false : true);
	}

	/**
	 * Method to get all defined Column width's (ym-g*) screen.basemod.css (template) and base.css (YAML core)
	 *
	 * @access public
	 * @return array unique integer values of column widths
	 */
	public function getDefinedColumnWidtsFromCss()
	{
		static $definedColWidths;

		if ($definedColWidths === null)
		{
			// Detect defined column width from screen.basemod.css
			$definedColWidths = array();

			$basemodCssPath = $this->getPath('css', true, 'screen.basemod.css');
			if (file_exists($basemodCssPath))
			{
				$_css = file_get_contents($basemodCssPath);

				// Remove comments
				$_css = preg_replace("#/\*.+\*/#sU", "", $_css);

				$baseCssPath = $this->getPath('yaml', true, 'core' . DIRECTORY_SEPARATOR . 'base.css');
				if (file_exists($baseCssPath))
				{
					$_css .= file_get_contents($baseCssPath);
				}

				preg_match_all('/\.ym-g(\d+)\s*[,{]/', $_css, $matches);
				if (isset($matches[1]) && ! empty($matches[1]))
				{
					$definedColWidths = $matches[1];
				}
			}
		}

		return $definedColWidths;
	}

	/**
	 * Adds an content on a specific position in current html buffer
	 *
	 * @param   string $contents Contents
	 * @param   string $position (Available: afterBodyStart, beforeBodyEnds)
	 *
	 * @access private
	 * @return void
	 */
	private function addHtmlContentByPosition($contents, $position)
	{
		$html = $this->get('__html__');

		switch ($position)
		{
			case 'afterBodyStart':
				// Affects only the first starting body tag that was found
				$html = preg_replace('#<body(.*)>#i', "<body\\1>\n" . $contents, $html, 1);
				break;
			case 'beforeBodyEnds':
				$_pos = strrpos($html, '</body>');
				if ($_pos !== false)
				{
					// Affects only the last closing body tag that was found
					$html = substr_replace($html, $contents . "\n</body>", $_pos, 7);
				}
				break;
		}

		$this->set('__html__', $html);
	}

	/**
	 * Prepare the HTML Markup
	 *
	 * @param   string $html HTML contents
	 *
	 * @access protected
	 * @return string $html
	 */
	protected function prepareMarkup($html = '')
	{
		// Add global JYAML JS functions before all other scripts (token replace)
		$_search  = '#<script src="__TOKEN__JYAML\._getGlobalJsFunctions__TOKEN__".*></script>#Uis';
		$_replace = $this->_getGlobalJsFunctions();
		$html     = preg_replace($_search, $_replace, $html, 1);

		// Remove the header to prevent replacing inline css and js
		$tmp  = explode('</head>', $html, 2);
		$head = $tmp[0] . '</head>';
		$body = isset($tmp[1]) ? $tmp[1] : '';

		// Modifiy the starting body tag
		if (preg_match('#<body(.*)>#Uis', $body, $matches))
		{
			$attribs = JUtility::parseAttributes($matches[1]);

			// Add dynamic body css classes
			if (($bodyClasses = $this->getBodyCssClass(false)))
			{
				if ( ! isset($attribs['class']))
				{
					$attribs['class'] = '';
				}
				$attribs['class'] .= ' ' . $bodyClasses;
				$attribs['class'] = trim(preg_replace('#\s+#', ' ', $attribs['class']));

				$classList = explode(' ', $attribs['class']);
				$classList = array_unique($classList);

				$attribs['class'] = implode(' ', $classList);

				$attribString = \Joomla\Utilities\ArrayHelper::toString($attribs);
				$newBodyTag   = '<body' . ($attribString ? ' ' . $attribString : '') . '>';

				$body = preg_replace('/' . preg_quote($matches[0], '/') . '/', $newBodyTag, $body, 1);
				$html = $head . $body;
			}
		}

		// Ignore scripts with <!-- ... //--> comments (avoid on simulate document.write string)
		// (The Joomla E-Mail Cloaking Plugin use this special commenting)
		$htmlNoComments = str_replace('//-->', '//-->__TOKEN_document.write_TOKEN__', $html);

		// Strip all comments
		$htmlNoComments = preg_replace('#<!--.*-->#Uis', '', $htmlNoComments);

		// Strip IE-Contidional-Comments (not required anymore)
		// $htmlNoComments = preg_replace('#<!(--)?(?=\[)(?:(?!<!\[endif\]\1>).)*<!\[endif\]\1>#Uis', '', $htmlNoComments);

		// Search all scripts (without IE conditional comment) - (js comments <!--\n ... //--> are ignored)
		if (strpos($html, '</body>') !== false && preg_match_all('#<script.*>.*</script>#Uis', $htmlNoComments, $matches))
		{
			// Move all script before closing body ends, if enabled
			if ($this->params->get('script_before_body_ends'))
			{
				// Remove scripts
				$moveScripts = array();
				foreach ($matches[0] as $script)
				{
					if (stripos($script, 'document.write') === false && stripos($script, 'data-jyaml-ignore="1"') === false)
					{
						// Do not move scripts containing document.write or has attribute data-jyaml-ignore="1"
						$html          = str_replace($script, '', $html);
						$moveScripts[] = $script;
					}
				}

				// Move IE-Contitional-Comments containing scripts
				if (preg_match_all('#<!(--)?(?=\[)(?:(?!<!\[endif\]\1>).)*<!\[endif\]\1>#Uis', $html, $matches))
				{
					foreach ($matches[0] as $_iecc)
					{
						if (stripos($_iecc, '</script') !== false)
						{
							// Ignore html5.js (must be on top)
							if (stripos($_iecc, 'media/jui/js/html5.js') === false)
							{
								$html          = str_replace($_iecc, '', $html);
								$moveScripts[] = $_iecc;
							}
						}
					}
				}

				// Set scripts before closing body ends
				$_pos = strrpos($html, '</body>');
				if ($_pos !== false)
				{
					// Affects only the last closing body tag that was found
					$html = substr_replace($html, implode("\n", $moveScripts) . "\n</body>", $_pos, 7);
				}
			}
		}

		// Set the new html
		$this->set('__html__', $html);

		// Exec queue
		$this->execMethodQueue('prepareMarkup');

		return $html;
	}

	/**
	 * Wrapper function for things to optimize html markup
	 *
	 * @param   string $html html string
	 *
	 * @access public
	 * @return string $body optimized html string
	 */
	public function optimizeMarkup($html = '')
	{
		if ( ! $this->isJYAML)
		{
			return $html;
		}

		$html = $this->prepareMarkup($html);

		// Special html tags to ignore
		$specialTags = array(
			'textarea',
			'pre'
		);

		// Replace specail tags with tokens
		$_reverse = array();
		foreach ($specialTags as $tag)
		{
			$tag = preg_quote($tag);

			if (preg_match_all('#<' . $tag . '[^>]*>.*</' . $tag . '>#Uis', $html, $matches))
			{
				foreach ($matches[0] as $k => $contents)
				{
					$replace            = '___STARTJYAMLTOKEN_removeEmptyLinesPreReverse_' . $tag . '_' . $k . '_ENDJYAMLTOKEN___';
					$html               = str_replace($contents, $replace, $html);
					$_reverse[$replace] = $contents;
				}
			}
		}

		// Remove all empty lines in HTML to reduce bytes and nicer source look
		$html = $this->removeEmptyLines($html);
		$this->addLog('Removed empty lines from html document to reduce size');

		// Reverse tokens with contents for special tags
		foreach ($_reverse as $token => $contents)
		{
			$html = str_replace($token, $contents, $html);
		}

		// Add debug html
		$this->set('__html__', $html);
		$this->debug();
		$html = $this->get('__html__');

		return $html;
	}

	/**
	 * Check the JS/CSS Chunker functionaltity
	 *
	 * @access protected
	 * @return void
	 */
	protected function chunkJsCssCheck()
	{
		if ( ! $this->params->get('chunkCssFiles') && ! $this->params->get('chunkJsFiles'))
		{
			return;
		}

		$caching   = $this->params->get('chunkCache');
		$cachePath = JFactory::getConfig()->get('cache_path', JPATH_SITE . DIRECTORY_SEPARATOR . 'cache');

		if ($caching && ! self::_isWritable($cachePath))
		{
			$_path = str_replace(JPATH_ROOT, '', $cachePath);
			JFactory::getApplication()->enqueueMessage(JText::sprintf('JYAML__ERROR_CHUNK_CHECK_CACHEDIR_IS_WRITABLE', $_path), 'warning');
			$caching = false;
		}

		if ( ! $caching)
		{
			JFactory::getApplication()->enqueueMessage(JText::_('JYAML__NOTICE_CHUNK_PERFORMACE_NOTICE'), 'notice');
		}

	}

	/**
	 * Execute JS/CSS Chunker
	 * Prepare the document header if enabled
	 *
	 * @access protected
	 * @return void
	 */
	protected function chunkJsCss()
	{
		if ( ! $this->params->get('chunkCssFiles') && ! $this->params->get('chunkJsFiles'))
		{
			$this->removeChunkParameter();
			return;
		}

		$document = JFactory::getDocument();
		$errors   = array();

		// Check head has data
		$data = $document->getHeadData();
		if (empty($data))
		{
			return;
		}

		// Get params
		$includeExternals = $this->params->get('chunkIncludeExternals');
		$jsCompressor     = $this->params->get('chunkJsCompressor', '0');
		$useFiletimeHash  = $this->params->get('chunkCacheFiletimeHash', '1');
		$cdnHost          = $this->params->get('chunkCdn', '');
		$caching          = $this->params->get('chunkCache');

		// Initialize variables
		$cachePath        = JFactory::getConfig()->get('cache_path', JPATH_SITE . DIRECTORY_SEPARATOR . 'cache');
		$cachePath        = $cachePath . DIRECTORY_SEPARATOR . 'tpl_' . $this->template . '_head';
		$cacheOnlyPathUrl = JPath::clean(str_replace(JPATH_ROOT . DIRECTORY_SEPARATOR, '', $cachePath) . '/', '/');
		$cachePathUrl     = JURI::base(true) . '/' . $cacheOnlyPathUrl;
		$targetUrl        = $cachePathUrl;

		// Activate filesize logging when debug is enabled
		$logFilesize = false;
		if ($this->isDebug())
		{
			$logFilesize = true;
		}

		// Clone URI (clone cause prevent global instance changes)
		$uri = clone JURI::getInstance(JURI::base(false));

		// Url' to local filesystem mapping (much better performance)
		$urlToLocalMap = array(
			JPATH_ROOT . DIRECTORY_SEPARATOR => array(
				$uri->toString(
					array(
						'host',
						'port',
						'path'
					)
				)
			)
		);

		// Define the CDN host
		if ($cdnHost)
		{
			$uri->setHost($cdnHost);
			$uri->setPath($uri->getPath() . $cacheOnlyPathUrl);
			$cachePathUrl = $uri->toString();
		}

		// Create the chunker instance
		$chunker = new JsCssChunker(
			JURI::base(false),
			array(
				'targetUrl'                 => $targetUrl,
				'protocolRelative'          => ($this->params->get('chunkCssProtocolRelative') ? true : false),
				'javascriptCompress'        => (empty($jsCompressor) ? false : true),
				'javascriptCompressorClass' => $jsCompressor,
				'javaBin'                   => $this->params->get('chunkJavaBin', 'java'),
				'logFilesize'               => $logFilesize,
				'url_to_local_map'          => $urlToLocalMap
			)
		);

		// Check the root cache directory is writable
		if ($caching && ! is_writable(JPATH_ROOT . DIRECTORY_SEPARATOR . 'cache'))
		{
			$caching = false;
		}

		// Create the cache subdirectory if not exists
		if ($caching && ! JFolder::exists($cachePath))
		{
			JFolder::create($cachePath);
		}

		// Chunk CSS files, if enabled
		if ($this->params->get('chunkCssFiles'))
		{
			$fromCache = false;

			$stylesheets    = (isset($data['styleSheets']) && ! empty($data['styleSheets'])) ? $data['styleSheets'] : array();
			$stylesheetsNew = $stylesheets;

			foreach ($stylesheets as $file => $attribs)
			{
				// Exclude files from libraies directory
				if (preg_match('#^' . $this->getUrl('libraries', '') . 'assets' . '#i', $file))
				{
					continue;
				}

				// Only files with .css

				// Remove params
				$tmp           = explode('?', $file);
				$checkFilename = $tmp[0];
				$fileParams    = isset($tmp[1]) ? (string) $tmp[1] : '';
				if ( ! preg_match("/.css$/i", $checkFilename))
				{
					continue;
				}
				if ( ! empty($fileParams) && strpos($fileParams, 'noChunk') !== false)
				{
					continue;
				}

				$type  = isset($attribs['mime']) ? $attribs['mime'] : 'text/css';
				$media = isset($attribs['media']) ? $attribs['media'] : 'all';

				// JURI::isInternal does not support protocol realtive so we need to replace // into http://
				$filetmp    = preg_replace('#^//#', 'http://', $file);
				$isExternal = ! self::isFileOrUrlInternal($filetmp);

				if ( ! $isExternal)
				{
					$chunker->addStylesheet($file, $type, $media);
					unset($stylesheetsNew[$file]);
				}
				elseif ($includeExternals)
				{
					$chunker->addStylesheet($file, $type, $media);
					unset($stylesheetsNew[$file]);
				}
			}

			$hash      = $chunker->getStylesheetHash($cachePath);
			$cacheFile = $cachePath . DIRECTORY_SEPARATOR . $hash . '.min.css';
			$cacheUrl  = $cachePathUrl . $hash . '.min.css';

			// Check for changes in css folder
			if ($caching && file_exists($cacheFile))
			{
				$hasModifications = $chunker->hasFoldersModifications($this->getPath('css'), $cacheFile, '.css$');

				// Delete cache file if css folder has changes
				if ($hasModifications)
				{
					JFile::delete($cacheFile);
					$fromCache = false;

					$chunker->addLog('Stylesheet - Changes in Folder /css/ detected. Cache deleted: ' . $cacheFile);
				}
			}

			if ($caching && file_exists($cacheFile))
			{
				$buffer = file_get_contents($cacheFile);
				$chunker->addLog('Stylesheet - Load from cache: ' . $cacheFile);
				$fromCache = true;
			}
			else
			{
				$buffer = $chunker->chunkStylesheets();

				if ($buffer && ! $chunker->getErrors())
				{
					// Always save if no cachefile was found
					JFile::write($cacheFile, $buffer);
					$chunker->addLog('Stylesheet - Save content into cache: ' . $cacheFile);
				}
			}

			if ($buffer && ! $chunker->getErrors())
			{
				if (file_exists($cacheFile))
				{
					$document->_styleSheets = $stylesheetsNew;
					$chunker->addLog('Stylesheet - Removed original Stylesheets from head are chunked');

					// Filetime as url attribute for better handling with browser caching
					$fileTimeHash = '';
					if ($useFiletimeHash)
					{
						$_filetime    = @filemtime($cacheFile);
						$fileTimeHash = $_filetime ? '?' . $_filetime . '.css' : '';
					}

					$this->addStylesheet($cacheUrl . $fileTimeHash);
					$chunker->addLog('Stylesheet - Loaded chunk file: ' . $cacheUrl);
				}
				else
				{
					$errors[] = 'Stylesheet - Can\'t output chunked File. Cache file not found.';
				}
			}
		}

		// Chunk Javascript files, if enabled
		$fromCache = false;
		if ($this->params->get('chunkJsFiles'))
		{
			$scripts    = (isset($data['scripts']) && ! empty($data['scripts'])) ? $data['scripts'] : array();
			$scriptsNew = $scripts;

			foreach ($scripts as $file => $type)
			{
				// Exclude files from libraies assets directory
				if (preg_match('#^' . $this->getUrl('libraries', '') . 'assets' . '#i', $file))
				{
					continue;
				}

				// Only files with .js

				// Remove params
				$tmp           = explode('?', $file);
				$checkFilename = $tmp[0];
				$fileParams    = isset($tmp[1]) ? (string) $tmp[1] : '';
				if ( ! preg_match("/.js$/i", $checkFilename))
				{
					continue;
				}
				if ( ! empty($fileParams) && strpos($fileParams, 'noChunk') !== false)
				{
					continue;
				}

				// JURI::isInternal does not support protocol realtive so we need to replace // into http://
				$filetmp    = preg_replace('#^//#', 'http://', $file);
				$isExternal = ! self::isFileOrUrlInternal($filetmp);

				if ( ! $isExternal)
				{
					$chunker->addJavascript($file, $type);
					unset($scriptsNew[$file]);
				}
				elseif ($includeExternals)
				{
					$chunker->addJavascript($file, $type);
					unset($scriptsNew[$file]);
				}
			}

			$hash      = $chunker->getJavascriptHash($cachePath);
			$cacheFile = $cachePath . DIRECTORY_SEPARATOR . $hash . '.min.js';
			$cacheUrl  = $cachePathUrl . $hash . '.min.js';

			// Check for changes in script folder
			if ($caching && file_exists($cacheFile))
			{
				$hasModifications = $chunker->hasFoldersModifications($this->getPath('script'), $cacheFile, '.js$');

				// Delete cache file if script folder has changes
				if ($hasModifications)
				{
					JFile::delete($cacheFile);
					$fromCache = false;

					$chunker->addLog('Javascript - Changes in Folder /script/ detected. Cache deleted: ' . $cacheFile);
				}
			}
			if ($caching && file_exists($cacheFile))
			{
				$buffer = file_get_contents($cacheFile);
				$chunker->addLog('Javascript - Load from cache: ' . $cacheFile);
				$fromCache = true;
			}
			else
			{
				$buffer = $chunker->chunkJavascripts();
				if ($buffer && ! $chunker->getErrors())
				{
					// Always save if no cachefile was found
					JFile::write($cacheFile, $buffer);
					$chunker->addLog('Javascript - Save content into cache: ' . $cacheFile);
				}
			}

			if ($buffer && ! $chunker->getErrors())
			{
				if (file_exists($cacheFile))
				{
					$document->_scripts = $scriptsNew;
					$chunker->addLog('Javascript - Removed original Scripts from head are chunked');

					// Filetime as url attribute for better handling with browser caching
					$fileTimeHash = '';
					if ($useFiletimeHash)
					{
						$_filetime    = @filemtime($cacheFile);
						$fileTimeHash = $_filetime ? '?' . $_filetime . '.js' : '';
					}

					$this->addScript($cacheUrl . $fileTimeHash);
					$chunker->addLog('Javascript - Loaded chunk file: ' . $cacheUrl);
				}
				else
				{
					$errors[] = 'Javascript - Can\'t output chunked File. Cache file not found.';
				}
			}
		}

		$logs = $chunker->getLogs(false);

		if ($logFilesize && ! $fromCache)
		{
			$sizeLog = $chunker->sizeLog;

			foreach (array('stylesheet', 'javascript') as $ctype)
			{
				$sizeBefore = ! empty($sizeLog['before'][$ctype]) ? $sizeLog['before'][$ctype] : 0;
				$sizeAfter  = ! empty($sizeLog['after'][$ctype]) ? $sizeLog['after'][$ctype] : 0;
				$_percent   = 0;

				if ($sizeBefore && $sizeBefore)
				{
					$_percent                         = (100 - ($sizeAfter * 100 / $sizeBefore));
					$_percent                         = ($_percent < 0 ? 0 : round($_percent, 2));
					$sizeLog['reduced-ratio'][$ctype] = $_percent . '%';
				}
			}

			$logs['fileSize'] = $sizeLog;
		}
		elseif ($logFilesize)
		{
			$logs['fileSize'] = 'Filesize (before/after) only logged without caching';
		}

		if ( ! empty($logs))
		{
			array_push($this->log, array('JsCssChunker' => $logs));
		}

		$errors = array_merge($chunker->getErrors(false), $errors);
		if ( ! empty($errors))
		{
			$errors = array_merge(array('Javascript/Stylesheet files are not optimized in html head, because errors'), $errors);
			array_push($this->errorLog, array('JsCssChunker' => $errors));
		}

		$this->removeChunkParameter();

		// Add htaccess file to enable mod_deflate (gzip), if available.
		if ( ! file_exists($cachePath . '/.htaccess'))
		{
			$_htcont = '';
			$_htcont .= '# JYAML - Template Framework includes YAML for Joomla! (http://www.jyaml.de)' . "\n\n";
			$_htcont .= '# Use of mod_deflate to compress (gzip) JavaScript and CSS files.' . "\n";
			$_htcont .= '<IfModule mod_deflate.c>' . "\n";
			$_htcont .= "\t" . '<FilesMatch "\.(css|js)$">' . "\n";
			$_htcont .= "\t\t" . 'SetOutputFilter DEFLATE' . "\n";
			$_htcont .= "\t" . '</FilesMatch>' . "\n";
			$_htcont .= '</IfModule>' . "\n";
			$_htcont .= '# Use of mod_expires to cache JavaScript, CSS files based on last modified date for 1 month.' . "\n";
			$_htcont .= '<IfModule mod_expires.c>' . "\n";
			$_htcont .= "\t" . 'ExpiresActive On' . "\n";
			$_htcont .= "\t" . 'ExpiresByType text/css M2592000' . "\n";
			$_htcont .= "\t" . 'ExpiresByType text/javascript M2592000' . "\n";
			$_htcont .= "\t" . 'ExpiresByType application/x-javascript M2592000' . "\n";
			$_htcont .= "\t" . 'ExpiresByType application/javascript M2592000' . "\n";
			$_htcont .= '</IfModule>' . "\n";

			file_put_contents($cachePath . '/.htaccess', $_htcont);
		}
	}

	/**
	 * Removes the noCunk parameter from script and stylesheet source urls to improve browser caching
	 * Note: the noChunk=1 parameter is for internal usage to ignore files in JsCssChunker
	 *
	 * @return void
	 */
	protected function removeChunkParameter()
	{
		$document = JFactory::getDocument();

		if (isset($document->_scripts))
		{
			$scripts = array();

			foreach ($document->_scripts as $_src => $attr)
			{
				if (strpos($_src, 'noChunk=1') !== false)
				{
					$_uri = clone JURI::getInstance($_src);
					$_uri->delVar('noChunk');
					$_src = $_uri->toString();
				}
				$scripts[$_src] = $attr;
			}
			$document->_scripts = $scripts;
		}

		if (isset($document->_styleSheets))
		{
			$stylesheets = array();

			foreach ($document->_styleSheets as $_src => $attr)
			{
				if (strpos($_src, 'noChunk=1') !== false)
				{
					$_uri = clone JURI::getInstance($_src);
					$_uri->delVar('noChunk');
					$_src = $_uri->toString();
				}

				$stylesheets[$_src] = $attr;
			}
			$document->_styleSheets = $stylesheets;
		}
	}

	/**
	 * Convert a Jdoc array to string
	 *
	 * @param   array $position Jdoc Statement as Array
	 *
	 * @access public
	 * @return string jdoc statement content
	 */
	public function convertToJdocString($position = array())
	{
		if (is_object($position))
		{
			$position = \Joomla\Utilities\ArrayHelper::fromObject($position);
		}

		$type = isset($position['type']) ? $position['type'] : '';

		if (empty($type))
		{
			return '';
		}

		$name  = isset($position['name']) ? $position['name'] : '';
		$style = isset($position['style']) ? $position['style'] : 'none';

		$attributes = '';
		if (isset($position['__attributes']))
		{
			$attributes = $position['__attributes'];

			if (($_isObject = is_object($attributes)) || is_array($attributes))
			{
				if ($_isObject)
				{
					$attributes = \Joomla\Utilities\ArrayHelper::fromObject($attributes);
				}
				if (isset($attributes['params']))
				{
					$attributes = \Joomla\Utilities\ArrayHelper::toString($attributes['params']);
				}
			}
			else
			{
				$attributes = trim($position['__attributes']);
			}
		}

		$outerHTML = isset($position['__outerHTML']) ? trim($position['__outerHTML']) : '';

		if ($attributes)
		{
			$attributes = ' ' . $attributes;
		}

		$content = '';

		if ( ! empty($type))
		{
			$content = '<jdoc:include type="' . $type . '"';

			if ($name)
			{
				$content .= ' name="' . $name . '"';
			}
			if ($style)
			{
				$content .= ' style="' . $style . '"';
			}
			if ($attributes)
			{
				$content .= $attributes;
			}

			$content .= ' />';
		}

		if ($outerHTML && $outerHTML != '{$position}' && strpos($outerHTML, '{$position}') !== false)
		{
			$content = str_replace('{$position}', $content, $outerHTML);
		}

		return $content;
	}

	/**
	 * Convert a JYAML Positions Array(or Object) on a specific namespace to an jdoc string
	 *
	 * @param   string $namespace Position namespace
	 * @param   mixed  $positions Positions
	 *
	 * @access public
	 * @return string jdoc statement contents
	 */
	public function convertPositionArrayToJdoc($namespace, $positions = array())
	{
		$contents = '';

		if (is_object($positions))
		{
			$positions = \Joomla\Utilities\ArrayHelper::fromObject($positions);
		}

		if ( ! is_array($positions))
		{
			$positions = trim($positions);
			if ($positions)
			{
				$contents = '<jdoc:include ' . $positions . ' />';
			}
		}
		else
		{
			if ( ! empty($namespace) && isset($positions[$namespace]) && ! empty($positions[$namespace]))
			{
				$jdocArray = array();

				foreach ($positions[$namespace] as $pos)
				{
					$jdocArray[] = $this->convertToJdocString($pos);
				}

				$contents = implode("\n", $jdocArray);
			}
		}

		return $contents;
	}

	/**
	 * Replace jdoc from type jyaml in html into real jdoc types by configuration
	 *
	 * @access private
	 * @return void
	 */
	private function _prepareJdocTypeJYAMLtoReal()
	{
		$html      = $this->get('__html__');
		$positions = $this->params->get('positions', array());

		if (preg_match_all('#<jdoc:include\ type="jyaml" (.*)\/>#iU', $html, $matches))
		{
			if (is_object($positions))
			{
				$positions = \Joomla\Utilities\ArrayHelper::fromObject($positions);
			}

			foreach ($matches[0] as $i => $jdoc)
			{
				$attribs = JUtility::parseAttributes($matches[1][$i]);
				$posname = isset($attribs['name']) ? $attribs['name'] : '';

				$jdocArr = array();

				if ( ! empty($posname) && isset($positions[$posname]))
				{
					foreach ($positions[$posname] as $position)
					{
						$jdocArr[] = $this->convertToJdocString($position);
					}
				}

				$jdocContents = implode("\n", $jdocArr);

				if ((int) $this->isDebug('tp'))
				{
					$title = '';
					if (isset($positions[$posname]))
					{
						$title = 'Positions:[BR]';
						$info  = array();
						foreach ($positions[$posname] as $_pos)
						{
							if (empty($_pos['type']))
							{
								continue;
							}

							$info[] = $_pos['type'] . (! empty($_pos['name']) ? ':[' . $_pos['name'] . ']' : '');
						}

						$title .= implode("[BR]", $info);
					}

					$_tmp         = $jdocContents;
					$jdocContents = '<div class="jyaml-debug-dynamic-positions"><div class="jyaml-debug-dynamic-position jyaml-debug-tip" title="' . $title . '">';
					$jdocContents .= htmlentities($jdoc) . '</div>' . $_tmp . '</div>';
				}

				$html = str_replace($jdoc, $jdocContents, $html);
			}
		}

		$this->set('__html__', $html);
	}

	/**
	 * Set script declaration for global js functions to access jyaml path's
	 *
	 * @access private
	 * @return string Script Declaration
	 */
	private function _getGlobalJsFunctions()
	{
		$js = 'var JYAML=this.JYAML={};';
		$js .= 'JYAML.get=function(type){';
		$js .= 'this.template="' . $this->template . '";';
		$js .= 'this.templatePath="' . $this->getPath('template', false) . '";';
		$js .= 'this.imagePath="' . $this->getPath('image', false) . '";';
		$js .= 'this.scriptPath="' . $this->getPath('script', false) . '";';
		$js .= 'this.cssPath="' . $this->getPath('css', false) . '";';
		$js .= 'this.yamlPath="' . $this->getPath('yaml', false) . '";';
		$js .= 'this.libraries="' . $this->getPath('libraries', false) . '";';
		$js .= 'this.blankIMG=this.templatePath + "/images/blank.gif";';
		$js .= 'if(typeof(type)!=\'undefined\'){return (typeof(this[type])!=\'undefined\')?this[type]:\'\'}else{return this}';
		$js .= '};';

		$script = '<script type="text/javascript">' . $this->trimmer($js) . '</script>';

		return $script;
	}

	/**
	 * Initialisize Layout(-names) for viewing an set to object
	 * Note: col3_content is always enabled except on 'disable_columns' as static_layout
	 *
	 * @access private
	 * @return void
	 */
	private function _initLayout()
	{
		$this->_initModules();

		$layout_static = trim($this->params->get('layout_static', ''));
		$col1Count     = $this->getPositionCount('col1_content');
		$col2Count     = $this->getPositionCount('col2_content');

		if ($layout_static == 'disable_columns')
		{
			$this->setPositionCount('col1_content', 0);
			$this->setPositionCount('col2_content', 0);
			$this->setPositionCount('col3_content', 0);
			$this->active_layout = 'cols_hide';
		}
		else
		{
			if ($layout_static)
			{
				$this->active_layout = $layout_static;
			}
			elseif ($col1Count && $col2Count)
			{
				$this->active_layout = $this->params->get('layout_3col');
			}
			elseif ($col1Count && ! $col2Count)
			{
				$this->active_layout = $this->params->get('layout_2col_1');
			}
			elseif ( ! $col1Count && $col2Count)
			{
				$this->active_layout = $this->params->get('layout_2col_2');
			}
			else
			{
				$this->active_layout = $this->params->get('layout_1col');
			}

			$layout = explode('_', $this->active_layout);

			if (isset($layout[1]))
			{
				$layout[2] = $layout[1];

				// Disable col1_content if not 1 in layout name
				if (strpos($layout[2], '1') === false)
				{
					$this->setPositionCount('col1_content', 0);
				}

				// Disable col1_content if not 2 in layout name
				if (strpos($layout[2], '2') === false)
				{
					$this->setPositionCount('col2_content', 0);
				}
			}
			else
			{
				// Fallback if layout name is not valid (all columns enabled with default layout 3col_132)
				$this->setError('Layout not found or invalid name format of: ' . $this->active_layout);

				$this->active_layout = '3col_132';

				if ( ! $col1Count)
				{
					$this->setPositionCount('col1_content:fallback_position', 1);
				}
				if ( ! $col2Count)
				{
					$this->setPositionCount('col2_content:fallback_position', 1);
				}
			}
		}

		self::addLog('Layout initialized');
	}

	/**
	 * Init (Module) Positions
	 * Set the counter of dynamic and general positions in class
	 *
	 * @access private
	 * @return void
	 */
	private function _initModules()
	{
		// Do not render modules when document type is not html
		if (JFactory::getDocument()->getType() != 'html')
		{
			// Disable debug for this case
			$this->params->set('debug', 0);
			return;
		}

		static $_modulesCache = array();

		if (empty($this->positions_count))
		{
			if (constant('JDEBUG'))
			{
				JProfiler::getInstance('Application')->mark('JYAML::_initModules before:(preCount/preRender)');
			}

			// $cache = $this->getCache('callback');

			$document     = JFactory::getDocument();
			$canPreRender = method_exists($document, 'setIsPreRender');
			if ( ! $canPreRender)
			{
				$this->setError('JDocumentHTML is not overwritten by JYAML. Maybe an other externsion overwrites the class, too!');
			}

			$allowedTags       = '<img><object><embed><iframe><output><source><video><audio><canvas>';
			$countEmptyModules = $this->params->get('layout_countemptymodules');

			$container = $this->params->get('positions', array());

			if (is_object($container))
			{
				$container = \Joomla\Utilities\ArrayHelper::fromObject($container);
			}

			$globalPositionNames = JYAMLModuleHelper::getLoadedModulePositions(false);
			$container           = array_merge($container, array('__global__' => JYAMLModuleHelper::getLoadedModulePositions(true)));

			foreach ($container as $namespace => $positions)
			{
				if (in_array($namespace, $globalPositionNames))
				{
					$this->setError(
						'The JYAML Dynamic Position ' . $namespace
						. ' is also a module position. Please change the name of the module or dynamic position.'
					);
					break;
				}

				foreach ($positions as $position)
				{
					$count = 0;

					$name = isset($position['name']) ? $position['name'] : '';
					$type = isset($position['type']) ? $position['type'] : '';

					$modules = array();

					if ($type == 'modules')
					{
						if ( ! isset($_modulesCache[$name]))
						{
							$modules              = JModuleHelper::getModules($name);
							$_modulesCache[$name] = $modules;
						}
						else
						{
							$modules = $_modulesCache[$name];
						}
					}
					elseif ($type == 'module')
					{
						if ( ! isset($_modulesCache[$name]))
						{
							$modules[0]           = JModuleHelper::getModule($name);
							$_modulesCache[$name] = $modules;
						}
						else
						{
							$modules = $_modulesCache[$name];
						}
					}
					else
					{
						// Count other types like component and message

						if ($type == 'message')
						{
							// Ignore
							$buffer = '';
						}
						else
						{
							$buffer = self::doc()->getBuffer($type);
							$buffer = $this->trimmer($buffer, true, $allowedTags);
						}

						if ($buffer != "")
						{
							$this->positions_count[$namespace]['#' . $type]   = 1;
							$this->positions_count['__global__']['#' . $type] = 1;
						}

						// Goto next - module parsing not needed here
						continue;
					}

					foreach ($modules as $module)
					{
						$modParams = new \Joomla\Registry\Registry;
						if (isset($module->params))
						{
							$modParams->loadString($module->params, 'JSON');
						}
						$countEmpty = $this->getExtConfig('mod', 'layout_countemptymodule', $countEmptyModules, $modParams);

						if ($countEmpty)
						{
							$count ++;
							$module->isCountEmpty = true;
							$this->registerModule($module);
						}
						else
						{
							if ($canPreRender)
							{
								$document->setIsPreRender(true, 'mod.' . $module->position . '.' . $module->id);
							}

							/*
							 * Permanent caching disabled, leave Joomlas own module cache if enabled.
							 * Each module have custom caching like (itemid, safeuri, etc)
							 */
							// $buffer = $cache->call(array('JYAMLModuleHelper', 'getRawModuleContent'), $module);
							$buffer = JYAMLModuleHelper::getRawModuleContent($module);
							$buffer = $this->trimmer($buffer, true, $allowedTags);

							if ($canPreRender)
							{
								$document->setIsPreRender(false);
							}

							if ($buffer != "")
							{
								$count ++;
								$module->isCountEmpty = false;
								$this->registerModule($module);
							}
							else
							{
								$this->emptyModules[] = $module->module . ' (Id: ' . $module->id . ')';
							}
						}
					}

					$this->positions_count[$namespace][$name] = $count;
				}

				if ($namespace != '__global__')
				{
					// Add namespace available in global as summary
					if (isset($this->positions_count[$namespace]))
					{
						$this->positions_count['__global__'][$namespace] = array_sum($this->positions_count[$namespace]);
					}
				}
			}

			if (constant('JDEBUG'))
			{
				JProfiler::getInstance('Application')->mark('JYAML::_initModules after:(preCount/preRender)');
			}
		}
	}

	/**
	 * Extended Version of countModules from JDocumentHtml
	 * with dynamic Position feature by namespaces
	 *
	 * @param   string $condition Name of position or dynamic postion with namespace:position|#type
	 *
	 * @access public
	 * @return int Count
	 */
	public function countModules($condition = '')
	{
		if ($this->isDebug())
		{
			if ((int) $this->isDebug('tp') == 2)
			{
				return true;
			}
		}

		$operators = '(\+|\-|\*|\/|==|\!=|\<\>|\<|\>|\<=|\>=|and|or|xor)';

		$words = preg_split('# ' . $operators . ' #i', $condition, null, PREG_SPLIT_DELIM_CAPTURE);

		for ($i = 0, $n = count($words); $i < $n; $i += 2)
		{
			$name      = strtolower($words[$i]);
			$words[$i] = $this->getPositionCount($name);
		}

		$result = eval('return intval(' . implode(' ', $words) . ');');

		return $result;
	}

	/**
	 * Get the count of a specific position or namespace position
	 *
	 * @param   string $pos Name of position or dynamic postion with namespace:position|#type
	 *
	 * @access public
	 * @return integer Count of the specific position
	 */
	public function getPositionCount($pos = '')
	{
		if ($this->isDebug())
		{
			if ((int) $this->isDebug('tp') == 2)
			{
				return 1;
			}
		}

		$count = 0;

		if ( ! $pos)
		{
			return $count;
		}

		$pos = explode(':', $pos);

		if (count($pos) == 1 && isset($this->positions_count['__global__']))
		{
			$position = $pos[0];

			if ($position == '#message')
			{
				$count = $this->hasMessages() ? 1 : 0;
				$this->setPositionCount($position, $count);
			}
			else
			{
				if (isset($this->positions_count['__global__'][$position]))
				{
					$count = $this->positions_count['__global__'][$position];
				}
			}
		}
		elseif (count($pos) == 2)
		{
			$name     = $pos[0];
			$position = $pos[1];

			if ($position == '#message')
			{
				$count = $this->hasMessages() ? 1 : 0;
				$this->setPositionCount($name . ':' . $position, $count);
			}
			else
			{
				if (isset($this->positions_count[$name]) && is_array($this->positions_count[$name]))
				{
					if (isset($this->positions_count[$name][$position]))
					{
						// Specific position
						$count = $this->positions_count[$name][$position];
					}
					elseif ($position == '*')
					{
						// Wildcard *=all child summary count
						$count = array_sum($this->positions_count[$name]);
					}
				}
			}
		}

		if (is_array($count))
		{
			$count = array_sum($count);
		}

		return $count;
	}

	/**
	 * Set the count of a specific position
	 *
	 * @param   string  $pos   Position condition ('positonname' or 'namespace:positionname')
	 * @param   integer $count Count of position condition
	 *
	 * @access public
	 * @return void
	 */
	public function setPositionCount($pos = '', $count = 0)
	{
		if ( ! $pos)
		{
			return;
		}

		$pos = explode(':', $pos);

		if (count($pos) == 1 && isset($this->positions_count['__global__']))
		{
			$position = $pos[0];

			if ( ! isset($this->positions_count[$position]))
			{
				$this->positions_count[$position] = array('_func_setPositionCount_adjustment*', $count);
			}
			else
			{
				// Does not overwrite existing positions - just add an adjusment count
				$adjustment                                                             = $count - array_sum($this->positions_count[$position]);
				$this->positions_count[$position]['_func_setPositionCount_adjustment*'] = $adjustment;
			}

			$this->positions_count['__global__'][$position] = $count;
		}
		elseif (count($pos) == 2)
		{
			$name     = $pos[0];
			$position = $pos[1];

			// Check is valid jyaml-namespace. must be registered in global
			if (isset($this->positions_count['__global__'][$name]) && ! isset($this->positions_count[$name]))
			{
				return;
			}

			if ( ! isset($this->positions_count[$name]))
			{
				$this->positions_count[$name] = array();
			}
			if ( ! isset($this->positions_count['__global__'][$name]))
			{
				$this->positions_count['__global__'][$name] = 0;
			}

			if ($position == '*')
			{
				// Does not overwrite existing positions - just add an adjusment count
				$adjustment                                                             = $count - array_sum($this->positions_count[$position]);
				$this->positions_count[$position]['_func_setPositionCount_adjustment*'] = $adjustment;
			}
			else
			{
				$this->positions_count[$name][$position] = $count;
			}

			$this->positions_count['__global__'][$name] = array_sum($this->positions_count[$name]);
		}
	}

	/**
	 * Add an Skiplink
	 *
	 * @param   string  $anchor       Anchor name of skiplink
	 * @param   string  $text         Skiplink text
	 * @param   string  $prefixtext   Prepend to text
	 * @param   string  $condition    Additional check with a countModules contition
	 * @param   boolean $withNameAttr With name Attribute or not. (The name attribute is obsolete in HTML5)
	 *
	 * @access public
	 * @return string Anchor html link
	 */
	public function addSkiplink($anchor = '', $text = '', $prefixtext = '', $condition = '', $withNameAttr = true)
	{
		if ($anchor && $text && empty($condition) || $this->countModules($condition))
		{
			$this->skiplink[$anchor] = array('prefixtext' => $prefixtext, 'text' => $text);

			$this->addLog('Skiplink added: #' . $anchor . ' (' . JText::_($text) . ')');

			return '<a id="' . $anchor . '"' . ($withNameAttr ? ' name="' . $anchor . '"' : '') . ' class="skiplink-anchor"></a>' . "\n";
		}

		return '';
	}

	/**
	 * Remove an Skiplink by anchor
	 *
	 * @param   string $anchor Anchor name of skiplink to remove (use * to remove all)
	 *
	 * @access public
	 * @return void
	 */
	public function removeSkiplink($anchor = '')
	{
		if ( ! $anchor)
		{
			return;
		}

		if ($anchor == '*')
		{
			$this->skiplink = array();
		}

		foreach ($this->skiplink as $key)
		{
			if ($key == $anchor)
			{
				unset($this->skiplink[$key]);
			}
		}
	}

	/**
	 * Get HTML for Skiplinks
	 *
	 * @param   string $outer      Outer HTML tag, defaults 'ul'
	 * @param   string $wrap       Wrapper html tag for links, defaults 'li'
	 * @param   string $outerClass CSS class for outer, defaults 'ym-skiplinks'
	 * @param   string $wrapClass  CSS class for wrap, defaults ''
	 * @param   string $linkClass  CSS class for link/anchor, defaults 'ym-skip'
	 *
	 * @access public
	 * @return string $html HTML code for skiplink html
	 */
	public function getSkiplinkHtml(
		$outer = 'ul',
		$wrap = 'li',
		$outerClass = 'ym-skiplinks',
		$wrapClass = '',
		$linkClass = 'ym-skip'
	) {
		$html = '';
		$html .= '<' . $outer . ($outerClass ? ' class="' . $outerClass . '"' : '') . '>';

		foreach ($this->skiplink as $anchor => $text)
		{
			$transtext = '';

			if ( ! empty($text['text']))
			{
				$transtext = JText::_($text['text']);
			}
			if ( ! empty($text['prefixtext']))
			{
				$transtext = JText::_($text['prefixtext']) . ': ' . $transtext;
			}

			$html .= '<' . $wrap . ($wrapClass ? ' class="' . $wrapClass . '"' : '') . '><a' . ($linkClass ? ' class="' . $linkClass . '"' : '');
			$html .= ' href="#' . $anchor . '">' . $transtext . '</a></' . $wrap . '> ';
		}

		$html .= '</' . $outer . '>' . "\n";

		return $html;
	}

	/**
	 * Get an specific Path
	 *
	 * @param   string  $key      Type/Name of path
	 * @param   boolean $absolute Define return path is absolute or relative
	 * @param   string  $file     Optional append filename
	 *
	 * @access public
	 * @return string Specific path was found
	 */
	public function getPath($key, $absolute = true, $file = '')
	{
		$path = '';

		if ($absolute)
		{
			$key .= '.absolute';
		}

		if (isset($this->paths[$key]))
		{
			$path = $this->paths[$key];

			if ('' != $file)
			{
				$path .= DIRECTORY_SEPARATOR . $file;
				$path = JPath::clean($path, ($absolute ? DIRECTORY_SEPARATOR : '/'));
			}
		}

		return $path;
	}

	/**
	 * Get a specific Url form Path (optional with filename)
	 *
	 * @param   string $key  Type/Name of path
	 * @param   string $file Filename
	 *
	 * @access public
	 * @return string Specific Url of path
	 */
	public function getUrl($key, $file = '')
	{
		if (preg_match('#^http#', $key))
		{
			$path = $key;
		}
		elseif (preg_match('#^http#', $file))
		{
			$path = $file;
		}
		else
		{
			$path = $this->getPath($key, false) . '/' . $file;
		}

		return $path;
	}

	/**
	 * Set specific path
	 *
	 * @param   string $key   Name of path
	 * @param   string $value Value
	 *
	 * @access protected
	 * @return void
	 */
	protected function setPath($key, $value)
	{
		if (preg_match('#^' . preg_quote(JPATH_ROOT, '#') . '#i', $value))
		{
			$key .= '.absolute';
		}

		$this->paths[$key] = $value;
	}

	/**
	 * Get an extension configuration value from template params
	 *
	 * @param   string $ext       Extension Name (ex. 'mod_menu')
	 * @param   string $key       Name of config value (syntax is: ext-[$ext]-[$key])
	 * @param   mixed  $default   Default value
	 * @param   object $extParams Optional extension params object to search the value (like a merge)
	 *
	 * @access public
	 * @return mixed value or $default
	 */
	public function getExtConfig($ext, $key, $default = null, $extParams = null)
	{
		$paramKey = 'ext-' . $ext . '-' . $key;

		if ($extParams && ($extParams instanceof \Joomla\Registry\Registry))
		{
			$extValue = $extParams->get($paramKey, $default);

			if ($extValue != null && $extValue != '_usedefault_')
			{
				return $extValue;
			}
		}

		return $this->params->get($paramKey, $default);
	}

	/**
	 * Method to overwrite/set an extension configuration value from template params
	 *
	 * @param   string $ext       Extension Name (ex. 'mod_menu')
	 * @param   string $key       Name of config value (syntax is: ext-[$ext]-[$key])
	 * @param   mixed  $value     The value to set
	 * @param   object $extParams Optional extension params object to search the value (like a merge)
	 *
	 * @access public
	 * @return void
	 */
	public function setExtConfig($ext, $key, $value, $extParams)
	{
		$paramKey = 'ext-' . $ext . '-' . $key;

		if ($ext && $key && $extParams && ($extParams instanceof \Joomla\Registry\Registry))
		{
			$extParams->set($paramKey, $value);
			$this->params->set($paramKey, $value);
		}
	}

	/**
	 * Set the current user Browser to class
	 *
	 * @access protected
	 * @return void
	 */
	protected function setBrowserEnvironment()
	{
		jimport('joomla.environment.browser');
		$browser = new JBrowser;

		$this->set('user_browser', $browser->getBrowser() . ' ' . $browser->getVersion());
		$this->set('user_agent', $browser->getAgentString());
		$this->set('user_platform', $browser->getPlatform());

		$this->addLog('Initialized Browser Enviroment');
	}

	/**
	 * Set current and active ItemIds to class
	 *
	 * @access protected
	 * @return void
	 */
	protected function setItemIds()
	{
		if (JFactory::getApplication()->isAdmin())
		{
			// Exclude admin
			return;
		}

		$menu   = JFactory::getApplication()->getMenu('site');
		$active = $menu->getActive();

		if ($active)
		{
			$active_tree   = $active->tree;
			$currentItemid = array_pop($active_tree);
		}
		else
		{
			$active_tree   = null;
			$currentItemid = null;
		}

		if ( ! $currentItemid)
		{
			$currentItemid = self::input()->get('Itemid', 0, 'int');
		}

		$this->currentItemID = $currentItemid;
		$this->parentItemIDs = (is_array($active_tree) && count($active_tree) ? $active_tree : array());
	}

	/**
	 * Add name of extension in class
	 *
	 * @param   string $extension The extension name
	 *
	 * @access protected
	 * @return void
	 */
	protected function addAcitveExtension($extension = '')
	{
		if ($extension && ! in_array($extension, $this->active_extensions))
		{
			$this->active_extensions[] = $extension;

			$this->addLog('Added active extension: ' . $extension);
		}
	}

	/**
	 * Register a module in object
	 *
	 * @param   object $module Module object
	 *
	 * @return void
	 */
	protected function registerModule($module)
	{
		if ( ! $module || ! is_object($module))
		{
			return;
		}

		if (empty($module->position))
		{
			$position = '_' . $module->module;
		}
		else
		{
			$position = $module->position;
		}

		if ( ! isset($this->registeredModules[$position]))
		{
			$this->registeredModules[$position] = array();
		}

		if ( ! isset($this->registeredModules[$position][$module->id]))
		{
			$this->registeredModules[$position][$module->id] = $module;
		}
	}

	/**
	 * Get all active modules of a specific position
	 * with consideration of param layout_countemptymodules
	 *
	 * @param   string $position Module position
	 *
	 * @return array
	 */
	public function getRegisteredModules($position = '')
	{
		if ($position)
		{
			$modules = isset($this->registeredModules[$position]) ? $this->registeredModules[$position] : array();
		}
		else
		{
			$modules = $this->registeredModules;
		}

		return $modules;
	}

	/**
	 * Generate string for open items for including as css class
	 *
	 * @access protected
	 * @return void
	 */
	protected function _generateBodyClassString()
	{
		$db = JFactory::getDBO();

		// Add is-homepage class
		if ($this->isHomepage() && $this->params->get('dynamic_body_classes-homepage'))
		{
			$this->addBodyCssClass('is', 'homepage');
		}

		// For more adjustable RTL support
		if ($this->direction == 'rtl')
		{
			$this->addBodyCssClass('dir', 'rtl');
		}

		// Add active html_file and html_folder alias
		$this->addBodyCssClass('tmpl', $this->params->get('html_folder'));
		$this->addBodyCssClass('html', $this->params->get('html_file'));

		// Add active column layout
		$this->addBodyCssClass('layout', $this->active_layout);

		// Add langtag if available in request
		$langTag = JFactory::getLanguage()->getTag();
		if ($langTag)
		{
			$langTag = explode('-', $langTag);
			$langTag = $langTag[0];
			$this->addBodyCssClass('lang', $langTag);
		}

		// Itemid parts
		if ($this->params->get('dynamic_body_classes-itemid'))
		{
			// Current Itemid
			if (($Itemid = (int) $this->currentItemID))
			{
				$this->addBodyCssClass('itemid', $Itemid);
			}
			// Parent Itemid's
			$parentItemIDs = (array) $this->parentItemIDs;
			if ( ! empty($parentItemIDs))
			{
				foreach ($parentItemIDs as $value)
				{
					$this->addBodyCssClass('parent-itemid-' . $value, '');
				}
			}
		}

		// Component parts
		$component     = self::input()->get('option');
		$componentName = '';

		if ($component)
		{
			$tmp           = explode('_', $component);
			$componentName = isset($tmp[1]) ? $tmp[1] : '';
		}

		// Add component name and view, task and id of reqeust
		if ($this->params->get('dynamic_body_classes-component'))
		{
			if ($component)
			{
				$this->addBodyCssClass('option', $component);

				if (($view = self::input()->get('view')))
				{
					$this->addBodyCssClass($component . '-view', $view);
				}
				if (($task = self::input()->get('task')))
				{
					$this->addBodyCssClass($component . '-task', $task);
				}
				if (($id = self::input()->get('id', 0, 'int')))
				{
					$additional = ($view ? '-' . $view : '') . ($task ? '-' . $task : '');
					$this->addBodyCssClass($component . $additional, $id);
				}
				if ($component == 'com_content' && self::input()->get('view') == 'article' && ($id = self::input()->get('id', 0, 'int')))
				{
					$this->addBodyCssClass('article', $id);
				}
			}
		}

		// Add category parts
		if ($this->params->get('dynamic_body_classes-categories'))
		{
			// Find category items including on article detail page from com_content
			if ($component == 'com_content')
			{
				if (self::input()->get('view') == 'article' && ($id = self::input()->get('id', 0, 'int')))
				{
					// From article
					$query = $db->getQuery(true);
					$query->select($db->qn('catid'));
					$query->from($db->qn('#__content'));
					$query->where($db->qn('id') . ' = ' . $db->q($id));
					$db->setQuery($query);

					if ($result = $db->loadObject())
					{
						if ($result->catid)
						{
							// From article categories (including parents)
							$this->addBodyCssClass('category', $result->catid);

							if ($_categories = JCategories::getInstance('Content'))
							{
								$_category = $_categories->get($result->catid);

								if ($_category && ($path = $_category->getPath()))
								{
									// Remove current category
									array_pop($path);

									foreach ($path as $cat)
									{
										$tmp   = explode(':', $cat);
										$catId = $tmp[0];
										$this->addBodyCssClass('parent-category-' . $catId, '');
									}
								}
							}
						}
					}
				}
			}

			// Global search category and parents on view=category|categories for all components
			if ($component && $componentName)
			{
				$_view = self::input()->get('view');

				if (($_view == 'category' || $_view == 'categories') && ($id = self::input()->get('id', 0, 'int')))
				{
					$this->addBodyCssClass('category', $id);

					if ($_categories = JCategories::getInstance(ucfirst($componentName)))
					{
						$_category = $_categories->get($id);

						if ($_category && ($path = $_category->getPath()))
						{
							// Remove current category
							array_pop($path);

							foreach ($path as $cat)
							{
								$tmp   = explode(':', $cat);
								$catId = $tmp[0];
								$this->addBodyCssClass('parent-category-' . $catId, '');
							}
						}
					}
				}
			}
		}

		// Add browser classes
		if ($this->get('user_browser') && $this->params->get('dynamic_body_classes-browser'))
		{
			$browser = $this->get('user_browser');
			$browser = strtolower($browser);
			$browser = preg_replace('#[^a-z0-9]#', '', $browser);
			$this->addBodyCssClass('browser', $browser);
		}

		// Add platform classes
		if ($this->get('user_platform') && $this->params->get('dynamic_body_classes-platform'))
		{
			$this->addBodyCssClass('platform', $this->get('user_platform'));
		}

		// Add editor-active class when a editor is currently open
		if ($this->isEditorActive())
		{
			$this->addBodyCssClass('editor', 'active');
		}

		// Add page class suffix from active menu item (except admin templates).
		if ( ! JFactory::getApplication()->isAdmin())
		{
			$menu       = JFactory::getApplication()->getMenu('site');
			$activeItem = $menu->getActive();
			if ($activeItem)
			{
				$pageClassSfx = trim($activeItem->params->get('pageclass_sfx', ''));

				if ($pageClassSfx)
				{
					$pageClassSfxClasses = explode(' ', $pageClassSfx);
					$this->addBodyCssClass('page-sfx', trim($pageClassSfxClasses[0]));
				}
			}
		}

		$this->addLog('Body CSS Class string generated');
	}

	/**
	 * isFileOrUrlInternal
	 *
	 * @param string $file
	 *
	 * @return bool
	 */
	public static function isFileOrUrlInternal($file)
	{
		$baseUrl = JURI::base(true) . '/';
		return (preg_match('#^' . preg_quote($baseUrl, '#') . '#', $file) || JURI::isInternal($file));
	}

	/**
	 * Returns an instance of JEditor Object if a editor is active
	 *
	 * @access public
	 * @return boolean $active
	 */
	public static function isEditorActive()
	{
		static $active;

		if ($active == null)
		{
			jimport('joomla.html.editor');
			$conf = JFactory::getConfig();

			// 'config.editor' is a Fallback for older Joomla versions
			$name = $conf->get('config.editor', $conf->get('editor'));

			$instance = JEditor::getInstance($name);

			if ($instance->get('_editor'))
			{
				$active = true;
			}
			else
			{
				$active = false;
			}
		}

		return $active;
	}

	/**
	 * Add or overwrite an body css class
	 *
	 * @param   string $key   Suffix for value
	 * @param   string $value Value of key
	 *
	 * @access public
	 * @return void
	 */
	public function addBodyCssClass($key, $value = '')
	{
		// Make strings css/html safe
		$regex = "/[^a-z0-9_-]/i";
		$key   = preg_replace($regex, '', $key);
		$value = preg_replace($regex, '', $value);

		// Add or overwrite part/value pair
		$this->body_css_class[$key] = $value;
	}

	/**
	 * Returns an string of added body_css_class
	 *
	 * @param   boolean $addAttribute If true it returns an string with class="xx" .  default true
	 *
	 * @access public
	 * @return string CSS classes with css Attribute with one space before
	 */
	public function getBodyCssClass($addAttribute = true)
	{
		$string = '';

		if (is_array($this->body_css_class) && ! empty($this->body_css_class))
		{
			foreach ($this->body_css_class as $k => $v)
			{
				if ($k)
				{
					$string .= ' ' . $k . ($v ? '-' . $v : '');
				}
			}
		}
		$string = trim($string);

		if ($string != "" && $addAttribute)
		{
			$string = ' class="' . $string . '"';
		}

		return $string;
	}

	/**
	 * Trigger JYAML plugin event
	 *
	 * @param   string $trigger Event Name
	 *
	 * @access private
	 * @return mixed $result Result of event are the plugins return
	 */
	private function triggerPlugin($trigger)
	{
		if (constant('JDEBUG'))
		{
			JProfiler::getInstance('Application')->mark('JYAML::triggerPlugin before:' . $trigger);
		}

		JPluginHelper::importPlugin('jyaml');

		if (class_exists('JEventDispatcher'))
		{
			$dispatcher = JEventDispatcher::getInstance();
		}
		else
		{
			/** @noinspection PhpDeprecationInspection */
			$dispatcher = JDispatcher::getInstance();
		}

		$this->addLog('Trigger JYAML Plugins: ' . $trigger);

		$results = $dispatcher->trigger($trigger, array(&$this));

		if (constant('JDEBUG'))
		{
			JProfiler::getInstance('Application')->mark('JYAML::triggerPlugin after:' . $trigger);
		}

		return $results;
	}

	/**
	 * Get the template path
	 *
	 * @param   boolean $absolute Absolute or Relative
	 *
	 * @access public
	 * @return string Absolute or relative template path
	 */
	public function getTemplatePath($absolute = true)
	{
		static $clientPath;

		if ($clientPath == null)
		{
			$client = JApplicationHelper::getClientInfo(self::app()->getClientId());

			if ($client && $client->path)
			{
				$clientPath = $client->path;
			}
			else
			{
				// Hard fallback
				$clientPath = JPATH_ROOT;
			}
		}

		if ($absolute)
		{
			return $clientPath . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . $this->template;
		}
		else
		{
			return JURI::base(true) . '/templates/' . $this->template;
		}
	}

	/**
	 * Get the current html file
	 *
	 * @param   string $folder        Folder of the html document template (from /html/)
	 * @param   string $forceTemplate Template Force as specific template to load in folder
	 *
	 * @access public
	 * @return string File path relative to the template main directory
	 */
	public function getHtmlFile($folder = 'index', $forceTemplate = '')
	{
		$path = '';

		if ($forceTemplate)
		{
			$file = $forceTemplate;
			$path = 'html' . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . $file . '.php';
		}
		else
		{
			$values = $this->params->get('html_file', 'default');

			if (is_object($values))
			{
				$values = \Joomla\Utilities\ArrayHelper::fromObject($values);
			}

			$file = ((is_array($values) && isset($values[$folder])) ? $values[$folder] : $values);
			$path = 'html' . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . $file . '.php';
		}

		// Set params for an easy access
		$this->params->set('html_file', $file);
		$this->params->set('html_folder', $folder);

		return $path;
	}

	/**
	 * Trim an string and remove linebreaks and tabs
	 *
	 * @param   string  $content        The string of content
	 * @param   boolean $strip_tags     Remove html code
	 * @param   string  $allowable_tags Allowable tags for $strip_tags (e.g. '<img><object><video>')
	 *
	 * @access public
	 * @return string Cleaned/Trimmed Contents
	 */
	public static function trimmer($content, $strip_tags = false, $allowable_tags = '')
	{
		// "\0" and "\x0B" are unicode whitespaces - convert to normal spaces
		$content = str_replace(array("\0", "\x0B"), ' ', $content);

		// Remove duplicated spaces
		$content = preg_replace('#\s+#', ' ', $content);

		// Remove tabs and line-breaks
		$content = str_replace(array("\n", "\r", "\t"), '', $content);

		if ($strip_tags)
		{
			// Strip tags, if set
			$content = strip_tags($content, $allowable_tags);
		}

		return $content;
	}

	/**
	 * Trim withespaces in a string
	 *
	 * @param   string $content The string of content
	 *
	 * @access public
	 * @return string Cleaned/Trimmed content
	 */
	public static function trimWhitespaces($content)
	{
		$whitespaces = array(" ", "\n", "\r", "\t", "\0", "\x0B");
		$content     = trim($content);
		$content     = str_replace($whitespaces, '', $content);

		return $content;
	}

	/**
	 * Remove empty lines
	 *
	 * @param   string $string The string of content
	 *
	 * @access public
	 * @return string Contents without empty lines
	 */
	public static function removeEmptyLines($string)
	{
		return preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $string);
	}

	/**
	 * Replace entities
	 *
	 * @param   string $text The string of content
	 *
	 * @access public
	 * @return string Replaced text
	 */
	public static function ampReplace($text)
	{
		$text = str_replace('&&', '*--*', $text);
		$text = str_replace('&#', '*-*', $text);
		$text = str_replace('&amp;', '&', $text);
		$text = preg_replace('|&(?![\w]+;)|', '&amp;', $text);
		$text = str_replace('*-*', '&#', $text);
		$text = str_replace('*--*', '&&', $text);

		return $text;
	}

	/**
	 * Get the Image Parser/Thumbnailer
	 *
	 * @param   string $src     Image Src
	 * @param   string $alt     Alt Attribute for HTML output
	 * @param   array  $options PhpThumb Options
	 * @param   string $attribs Additional Attributes (e.g. onclick="alert('hello world')")
	 *
	 * @access public
	 * @return phpImage object
	 */
	public static function getImage($src, $alt = '', $options = array(), $attribs = '')
	{
		// Configuration
		if (empty(phpImage::$cachePath))
		{
			phpImage::$cachePath = JFactory::getConfig()->get('cache_path', JPATH_SITE . '/cache') . '/jyaml_image';
			phpImage::$rootPath  = JPATH_ROOT;
		}

		$image = new phpImage($src, $alt, $options, $attribs);

		return $image;
	}

	/**
	 * Get the Mobile Device Detector
	 *
	 * @param   boolean $initialEnabled Overwrite the default enabled state
	 *
	 * @access public
	 * @return JYAMLMobile object
	 */
	public function getMobile($initialEnabled = true)
	{
		static $mobile = null;

		if ($mobile === null)
		{
			$mobile = new JYAMLMobile($initialEnabled);
		}

		return $mobile;
	}

	/**
	 * Method to import modChrome styles from templates subfolder /html/modChome/
	 *
	 * @access public
	 * @return void
	 */
	public function importModChromeStyles()
	{
		static $loaded;

		if ($loaded === null)
		{
			$loaded   = array();
			$tmplPath = $this->getPath('template') . DIRECTORY_SEPARATOR . 'html' . DIRECTORY_SEPARATOR . 'modChrome';
			$libPath  = $this->getPath('libraries') . DIRECTORY_SEPARATOR . 'html' . DIRECTORY_SEPARATOR . 'modChrome';

			// First seach in template for own styles and overrides
			if (JFolder::exists($tmplPath))
			{
				$files = JFolder::files($tmplPath, '.php$', false, false);

				if ($files)
				{
					foreach ($files as $_file)
					{
						$styleName = strtolower(JFile::stripExt($_file));

						$loaded[] = $styleName;
						include_once $tmplPath . DIRECTORY_SEPARATOR . $_file;
					}
				}
			}

			// Now include the jyaml librarie styles
			if (JFolder::exists($libPath))
			{
				$files = JFolder::files($libPath, '.php$', false, false);

				if ($files)
				{
					foreach ($files as $_file)
					{
						$styleName = strtolower(JFile::stripExt($_file));

						// Check is already loaded or overwritten by template
						if ( ! in_array($styleName, $loaded))
						{
							$loaded[] = $styleName;
							include_once $libPath . DIRECTORY_SEPARATOR . $_file;
						}
					}
				}
			}
		}
	}

	/**
	 * Function to load JYAML Libraray language files
	 *
	 * @since 4.5.0
	 *
	 * @static
	 * @return void
	 */
	public static function loadLanguage()
	{
		$lang = JFactory::getLanguage();
		$lang->load('lib_jyaml', JYAML_LIB_PATH) || $lang->load('lib_jyaml', JPATH_ADMINISTRATOR);
	}

	/**
	 * Extend JForm data and fields
	 *
	 * @param   object $form JForm Object
	 * @param   mixed  $data JForm Data
	 *
	 * @todo   Need more testing in Feature to extend
	 *       jform fields also in frontend (see at JYAMLAdmin::onContentPrepareForm)
	 *
	 * @static
	 * @access public
	 * @return bool
	 */
	public static function onContentPrepareForm($form, $data)
	{
		if ( ! ($form instanceof JForm))
		{
			return false;
		}

		$jyaml = self::getDocument();
		$jyaml->addLog('Start: onContentPrepareForm');

		$customLibPaths = array();

		$formName = $form->getName();

		$jyaml->addLog('Using form name: ' . $formName);

		// Additional user defined config enhancements
		if ( ! empty($formName) && $formName != 'com_templates.style')
		{
			$lang = JFactory::getLanguage();
			$lang->load('custom', JYAML_LIB_PATH, null, false, false);

			$customLibPaths[] = JPath::clean(JYAML_LIB_PATH . '/config/custom/' . $formName . '.xml');

			// Find plugin jform xml overrides
			self::applyPluginToPrepareForm($formName, $customLibPaths);

			// Additional file by specific module type
			if ( ! empty($data->module))
			{
				$customLibPaths[] = JPath::clean(JYAML_LIB_PATH . '/config/custom/' . $formName . '.' . $data->module . '.xml');

				// Find plugin jform xml overrides
				self::applyPluginToPrepareForm($formName . '.' . $data->module, $customLibPaths);
			}
		}

		if ( ! empty($customLibPaths))
		{
			foreach ($customLibPaths as $_file)
			{
				if (file_exists($_file))
				{
					$form->loadFile($_file, true, '/config');

					$jyaml->addLog('Load additional form xml file: ' . str_replace(JPATH_ROOT, '[JPATH_ROOT]', $_file));
				}
				else
				{
					$jyaml->addLog('Custom form xml file not used: ' . str_replace(JPATH_ROOT, '[JPATH_ROOT]', $_file));
				}
			}
		}

		$jyaml->addLog('End: onContentPrepareForm');

		return true;
	}

	/**
	 * Helper to apply JYAML plugin xml overrides of jform
	 *
	 * @param   string $formName The name of the form
	 * @param   array  &$arr     The Array to push the file
	 *
	 * @static
	 * @access protected
	 * @return void
	 */
	protected static function applyPluginToPrepareForm($formName, &$arr)
	{
		static $pluginsImported;

		// Find jyaml plugins for jform xml overrides
		$files  = array();
		$folder = JPATH_ROOT . DIRECTORY_SEPARATOR . 'plugins' . DIRECTORY_SEPARATOR . 'jyaml';

		if (JFolder::exists($folder))
		{
			$files = JFolder::files($folder, '^' . $formName . '.xml$', true, true);
			if ($files && is_array($files))
			{
				if ( ! $pluginsImported)
				{
					// Force to init plugin group to (load language, etc.)
					JPluginHelper::importPlugin('jyaml');
				}

				foreach ($files as $_file)
				{
					$arr[] = $_file;
				}
			}
		}
	}

	/**
	 * Check debug is active
	 *
	 * @param   string $var Optional check a UserState variable, prefixed with jyaml.debug. Always false when debug is disabled.
	 *
	 * @access protected
	 * @return boolean State isShow
	 */
	protected function isDebug($var = null)
	{
		$debug   = ($this->params->get('debug', 0) ? true : false);
		$debugIP = trim($this->params->get('debugIP', ''));

		$r         = self::input()->get('jyamlC', array('debug' => false), 'array');
		$sessionId = JFactory::getSession()->getId();

		if ( ! $debug && ( ! empty($r['debug']) && ! empty($sessionId) && $r['debug'] == $sessionId))
		{
			$debug   = true;
			$debugIP = '';
		}

		if ($debugIP)
		{
			if ($_SERVER['REMOTE_ADDR'] != $debugIP)
			{
				$debug = false;
			}
		}

		if ($var !== null && $debug)
		{
			$debug = self::app()->getUserState('jyaml.debug.' . $var);
		}

		return $debug;
	}

	/**
	 * Add some thing needed for Debug view
	 *
	 * @access protected
	 * @return void
	 */
	protected function debugPrepare()
	{
		if ($this->isDebug())
		{
			// Handle hidden debug requests
			$vars     = array(
				'jyaml_debug_tp'
			);
			$redirect = false;
			$uri      = clone JURI::getInstance();

			foreach ($vars as $var)
			{
				$data = self::app()->input->get($var);

				if ($data !== null)
				{
					$stateVar = preg_replace('#^jyaml_debug_#', '', $var);

					self::app()->setUserState('jyaml.debug.' . $stateVar, $data);
					$uri->delVar($var);

					$redirect = true;
				}
			}

			if ($redirect)
			{
				self::app()->redirect($uri->toString());
			}

			// Force to disable Joomla's positions preview
			JComponentHelper::getParams('com_templates')->set('template_positions_display', false);
			self::app()->input->set('tp', 0);

			// Apply requirements
			$this->addStylesheet($this->getUrl('libraries', 'assets/css/debug.css') . '?noChunk=1');
			$this->addScript($this->getUrl('libraries', 'assets/js/jyaml.jquery.js') . '?noChunk=1');
			$this->addScript($this->getUrl('libraries', 'assets/js/jyaml.debug.js') . '?noChunk=1');
		}
	}

	/**
	 * Output debug informations if enabled
	 *
	 * @access protected
	 * @return void
	 */
	protected function debug()
	{
		if ( ! $this->isDebug())
		{
			return;
		}

		// Load required Javascript translations
		$jslangKeys  = array(
			'JYAML_DEBUG_NO_LOG_ENTRIES',
			'JYAML_DEBUG_NO_ERRORS'
		);
		$langStrings = new JObject;
		foreach ($jslangKeys as $v)
		{
			$langStrings->set($v, JText::_($v));
		}

		$jyamlObject = array();
		foreach (get_object_vars(clone $this) as $k => $v)
		{
			if ($v instanceof \Joomla\Registry\Registry)
			{
				$jyamlObject[$k] = clone $v;
				$jyamlObject[$k] = $jyamlObject[$k]->toArray();
			}
			else
			{
				$jyamlObject[$k] = $v;
			}
		}

		$jyamlObject['__html__'] = array('&nbsp;' => $jyamlObject['__html__']);

		$toolbarHtml = '<div id="jyaml_debug_toolbar" style="display:none;"><div id="jyaml_debug_toolbar_content" class="ym-clearfix">';
		$toolbarHtml .= '<ul>';
		$toolbarHtml .= '<li class="jyaml_debug_toolbar_fulllink jyaml_d_logo">';
		$toolbarHtml .= '<a href="http://www.jyaml.de" target="_blank" title="JYAML Homepage"><span class="ym-hideme">JYAML Homepage</span></a>';
		$toolbarHtml .= '</li>';
		$toolbarHtml .= '<li class="jyaml_debug_toolbar_fulllink"><a href="#" id="jyaml_debug_btn_log">' . JText::_('JYAML_DEBUG_SHOW_LOG')
		                . ' <span class="jyaml_debug_cnt jyaml_debug_cnt-log">' . count($this->log) . '</span></a></li>';
		$toolbarHtml .= '<li class="jyaml_debug_toolbar_fulllink"><a href="#" id="jyaml_debug_btn_errors">' . JText::_('JYAML_DEBUG_SHOW_ERRORS')
		                . ' <span class="jyaml_debug_cnt jyaml_debug_cnt-error">' . count($this->errorLog) . '</span></a></li>';
		$toolbarHtml .= '<li class="jyaml_debug_toolbar_fulllink"><a href="#" id="jyaml_debug_btn_phpobject">' . JText::_('JYAML_DEBUG_PHP_OBJECT')
		                . '<br /><span><small>JYAML::getDocument()</small></span></a></li>';
		$toolbarHtml .= '<li><div class="jyaml_debug_toolbar_multiline">' . JText::_('JYAML_DEBUG_PREVIEW_POSITIONS') . ': <br />'
		                . '<a href="#" id="jyaml_debug_btn_positions_off"><span class="jyaml_debug_cnt '
		                . (! $this->isDebug('tp') ? 'jyaml_debug_cnt-off' : 'jyaml_debug_cnt-inactive') . '">'
		                . JText::_('JYAML_DEBUG_PREVIEW_POSITIONS_OFF') . '</span></a> '
		                . '<a href="#" id="jyaml_debug_btn_positions_on"><span class="jyaml_debug_cnt '
		                . ((int) $this->isDebug('tp') == 1 ? 'jyaml_debug_cnt-on' : 'jyaml_debug_cnt-inactive') . '">'
		                . JText::_('JYAML_DEBUG_PREVIEW_POSITIONS_ON') . '</span></a> '
		                . '<a href="#" id="jyaml_debug_btn_positions_all"><span class="jyaml_debug_cnt '
		                . ((int) $this->isDebug('tp') == 2 ? 'jyaml_debug_cnt-on' : 'jyaml_debug_cnt-inactive') . '">'
		                . JText::_('JYAML_DEBUG_PREVIEW_POSITIONS_ALL') . '</span></a> '
		                . '<div></li>';
		$toolbarHtml .= '</ul>';
		$toolbarHtml .= '</div></div>';

		// JText::_('JYAML_DEBUG_YAML_TOOL');

		$jyamlObject         = json_encode($jyamlObject);
		$jyamlObject         = str_replace(
			substr(json_encode(JPATH_ROOT), 1, - 1),
			substr(json_encode('[JPATH_ROOT]'), 1, - 1),
			$jyamlObject
		);
		$jyamlObjectPathSafe = json_decode($jyamlObject);

		$uri = clone JURI::getInstance();

		$uri->setVar('jyaml_debug_tp', '1');
		$mPosPrevEnable = $uri->toString();

		$uri->setVar('jyaml_debug_tp', '2');
		$mPosPrevEnableAll = $uri->toString();

		$uri->setVar('jyaml_debug_tp', '0');
		$mPosPrevDisable = $uri->toString();

		/*
		$templateRoot = $this->getUrl('template');
		$templatePathAbsolute = $this->getPath('template', true);
		$loader = new JYAML_Autoload;

		$editableStylesheets = array();
		$_allStylesheets = isset($doc->_styleSheets) ? (array) $doc->_styleSheets : array();
		foreach ($_allStylesheets as $_src => $_attribs)
		{
			$checkPath = substr($_src, 0, strlen($templateRoot));

			if ($checkPath == $templateRoot)
			{
				$srcRel = substr($_src, strlen($templateRoot));
				$tree = $loader->getTemplateStylesheetTree($srcRel, $templatePathAbsolute);

				$editableStylesheets = array_merge($editableStylesheets, $tree);
			}
		}

		$editableScripts = array();
		$_allScripts = isset($doc->_scripts) ? (array) $doc->_scripts : array();
		foreach ($_allScripts as $_src => $_attribs)
		{
			$checkPath = substr($_src, 0, strlen($templateRoot));

			if ($checkPath == $templateRoot) {
				$_src = substr($_src, strlen($templateRoot));
				$editableScripts[$_src] = array();
			}
		}
		*/

		$debugHtml = '<script type="text/javascript">' . "\n";
		$debugHtml .= '  JYAML.debug.toolbarHtml=' . json_encode($toolbarHtml) . ";\n";
		$debugHtml .= '  JYAML.debug.langStrings=' . json_encode($langStrings) . ";\n";
		$debugHtml .= '  JYAML.debug.log=' . json_encode($jyamlObjectPathSafe->log) . ";\n";
		$debugHtml .= '  JYAML.debug.errors=' . json_encode($jyamlObjectPathSafe->errorLog) . ";\n";
		$debugHtml .= '  JYAML.debug.phpObject=' . $jyamlObject . ";\n";
		$debugHtml .= '  JYAML.debug.positionPreviewEnableLink=' . json_encode($mPosPrevEnable) . ";\n";
		$debugHtml .= '  JYAML.debug.positionPreviewEnableAllLink=' . json_encode($mPosPrevEnableAll) . ";\n";
		$debugHtml .= '  JYAML.debug.positionPreviewDisableLink=' . json_encode($mPosPrevDisable) . ";\n";
		$debugHtml .= '  JYAML.debug.positionPreviewIsEnabled=' . (int) $this->isDebug('tp') . ";\n";
		$debugHtml .= '  JYAML.debug.phpObject=' . $jyamlObject . ";\n";

		// $debugHtml .= '  JYAML.debug.editableStylesheets=' . json_encode($editableStylesheets) . ";\n";
		// $debugHtml .= '  JYAML.debug.editableScripts=' . json_encode($editableScripts) . ";\n";
		$debugHtml .= '</script>' . "\n";

		$this->addHtmlContentByPosition($debugHtml, 'beforeBodyEnds');
	}

	/**
	 * is_writable Wrapper
	 * Will work in despite of Windows ACLs bug
	 *
	 * NOTE: use a trailing slash for folders!!!
	 *
	 * @param   string $path Folder or File path
	 *
	 * @see http://bugs.php.net/bug.php?id=27609
	 * @see http://bugs.php.net/bug.php?id=30931
	 *
	 * @static
	 * @return boolean is writable
	 */
	static public function _isWritable($path)
	{
		if ($path{strlen($path) - 1} == '/')
		{
			// Recursively return a temporary file path
			return self::_isWritable($path . uniqid(mt_rand()) . '.tmp');
		}
		elseif (is_dir($path))
		{
			return self::_isWritable($path . '/' . uniqid(mt_rand()) . '.tmp');
		}

		// Check tmp file for read/write capabilities
		$rm = file_exists($path);
		$f  = @fopen($path, 'a');

		if ($f === false)
		{
			return false;
		}

		fclose($f);

		if ( ! $rm)
		{
			unlink($path);
		}

		return true;
	}

	/**
	 * Add a method to the queue list
	 * - First argument: Namespace of the queue
	 * - Second argument: Method name to call later
	 *
	 * @access public
	 * @return void
	 */
	public function addMethodQueue()
	{
		if (func_num_args() >= 2)
		{
			$arguments = func_get_args();
			$queue     = array_shift($arguments);
			$method    = array_shift($arguments);

			if ( ! isset($this->methodCallQueue[$queue]))
			{
				$this->methodCallQueue[$queue] = array();
			}

			$this->methodCallQueue[$queue][] = array($method, $arguments);
		}
	}

	/**
	 * Executes an queue list by namespace
	 *
	 * @param   string $queue Namespace of the queue
	 *
	 * @access protected
	 * @return void
	 */
	protected function execMethodQueue($queue)
	{
		if (isset($this->methodCallQueue[$queue]))
		{
			$arr = (array) $this->methodCallQueue[$queue];

			foreach ($arr as $call)
			{
				$method    = isset($call[0]) ? $call[0] : '';
				$arguments = isset($call[1]) ? $call[1] : array();

				if ('' != $method)
				{
					$_call = array($this, $method);

					if (is_callable($_call))
					{
						call_user_func_array($_call, $arguments);
					}
				}
			}

			unset($this->methodCallQueue[$queue]);
		}
	}

	/**
	 * Get the JYAML library version
	 *
	 * @param   boolean $includeDate Include date
	 * @param   boolean $asHash      Return as md5 hash value
	 *
	 * @static
	 * @access public
	 *
	 * @return string Version number
	 */
	public static function getVersion($includeDate = false, $asHash = false)
	{
		static $version = null;
		static $date = null;

		if ($version === null)
		{
			$db    = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('*');
			$query->from($db->qn('#__extensions'));

			$query->where($db->qn('type') . ' = ' . $db->q('library'));
			$query->where($db->qn('element') . ' = ' . $db->q('jyaml'));

			$db->setQuery($query);
			$result = $db->loadObject();

			if ($result)
			{
				$manifest = new \Joomla\Registry\Registry;
				$manifest->loadString($result->manifest_cache, 'JSON');
				$version = (string) $manifest->get('version');
				$date    = (string) $manifest->get('creationDate');
			}

			if ( ! $version)
			{
				$version = 'DEV_' . time();
			}
		}

		$value = trim($version . ($includeDate ? ' ' . $date : ''));

		if ($asHash)
		{
			$value = md5($value);
		}

		return $value;
	}

	/**
	 * Get the application (JSite)
	 *
	 * @static
	 * @return JApplication
	 */
	protected static function app()
	{
		return JFactory::getApplication();
	}

	/**
	 * Get instance of JDocumentHtml
	 *
	 * @static
	 * @return JDocument
	 */
	protected static function doc()
	{
		return JFactory::getDocument();
	}

	/**
	 * Get JInput
	 *
	 * @static
	 * @return JInput
	 */
	protected static function input()
	{
		return self::app()->input;
	}

	/**
	 * Add an log message.
	 *
	 * @param   string $log Log message.
	 *
	 * @access public
	 * @return void
	 */
	public function addLog($log)
	{
		array_push($this->log, $log);
	}

	/**
	 * Add an error log message.
	 *
	 * @param   string $msg Error message.
	 *
	 * @access public
	 * @return void
	 */
	public function addErrorLog($msg)
	{
		array_push($this->errorLog, $msg);
	}

	/**
	 * Set an Error an throw an error to the application
	 *
	 * @param   string $msg Error message.
	 *
	 * @access public
	 * @throws RuntimeException
	 *
	 * @return void
	 */
	public function setError($msg)
	{
		throw new RuntimeException($msg, E_USER_WARNING);
	}


	/**
	 * __get
	 * (#JYAML-66 - Instead of linkJDocumentProperties get properties live from JDocument)
	 *
	 * @param $name
	 *
	 * @return mixed
	 * @throws InvalidArgumentException
	 */
	public function __get($name)
	{
		$object = self::doc();
		if (property_exists($object, $name))
		{
			return $object->$name;
		}

		// @TODO: if throw error the template is not loaded fully. E.g. jdoc head is not parsed and so on...
		throw new \InvalidArgumentException(sprintf('Property %s does not exist', $name));
	}

	/**
	 * Wrapper to call JDocument
	 *
	 * @param   string $method    Method
	 * @param   array  $arguments Arguments
	 *
	 * @return mixed
	 */
	public function __call($method, $arguments)
	{
		$document = self::doc();

		$ignoreErrors = array(
			'addFavicon',
			'setIsPreRender',
			'getIsPreRender',
			'getIsPreRenderScope'
		);

		if (method_exists($document, $method))
		{
			return call_user_func_array(array($document, $method), $arguments);
		}
		elseif ( ! in_array($method, $ignoreErrors))
		{
			if ($document->getType() == 'html')
			{
				trigger_error(
					"Call to undefined method " . get_class($this) . '(subcall ' . get_class(
						$document
					) . ')' . '::' . $method,
					E_USER_ERROR
				);
			}
		}
	}
}
