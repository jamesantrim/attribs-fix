<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  Loader
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 *
 */

defined('_JEXEC') or die;

/**
 * JYAML Loader Class
 *
 * @package     JYAML
 * @subpackage  Loader
 * @since       4.0.0
 */
abstract class JYAMLloader
{
	static protected $files = array();

	/**
	 * Class constructor
	 */
	public function __construct()
	{
		$this->setFiles();
	}

	/**
	 * This method called by constructor, overridden in descendant classes.
	 *
	 * @return void
	 */
	protected function setFiles()
	{

	}

	/**
	 * Add a file in object
	 *
	 * @param   string  $type     Name of type (stylesheet, script)
	 * @param   array   $attribs  Array of values (file is required as array key)
	 *
	 * @return void
	 */
	protected function addFile($type, $attribs)
	{
		$type = $this->convertType($type);
		if (!$type)
		{
			return;
		}

		if (!isset(self::$files[$type]) || !is_array(self::$files[$type]))
		{
			self::$files[$type] = array();
		}

		if (isset($attribs['file']) && !empty($attribs['file']))
		{
			$hash = md5(serialize($attribs));

			if (!isset(self::$files[$type][$hash]))
			{
				self::$files[$type][$hash] = $attribs;
			}
		}
	}

	/**
	 * Get a list of file to load automaticly
	 *
	 * @param   string   $type      Type of files
	 * @param   boolean  $fullInfo  If false returns only filenames
	 *
	 * @access public
	 * @return array List of files
	 */
	public function getAutoLoadFiles($type, $fullInfo=true)
	{
		$files = array();
		$filesOnlyNames = array();

		$type = $this->convertType($type);
		if (!$type)
		{
			return $files;
		}

		if (isset(self::$files[$type]) && is_array(self::$files[$type]))
		{
			$files = self::$files[$type];
		}

		if ($fullInfo)
		{
			return $files;
		}
		else
		{
			foreach ($files as $file)
			{
				if (isset($file['inline']) && $file['inline'])
				{
					// Ignore content inline declarations
					continue;
				}

				$filesOnlyNames[] = $file['file'];
			}

			return $filesOnlyNames;
		}
	}

	/**
	 * Get a list of stylesheets to load recursivly automaticly
	 *
	 * @param   string  $templatePath  Absolute Path of Template
	 *
	 * @access public
	 * @return array List all of files
	 */
	public function getAutoLoadStylesheetsRecursivly($templatePath)
	{
		static $files;

		if ($files == null)
		{
			$templatePath = JPath::clean($templatePath, DIRECTORY_SEPARATOR);
			$files = (array) $this->getAutoLoadFiles('css', false);

			foreach ($files as $_file)
			{
				$_file = JPath::clean($_file, DIRECTORY_SEPARATOR);

				if (strpos($_file, '{$source.theme}') !== false)
				{
					$_file = str_replace('{$source.theme}', $templatePath . DIRECTORY_SEPARATOR . 'css' . DIRECTORY_SEPARATOR, $_file);
				}

				if (strpos($_file, '{$source.yaml}') !== false)
				{
					$_replace = JPATH_ROOT . DIRECTORY_SEPARATOR . 'libraries' . DIRECTORY_SEPARATOR . 'jyaml' . DIRECTORY_SEPARATOR . 'yaml' . DIRECTORY_SEPARATOR;
					$_file = str_replace('{$source.yaml}', $_replace, $_file);
				}

				$this->_getStylesheetFilesRecursivly($_file, $templatePath, $files);
			}

			$files = array_unique($files);
		}

		return $files;
	}

	/**
	 * Method to get all imported stylesheets within a template in a multi dimensional array as tree
	 *
	 * @param   string  $src   Filename relative to $base
	 * @param   string  $base  Absolute template base path
	 *
	 * @return array Stylesheets tree as multi dimensional array
	 */
	public function getTemplateStylesheetTree($src, $base)
	{
		$tree = array();
		$files = array();

		$path = $base . '/' . $src;

		$this->_getStylesheetFilesRecursivly($path, $base, $files, $tree);

		return $tree;
	}

	/**
	 * Helper to parse Styleshet Files recursivly with @import rule
	 *
	 * @param   string   $fileName      File
	 * @param   string   $templatePath  Absolute Path of Template
	 * @param   array    &$files        Reference of file list to hold current files
	 * @param   boolean  &$tree         Tree Result (multidimensional array), default false
	 *
	 * @access private
	 * @return array List all of files
	 */
	private function _getStylesheetFilesRecursivly($fileName, $templatePath, &$files, &$tree = false)
	{
		static $loadedFiles = array();
		$hash = ($tree ? '1' : '0');
		$isTree = (is_array($tree) || $tree == true);

		if (!isset($loadedFiles[$hash]))
		{
			$loadedFiles[$hash] = array();
		}

		if (!is_array($tree))
		{
			$tree = array();
		}

		$fileName = JPATH::clean($fileName);

		if (in_array($fileName, $loadedFiles[$hash]))
		{
			// Prevent loops
			return;
		}

		$contents = @file_get_contents($fileName);
		$contents = trim($contents);

		if (empty($contents))
		{
			// Finished from parents
			return;
		}

		$loadedFiles[$hash][] = $fileName;

		if ($isTree)
		{
			$checkPath = substr($fileName, 0, strlen($templatePath));
			$fileNameRel = JPath::clean(substr($fileName, (strlen($templatePath)) + 1), '/');

			if ($checkPath == $templatePath)
			{
				$tree[$fileNameRel] = array();
			}
			else
			{
				$isTree = false;
			}
		}

		// Is important to remove comments before search @import rules
		$contents = preg_replace("#/\*.+\*/#sU", "", $contents);

		$base = dirname($fileName);

		$result = preg_match_all('/@import\s*(?:url\()?[\'"]?(?![a-z]+:)([^\'"\()]+)[\'"]?\)?(.*);/iS', $contents, $matches);

		if ($result)
		{
			$relpaths = isset($matches[1]) ? $matches[1] : array();

			foreach ($relpaths as $relfile)
			{
				$test = strtolower($relfile);
				if (empty($test) || $test == 'url')
				{
					continue;
				}

				if (preg_match('#^(http|https|\/\/)#', $relfile))
				{
					// External file or absolute Url
					$files[] = $relfile;
					continue;
				}

				$importPath = $base . DIRECTORY_SEPARATOR . $relfile;
				$importPath = realpath($importPath);

				$fileNameBySource = $importPath;
				$fileNameBySource = str_replace($templatePath . DIRECTORY_SEPARATOR . 'css' . DIRECTORY_SEPARATOR, '{$source.theme}', $fileNameBySource);
				$_search = JPATH_ROOT . DIRECTORY_SEPARATOR . 'libraries' . DIRECTORY_SEPARATOR . 'jyaml' . DIRECTORY_SEPARATOR . 'yaml' . DIRECTORY_SEPARATOR;
				$fileNameBySource = str_replace($_search, '{$source.yaml}', $fileNameBySource);

				$files[] = JPath::clean($fileNameBySource, '/');

				// Recursiv subcall
				if ($isTree)
				{
					$this->_getStylesheetFilesRecursivly($importPath, $templatePath, $files, $tree[$fileNameRel]);
				}
				else
				{
					$this->_getStylesheetFilesRecursivly($importPath, $templatePath, $files);
				}
			}
		}
	}

	/**
	 * Convert a specific type string to unique
	 *
	 * @param   string  $type  Name of type
	 *
	 * @return string Converted name of type or blank
	 */
	public function convertType($type)
	{
		$type = strtolower($type);

		switch ($type)
		{
			case 'css':
			case 'stylesheet':
			case 'stylesheets':
				$type = 'stylesheets';
				break;

			case 'js':
			case 'script':
			case 'scripts':
			case 'javascript':
				$type = 'scripts';
				break;

			default:
				$type = '';
			break;
		}

		return $type;
	}

}
