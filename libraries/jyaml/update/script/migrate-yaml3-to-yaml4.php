<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  UpdateScript
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 *
 */

defined('_JEXEC') or die;

/**
 * JYAMLUpdateScript class
 *
 * @package     JYAML
 * @subpackage  UpdateScript
 * @since       4.5.0
 */
class JYAMLUpdateScriptMigrateYaml3ToYaml4 extends JYAMLUpdateScript
{
	/**
	 * Get title text
	 *
	 * @return title text
	 */
	public function getTitleText()
	{
		return JText::_('JYAML_UPDATE_SCRIPT_MIGRATE_YAML3_TO_YAML4_TITLE');
	}

	/**
	 * Get description text
	 *
	 * @return Description text
	 */
	public function getDescriptionText()
	{
		return JText::_('JYAML_UPDATE_SCRIPT_MIGRATE_YAML3_TO_YAML4_DESC');
	}

	/**
	 * Start the update
	 *
	 * @param   string   $template  Template
	 * @param   integer  $clientId  Client Id
	 * @param   boolean  $test      If true changes are not applied, only logged
	 *
	 * @return True on Success
	 */
	public function run($template, $clientId, $test = false)
	{
		$template = (string) $template;
		$clientId = (int) $clientId;

		$this->test = $test;
		$this->template = $template;
		$this->clientId = $clientId;

		$client = JApplicationHelper::getClientInfo($clientId);

		if (!$client)
		{
			$this->addErrorLog('Invalid client info (JApplicationHelper::getClientInfo) - $clientId = ' . $clientId);
			return false;
		}

		// Set absolute template path
		$this->templatePath = $client->path . '/templates/' . $template;

		if (!JFolder::exists($this->templatePath))
		{
			$this->addErrorLog('Template directory does not exist: ' . $this->templatePath);
			return false;
		}

		// Update contents in template files and common Joomla database tables
		$result = $this->parseFilesAndDatabase();

		if ($result)
		{
			$result = $this->migrateTemplateConfig();
		}

		$logFile = JPath::clean($this->templatePath . '/migrate-yaml3-to-yaml4.log');
		$logEntries = $this->getLog();
		if (!$this->test && $result && !empty($logEntries) && JYAML::_isWritable($logFile))
		{
			$date = new JDate(time());
			$time = $date->toSql();

			$logString = '';
			if (JFile::exists($logFile))
			{
				$logString = file_get_contents($logFile);
			}
			$logString .= ':::::::::::::::::::::::::::' . "\n";
			$logString .= ':: [' . $time . '] ::' . "\n";
			$logString .= ':::::::::::::::::::::::::::' . "\n\n";

			foreach ($logEntries as $title => $entries)
			{
				if (empty ($entries))
				{
					continue;
				}

				$logString .= "\t" . $title . "\n";
				foreach ($entries as $entry)
				{
					$logString .= "\t\t\t" . $entry . "\n";
				}
				$logString .= "\n\n";
			}
			$logString .= "\n\n";

			JFile::write($logFile, $logString);
			$this->addLog('Created log file in template folder', $logFile);
		}

		return $result;
	}

	/**
	 * Update contents in template files and common Joomla database tables
	 * Also create backup files
	 *
	 * @return True on success
	 */
	private function parseFilesAndDatabase()
	{
		static $files;

		$result = true;

		// Database Contents
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Exclude all necessary updated files defined in the update XML and all new files (from parent update steps)
		$exclude = $this->excludeFiles;

		if ($files === null)
		{
			$htmlFiles = JFolder::files($this->templatePath . '/html', '.php$', true, true);
			$cssFiles = JFolder::files($this->templatePath . '/css', '.css$', true, true);
			$jsFiles = JFolder::files($this->templatePath . '/script', '.js$', true, true);

			$files = (array) array_merge($htmlFiles, $cssFiles, $jsFiles);

			$databaseTables = array(
				'#__content'    => array('primayKey' => 'id', 'fields' => array('introtext', 'fulltext')),
				'#__categories' => array('primayKey' => 'id', 'fields' => array('description')),
				'#__modules'    => array('primayKey' => 'id', 'fields' => array('content'))
			);
			foreach ($databaseTables as $table => $options)
			{
				foreach ($options['fields'] as $field)
				{
					$query->clear()
						->select($db->qn($options['primayKey']) . ', ' . $db->qn($field))
						->from($db->qn($table))
						->where($db->qn($field) . ' != ' . $db->q(''))
						->where($db->qn($field) . ' LIKE ' . $db->q('%</%')); /* Simple detect of containing html code */
					$db->setQuery($query);
					$results = $db->loadObjectList();

					if ($results)
					{
						foreach ($results as $result)
						{
							$result->_tbl_name = $table;
							$result->_tbl_key = $options['primayKey'];
							$result->_tbl_field = $field;
							$files[] = $result;
						}
					}
				}
			}
		}

		foreach ($files as $file)
		{
			if (is_string($file))
			{
				$file = JPath::clean($file);

				if (in_array($file, $exclude))
				{
					continue;
				}

				$filename = basename($file);
				$ext = strtolower(JFile::getExt($file));
				$content = file_get_contents($file);

			}
			elseif(is_object($file))
			{
				$filename = $file->_tbl_name . '.' . $file->_tbl_field;
				$ext = 'php';
				$content = $file->{$file->_tbl_field};
			}
			else
			{
				continue;
			}

			$origContent = $content;

			$content = $this->replaceString($file, $content, 'core/slim_base.css', 'core/base.min.css');
			$content = $this->replaceString($file, $content, 'core/slim_iehacks.css', 'core/iehacks.min.css');
			$content = $this->replaceString($file, $content, 'core/slim_base-rtl.css', 'core/base-rtl.min.css');
			$content = $this->replaceString($file, $content, 'screen/forms.css', 'forms/gray-theme.css');

			if ($ext == 'php')
			{
				if (strpos($content, '</body>'))
				{
					$_search = 'defined(\'_JEXEC\') or die;';
					$_replace = 'defined(\'_JEXEC\') or die;' . "\n\n" . '$this->setHtml5(true);' . "\n\n"
								. '$this->setMetaData(\'viewport\', \'width=device-width, initial-scale=1.0\');' . "\n";
					$content = $this->replaceString($file, $content, $_search, $_replace);
				}

				$content = $this->replaceString($file, $content, '<div id="col3_content_wrapper">', '<div class="ym-contain-fl">');

				$content = $this->addClassOnIdHtmlMarkup($file, $content, 'ym-ie-clearing', 'div', 'ie_clearing');
				$content = $this->replaceClassHtmlMarkup($file, $content, 'equalize', 'ym-equalize', 'div');
				$content = $this->replaceClassHtmlMarkup($file, $content, 'hideme', 'ym-hideme', '*');
				$content = $this->replaceClassHtmlMarkup($file, $content, 'skip', 'ym-skip', '*');
				$content = $this->replaceClassHtmlMarkup($file, $content, 'print', 'ym-print', '*');
				$content = $this->replaceClassHtmlMarkup($file, $content, 'noprint', 'ym-noprint', '*');

				$content = $this->replaceClassHtmlMarkup($file, $content, 'clearfix', 'ym-clearfix', 'div');
				$content = $this->replaceClassHtmlMarkup($file, $content, 'clearfix', 'ym-clearfix', 'p');
				$content = $this->replaceClassHtmlMarkup($file, $content, 'floatbox', 'ym-contain-fl', 'div');
				$content = $this->replaceClassHtmlMarkup($file, $content, 'floatbox', 'ym-contain-fl', 'p');

				$content = $this->replaceClassHtmlMarkup($file, $content, 'page_margins', 'ym-wrapper', 'div');
				$content = $this->replaceClassHtmlMarkup($file, $content, 'page', 'ym-wbox', 'div');

				$content = $this->replaceClassHtmlMarkup($file, $content, 'yform', 'ym-form', 'form');
				$content = $this->replaceClassHtmlMarkup($file, $content, 'full', 'ym-full', 'form');
				$content = $this->replaceClassHtmlMarkup($file, $content, 'columnar', 'ym-columnar', 'form');

				$content = $this->replaceClassHtmlMarkup($file, $content, 'type-text', 'ym-fbox-text', 'div');
				$content = $this->replaceClassHtmlMarkup($file, $content, 'type-select', 'ym-fbox-select', 'div');
				$content = $this->replaceClassHtmlMarkup($file, $content, 'type-check', 'ym-fbox-check', 'div');
				$content = $this->replaceClassHtmlMarkup($file, $content, 'type-button', 'ym-fbox-button', 'div');

				if ($filename == 'shinybuttons.php' || $filename = 'slidingdoor.php')
				{
					$content = $this->replaceString($file, $content, '$classes[] = \'hlist\'', '$classes[] = \'ym-hlist\'');
					$content = $this->replaceString($file, $content, '$classes[] = \'hlist-sfx-\'', '$classes[] = \'ym-hlist-sfx-\'');
				}

				$content = $this->replaceClassHtmlMarkup($file, $content, 'subcolumns', 'ym-grid', 'div');
				$colRanges = range(1, 100, 1);
				foreach ($colRanges as $colRange)
				{
					$content = $this->replaceClassHtmlMarkup($file, $content, 'c' . $colRange . 'l', 'ym-g' . $colRange . ' ym-gl', 'div');
					$content = $this->replaceClassHtmlMarkup($file, $content, 'c' . $colRange . 'r', 'ym-g' . $colRange . ' ym-gr', 'div');
				}
				$content = $this->replaceClassHtmlMarkup($file, $content, 'subcl', 'ym-gbox ym-gbox-left', 'div');
				$content = $this->replaceClassHtmlMarkup($file, $content, 'subcr', 'ym-gbox ym-gbox-right', 'div');
				$content = $this->replaceClassHtmlMarkup($file, $content, 'subc', 'ym-gbox', 'div');

				if ($filename != 'flexible_grid_simple_draft.php')
				{
					$content = $this->addClassOnIdHtmlMarkup($file, $content, 'ym-column', 'div', 'main');
					$content = $this->addClassOnIdHtmlMarkup($file, $content, 'ym-col1', 'div', 'col1');
					$content = $this->addClassOnIdHtmlMarkup($file, $content, 'ym-col2', 'div', 'col2');
					$content = $this->addClassOnIdHtmlMarkup($file, $content, 'ym-col3', 'div', 'col3');
					$content = $this->addClassOnIdHtmlMarkup($file, $content, 'ym-cbox', 'div', 'col1_content');
					$content = $this->addClassOnIdHtmlMarkup($file, $content, 'ym-cbox', 'div', 'col2_content');
					$content = $this->addClassOnIdHtmlMarkup($file, $content, 'ym-cbox', 'div', 'col3_content');
				}

				// Simple jdoc replace jdoc from subtemplates to grids
				$content = $this->replaceString($file, $content, ' style="subtemplates"', ' style="grids"');

				// Repalce the removed DS constant with DIRECTORY_SEPARATOR
				$content = preg_replace('#(.*\.\s*)(DS)(\s*\..*)#U', '$1DIRECTORY_SEPARATOR$3', $content);
			}

			if ($ext == 'css' || $ext == 'js')
			{
				$content = $this->replaceCssSelector($file, $content, '#skiplinks a.skip', '.ym-skiplinks a.ym-skip');

				$content = $this->replaceCssSelector($file, $content, '.clearfix', '.ym-clearfix');
				$content = $this->replaceCssSelector($file, $content, '.floatbox', '.ym-contain-fl');

				$content = $this->replaceCssSelector($file, $content, '.equalize', '.ym-equalize');
				$content = $this->replaceCssSelector($file, $content, '.hideme', '.ym-hideme');
				$content = $this->replaceCssSelector($file, $content, '.skip', '.ym-skip');
				$content = $this->replaceCssSelector($file, $content, '.print', '.ym-print');
				$content = $this->replaceCssSelector($file, $content, '.noprint', '.ym-noprint');

				$content = $this->replaceCssSelector($file, $content, '.page_margins', '.ym-wrapper');
				$content = $this->replaceCssSelector($file, $content, '.page', '.ym-wbox');

				$content = $this->replaceCssSelector($file, $content, '.vlist', '.ym-vlist');
				$content = $this->replaceCssSelector($file, $content, '.hlist', '.ym-hlist');

				$content = $this->replaceCssSelector($file, $content, '.yform', '.ym-form');
				$content = $this->replaceCssSelector($file, $content, 'form.full', 'form.ym-full');
				$content = $this->replaceCssSelector($file, $content, 'form.columnar', 'form.ym-columnar');

				$content = $this->replaceCssSelector($file, $content, '.type-text', '.ym-fbox-text');
				$content = $this->replaceCssSelector($file, $content, '.type-select', '.ym-fbox-select');
				$content = $this->replaceCssSelector($file, $content, '.type-check', '.ym-fbox-check');
				$content = $this->replaceCssSelector($file, $content, '.type-button', '.ym-fbox-button');

				$content = $this->replaceCssSelector($file, $content, '.subcolumns', '.ym-grid');
				$colRanges = range(1, 100, 1);
				foreach ($colRanges as $colRange)
				{
					$content = $this->replaceCssSelector($file, $content, '.c' . $colRange . 'l', '.ym-g' . $colRange);
					$content = $this->replaceCssSelector($file, $content, '.c' . $colRange . 'r', '.ym-g' . $colRange);
				}
				$content = $this->replaceCssSelector($file, $content, '.subcl', 'ym-gbox ym-gbox-left');
				$content = $this->replaceCssSelector($file, $content, '.subcr', 'ym-gbox');
				$content = $this->replaceCssSelector($file, $content, '.subc', 'ym-gbox ym-gbox-right');

				$content = $this->replaceCssSelector($file, $content, '#col1', '.ym-col1');
				$content = $this->replaceCssSelector($file, $content, '#col2', '.ym-col2');
				$content = $this->replaceCssSelector($file, $content, '#col3', '.ym-col3');
				$content = $this->replaceCssSelector($file, $content, '#col1_content', '.ym-col1 .ym-cbox');
				$content = $this->replaceCssSelector($file, $content, '#col2_content', '.ym-col2 .ym-cbox');
				$content = $this->replaceCssSelector($file, $content, '#col3_content', '.ym-col3 .ym-cbox');

				$content = $this->replaceCssSelector($file, $content, '#system-message dd.message', '#system-message .alert-message');
				$content = $this->replaceCssSelector($file, $content, '#system-message dd.error', '#system-message .alert-error');
				$content = $this->replaceCssSelector($file, $content, '#system-message dd.warning', '#system-message .alert-warning');
				$content = $this->replaceCssSelector($file, $content, '#system-message dd.notice', '#system-message .alert-notice');
				$content = $this->replaceCssSelector($file, $content, '#system-message dd ul', '#system-message .alert > div > p');
				$content = $this->replaceCssSelector($file, $content, '#system-message dt', '#system-message .alert-heading');
				$content = $this->replaceCssSelector($file, $content, '#system-message dd', '#system-message .alert > div');

				if ($filename == 'screen.basemod.css')
				{
					$_search = '#system-message .alert-heading';
					$_replace = "#system-message a.close { display: none; }\n\t#system-message .alert-heading";
					$content = $this->replaceString($file, $content, $_search, $_replace);
				}

				if ($filename == 'screen.content.css')
				{
					$content = $this->replaceString($file, $content, '.tip', '.tip-wrap .tip');

					$_search = '/* content article info */';
					$_replace = '';
					$_replace .= "\t" . '.jyaml-bootstrap-enabled ul.dropdown-menu.actions {' . "\n";
					$_replace .= "\t\t" . 'padding: 5px 0;' . "\n";
					$_replace .= "\t\t" . 'margin: 2px 0 0;' . "\n";
					$_replace .= "\t" . '}' . "\n";
					$_replace .= "\t" . '.jyaml-bootstrap-enabled ul.dropdown-menu.actions li {' . "\n";
					$_replace .= "\t\t" . 'width: auto; margin: 0;' . "\n";
					$_replace .= "\t\t" . 'padding: 0; float: none;' . "\n";
					$_replace .= "\t" . '}' . "\n\n";
					$_replace .= $_search;

					$content = $this->replaceString($file, $content, $_search, $_replace);
				}

				if ($filename == 'navigation.dropdown.css')
				{
					$_search = array(
						'libraries/jyaml/dropdown/css/base.css',
						'libraries/jyaml/dropdown/css/patch.css'
					);
					$_replace = array(
						'libraries/jyaml/html/css/accessible_dropdown_base.css',
						'libraries/jyaml/html/css/accessible_dropdown_patch.css'
					);

					foreach ($_search as $_k => $_v)
					{
						$content = $this->replaceString($file, $content, $_search[$_k], $_replace[$_k]);
					}
				}
			}

			$cleanOrigContent = str_replace(array(' ', "\n", "\r"), '', $origContent);
			$cleanContent = str_replace(array(' ', "\n", "\r"), '', $content);

			$hasChanges = ($cleanOrigContent !== $cleanContent);

			if ($hasChanges)
			{
				if (is_string($file))
				{
					// Check file is writeable
					if (!JYAML::_isWritable($file))
					{
						$result = false;
						$this->addErrorLog('File or Directory not writable (please check permissions): ' . $file);
						continue;
					}

					$backupFilename = JPath::clean($file . '.bak-migrate-yaml3-to-yaml4');
					if (JYAML::_isWritable($backupFilename))
					{
						// Create backup of file
						if (!$this->test && !JFile::exists($backupFilename))
						{
							JFile::write($backupFilename, $origContent);
							$this->addLog('Created backup file: ' . basename($backupFilename), $file);
						}

						// Write the new content
						if (!$this->test)
						{
							JFile::write($file, $content);
						}
					}
					else
					{
						$result = false;
						$this->addErrorLog('Backupfile not writable (please check permissions): ' . $backupFilename);
						continue;
					}
				}
				elseif (is_object($file))
				{
					$item = $file;
					$itemId = $item->{$item->_tbl_key};

					$backupFilename = JPath::clean($this->templatePath . '/migrate-yaml3-to-yaml4-database-backup.sql');
					if (JYAML::_isWritable($backupFilename))
					{
						// Create backup of database entry
						if (!JFile::exists($backupFilename))
						{
							$_comment = "-- \n";
							$_comment .= "-- SQL-Backup of contents before migrating JYAML4.0 (YAML3) to JYAML4.5 (YAML4).\n";
							$_comment .= "-- You can put this as sql statement (e.g. in phpMyAdmin) to restore the contents.\n";
							$_comment .= "-- If no SQL statements shown below, then nothing has changed in the database.\n";
							$_comment .= "-- \n\n";
							JFile::write($backupFilename, $_comment);
						}
						$sql = file_get_contents($backupFilename);

						$query->clear()
							->update($db->qn($item->_tbl_name))
							->set($db->qn($item->_tbl_field) . ' = ' . $db->q($origContent))
							->where($db->qn($item->_tbl_key) . ' = ' . $db->q($itemId));
						$addSql = $db->replacePrefix((string) $query) . ";\n";

						if (strpos($sql, $addSql) === false)
						{
							$sql .= $addSql;

							if (!$this->test)
							{
								JFile::write($backupFilename, $sql);
							}
						}

						$this->addLog('Created SQL backup in template folder in file: ' . basename($backupFilename), $file);

						// Write the new content
						$query->clear()
							->update($db->qn($item->_tbl_name))
							->set($db->qn($item->_tbl_field) . ' = ' . $db->q($content))
							->where($db->qn($item->_tbl_key) . ' = ' . $db->q($itemId));
						$db->setQuery($query);

						// Update database entry
						if (!$this->test)
						{
							$db->execute();
						}
					}
					else
					{
						$result = false;
						$this->addErrorLog('Backupfile not writable (please check permissions): ' . $backupFilename);
						continue;
					}
				}
			}
		}

		return $result;
	}

	/**
	 * Method to prepare some things in the template configurations
	 *
	 * @return Turue on success
	 */
	private function migrateTemplateConfig()
	{
		$success = true;
		$template = $this->template;
		$clientId = $this->clientId;

		// Search all Template configurations (styles) of this template
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->select('*')
			->from($db->qn('#__template_styles'))
			->where($db->qn('template') . ' = ' . $db->q($template))
			->where($db->qn('client_id') . ' = ' . $db->q($clientId));
		$db->setQuery($query);

		if (($styles = $db->loadObjectList()))
		{
			foreach ($styles as $style)
			{
				$doUpdate = false;
				$params = new \Joomla\Registry\Registry;
				$params->loadString($style->params);
				$positions = $params->get('positions');

				if (is_object($positions) || is_array($positions))
				{
					foreach ($positions as $pos => $_positons)
					{
						if (is_object($_positons) || is_array($_positons))
						{
							foreach ($_positons as $k => $_positon)
							{
								// Convert modChrome subtemplates to grids
								if (isset($_positon->style) && $_positon->style == 'subtemplates')
								{
									$doUpdate = true;
									$positions->{$pos}->{$k}->style = 'grids';
									$this->addLog('Converted modChrome style "subtemplates" to "grids"', 'Template Configuration [Style: ' . $style->title . ']');
								}

								if (isset($_positon->style))
								{
									if (isset($_positon->__attributes) && isset($_positon->__attributes->params))
									{
										if (isset($_positon->__attributes->params->headerLevel))
										{
											$_headerLevel = $_positon->__attributes->params->headerLevel;
											$_search = array('1', '2', '3', '4', '5', '6');
											$_replace = array('h1', 'h2', 'h3', 'h4', 'h5', 'h6');

											$positions->{$pos}->{$k}->__attributes->params->headlineTag = str_replace($_search, $_replace, $_headerLevel, $_rCount);
											unset($positions->{$pos}->{$k}->__attributes->params->headerLevel);

											if ($_rCount)
											{
												$doUpdate = true;
												$this->addLog('Converted modChrome options headerLevel into headlineTag', 'Template Configuration [Style: ' . $style->title . ']');
											}

										}

										if (isset($_positon->__attributes->params->effects) && $_positon->style == 'accessible_tabs')
										{
											$_effects = $_positon->__attributes->params->effects;
											$_search = array('null', '[\'fade\']', '[\'slide\']', '[\'fullslide\']', '[\'slide\', \'fade\']', '[\'fullslide\', \'fade\']');
											$_replace = array('show', 'fadeIn', 'slideDown', 'slideDown', 'slideDown', 'slideDown');

											$positions->{$pos}->{$k}->__attributes->params->effects = str_replace($_search, $_replace, $_effects, $_rCount);

											if ($_rCount)
											{
												$doUpdate = true;
												$this->addLog('Converted modChrome accessible_tabs effects options', 'Template Configuration [Style: ' . $style->title . ']');
											}
										}

										if (isset($_positon->__attributes->params->transition))
										{
											$_transition = $_positon->__attributes->params->transition;
											$_search = array(
													'Fx.Transitions.linear.easeOut',
													'Fx.Transitions.linear.easeIn',
													'Fx.Transitions.linear.easeInOut',
													'Fx.Transitions.Quad.easeOut',
													'Fx.Transitions.Quad.easeIn',
													'Fx.Transitions.Quad.easeInOut',
													'Fx.Transitions.Cubic.easeOut',
													'Fx.Transitions.Cubic.easeIn',
													'Fx.Transitions.Cubic.easeInOut',
													'Fx.Transitions.Quart.easeOut',
													'Fx.Transitions.Quart.easeIn',
													'Fx.Transitions.Quart.easeInOut',
													'Fx.Transitions.Quint.easeOut',
													'Fx.Transitions.Quint.easeIn',
													'Fx.Transitions.Quint.easeInOut',
													'Fx.Transitions.Pow.easeOut',
													'Fx.Transitions.Pow.easeIn',
													'Fx.Transitions.Pow.easeInOut',
													'Fx.Transitions.Expo.easeOut',
													'Fx.Transitions.Expo.easeIn',
													'Fx.Transitions.Expo.easeInOut',
													'Fx.Transitions.Circ.easeOut',
													'Fx.Transitions.Circ.easeIn',
													'Fx.Transitions.Circ.easeInOut',
													'Fx.Transitions.Sine.easeOut',
													'Fx.Transitions.Sine.easeIn',
													'Fx.Transitions.Sine.easeInOut',
													'Fx.Transitions.Back.easeOut',
													'Fx.Transitions.Back.easeIn',
													'Fx.Transitions.Back.easeInOut',
													'Fx.Transitions.Bounce.easeOut',
													'Fx.Transitions.Bounce.easeIn',
													'Fx.Transitions.Bounce.easeInOut',
													'Fx.Transitions.Elastic.easeOut',
													'Fx.Transitions.Elastic.easeIn',
													'Fx.Transitions.Elastic.easeInOut'
											);
											$_replace = array(
													'linear',
													'linear',
													'linear',
													'easeInOutQuad',
													'easeInOutQuad',
													'easeInOutQuad',
													'easeInOutCubic',
													'easeInOutCubic',
													'easeInOutCubic',
													'easeInOutQuart',
													'easeInOutQuart',
													'easeInOutQuart',
													'easeInOutQuint',
													'easeInOutQuint',
													'easeInOutQuint',
													'swing',
													'swing',
													'swing',
													'easeInOutExpo',
													'easeInOutExpo',
													'easeInOutExpo',
													'easeInOutCirc',
													'easeInOutCirc',
													'easeInOutCirc',
													'easeInOutSine',
													'easeInOutSine',
													'easeInOutSine',
													'easeInOutBack',
													'easeInOutBack',
													'easeInOutBack',
													'easeInOutBounce',
													'easeInOutBounce',
													'easeInOutBounce',
													'easeInOutElastic',
													'easeInOutElastic',
													'easeInOutElastic'
											);

											$positions->{$pos}->{$k}->__attributes->params->transition = str_replace($_search, $_replace, $_transition, $_rCount);

											if ($_rCount)
											{
												$doUpdate = true;
												$this->addLog('Converted modChrome transitions options from MooTools to jQuery', 'Template Configuration [Style: ' . $style->title . ']');
											}
										}
									}
								}
							}
						}
					}
				}

				if ($doUpdate)
				{
					$params->set('positions', $positions);
					$style->params = (string) $params->toString('JSON');

					$query->clear()
						->update($db->qn('#__template_styles'))
						->set($db->qn('params') . ' = ' . $db->q($style->params))
						->where($db->qn('id') . ' = ' . $db->q($style->id));
					$db->setQuery($query);

					if (!$this->test)
					{
						$db->execute();
					}
				}
			}
		}

		// Modules
		$query->clear()
			->select('*')
			->from($db->qn('#__modules'))
			->where($db->qn('module') . ' = ' . $db->q('mod_menu'));
		$db->setQuery($query);

		if (($modules = $db->loadObjectList()))
		{
			foreach ($modules as $module)
			{
				$params = new \Joomla\Registry\Registry();
				$params->loadString($module->params);
				$hideTransition = $params->get('jyaml_dropdown_hide_transition');
				$showTransition = $params->get('jyaml_dropdown_show_duration');

				$_search = array(
						'Fx.Transitions.linear.easeOut',
						'Fx.Transitions.linear.easeIn',
						'Fx.Transitions.linear.easeInOut',
						'Fx.Transitions.Quad.easeOut',
						'Fx.Transitions.Quad.easeIn',
						'Fx.Transitions.Quad.easeInOut',
						'Fx.Transitions.Cubic.easeOut',
						'Fx.Transitions.Cubic.easeIn',
						'Fx.Transitions.Cubic.easeInOut',
						'Fx.Transitions.Quart.easeOut',
						'Fx.Transitions.Quart.easeIn',
						'Fx.Transitions.Quart.easeInOut',
						'Fx.Transitions.Quint.easeOut',
						'Fx.Transitions.Quint.easeIn',
						'Fx.Transitions.Quint.easeInOut',
						'Fx.Transitions.Pow.easeOut',
						'Fx.Transitions.Pow.easeIn',
						'Fx.Transitions.Pow.easeInOut',
						'Fx.Transitions.Expo.easeOut',
						'Fx.Transitions.Expo.easeIn',
						'Fx.Transitions.Expo.easeInOut',
						'Fx.Transitions.Circ.easeOut',
						'Fx.Transitions.Circ.easeIn',
						'Fx.Transitions.Circ.easeInOut',
						'Fx.Transitions.Sine.easeOut',
						'Fx.Transitions.Sine.easeIn',
						'Fx.Transitions.Sine.easeInOut',
						'Fx.Transitions.Back.easeOut',
						'Fx.Transitions.Back.easeIn',
						'Fx.Transitions.Back.easeInOut',
						'Fx.Transitions.Bounce.easeOut',
						'Fx.Transitions.Bounce.easeIn',
						'Fx.Transitions.Bounce.easeInOut',
						'Fx.Transitions.Elastic.easeOut',
						'Fx.Transitions.Elastic.easeIn',
						'Fx.Transitions.Elastic.easeInOut'
				);
				$_replace = array(
						'linear',
						'linear',
						'linear',
						'easeInOutQuad',
						'easeInOutQuad',
						'easeInOutQuad',
						'easeInOutCubic',
						'easeInOutCubic',
						'easeInOutCubic',
						'easeInOutQuart',
						'easeInOutQuart',
						'easeInOutQuart',
						'easeInOutQuint',
						'easeInOutQuint',
						'easeInOutQuint',
						'swing',
						'swing',
						'swing',
						'easeInOutExpo',
						'easeInOutExpo',
						'easeInOutExpo',
						'easeInOutCirc',
						'easeInOutCirc',
						'easeInOutCirc',
						'easeInOutSine',
						'easeInOutSine',
						'easeInOutSine',
						'easeInOutBack',
						'easeInOutBack',
						'easeInOutBack',
						'easeInOutBounce',
						'easeInOutBounce',
						'easeInOutBounce',
						'easeInOutElastic',
						'easeInOutElastic',
						'easeInOutElastic'
				);

				$hideTransition = str_replace($_search, $_replace, $hideTransition);
				$showTransition = str_replace($_search, $_replace, $showTransition, $_rCount);

				$params->set('jyaml_dropdown_hide_transition', $hideTransition);
				$params->set('jyaml_dropdown_show_duration', $showTransition);

				$module->params = (string) $params->toString('JSON');

				$query->clear()
					->update($db->qn('#__modules'))
					->set($db->qn('params') . ' = ' . $db->q($module->params))
					->where($db->qn('id') . ' = ' . $db->q($module->id));
				$db->setQuery($query);

				if ($_rCount)
				{
					$this->addLog('Converted dropdown transitions options from MooTools to jQuery');
				}

				if (!$this->test)
				{
					$db->execute();
				}
			}
		}

		return $success;
	}
}
