<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  UpdateScript
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 *
 */

defined('_JEXEC') or die;

/**
 * JYAMLUpdateScript class
 *
 * @package     JYAML
 * @subpackage  UpdateScript
 */
class JYAMLUpdateScriptUpdate451To452 extends JYAMLUpdateScript
{
	/**
	 * Get title text
	 *
	 * @return string title text
	 */
	public function getTitleText()
	{
		return 'Update 4.5.1 to 4.5.2';
	}

	/**
	 * Get description text
	 *
	 * @return string Description text
	 */
	public function getDescriptionText()
	{
		return 'This migration resolves an issue with the column display (e.g. col1/2 is shown always)';
	}

	/**
	 * Start the update
	 *
	 * @param   string   $template  Template
	 * @param   integer  $clientId  Client Id
	 * @param   boolean  $test      If true changes are not applied, only logged
	 *
	 * @return True on Success
	 */
	public function run($template, $clientId, $test = false)
	{
		$template = (string) $template;
		$clientId = (int) $clientId;

		$this->test = $test;
		$this->template = $template;
		$this->clientId = $clientId;

		$client = JApplicationHelper::getClientInfo($clientId);

		if (!$client)
		{
			$this->addErrorLog('Invalid client info (JApplicationHelper::getClientInfo) - $clientId = ' . $clientId);
			return false;
		}

		// Set absolute template path
		$this->templatePath = $client->path . '/templates/' . $template;

		if (!JFolder::exists($this->templatePath))
		{
			$this->addErrorLog('Template directory does not exist: ' . $this->templatePath);
			return false;
		}

		// Update contents in template files
		$result = $this->parseFiles();

		$logFile = JPath::clean($this->templatePath . '/update-jyaml-451-to-452.log');
		$logEntries = $this->getLog();
		if (!$this->test && $result && !empty($logEntries) && JYAML::_isWritable($logFile))
		{
			$date = new JDate(time());
			$time = $date->toSql();

			$logString = '';
			if (JFile::exists($logFile))
			{
				$logString = file_get_contents($logFile);
			}
			$logString .= ':::::::::::::::::::::::::::' . "\n";
			$logString .= ':: [' . $time . '] ::' . "\n";
			$logString .= ':::::::::::::::::::::::::::' . "\n\n";

			foreach ($logEntries as $title => $entries)
			{
				if (empty ($entries))
				{
					continue;
				}

				$logString .= "\t" . $title . "\n";
				foreach ($entries as $entry)
				{
					$logString .= "\t\t\t" . $entry . "\n";
				}
				$logString .= "\n\n";
			}
			$logString .= "\n\n";

			JFile::write($logFile, $logString);
			$this->addLog('Created log file in template folder', $logFile);
		}

		return $result;
	}

	/**
	 * Update contents in template files
	 * Also create backup files
	 *
	 * @return True on success
	 */
	private function parseFiles()
	{
		static $files;

		$result = true;

		// Exclude all necessary updated files defined in the update XML and all new files (from parent update steps)
		$exclude = $this->excludeFiles;

		if ($files === null)
		{
			$htmlFiles = JFolder::files($this->templatePath . '/html/index', '.php$', true, true);
			$cssFiles = JFolder::files($this->templatePath . '/css', '.css$', true, true);
			$files = (array) array_merge($htmlFiles, $cssFiles);
		}

		foreach ($files as $file)
		{
			if (is_string($file))
			{
				$file = JPath::clean($file);

				if (in_array($file, $exclude))
				{
					continue;
				}

				$filename = basename($file);
				$ext = strtolower(JFile::getExt($file));
				$content = file_get_contents($file);

			}
			elseif(is_object($file))
			{
				$filename = $file->_tbl_name . '.' . $file->_tbl_field;
				$ext = 'php';
				$content = $file->{$file->_tbl_field};
			}
			else
			{
				continue;
			}

			$origContent = $content;

			if ($ext == 'php')
			{
				$content = $this->replaceString($file, $content, '<div class="ym-column">', '<div class="ym-column ym-column-main">');
			}

			if ($ext == 'css')
			{
				if ($filename == 'screen.basemod.css')
				{
					foreach (array('3', '13', '32', '31', '23', '132', '123', '213', '231', '312', '321') as $layout)
					{
						$cols = strlen($layout);
						$content = $this->replaceCssSelector($file, $content, 'body.layout-' . $cols . 'col_' . $layout . ' #main > div > .ym-col1', 'body.layout-' . $cols . 'col_' . $layout . ' .ym-column-main > .ym-col1');
						$content = $this->replaceCssSelector($file, $content, 'body.layout-' . $cols . 'col_' . $layout . ' #main > div > .ym-col2', 'body.layout-' . $cols . 'col_' . $layout . ' .ym-column-main > .ym-col2');
						$content = $this->replaceCssSelector($file, $content, 'body.layout-' . $cols . 'col_' . $layout . ' #main > div > .ym-col3', 'body.layout-' . $cols . 'col_' . $layout . ' .ym-column-main > .ym-col3');
					}
				}
			}

			$cleanOrigContent = str_replace(array(' ', "\n", "\r"), '', $origContent);
			$cleanContent = str_replace(array(' ', "\n", "\r"), '', $content);

			$hasChanges = ($cleanOrigContent !== $cleanContent);

			if ($hasChanges)
			{
				if (is_string($file))
				{
					// Check file is writeable
					if (!JYAML::_isWritable($file))
					{
						$result = false;
						$this->addErrorLog('File or Directory not writable (please check permissions): ' . $file);
						continue;
					}

					$backupFilename = JPath::clean($file . '.bak-update-jyaml-451-to-452');
					if (JYAML::_isWritable($backupFilename))
					{
						// Create backup of file
						if (!$this->test && !JFile::exists($backupFilename))
						{
							JFile::write($backupFilename, $origContent);
							$this->addLog('Created backup file: ' . basename($backupFilename), $file);
						}

						// Write the new content
						if (!$this->test)
						{
							JFile::write($file, $content);
						}
					}
					else
					{
						$result = false;
						$this->addErrorLog('Backup file not writable (please check permissions): ' . $backupFilename);
						continue;
					}
				}
			}
		}

		return $result;
	}
}
