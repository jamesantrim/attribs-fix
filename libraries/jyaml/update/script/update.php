<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  UpdateScript
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 *
 */

defined('_JEXEC') or die;

jimport('jyaml.jyaml');
jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.file');

/**
 * JYAMLUpdateScript class
 *
 * @package     JYAML
 * @subpackage  UpdateScript
 * @since       4.5.0
 */
class JYAMLUpdateScript
{
	protected $template = null;

	protected $clientId = null;

	protected $test = false;

	protected $templatePath = '';

	protected $excludeFiles = array();

	protected $log = array();

	protected $errorLog = array();

	/**
	 * Get the instance of an update script
	 *
	 * @param   string  $script  Absolute path of the update script
	 *
	 * @return self extended
	 */
	public static function getInstance($script)
	{
		static $instances = array();

		$hash = md5($script);

		if (!isset($instances[$hash]))
		{
			if (JFile::exists($script))
			{
				require_once $script;
			}
			else
			{
				$instances[$hash] = false;
				return false;
			}

			$file = basename($script);
			$className = JFile::stripExt($file);
			$className = str_replace('-', ' ', $className);
			$className = ucwords($className);
			$className = 'JYAMLUpdateScript' . str_replace(' ', '', $className);

			if (class_exists($className))
			{
				$instances[$hash] = new $className;
			}
			else
			{
				$instances[$hash] = false;
				return false;
			}
		}

		return $instances[$hash];
	}

	/**
	 * Get title text
	 * Overloaded by subclasses
	 *
	 * @return title text
	 */
	public function getTitleText()
	{
		return '';
	}

	/**
	 * Get description text
	 * Overloaded by subclasses
	 *
	 * @return Description text
	 */
	public function getDescriptionText()
	{
		return '';
	}

	/**
	 * Set exclude Files
	 *
	 * @param   array  $files  Array of files with absolute paths
	 *
	 * @return void
	 */
	public function setExcludeFiles($files)
	{
		if (is_array($files) && !empty($files))
		{
			$this->excludeFiles = $files;
		}
	}

	/**
	 * Start the update
	 * Overloaded by subclasses
	 *
	 * @param   string   $template  Template
	 * @param   integer  $clientId  Client Id
	 * @param   boolean  $test      If true changes are not applied, only logged
	 *
	 * @return True on Success
	 */
	public function run($template, $clientId, $test = false)
	{
		return true;
	}

	/**
	 * Method to add an css class within a given id on html tag
	 *
	 * @param   string   $file      File
	 * @param   string   $content   Content
	 * @param   string   $class     The class to add
	 * @param   string   $htmlTag   Html-Tag
	 * @param   string   $id        The Id to search
	 * @param   boolean  $removeId  Remove the matched id, default false
	 *
	 * @return mixed
	 */
	protected function addClassOnIdHtmlMarkup($file, $content, $class, $htmlTag, $id, $removeId = false)
	{
		$this->_callback_class = $class;
		$this->_callback_htmlTag = $htmlTag;
		$this->_callback_id = $id;
		$this->_callback_removeId = $removeId;
		$this->_callback_file = $file;

		/* Parse html tag with negative lockbehind for php code (like -> and ?>) */
		$regex = '#<' . $htmlTag . '(.*)(?<![-\?])>#U';
		$callback = array($this, 'addClassOnIdHtmlMarkup_callback');
		$content = preg_replace_callback($regex, $callback, $content);

		unset($this->_callback_class);
		unset($this->_callback_htmlTag);
		unset($this->_callback_id);
		unset($this->_callback_file);

		return $content;
	}

	/**
	 * Preg Preplace Callback for self::addClassOnIdHtmlMarkup
	 *
	 * @param   array  $m  Matches
	 *
	 * @return Replaced conent
	 */
	private function addClassOnIdHtmlMarkup_callback($m)
	{
		$class = $this->_callback_class;
		$htmlTag = $this->_callback_htmlTag;
		$id = $this->_callback_id;
		$removeId = $this->_callback_removeId;
		$file = $this->_callback_file;

		$attributes = $m[1];

		// Encode inline php code without withespace
		$attributes = preg_replace_callback('#<\?php.*\?>#Uis', create_function('$m', 'return " [php]".base64_encode($m[0])."[/php]";'), $attributes);

		$attributes = JUtility::parseAttributes($attributes);

		if (isset($attributes['id']) && $attributes['id'] == $id)
		{
			if (!isset($attributes['class']))
			{
				$attributes['class'] = '';
			}

			$classes = explode(' ', $attributes['class']);
			foreach ($classes as $k => $v)
			{
				$classes[$k] = trim($v);
			}

			if (!in_array($class, $classes))
			{
				$classes[] = trim($class);
				$classes = array_unique($classes);
				$attributes['class'] = trim(implode(' ', $classes));

				if ($removeId)
				{
					unset($attributes['id']);
				}

				if ($attributes['class'] == '')
				{
					unset($attributes['class']);
				}

				$this->addLog('Add Class "' . $class . '" on "' . $htmlTag . '#' . $id . '"', $file);
			}
		}
		else
		{
			return $m[0];
		}

		$attributes = \Joomla\Utilities\ArrayHelper::toString($attributes);

		// Encode inline php code
		$attributes = str_replace('" [php]', '"[php]', $attributes);
		$attributes = preg_replace_callback('#\[php\](.*)\[/php\]#Uis', create_function('$m', 'return base64_decode($m[1]);'), $attributes);

		return '<' . $htmlTag . ' ' . $attributes . '>';
	}

	/**
	 * Method to replace css class within a given html tag
	 *
	 * @param   string  $file     File
	 * @param   string  $content  Content
	 * @param   string  $search   Search context
	 * @param   string  $replace  Replace context
	 * @param   string  $htmlTag  Html-Tag
	 *
	 * @return Replaced content
	 */
	protected function replaceClassHtmlMarkup($file, $content, $search, $replace, $htmlTag)
	{
		if (strpos($content, $search) !== false)
		{
			$this->_callback_htmlTag = $htmlTag;
			$this->_callback_file = $file;
			$this->_callback_search = $search;
			$this->_callback_replace = $replace;
			$this->_callback_isStarSelector = ($htmlTag == '*' ? true : false);

			/* Parse html tag with negative lockbehind for php code (like -> and ?>) */
			$regex = '#([^"\'])([ \t]*)<' . $htmlTag . '(.*)(?<![-\?])>#U';
			$callback = array($this, 'replaceClassHtmlMarkup_callback');
			$content = preg_replace_callback($regex, $callback, $content);

			unset($this->_callback_htmlTag);
			unset($this->_callback_file);
			unset($this->_callback_search);
			unset($this->_callback_replace);
		}

		return $content;
	}

	/**
	 * Preg Preplace Callback for self::replaceClassHtmlMarkup
	 *
	 * @param   array  $m  Matches
	 *
	 * @return Replaced conent
	 */
	private function replaceClassHtmlMarkup_callback($m)
	{
		$htmlTag = $this->_callback_htmlTag;
		$file = $this->_callback_file;
		$search = $this->_callback_search;
		$replace = $this->_callback_replace;
		$isStarSelector = $this->_callback_isStarSelector;

		$attributes = $m[3];

		// Encode inline php code without withespace
		$attributes = preg_replace_callback('#<\?php.*\?>#Uis', create_function('$m', 'return " [php]".base64_encode($m[0])."[/php]";'), $attributes);

		$test = JUtility::parseAttributes($attributes);
		if (empty($test))
		{
			return $m[0];
		}
		$attributes = $test;

		if (!isset($attributes['class']))
		{
			return $m[0];
		}

		$classes = explode(' ', $attributes['class']);

		foreach ($classes as $k => $v)
		{
			$v = trim($v);
			if ($v == $search)
			{
				$classes[$k] = $replace;
				$this->addLog('Replaced CSS class in HTML: "' . $search . '" with "' . $replace . '"', $file);
			}
		}
		$classes = array_unique($classes);

		$attributes['class'] = trim(implode(' ', $classes));

		if ($attributes['class'] == '')
		{
			unset($attributes['class']);
		}

		$attributes = \Joomla\Utilities\ArrayHelper::toString($attributes);

		// Encode inline php code
		$attributes = str_replace('" [php]', '"[php]', $attributes);
		$attributes = preg_replace_callback('#\[php\](.*)\[/php\]#Uis', create_function('$m', 'return base64_decode($m[1]);'), $attributes);

		if ($isStarSelector)
		{
			// If html tag search is star we need to detect the current tag
			$tmp = explode('<', $m[0], 2);
			$subject = isset($tmp[1]) ? '<' . $tmp[1] : null;

			if ($subject === null)
			{
				return $m[0];
			}

			// Whitespace before
			$m[2] = preg_replace('/[^\t ]+/', '', $tmp[0]);

			if (preg_match('#^<(.*) #U', $subject, $matches))
			{
				$htmlTag = $matches[1];
			}
		}

		return $m[1] . $m[2] . '<' . $htmlTag . ' ' . $attributes . '>';
	}

	/**
	 * Method to replace an CSS-Selector in content
	 *
	 * @param   string  $file     File
	 * @param   string  $content  Content
	 * @param   string  $search   Search context
	 * @param   string  $replace  Replace context
	 *
	 * @return Replaced content
	 */
	protected function replaceCssSelector($file, $content, $search, $replace)
	{
		if (strpos($content, $search) !== false && strpos($content, $replace) === false)
		{
			$regex = '/' . preg_quote($search) . '([\W])/U';

			if (preg_match($regex, $content))
			{
				$content = preg_replace($regex, "$replace\\1", $content);
				$this->addLog('Replaced CSS-Selector: "' . $search . '" with "' . $replace . '"', $file);
			}
		}

		return $content;
	}

	/**
	 * Method to replace an string in content
	 *
	 * @param   string  $file     File
	 * @param   string  $content  Content
	 * @param   string  $search   Search context
	 * @param   string  $replace  Replace context
	 *
	 * @return Replaced content
	 */
	protected function replaceString($file, $content, $search, $replace)
	{
		if (strpos($content, $search) !== false && strpos($content, $replace) === false)
		{
			$content = str_replace($search, $replace, $content);
			$this->addLog('Replaced string: "' . $search . '" with "' . $replace . '"', $file);
		}

		return $content;
	}

	/**
	 * Get Log messages
	 *
	 * @param   boolean  $asString  Return log as string
	 *
	 * @return Array of log entries
	 */
	public function getLog($asString = false)
	{
		if ($asString)
		{
			$logString = '';
			foreach ($this->log as $title => $entries)
			{
				if (empty ($entries))
				{
					continue;
				}

				$logString .= $title . "\n";
				foreach ($entries as $entry)
				{
					$logString .= "\t" . $entry . "\n";
				}
				$logString .= "\n\n";
			}
			$logString .= "\n\n";

			return htmlentities($logString);
		}

		return $this->log;
	}

	/**
	 * Get Error messages
	 *
	 * @return Array of error entries
	 */
	public function getErrorLog()
	{
		return $this->errorLog;
	}

	/**
	 * Add an log message.
	 *
	 * @param   string  $log    Log message
	 * @param   string  $group  Groupname
	 *
	 * @return void
	 */
	protected function addLog($log, $group)
	{
		if (is_string($group))
		{
			$group = '[JPATH_ROOT]' . substr($group, strlen(JPATH_ROOT));
			$hash = $group;
		}
		elseif (is_object($group))
		{
			$hash = 'Database Table: ' . $group->_tbl_name . ' -> ' . $group->_tbl_field . ' on ' . $group->_tbl_key . ' ' . $group->{$group->_tbl_key};
		}
		else
		{
			return;
		}

		if (!isset($this->log[$hash]))
		{
			$this->log[$hash] = array();
		}

		$log = str_replace(array("\n", "\r"), array('\n', '\r'), $log);
		$this->log[$hash][] = $log;
		$this->log[$hash] = array_unique($this->log[$hash]);
	}

	/**
	 * Add an error log message.
	 *
	 * @param   string  $msg  Error message.
	 *
	 * @return void
	 */
	protected function addErrorLog($msg)
	{
		array_push($this->errorLog, $msg);
	}
}
