<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  Plugin
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

// No direct access
defined('_JEXEC') or die;

// Import library dependencies
jimport('joomla.plugin.plugin');
jimport('jyaml.jyaml');

/**
 * JYAML System Plugin
 *
 * @package     JYAML
 * @subpackage  Plugin
 * @since       4.0.0
 */
class PlgSystemJyaml extends JPlugin
{
	/**
	 * PHP Constructor
	 *
	 * @param   object  &$subject  The object to observe
	 * @param   array   $config    An array that holds the plugin configuration
	 */
	public function __construct(&$subject, $config)
	{
		$app = JFactory::getApplication();

		if (defined('JYAML_LIB_PATH'))
		{
			// Class overwrites
			JLoader::register('JDocumentHTML', JYAML_LIB_PATH . '/overrides/libraries/joomla/document/html/html.php', true);

			JHtml::addIncludePath(JYAML_LIB_PATH . '/html');
			JHtml::_('jyaml.init');

			parent::__construct($subject, $config);

			$jinput = $app->input;
			$jyamlAdminController = $jinput->post->get('jyamladmincontroller');

			if ($jyamlAdminController && $this->dependencyCheck(true))
			{
				JYAMLAdmin::getInstance()->executeController($jyamlAdminController);
			}
		}

		// Clean framework caches on each post request
		if ($app->input->getMethod() == 'POST')
		{
			$this->cleanFrameworkCaches();
		}
	}

	/**
	 * On Joomla initialize
	 *
	 * @return void
	 */
	public function onAfterInitialise()
	{
		/*
		 * Note:
		 * JYAML::getDocument(); here breaks some things like
		 * request checks (active menu item, etc) if SEF enabled.
		 */
	}

	/**
	 * Initialize JYAML
	 *  ! Important to do this at first time on AFTER route (or later).
	 *  ! Elsewhere request checks (like active menu item) are broken, if SEF enabled.
	 *
	 * @return void
	 */
	public function onAfterRoute()
	{
		if ($this->dependencyCheck(false))
		{
			// Set ready to start the static instance
			JYAML::$readyToStart = true;

			// Needed global to load libraries, language, etc.
			JYAML::getDocument();
		}

		if ($this->dependencyCheck(true))
		{
			// Set ready to start the static instance
			JYAML::$readyToStart = true;

			// Run admin action handlers (only if required)
			JYAMLAdmin::run();

			// JYAMLAdmin debug prepare
			if ($this->params->get('show_admin_log', 0))
			{
				JFactory::getDocument()->addStylesheet(JUri::root() . 'libraries/jyaml/assets/css/debug.css');
			}
		}
	}

	/**
	 * Head preparations
	 *
	 * @return void
	 */
	public function onBeforeCompileHead()
	{
		$app = JFactory::getApplication();

		// Do head preparations only if the document format is html
		if (JFactory::getDocument()->getType() != 'html')
		{
			return;
		}

		if ($this->dependencyCheck(false))
		{
			JYAML::getDocument()->onBeforeLoadHead();
		}

		// Some fixes
		$option = $app->input->get('option');
		//$layout = $app->input->get('layout');

		if ($app->isAdmin() && ($option == 'com_modules' || $option == 'com_advancedmodules'))
		{
			// Fix [Module Manager]: Add feature to add custom position on enter
			$_script = "
				try{
					jQuery(document).ready(function($){
						$('select#jform_position,select#batch-position-id').each(function(){
							var select=$(this),chosen=select.data('chosen'),
								chcont=chosen.container,group=select.data('custom_group_text');
							chcont.find('input').on('keyup', function(e){
								var noresult = chcont.find('.no-results'),noresultHtml = noresult.html();
								if (noresult.length && !noresultHtml.test(/ \[Enter\]/)){
									noresult.html(noresultHtml + ' [Enter]');
								}
								if (e.keyCode==13 && noresult.length){
									e.preventDefault();
									var optgroup=select.find('optgroup:last'),
										value=$.trim(chcont.find('input').val()).replace(/[^a-zA-Z0-9_-]+/g, '_');
					                if (!optgroup.length){
										optgroup = select;
									}
									select.find('option:selected').removeAttr('selected');
									optgroup.append('<option selected=\"selected\" value=\"' + value + '\">' + value + '</option>');
									select.trigger(\"liszt:updated\");
									chosen.close_field();
								}
							});
						});
					});
				}catch(e){};
			";
			JFactory::getDocument()->addScriptDeclaration(JYAML::trimmer($_script));
		}
	}

	/**
	 * Markup preparations
	 *
	 * @return void
	 */
	public function onAfterRender()
	{
		// Do markup preparations only if the document format is html
		if (JFactory::getDocument()->getType() != 'html')
		{
			return;
		}

		if ($this->dependencyCheck(false))
		{
			$jyaml = JYAML::getDocument();

			JResponse::setBody(
				$jyaml->optimizeMarkup(
					JResponse::getBody()
				)
			);
		}

		// JYAMLAdmin debug show
		if ($this->dependencyCheck(true) && $this->params->get('show_admin_log', 0))
		{
			$admin = JYAMLAdmin::getInstance();

			ob_start();
				$logData = '';
				echo htmlentities(print_r($admin->getLog(), true));
			/** @noinspection PhpUsageOfSilenceOperatorInspection */
			$logData = @ob_get_contents();
			ob_get_clean();

			$logData = str_replace(JPATH_ROOT, '[JPATH_ROOT]', $logData);
			$logData = '<pre class="trans75 jyaml-debug jyaml-debug-pre"><h1>JYAMLAdmin Debug (Log Messages)</h1><br />' . $logData . '</pre>';

			$html = JResponse::getBody();

			$bootstrapBody = '<section id="content">';
			if (strpos($html, $bootstrapBody) !== false)
			{
				$html = str_replace($bootstrapBody, $logData . "\n" . $bootstrapBody, $html);
			}
			else
			{
				$html = str_replace('</body>', $logData . "\n</body>", $html);
			}

			JResponse::setBody($html);
		}

		// Some Fixes
		$html = JResponse::getBody();
		if (stripos($html, "('select').chosen("))
		{
			/* Fix for chosen plugin. Joomla does enable it generally some times and break events triggers :( */
			$_search = "jQuery('select').chosen(";
			$_replace = "jQuery('select').not('.jyamltemplatepositions_form select').chosen(";
			$html = str_replace($_search, $_replace, $html);
			JResponse::setBody($html);
		}
	}

	/**
	 * onExtensionAfterSave
	 *
	 * @param   string   $table  Extension table name
	 * @param   boolean  $isNew  True, if entry is new
	 *
	 * @since 4.5.0
	 *
	 * @return void
	 */
	public function onExtensionAfterSave($table, $isNew)
	{
		// Clean framework caches on extension save (e.g. module changes)
		$this->cleanFrameworkCaches();
	}

	/**
	 * Clean caches used by the JYAML Framework
	 *
	 * @since 4.5.0
	 *
	 * @return void
	 */
	protected function cleanFrameworkCaches()
	{
		$jyaml = new JYAML;
		/** @noinspection PhpUndefinedMethodInspection */
		$jyaml->getCache()->clean();
	}

	/**
	 * Prepare JForm
	 *
	 * @param   object  $form  (JForm) The form to be altered.
	 * @param   array   $data  The associated data for the form.
	 *
	 * @return bool
	 */
	public function onContentPrepareForm($form, $data)
	{
		if ($this->dependencyCheck(true))
		{
			return JYAMLAdmin::onContentPrepareForm($form, $data);
		}
		elseif ($this->dependencyCheck(false))
		{
			return JYAML::onContentPrepareForm($form, $data);
		}

		return null;
	}

	/**
	 * dependencyCheck
	 *
	 * @param bool $checkAdmin
	 *
	 * @return bool
	 */
	protected function dependencyCheck($checkAdmin=false)
	{
		static $adminLoaded;

		if (!$checkAdmin)
		{
			return class_exists('JYAML');
		}
		else
		{
			if (JFactory::getApplication()->isAdmin())
			{
				if (!class_exists('JYAML'))
				{
					return false;
				}

				if (!$adminLoaded)
				{
					JLoader::register('JYAMLAdmin', JYAML_LIB_PATH . DIRECTORY_SEPARATOR . 'jyaml.admin.php');
					$adminLoaded = true;
				}

				return (class_exists('JYAML') && class_exists('JYAMLAdmin'));
			}
			else
			{
				return false;
			}
		}
	}
}
