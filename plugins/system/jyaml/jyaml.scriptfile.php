<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  InstallerScript
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

// No direct access
defined('_JEXEC') or die;

// Import library dependencies
jimport('joomla.plugin.plugin');

/**
 * JYAML System Plugin - Installer Script
 *
 * @package     JYAML
 * @subpackage  InstallerScript
 * @since       4.0.0
 */
class PlgSystemJyamlInstallerScript
{
	static private $_installed_templates = array();

	static private $_installed_library = null;

	static private $_installed_plugin = null;

	static private $_installed_default_template = false;

	static private $_is_package = false;

	/**
	 * Preflight (before any files copied)
	 *
	 * @param   string  $type    Install Method
	 * @param   object  $parent  Installer Object
	 *
	 * @return bool
	 */
	public function preflight($type, $parent)
	{
		$minJoomlaVersion = (string) $parent->get('manifest')->required_joomla_version;

		if ($minJoomlaVersion && version_compare(JVERSION, $minJoomlaVersion, '<'))
		{
			$msg = sprintf('JYAML minimum requirements: Joomla! %s+ is required, but %s is installed.', $minJoomlaVersion, JVERSION);
			JFactory::getApplication()->enqueueMessage($msg, 'error');
			return false;
		}

		$minPhpVersion = '5.5.0';
		if (version_compare(PHP_VERSION, $minPhpVersion, '<'))
		{
			$msg = sprintf('JYAML minimum requirements: Your server runs on PHP Version %s. But JYAML require PHP %s+ (7.0 recommended).', PHP_VERSION, $minPhpVersion);
			JFactory::getApplication()->enqueueMessage($msg, 'error');
			return false;
		}

		$requiredPhpExtensions = array('dom', 'gd', 'curl');
		foreach ($requiredPhpExtensions as $phpExt)
		{
			if (!extension_loaded($phpExt))
			{

				$msg = sprintf('JYAML minimum requirements: PHP Extension %s is not installed. Please ask your hosting provider to enable.', strtoupper($phpExt));
				JFactory::getApplication()->enqueueMessage($msg, 'error');
			}
		}

		self::_detectInstalledExtensions();
		self::_backupCustomEnduserFiles();

		return true;
	}

	/**
	 * Postflight (direct after files copied)
	 *
	 * @param   string  $type    Install Method
	 * @param   object  $parent  Installer Object
	 *
	 * @return void
	 */
	public function postflight($type, $parent)
	{
		$installer = $parent->getParent();

		// Overwrite description as message...
		// $installer->set('message', 'TEST');

		self::_enablePlugin();
		self::_installExampleModules($type, $installer);

		self::_installPackages($type, $installer);
		self::_checkInstallAndOutputStatus($type, $installer);

		self::_restoreCustomEnduserFiles();
	}

	/**
	 * Detect installed JYAML extensions
	 *
	 * @return void
	 */
	private static function _detectInstalledExtensions()
	{
		self::$_installed_library = self::_getExtension('JYAML Framework Library', 'library', 'jyaml');
		self::$_installed_plugin = self::_getExtension('JYAML Framework - System Plugin', 'plugin', 'jyaml', 'system');

		$siteTemplatePath = JPATH_ROOT . '/templates';
		$siteTemplates = JFolder::folders($siteTemplatePath);

		// Key value is client_id (0=site, 1=admin)
		$templates = array(
				0 => $siteTemplates,
		);
		$templatePaths = array(
				0 => $siteTemplatePath,
		);

		foreach ($templates as $client_id => $_templates)
		{
			foreach ($_templates as $_template)
			{
				$xmlFile = JPath::clean($templatePaths[$client_id] . '/' . $_template . '/templateDetails.xml');

				if (JFile::exists($xmlFile))
				{
					if (!$xml = simplexml_load_file($xmlFile))
					{
						continue;
					}

					$type = (string) $xml->attributes()->type;

					if ($type != 'template')
					{
						continue;
					}

					$isJYAML = false;

					$tmp = $xml->xpath("//config/fields[@name='jyaml']/fieldset[@name='jyaml']/field");
					if (is_array($tmp) && !empty($tmp))
					{
						$tmp = $tmp[0];
						$tmpA = $tmp->attributes();

						if ($tmpA->name == 'support' && $tmpA->value == 'true')
						{
							$isJYAML = true;
						}
					}

					if ($isJYAML)
					{
						$extension = self::_getExtension('JYAML Template (' . $_template . ')', 'template', $_template, '', $client_id);

						if ($extension && $extension->get('extension_id'))
						{
							if ($client_id == 0 && $extension->get('element') == 'jyaml' && $extension->get('name'))
							{
								self::$_installed_default_template = true;
							}

							self::$_installed_templates[] = $extension;
						}
					}

					unset($xml);
				}
			}
		}
	}

	/**
	 * Backup custom enduser files
	 *
	 * @return void
	 */
	private static function _backupCustomEnduserFiles()
	{
		$config = JFactory::getConfig();

		$libBackupDir = JPath::clean($config->get('tmp_path') . '/jyaml_preflight_backup/library');
		$libPath = JPath::clean(JPATH_ROOT . '/libraries/jyaml');
		$libCustomConfigPath = JPath::clean($libPath . '/config/custom');
		$libFormFieldPath = JPath::clean($libPath . '/form/fields');
		$libLanguagePath = JPath::clean($libPath . '/language');
		$libTemplateUpdatesPath = JPath::clean($libPath . '/update');

		$backupFiles = array();

		if (JFolder::exists($libCustomConfigPath))
		{
			$files = JFolder::files($libCustomConfigPath, '\.(xml|php)$', true, true);
			if ($files)
			{
				$backupFiles = array_merge($backupFiles, $files);
			}
		}

		if (JFolder::exists($libFormFieldPath))
		{
			$files = JFolder::files($libFormFieldPath, '\.php$', true, true);
			if ($files)
			{
				$backupFiles = array_merge($backupFiles, $files);
			}
		}

		if (JFolder::exists($libLanguagePath))
		{
			$files = JFolder::files($libLanguagePath, '\.custom\.ini$', true, true);
			if ($files)
			{
				$backupFiles = array_merge($backupFiles, $files);
			}
		}

		if (JFolder::exists($libTemplateUpdatesPath))
		{
			$files = JFolder::files($libTemplateUpdatesPath, 'tpl_(.*)\.zip$', true, true);
			if ($files)
			{
				$backupFiles = array_merge($backupFiles, $files);
			}
		}

		if (!empty($backupFiles))
		{
			if (!JFolder::exists($libBackupDir))
			{
				JFolder::create($libBackupDir);
			}

			foreach ($backupFiles as $_backupFile)
			{
				$source = JPath::clean($_backupFile);
				$target = JPath::clean($libBackupDir . substr($_backupFile, strlen($libPath)));
				$targetFolder = dirname($target);

				if (!JFolder::exists($targetFolder))
				{
					JFolder::create($targetFolder);
				}
				JFile::copy($source, $target);
			}
		}
	}

	/**
	 * Restore custom enduser files
	 *
	 * @return void
	 */
	private static function _restoreCustomEnduserFiles()
	{
		$config = JFactory::getConfig();

		$libBackupDir = JPath::clean($config->get('tmp_path') . '/jyaml_preflight_backup/library');
		$libPath = JPath::clean(JPATH_ROOT . '/libraries/jyaml');

		if (JFolder::exists($libBackupDir))
		{
			$backupFiles = JFolder::files($libBackupDir, '.', true, true);

			if (!empty($backupFiles))
			{
				foreach ($backupFiles as $_backupFile)
				{
					$source = JPath::clean($_backupFile);
					$target = JPath::clean($libPath . substr($_backupFile, strlen($libBackupDir)));

					// Restore file only if not exists
					if (!JFile::exists($target))
					{
						JFile::copy($source, $target);
					}
				}
			}

			JFolder::delete($libBackupDir);
		}
	}

	/**
	 * Install devlivered packages within the packages folder on this plugin
	 *
	 * @param   string  $type       Install Method
	 * @param   object  $installer  Installer Object
	 *
	 * @return void
	 */
	private static function _installPackages($type, $installer)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Remove the old JYAML Package extension
		$jyamlPackageExtension = self::_getExtension('', 'package', 'pkg_jyaml', '', 0);
		if ($jyamlPackageExtension && $jyamlPackageExtension->get('extension_id'))
		{
			$query->clear()
				->delete($db->qn('#__extensions'))
				->where($db->qn('extension_id') . ' = ' . $db->q((int) $jyamlPackageExtension->get('extension_id')));
			$db->setQuery($query);
			$db->execute();
		}

		// Remove old update sites
		$query->clear()
			->select($db->qn('update_site_id'))
			->from($db->qn('#__update_sites'))
			->where($db->qn('location') . ' = ' . $db->q('http://update.jyaml.de/list.xml'))
			->where($db->qn('type') . ' = ' . $db->q('collection'));
		$db->setQuery($query);

		if ($oldUpdateSiteId = $db->loadResult())
		{
			$query->clear()
				->delete($db->qn('#__update_sites'))
				->where($db->qn('update_site_id') . ' = ' . $db->q($oldUpdateSiteId));
			$db->setQuery($query);
			$db->execute();

			$query->clear()
				->delete($db->qn('#__update_sites_extensions'))
				->where($db->qn('update_site_id') . ' = ' . $db->q($oldUpdateSiteId));
			$db->setQuery($query);
			$db->execute();
		}

		// Install Packages
		$config = JFactory::getConfig();
		$extension_root = dirname(__FILE__);
		$package_dir = JPath::clean($extension_root . '/packages');

		if (self::$_installed_default_template)
		{
			$files = JFolder::files($package_dir, 'lib_jyaml.zip$', false, true);
		}
		else
		{
			$files = JFolder::files($package_dir, '(tpl_(.*)\.zip|lib_jyaml\.zip)$', false, true);
		}

		if ($files)
		{
			self::$_is_package = true;

			// Remove protection for the JYAML library
			$query->clear()
				->update($db->qn('#__extensions'))
				->set($db->qn('protected') . ' = ' . $db->q(0))
				->where($db->qn('type') . ' = ' . $db->q('library'))
				->where($db->qn('element') . ' = ' . $db->q('jyaml'));
			$db->setQuery($query);
			$db->execute();

			foreach ($files as $file)
			{
				$file = JPath::clean($file);

				// Unpack the archive
				$tmpdir = uniqid('jyaml_install_');
				$extractdir = JPath::clean($config->get('tmp_path') . '/' . $tmpdir);
				$result = JArchive::extract($file, $extractdir);

				if ($result === false)
				{
					continue;
				}

				$extensionType = JInstallerHelper::detectType($extractdir);

				// Install the extension if valid
				if ($extensionType)
				{
					if ($extensionType == 'template')
					{
						if (JFile::exists($extractdir . '/templateDetails.xml'))
						{
							$_xml = simplexml_load_file($extractdir . '/templateDetails.xml');
							$_cname = (string) $_xml->attributes()->client;
							$_name = JFilterInput::getInstance()->clean((string) $_xml->name, 'cmd');
							$_tname = strtolower(str_replace(" ", "_", $_name));
							$_client = JApplicationHelper::getClientInfo($_cname, true);
							$_templatePath = $_client->path . '/templates/' . $_tname;

							if (JFolder::exists($_templatePath))
							{
								// Do not try to reinstall/update a template, if it currently installed.
								continue;
							}
						}

						// For old JYAML (4.0.x) installations we need to simulate an non existing source path to prevent overwrite this install source
						$simulateSource = JPath::clean($config->get('tmp_path') . '/' . uniqid('__jyaml_simulate_non_existing_path__'));
						$installer = JInstaller::getInstance();
						$currentSource = $installer->getPath('source');
						$installer->setPath('source', $simulateSource);
					}

					// Install the extension
					$tmpInstaller = new JInstaller;
					$installResult = $tmpInstaller->install($extractdir);

					if ($extensionType == 'template')
					{
						if ($installResult)
						{
							// Dont work, because on install runtime not all form tiggers are available

							/*
							$_manifest = $tmpInstaller->getManifest();
							if (is_object($_manifest))
							{
								$_templateName = (string) $_manifest->name;
								$_templateClientId = (string) $_manifest->attributes()->client_id;
								self::_installDefaultTemplateStyleParamsToDb($_templateName, $_templateClientId);
							}
							*/
						}

						// Set back the current source
						$installer->setPath('source', $currentSource);
					}

					// If still exists, delete the temporary install dir
					if (JFolder::exists($extractdir))
					{
						JFolder::delete($extractdir);
					}
				}
			}
		}

		// [Removed protection: Try to set the JYAML Library as protected (to protect for uninstall and data lost of custom files).]
		// Disable protection to allow uninstall.
		$query->clear()
			->update($db->qn('#__extensions'))
			->set($db->qn('protected') . ' = ' . $db->q(0))
			->where($db->qn('type') . ' = ' . $db->q('library'))
			->where($db->qn('element') . ' = ' . $db->q('jyaml'));
		$db->setQuery($query);
		$db->execute();

		/* Copy all Template archives to the library update directory
		 * Required for new updates
		 *
		 * @since 4.5.0
		 */
		$files = JFolder::files($package_dir, 'tpl_(.*)\.zip$', false, true);
		if ($files)
		{
			foreach ($files as $source)
			{
				$target = JPATH_ROOT . '/libraries/jyaml/update/' . basename($source);
				if (!JFile::exists($target))
				{
					JFile::copy($source, $target);
				}
			}
		}
	}

	/**
	 * Method to set default params after new install in template style
	 *
	 * @param   string  $template  Template name
	 * @param   int     $clientId  Client id
	 *
	 * @disabled
	 * @note Dont work, because on install runtime not all form tiggers are available
	 *
	 * @return void
	 */
	/*
	private static function _installDefaultTemplateStyleParamsToDb($template, $clientId = 0)
	{
		try
		{
			$template = (string) $template;
			$clientId = (int) $clientId;

			$db = JFactory::getDbo();

			$query = $db->getQuery(true);
			$query->select($db->qn('id'));
			$query->from($db->qn('#__template_styles'));
			$query->where($db->qn('template') . ' = ' . $db->q($template));
			$query->where($db->qn('client_id') . ' = ' . $db->q($clientId));
			$query->where($db->qn('params') . ' NOT LIKE ' . $db->q('%_is_jyaml%'));
			$db->setQuery($query);
			$styleId = $db->loadResult();

			if (!$styleId)
			{
				return;
			}

			JModelLegacy::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_templates/models');

			$modelConfig = array(
				'ignore_request' => true,
				'table_path' => JPATH_ADMINISTRATOR . '/components/com_templates/tables'
			);
			$model = JModelAdmin::getInstance('Style', 'TemplatesModel', $modelConfig);
			$model->setState('style.id', $styleId);

			JForm::addFormPath(JPATH_ADMINISTRATOR . '/components/com_templates/models/forms');
			JForm::addFieldPath(JPATH_ADMINISTRATOR . '/components/com_templates/models/fields');
			$form = $model->getForm(array('client_id' => $clientId, 'template' => $template), true);
			$fieldSets = $form->getFieldsets('params');
			$formHtml = '';

			foreach ($fieldSets as $name => $fieldSet)
			{
				foreach ($form->getFieldset($name) as $field)
				{
					$formHtml .= $field->input;
				}
			}

			$dom = new DOMDocument;
			$dom->loadHTML($formHtml);
			$tags = array('input','textarea', 'select');
			$formValues = array();

			foreach ($tags as $_tag)
			{
				$_elements = $dom->getElementsByTagName($_tag);
				foreach ($_elements as $_element)
				{
					$_disabled = trim($_element->getAttribute('disabled')) ? true : false;
					if ($_disabled)
					{
						continue;
					}

					$_ignore = false;
					$_tag = strtolower($_element->nodeName);
					$_name = trim($_element->getAttribute('name'));
					$_value = '';

					switch ($_tag)
					{
						case 'input':
							$_type = strtolower(trim($_element->getAttribute('type')));

							if ($_type == 'checkbox' || $_type == 'radio')
							{
								$_checked = trim($_element->getAttribute('checked')) ? true : false;

								if ($_checked)
								{
									$_value = $_element->getAttribute('value');
								}
								else
								{
									$_ignore = true;
								}
							}
							else
							{
								$_value = $_element->getAttribute('value');
							}
							break;
						case 'textarea':
							$_value = $_element->nodeValue;
							break;
						case 'select':
							if ($_element->hasChildNodes())
							{
								foreach ($_element->childNodes as $_childNode)
								{
									$_childTag = strtolower($_childNode->nodeName);
									if ($_childTag == 'option')
									{
										$_childs = array($_childNode);
									}
									elseif ($_childTag = 'optgroup')
									{
										foreach ($_childNode->childNodes as $__child)
										{
											$_childs[] = $__child;
										}
									}

									foreach ($_childs as $_child)
									{
										$_childTag = strtolower($_child->nodeName);

										if ($_childTag == 'option')
										{
											$_selectedValues = array();
											$_selected = trim($_child->getAttribute('selected')) ? true : false;
											if ($_selected)
											{
												$_selectedValues[] = $_child->getAttribute('value');
											}
											if ($_selectedValues)
											{
												$_value = implode(',', $_selectedValues);
											}
										}
									}
								}
							}
							break;
					}

					if ($_ignore)
					{
						continue;
					}

					$formValues[] = $_name . '=' . $_value;
				}
			}

			$formValues = implode('&', $formValues);
			$templateStyleParams = '';
			parse_str($formValues, $formValues);

			if (isset($formValues['jform']) && isset($formValues['jform']['params']))
			{
				$templateStyleParams = json_encode($formValues['jform']['params']);
			}

			if ($templateStyleParams)
			{
				$query = $db->getQuery(true);
				$query->clear();
				$query->update($db->qn('#__template_styles'));
				$query->set($db->qn('params') . ' = ' . $db->q($templateStyleParams));
				$query->where($db->qn('id') . ' = ' . $db->q($styleId));
				$db->setQuery($query);
				$db->query();
			}
		}
		catch (Exception $e)
		{

		}
	}
	*/

	/**
	 * Status Output
	 *
	 * @param   string  $type       Install Method
	 * @param   object  $installer  Installer Object
	 *
	 * @return void
	 */
	private static function _checkInstallAndOutputStatus($type, $installer)
	{
		if (!self::$_is_package)
		{
			return;
		}

		$anyUpgrade = false;

		$html = '<div class="clearfix">';
		$html .= '<div class="pull-left">' . JText::_('JYAML_PLG_INSTALLER_TITLE') . '</div>';
		$html .= '</div>';

		$template = (string) $installer->manifest->template_layout_name;
		if (empty($template))
		{
			$template = 'jyaml';
		}

		$html .= '<br />';
		$html .= '<table class="table table-striped table-bordered">';
		$html .= '<thead>';
		$html .= '<tr>';
		$html .= '<th class="left">'
					. JText::_('JYAML_PLG_INSTALLER_TITLE_EXTENSION') . '</th>';
		$html .= '<th class="left">' . JText::_('JYAML_PLG_INSTALLER_TITLE_INSALLED') . '</th>';
		$html .= '<th class="left">'
					. JText::_('JYAML_PLG_INSTALLER_TITLE_INSALLED_VERSION') . '</th>';
		$html .= '<th class="left">' . JText::_('JYAML_PLG_INSTALLER_TITLE_ENABLED') . '</th>';
		$html .= '</tr>';
		$html .= '</thead>';

		$extensions = array();

		$systemPlugin = self::_getExtension('JYAML Framework - System Plugin', 'plugin', 'jyaml', 'system');
		if (self::$_installed_plugin && self::$_installed_plugin->get('extension_id'))
		{
			$systemPlugin->set('_upgraded_from', self::$_installed_plugin);
		}
		$extensions[] = $systemPlugin;

		$library = self::_getExtension('JYAML Framework Library', 'library', 'jyaml');
		if (self::$_installed_library && self::$_installed_library->get('extension_id'))
		{
			$library->set('_upgraded_from', self::$_installed_library);
		}
		$extensions[] = $library;

		if (!self::$_installed_default_template && empty(self::$_installed_templates))
		{
			$extensions[] = self::_getExtension('JYAML Template (' . $template . ')', 'template', $template);
		}

		if (!empty(self::$_installed_templates))
		{
			$installedTemplates = self::$_installed_templates;
			foreach ($installedTemplates as $k => $v)
			{
				$installedTemplates[$k]->set('_already_installed', true);
			}

			$extensions = array_merge($extensions, $installedTemplates);
		}

		$html .= '<tbody>';

		foreach ($extensions as $_extension)
		{
			$html .= '<tr>';

			$id = $_extension->get('extension_id');
			$enabled = $_extension->get('enabled');
			$name = $_extension->get('name');
			$type = $_extension->get('type');
			$clientId = $_extension->get('client_id', 0);

			$manifest = $_extension->get('manifest');
			$updateFrom = $_extension->get('_upgraded_from');

			$html .= '<td>' . ($id ? $name : '<strong class="text-error">' . $name . '</strong>') . '</td>';

			if ($type == 'template' && $_extension->get('_already_installed'))
			{
				$updateButton = '';
				$requireUpgrade = false;

				if ($library && $library->get('extension_id'))
				{
					$templateVersionString = $_extension->get('manifest')->get('version') . '.' . strtotime($_extension->get('manifest')->get('creationDate'));
					$templateVersionString = str_replace(' ', '.', $templateVersionString);
					$templateVersionString = str_ireplace('.stable', '', $templateVersionString);

					$libraryVersionString = $library->get('manifest')->get('version') . '.' . strtotime($library->get('manifest')->get('creationDate'));
					$libraryVersionString = str_replace(' ', '.', $libraryVersionString);
					$libraryVersionString = str_ireplace('.stable', '', $libraryVersionString);

					if (version_compare($libraryVersionString, $templateVersionString, '>') == 1)
					{
						// Find first template style
						$db = JFactory::getDbo();
						$query = $db->getQuery(true);
						$query->clear()
							->select('id')
							->from($db->qn('#__template_styles'))
							->where($db->qn('template') . ' = ' . $db->q($_extension->get('element')))
							->where($db->qn('client_id') . ' = ' . $db->q($clientId))
							->order($db->qn('home') . ' DESC');
						$db->setQuery($query);
						$tmplStyleId = $db->loadResult();

						if ($tmplStyleId)
						{
							$btnUrl = 'index.php?option=com_templates&view=style&layout=edit&id=' . $tmplStyleId . '#jyamlupgrade';

							// Allow direct access of the edit style link
							$app = JFactory::getApplication();
							$_editableStyleValues = (array) $app->getUserState('com_templates.edit.style.id');
							$_editableStyleValues[] = $tmplStyleId;
							$_editableStyleValues = array_unique($_editableStyleValues);
							$app->setUserState('com_templates.edit.style.id', $_editableStyleValues);
						}
						else
						{
							$btnUrl = 'index.php?option=com_templates&view=styles';
						}

						$updateButton = ' - <a href="' . JRoute::_($btnUrl) . '" target="_blank">'
							. JText::_('JYAML_PLG_INSTALLER_UPGRADE_BTN') . '</a>';

						$requireUpgrade = true;
						$anyUpgrade = true;
					}
				}

				if ($requireUpgrade)
				{
					$html .= '<td><strong class="text-error">'
							. JText::_('JNo') . '</strong>' . $updateButton . '</td>';
				}
				else
				{
					$html .= '<td>' . JText::_('JYAML_PLG_INSTALLER_ALREADY_INSTALLED') . '</td>';
				}
			}
			else
			{
				if ($id && $updateFrom && $updateFrom->get('extension_id'))
				{
					$html .= '<td>'
						. JText::sprintf('JYAML_PLG_INSTALLER_UPDATED_FROM', $updateFrom->get('manifest')->get('version')) . '</td>';
				}
				else
				{
					$discover = '';
					if (!$id && $type == 'template')
					{
						$_client = JApplicationHelper::getClientInfo($clientId);
						$_templatePath = $_client->path . '/templates/' . $_extension->get('element');
						if (JFolder::exists($_templatePath))
						{
							$discoverLink = '<a href="' . JRoute::_('index.php?option=com_installer&view=discover') . '">'
									. JText::_('JYAML_PLG_INSTALLER_EXTENSION_DISCOVER') . '</a>';

							$discover = '<hr />';
							$discover .= JText::_('JYAML_PLG_INSTALLER_FOLDER_EXISTS');
							$discover .= ' (' . JText::sprintf('JYAML_PLG_INSTALLER_TRY_DISCOVER_AND_REINSTALL', $discoverLink) . ')';
						}
					}

					$html .= '<td>' . ($id ? JText::_('JYes') : '<strong class="text-error">' . JText::_('JNo') . '</strong>') . $discover . '</td>';
				}
			}

			$html .= '<td>' . $manifest->get('version') . '</td>';
			$html .= '<td>' . ($enabled ? JText::_('JYes') : '<strong class="text-error">' . JText::_('JNo') . '</strong>') . '</td>';

			$html .= '</tr>';
		}

		$html .= '</tbody>';
		$html .= '</table>';
		$html .= '<hr />';

		$html .= '<div>';
		$html .= (string) $installer->manifest->package_description;
		$html .= '</div>';
		$html .= '<hr />';

		$html .= '<div>';
		$html .= '<p class="lead">' . JText::_('JYAML_PLG_DONATION_TITLE') . '</p>';
		$link1 = '<a target="_blank" href="http://www.amazon.de/wishlist/U7K0391ZVO84">Amazon</a>';
		$link2 = '<a target="_blank" href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&amp;hosted_button_id=9BKYS2NFXJUZY">Paypal</a>';
		$html .= '<p>' . JText::sprintf('JYAML_PLG_DONATION_TEXT', $link1, $link2) . '</p>';
		$html .= '</div>';
		$html .= '<hr />';

		if ($anyUpgrade)
		{
			JFactory::getApplication()->enqueueMessage(JText::_('JYAML_PLG_INSTALLER_UPGRADE_NOTE'), 'notice');
		}

		$installer->set('message', $html);
	}

	/**
	 * Method to get an extension object
	 *
	 * @param   string   $name       Exntesion Name
	 * @param   string   $type       Extension Type
	 * @param   string   $element    Extension Element Name
	 * @param   string   $folder     Extension Folder (plugin group)
	 * @param   integer  $client_id  Client Id
	 *
	 * @return \Joomla\Registry\Registry object of result
	 */
	private static function _getExtension($name, $type, $element, $folder = '', $client_id = null)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->clear()
			->select('*')
			->from($db->qn('#__extensions'))
			->where($db->qn('type') . ' = ' . $db->q($type))
			->where($db->qn('element') . ' = ' . $db->q($element));

		if ($folder)
		{
			$query->where($db->qn('folder') . ' = ' . $db->q($folder));
		}

		if ($client_id !== null)
		{
			$query->where($db->qn('client_id') . ' = ' . $db->q((int) $client_id));
		}

		$db->setQuery($query);
		$result = $db->loadObject();

		$extension = new \Joomla\Registry\Registry();
		$manifest = new \Joomla\Registry\Registry();

		if ($result)
		{
			$extension->loadObject($result);
			$manifest->loadString($extension->get('manifest_cache'));

			if ($extension->get('type') == 'template')
			{
				$extension->set('name', $name);
			}
		}
		else
		{
			$extension->set('id', 0);
			$extension->set('name', $name);
			$extension->set('type', $type);
			$extension->set('element', $element);
			$extension->set('folder', $folder);
		}

		$extension->set('manifest', $manifest);

		return $extension;
	}

	/**
	 * Enable this plugin by default
	 *
	 * @return void
	 */
	private static function _enablePlugin()
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->clear()
			->update($db->qn('#__extensions'))
			->set($db->qn('enabled') . ' = ' . $db->q(1))
			->where($db->qn('type') . ' = ' . $db->q('plugin'))
			->where($db->qn('element') . ' = ' . $db->q('jyaml'))
			->where($db->qn('folder') . ' = ' . $db->q('system'));
		$db->setQuery($query);
		$db->execute();
	}

	/**
	 * Install of example modules
	 *
	 * @param   string  $type       Install method
	 * @param   object  $installer  Installer
	 *
	 * @return void
	 */
	private static function _installExampleModules($type, $installer)
	{
		// Create example Modules on first install (not on update or upgrade)
		$createModuleExamplesOnInstall = (int) $installer->manifest->create_module_examples_on_install;

		if ($type == 'install' && $createModuleExamplesOnInstall)
		{
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);

			$templateLayoutName = (string) $installer->manifest->template_layout_name;
			if (empty($templateLayoutName))
			{
				$templateLayoutName = 'jyaml';
			}

			$menuModules = self::_getExampleMenuModules($templateLayoutName);

			foreach ($menuModules as $pos => $data)
			{
				$checkLayout = isset($data['checkLayout']) ? $data['checkLayout'] : true;
				$dbData = isset($data['dbData']) ? $data['dbData'] : array();

				// Searching equivalent example already exists
				$query->clear()
					->select('*')
					->from($db->qn('#__modules'))
					->where($db->qn('module') . ' = ' . $db->q('mod_menu'))
					->where($db->qn('position') . ' = ' . $db->q($pos));

				if ($checkLayout)
				{
					$query->where($db->qn('params') . ' LIKE ' . $db->q('%"layout":"' . $templateLayoutName . ':%'));
				}
				$db->setQuery($query);
				$result = $db->loadObjectList();

				if (!$result && !empty($dbData))
				{
					// Create
					$table = JTable::getInstance('Module', 'JTable', array());
					$table->bind($dbData);

					if ($table->store())
					{
						$query->clear()
							->insert($db->qn('#__modules_menu'))
							->set($db->qn('moduleid') . ' = ' . $db->q((int) $table->id))
							->set($db->qn('menuid') . ' = ' . $db->q(0));
						$db->setQuery($query);
						$db->execute();
					}
				}
			}
		}
	}

	/**
	 * Get Data for example menu modules
	 *
	 * @param   string  $tmpl  Name of the Template
	 *
	 * @return array Array of modules
	 */
	private static function _getExampleMenuModules($tmpl)
	{
		$arr = array();
		$arr['main_navigation'] = array(
			'checkLayout' => true,
			'dbData' => array(
				'title' => 'JYAML: main_navigation (hlist)',
				'note' => 'Auto created module example for a JYAML template (' . $tmpl . ')',
				'position' => 'main_navigation',
				'published' => '1',
				'module' => 'mod_menu',
				'access' => '1',
				'showtitle' => '1',
				'params' => '{"menutype":"mainmenu","startLevel":"1","endLevel":"1","showAllChildren":"0","tag_id":"",'
							. '"class_sfx":"","window_open":"","layout":"' . $tmpl . ':hlist",'
							. '"moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"itemid","ext-mod_menu-replaceModuleTitle":"_usedefault_",'
							. '"ext-mod_menu-disableClickActiveLink":"_usedefault_","ext-mod_menu-hideHomeWhenHome":"0",'
							. '"jyaml_mobilenav_enable":"1","jyaml_mobilenav_with_threshold":"768","jyaml_mobilenav_show_active_text":"1",'
							. '"jyaml_mobilenav_insert_selector":"body","jyaml_mobilenav_insert_method":"prepend"}',
				'client_id' => '0',
				'language' => '*'
			)
		);

		$arr['top_navigation'] = array(
			'checkLayout' => false,
			'dbData' => array(
				'title' => 'JYAML: top_navigation',
				'note' => 'Auto created module example for a JYAML template (' . $tmpl . ')',
				'position' => 'top_navigation',
				'published' => '1',
				'module' => 'mod_menu',
				'access' => '1',
				'showtitle' => '0',
				'params' => '{"menutype":"top","startLevel":"1","endLevel":"1","showAllChildren":"0","tag_id":"","class_sfx":"","window_open":"",'
										. '"layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"itemid",'
										. '"ext-mod_menu-replaceModuleTitle":"_usedefault_","ext-mod_menu-disableClickActiveLink":"_usedefault_",'
										. '"ext-mod_menu-hideHomeWhenHome":"0"}',
				'client_id' => '0',
				'language' => '*'
			)
		);

		$arr['left'] = array(
			'checkLayout' => true,
			'dbData' => array(
				'title' => 'JYAML: left (vlist)',
				'note' => 'Auto created module example for a JYAML template (' . $tmpl . ')',
				'position' => 'left',
				'published' => '1',
				'module' => 'mod_menu',
				'access' => '1',
				'showtitle' => '1',
				'params' => '{"menutype":"mainmenu","startLevel":"1","endLevel":"0","showAllChildren":"0","tag_id":"","class_sfx":"","window_open":"",'
										. '"layout":"' . $tmpl . ':vlist","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"itemid",'
										. '"ext-mod_menu-replaceModuleTitle":"_usedefault_","ext-mod_menu-disableClickActiveLink":"_usedefault_",'
										. '"ext-mod_menu-hideHomeWhenHome":"0"}',
				'client_id' => '0',
				'language' => '*'
			)
		);

		return $arr;
	}
}
