<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  Template
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

/* Note:
 * -----------------------------------------------------------------------------
 * JYAML use configuration based html files.
 * Look in "/html/component/[configured-html_file].php" to modify.
 *
 */

defined('_JEXEC') or die;

// Dependency check
if (!class_exists('JYAML'))
{
	die('JYAML Framework not available: Please check if the JYAML System Plugin and JYAML Framework Library is installed and enabled!');
}

// Load the html component template
$jyaml = JYAML::getDocument();
$jyaml->dispatchTemplate(
	JFile::stripExt(basename(__FILE__))
);
