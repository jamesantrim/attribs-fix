<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  Template
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

// Restrict direct access
defined('_JEXEC') or die;

/*
 * With this file you can create dynamic CSS.
 * Use the file only if there is absolutely no other way to do your task.
 *
 * For example you have an skin builder
 * or whatever you can use this file to create the CSS.
 *
 * All contents are created here will output as CSS-Declaration (inline) after all other CSS-Files.
 * CSS-Declaration (inline), Why?
 *   Because it does not require additional requests (up to 100% faster page load).
 *   And can use all available things here as develop a normal extension.
 *
 * ==============
 * SECURITY Note!
 * ==============
 * Please do not use $_GET or $_POST without input filters to get request variables.
 * Use
 *   JInput: http://docs.joomla.org/JInput_Background_for_Joomla_Platform
 * or use
 *   PHP Input Fiter: http://php.net/manual/book.filter.php
 *
 * Additionally JYAML execute strip_tags of the generated content
 * to prevent XSS and File-Inclusion attacks
 */

/*
 * Example:
 *
 * echo 'body {background: #000; }';
 * if (($fontColor = JFactory::getApplication()->input->get('font_color')))
 * {
 *   echo 'body { color: ' . $fontColor . '}';
 * }
 *
 */
