/* $Id$ */

---------------


!! Note:
Please only use Fonts which specifically allow @font-face embedding
Otherwise (depending on the font license), you usually have to you make sure to restrict
the direct download of the Font files (expect the .css)


---------------

You can download free Webfont (@font-face) Kits here:
http://www.fontsquirrel.com/fontface

Please make sure to define here only @font-face without any other CSS rules. (Its better for more clarity)

Usage:
1. Create or Download a Webfont-Kit (see the 'tiza' font for example)
2. Import the Stylesheet with the @font-face definition for example in '/css/screen.content.css'.
   E.g.: @import url(../fonts/tiza/tiza.css); - see: '/css/screen.content.css'
         h1, h2, h3 { font-family: "TizaRegular"; } - see: '/css/screen.content.css'


For sure you can use Google Webfonts (http://www.google.com/webfonts) or something else. That is your decision.
