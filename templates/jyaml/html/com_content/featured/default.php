<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  JYAML.Template
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

defined('_JEXEC') or die;

// Dependency check
if (!class_exists('JYAML'))
{
	die('JYAML Framework not available: Please check if the JYAML System Plugin and JYAML Framework Library is installed and enabled!');
}

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');

$jyaml = JYAML::getDocument();

// Detect defined column width's from screen.basemod.css (template) and base.css (yaml core)
$definedColWidths = (array) $jyaml->getDefinedColumnWidtsFromCss();

// If the page class is defined, add to class as suffix.
// It will be a separate class if the user starts it with a space
$pageClass = $this->params->get('pageclass_sfx');

?>
<div class="blog-featured<?php echo $pageClass;?>">
<?php if ($this->params->get('show_page_heading') != 0) : ?>
	<h1><?php echo $this->escape($this->params->get('page_heading')); ?></h1>
<?php endif; ?>
<?php $leadingcount = 0; ?>
<?php if (!empty($this->lead_items)) : ?>
<div class="items-leading">
	<?php foreach ($this->lead_items as &$item) : ?>
		<div class="leading-<?php echo $leadingcount; ?><?php echo $item->state == 0 ? ' system-unpublished' : null; ?>">
			<?php
				$this->item = &$item;
				echo $this->loadTemplate('item');
			?>
		</div>
		<?php $leadingcount++; ?>
	<?php endforeach; ?>
</div>
<?php endif; ?>
<?php
	$this->columns = (int) $this->columns;
	$introcount=(count($this->intro_items));
	$counter = 0;

	// Fill items on row with empty columns in last row
	// required for equalized subtemplates for correct column width and a robust markup
	$filler = ($this->columns - ($introcount % $this->columns));
	$filler = ($filler == $this->columns ? 0 : $filler);

	if ($filler > 0)
	{
		$dummyItem = JTable::getInstance('content');
		$dummyItem->state = 1;
		for ($fi = 0; $fi < $filler; $fi++)
		{
			array_push($this->intro_items, $dummyItem);
		}
		$introcount=(count($this->intro_items));
	}
?>
<?php if (!empty($this->intro_items)) : ?>
	<?php $key = 0; foreach ($this->intro_items as &$item) : ?>
		<?php
			$isItemEmpty = (!$item || !$item->id);
			$colWidthStyle = '';
			// Handle rows
			$key = $key + 1;
			$rowcount = (((int) $key - 1) % $this->columns) + 1;
			$row = $counter / $this->columns;

			$isFirstRow = ($key == 1);
			$isLastRow = ($introcount - $counter <= $this->columns);
			$isBetweenRow = (!$isFirstRow && !$isLastRow);
			$rowFirstLastClass = (!$isBetweenRow ? ($isFirstRow ? ' row-first ' : ' row-last ') : ' ');

			// Handle cols
			$isFirstCol = ($rowcount == 1);
			$isLastCol = (($rowcount == $this->columns) || ($counter == $introcount));
			$isBetweenCol = (!$isFirstCol && !$isLastCol);

			if ($this->columns > 1)
			{
				$rowSubtemplateClass = 'ym-grid ym-equalize ';

				$colWidth = floor(100 / $this->columns);
				$colWidthClass = 'ym-g' . $colWidth . ' ym-g' . ($isLastCol ? 'r' : 'l') . ' ';
				$colSubcClass = 'ym-gbox' . (!$isBetweenCol ? ($isFirstCol ? '-left' : '-right') : '');
				$colFirstLastClass = (!$isBetweenCol ? ($isFirstCol ? ' column-first' : ' column-last') : '');

				if (!in_array($colWidth, $definedColWidths))
				{
					$_styleWidth = (100 / $modules['count']);
					if (($_dotPos = strpos($_styleWidth, '.')) !== false)
					{
						$_styleWidth = substr($_styleWidth, 0, $_dotPos + 1) . substr($_styleWidth, $_dotPos + 1, 3);
					}

					// Set width inline style if the width class (.ym-g[n]) is not defined in screen.basemod.css
					$colWidthStyle = ' style="width:' . $_styleWidth . '%"';
				}
			}
			else
			{
				$rowSubtemplateClass = '';
				$colWidthClass = '';
				$colSubcClass = 'ym-gbox-full';
				$colFirstLastClass = '';
			}
		?>
		<?php if ($rowcount == 1) : ?>
			<?php
				$_classes = array(
						trim($rowSubtemplateClass),
						'items-row',
						trim($rowFirstLastClass),
						'cols-' . (int) $this->columns,
						'row-' . $row
				);
			?>
			<div class="<?php echo implode(' ', $_classes); ?>">
		<?php endif; ?>
				<?php $counter++; ?>
				<?php
					$_classes = array(
							trim($colWidthClass),
							'item'
					);
					if ($colFirstLastClass)
					{
						$_classes[] = trim($colFirstLastClass);
					}
					$_classes[] = 'column-' . $rowcount;

					if ($item->state == 0)
					{
						$_classes[] = 'system-unpublished';
					}
					if ($isItemEmpty)
					{
						$_classes[] = 'item-empty';
					}
				?>
				<div<?php echo $colWidthStyle; ?> class="<?php echo implode(' ', $_classes); ?>">
					<div class="<?php echo $colSubcClass; ?>">
						<div class="ym-clearfix">
						<?php
							if (!$isItemEmpty)
							{
								$this->item = &$item;
								echo $this->loadTemplate('item');
							}
							else
							{
								// Fill empty column with breaking space
								echo '&nbsp;';
							}
						?>
						</div>
					</div>
				</div>
		<?php if (($rowcount == $this->columns) or ($counter == $introcount)) : ?>
			</div>
		<?php endif; ?>
	<?php endforeach; ?>
<?php endif; ?>
<?php if (!empty($this->link_items)) : ?>
	<div class="items-more">
		<?php echo $this->loadTemplate('links'); ?>
	</div>
<?php endif; ?>
<?php if ($this->pagination->get('pages.total') > 1) : ?>
	<?php if ($this->params->def('show_pagination', 1) == 1 || ($this->params->get('show_pagination') == 2)) : ?>
		<div class="pagination">
			<?php if ($this->params->def('show_pagination_results', 1)) : ?>
				<p class="counter">
					<?php echo $this->pagination->getPagesCounter(); ?>
				</p>
			<?php endif; ?>
			<?php echo $this->pagination->getPagesLinks(); ?>
		</div>
	<?php endif; ?>
<?php endif; ?>
</div>
