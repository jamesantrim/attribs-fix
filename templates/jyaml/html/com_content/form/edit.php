<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.tabstate');
JHtml::_('behavior.keepalive');
JHtml::_('behavior.calendar');
#JHtml::_('behavior.formvalidator');
JHtml::_('formbehavior.chosen', 'select');
// Load the form validator (using the form class: jyaml-form-validate)
JHtml::_('jyaml.validator');

// JYAML Instance
$jyaml = JYAML::getDocument();

// JYAML Tabs Options (see: [JOOMLA_ROOT]/libraries/jyaml/html/js/accessible_tabs.js)
$options   = array();
$options[] = "tabhead: 'h3'";
$options[] = "syncheights: true";
$options[] = "autoAnchor: true";
$options[] = "currentInfoText: ' " . JText::_('JYAML_TABS_CURRENT_TEXT') . " '";
$options[] = "currentInfoPosition: '" . ($jyaml->direction == 'rtl' ? 'after' : 'before') . "'";
$options[] = "orientation: 'horizontal'";
$options[] = "effects: null"; // ['slide'], ['fade'], ['slide', 'fade'] or null
$options[] = "slideDirection: 'horizontal'";
$options[] = "position: 'top'";
$options[] = "pagination: false";
$options[] = "saveState: true";

// Add Javascript
JHtml::_('jyaml.tabs', '.article-form-edit-tabs', $options);
// Add Stylesheet
$jyaml->addStylesheet($jyaml->getUrl('css', 'screen.tabs.css'));
$jyaml->addStylesheet($jyaml->getUrl('css', 'icons.css'));
$jyaml->addStylesheet($jyaml->getUrl('css', 'screen.forms.article.edit.css'));
$jyaml->addStyleDeclaration('

');

// Create shortcut to parameters.
$params = $this->state->get('params');

// This checks if the editor config options have ever been saved. If they haven't they will fall back to the original settings.
$editoroptions = isset($params->show_publishing_options);

if ( ! $editoroptions)
{
	$params->show_urls_images_frontend = '0';
}

JFactory::getDocument()->addScriptDeclaration(
	"
	Joomla.submitbutton = function(task)
	{
		if (task == 'article.cancel' || jQuery('#adminForm').valid())
		{
			" . $this->form->getField('articletext')->save() . "
			Joomla.submitform(task);
		}
	}
"
);
?>
<div class="edit item-page<?php echo $this->pageclass_sfx; ?>">
	<?php if ($params->get('show_page_heading')) : ?>
		<div class="page-header">
			<h1>
				<?php echo $this->escape($params->get('page_heading')); ?>
			</h1>
		</div>
	<?php endif; ?>

	<form action="<?php echo JRoute::_('index.php?option=com_content&a_id=' . (int) $this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="ym-form jyaml-form-validate">
		<div class="ym-fbox-footer">
			<button type="button" class="ym-button ym-primary" onclick="Joomla.submitbutton('article.save')">
				<span class="icon-ok"></span><?php echo JText::_('JSAVE') ?>
			</button>
			<button type="button" class="ym-button ym-warning" onclick="Joomla.submitbutton('article.cancel')">
				<span class="icon-cancel"></span><?php echo JText::_('JCANCEL') ?>
			</button>
			<?php if ($params->get('save_history', 0) && $this->item->id) : ?>
				<?php echo $this->form->getInput('contenthistory'); ?>
			<?php endif; ?>
		</div>
		<fieldset>
			<div class="jyaml-tabs article-form-edit-tabs ym-clearfix">
				<h3><span><?php echo JText::_('COM_CONTENT_ARTICLE_CONTENT') ?></span></h3>
				<div class="tabbody" id="editor">
					<div class="tabbody-content">
						<?php echo $this->form->renderField('title'); ?>
						<?php if (is_null($this->item->id)) : ?>
							<?php echo $this->form->renderField('alias'); ?>
						<?php endif; ?>
						<div class="ym-fbox">
						<?php echo $this->form->getInput('articletext'); ?>
						</div>
					</div>
				</div>
				<?php if ($params->get('show_urls_images_frontend')): ?>
					<h3><span><?php echo JText::_('COM_CONTENT_IMAGES_AND_URLS') ?></span></h3>
					<div class="tabbody" id="images">
						<div class="tabbody-content">
							<?php echo $this->form->renderField('image_intro', 'images'); ?>
							<?php echo $this->form->renderField('image_intro_alt', 'images'); ?>
							<?php echo $this->form->renderField('image_intro_caption', 'images'); ?>
							<?php echo $this->form->renderField('float_intro', 'images'); ?>
							<?php echo $this->form->renderField('image_fulltext', 'images'); ?>
							<?php echo $this->form->renderField('image_fulltext_alt', 'images'); ?>
							<?php echo $this->form->renderField('image_fulltext_caption', 'images'); ?>
							<?php echo $this->form->renderField('float_fulltext', 'images'); ?>
							<?php echo $this->form->renderField('urla', 'urls'); ?>
							<?php echo $this->form->renderField('urlatext', 'urls'); ?>
							<?php echo $this->form->getInput('targeta', 'urls'); ?>
							<?php echo $this->form->renderField('urlb', 'urls'); ?>
							<?php echo $this->form->renderField('urlbtext', 'urls'); ?>
							<?php echo $this->form->getInput('targetb', 'urls'); ?>
							<?php echo $this->form->renderField('urlc', 'urls'); ?>
							<?php echo $this->form->renderField('urlctext', 'urls'); ?>
							<?php echo $this->form->getInput('targetc', 'urls'); ?>
						</div>
					</div>
				<?php endif; ?>
				<?php foreach ($this->form->getFieldsets('params') as $name => $fieldSet) : ?>
					<h3><span><?php echo JText::_($fieldSet->label); ?></span></h3>
					<div class="tabbody" id="params-<?php echo $name; ?>">
						<div class="tabbody-content">
							<?php foreach ($this->form->getFieldset($name) as $field) : ?>
								<?php echo $field->renderField(); ?>
							<?php endforeach; ?>
						</div>
					</div>
				<?php endforeach; ?>
				<h3><span><?php echo JText::_('COM_CONTENT_PUBLISHING') ?></span></h3>
				<div class="tabbody" id="publishing">
					<div class="tabbody-content">
						<?php echo $this->form->renderField('catid'); ?>
						<?php echo $this->form->renderField('tags'); ?>
						<?php if ($params->get('save_history', 0)) : ?>
							<?php echo $this->form->renderField('version_note'); ?>
						<?php endif; ?>
						<?php echo $this->form->renderField('created_by_alias'); ?>
						<?php if ($this->item->params->get('access-change')) : ?>
							<?php echo $this->form->renderField('state'); ?>
							<?php echo $this->form->renderField('featured'); ?>
							<?php echo $this->form->renderField('publish_up'); ?>
							<?php echo $this->form->renderField('publish_down'); ?>
						<?php endif; ?>
						<?php echo $this->form->renderField('access'); ?>
						<?php if (is_null($this->item->id)): ?>
							<div class="control-group">
								<div class="control-label">
								</div>
								<div class="controls">
									<?php echo JText::_('COM_CONTENT_ORDERING'); ?>
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>
				<h3><span><?php echo JText::_('JFIELD_LANGUAGE_LABEL') ?></span></h3>
				<div class="tabbody" id="language">
					<div class="tabbody-content">
						<?php echo $this->form->renderField('language'); ?>
					</div>
				</div>
				<h3><span><?php echo JText::_('COM_CONTENT_METADATA') ?></span></h3>
				<div class="tabbody" id="metadata">
					<div class="tabbody-content">
						<?php echo $this->form->renderField('metadesc'); ?>
						<?php echo $this->form->renderField('metakey'); ?>

						<input type="hidden" name="task" value="" />
						<input type="hidden" name="return" value="<?php echo $this->return_page; ?>" />
						<?php if ($this->params->get('enable_category', 0) == 1) : ?>
							<input type="hidden" name="jform[catid]" value="<?php echo $this->params->get('catid', 1); ?>" />
						<?php endif; ?>
					</div>
				</div>
			</div>
			<?php echo JHtml::_('form.token'); ?>
		</fieldset>
	</form>
</div>
