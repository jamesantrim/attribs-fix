<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  Template
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

defined('_JEXEC') or die;

// Dependency check
if (!class_exists('JYAML'))
{
	die('JYAML Framework not available: Please check if the JYAML System Plugin and JYAML Framework Library is installed and enabled!');
}

JHtml::_('behavior.keepalive');

// Load the form validator (using the form class: jyaml-form-validate)
JHtml::_('jyaml.validator');

?>
<div class="login<?php echo $this->pageclass_sfx?>">
	<?php if ($this->params->get('show_page_heading')) : ?>
	<h1>
		<?php echo $this->escape($this->params->get('page_heading')); ?>
	</h1>
	<?php endif; ?>

	<?php if ($this->params->get('logindescription_show') == 1 || $this->params->get('login_image') != '') : ?>
	<div class="login-description">
	<?php endif; ?>
		<?php if ($this->params->get('logindescription_show') == 1) : ?>
			<?php echo $this->params->get('login_description'); ?>
		<?php endif; ?>
		<?php if (($this->params->get('login_image') != '')) : ?>
			<?php $_src = $this->escape($this->params->get('login_image')); ?>
			<img src="<?php echo $_src; ?>" class="login-image" alt="<?php echo JTEXT::_('COM_USER_LOGIN_IMAGE_ALT'); ?>"/>
		<?php endif; ?>
	<?php if ($this->params->get('logindescription_show') == 1 || $this->params->get('login_image') != '') : ?>
	</div>
	<?php endif; ?>

	<form class="ym-form jyaml-form-validate" action="<?php echo JRoute::_('index.php?option=com_users&task=user.login'); ?>" method="post">
		<fieldset>
			<?php foreach ($this->form->getFieldset('credentials') as $field): ?>
				<?php if (!$field->hidden) : ?>
					<div class="ym-fbox ym-fbox-text">
						<?php echo $field->label; ?>
						<?php echo $field->input; ?>
					</div>
				<?php endif; ?>
			<?php endforeach; ?>

			<?php if (JPluginHelper::isEnabled('system', 'remember')) : ?>
			<div class="ym-fbox ym-fbox-check">
				<input id="user_check_remember" type="checkbox" name="remember" class="inputbox" value="yes" />
				<label for="user_check_remember"><?php echo JText::_('JGLOBAL_REMEMBER_ME') ?></label>
			</div>
			<?php endif; ?>
		</fieldset>

		<div class="ym-fbox-footer ym-fbox-button">
			<button type="submit" class="ym-button ym-save"><?php echo JText::_('JLOGIN'); ?></button>
		</div>

		<?php echo JHtml::_('form.token'); ?>
		<?php $_value = base64_encode($this->params->get('login_redirect_url', $this->form->getValue('return'))); ?>
		<input type="hidden" name="return" value="<?php echo $_value; ?>" />
	</form>
</div>
<div>
	<ul>
		<li>
			<a href="<?php echo JRoute::_('index.php?option=com_users&view=reset'); ?>">
			<?php echo JText::_('COM_USERS_LOGIN_RESET'); ?></a>
		</li>
		<li>
			<a href="<?php echo JRoute::_('index.php?option=com_users&view=remind'); ?>">
			<?php echo JText::_('COM_USERS_LOGIN_REMIND'); ?></a>
		</li>
		<?php
		$usersConfig = JComponentHelper::getParams('com_users');
		if ($usersConfig->get('allowUserRegistration')) : ?>
		<li>
			<a href="<?php echo JRoute::_('index.php?option=com_users&view=registration'); ?>">
				<?php echo JText::_('COM_USERS_LOGIN_REGISTER'); ?></a>
		</li>
		<?php endif; ?>
	</ul>
</div>
