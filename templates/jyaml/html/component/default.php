<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  Template
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

/* Note:
 * -----------------------------------------------------------------------------
 *
 * If your wish to REMOVE THE BACKLINK from the footer of HTML-Documents
 * PLEASE READ THE JYAML AND YAML LICENCE CONDITIONS:
 * - (JYAML) http://www.jyaml.de/en/license
 * - (YAML)  http://www.yaml.de/en/license/license-conditions.html
 *
 */

defined('_JEXEC') or die;

// Tell Joomla the template is HTML5
$this->setHtml5(true);

// Mobile viewport optimisation
$this->setMetaData('viewport', 'width=device-width, initial-scale=1.0');

$this->addBodyCssClass('contentpane');

?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>">
<head>
<jdoc:include type="head" />
</head>
<body class="no-js">
	<script type="text/javascript">document.body.className=(document.body.className.replace(/(\s|^)no-js(\s|$)/, '$1$2') + ' js').replace(/^\s+|\s+$/g, "");</script>

	<?php if ($this->params->get('showjsalert', 0)) : ?>
	<noscript class="jswarn-global jsremove">
		<div>
			<strong><?php echo JText::_('JYAML_JSALERT_TITLE'); ?></strong><br />
			<?php echo JText::_('JYAML_JSALERT_DESC'); ?>
		</div>
	</noscript>
	<?php endif; ?>

	<?php if ($this->hasMessages()) : ?>
	<div id="system-messages" role="complementary">
		<div id="system-messages-content" class="ym-clearfix">
			<jdoc:include type="message" />
		</div>
	</div>
	<?php endif; ?>

	<div id="main">
		<jdoc:include type="component" />
	</div>
</body>
</html>
