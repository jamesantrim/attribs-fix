<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  Template
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

/* Note:
 * -----------------------------------------------------------------------------
 *
 * If your wish to REMOVE THE BACKLINK from the footer of HTML-Documents
 * PLEASE READ THE JYAML AND YAML LICENCE CONDITIONS:
 * - (JYAML) http://www.jyaml.de/en/license
 * - (YAML)  http://www.yaml.de/en/license/license-conditions.html
 *
 */

defined('_JEXEC') or die;

// Tell Joomla the template is HTML5
$this->setHtml5(true);

// Mobile viewport optimisation
$this->setMetaData('viewport', 'width=device-width, initial-scale=1.0');

// Force one column layout
$this->addBodyCssClass('layout', '1col_3');

$document = JFactory::getDocument();
$errorDocument = $this->get('errorDocument');

$document->setTitle($errorDocument->error->getCode() . ' - ' . $errorDocument->title);

// Optional: Joomla! system error stylesheet
// $this->addStylesheet($this->getUrl('css', 'system.error.css'));

?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>">
<head>
<?php if ($document->getType() != 'html') : ?>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo $document->getTitle(); ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="<?php echo $this->getUrl('css', 'base.layout.css'); ?>" type="text/css" />
	<!--[if lte IE 7]>
		<link rel="stylesheet" href="<?php echo $this->getUrl('css', 'base.patch.css'); ?>" type="text/css" media="all" />
	<![endif]-->
	<?php if ($this->direction == 'rtl') : ?>
		<link rel="stylesheet" href="<?php echo $this->getUrl('css', 'screen.basemod-rtl.css'); ?>" type="text/css" />
		<link rel="stylesheet" href="<?php echo $this->getUrl('css', 'screen.content-rtl.css'); ?>" type="text/css" />
	<?php endif; ?>
<?php else : ?>
<jdoc:include type="head" />
<?php endif; ?>
</head>
<body class="no-js">
	<script type="text/javascript">document.body.className=(document.body.className.replace(/(\s|^)no-js(\s|$)/, '$1$2') + ' js').replace(/^\s+|\s+$/g, "");</script>

	<?php if ($this->params->get('showjsalert', 0)) : ?>
	<noscript class="jswarn-global jsremove">
		<div>
			<strong><?php echo JText::_('JYAML_JSALERT_TITLE'); ?></strong><br />
			<?php echo JText::_('JYAML_JSALERT_DESC'); ?>
		</div>
	</noscript>
	<?php endif; ?>

	<!-- skip link navigation -->
	<jdoc:include type="jyaml-skiplinks" outer="ul" wrap="li" />

	<div class="ym-wrapper">
		<div class="ym-wbox">
			<header role="banner">
				<?php if ($this->countModules('nav_top')) : ?>
				<nav id="topnav" role="navigation">
					<jdoc:include type="jyaml" name="nav_top" />
				</nav>
				<?php endif; ?>
				<h1>
					<a href="<?php echo $this->getHomepageLink(); ?>">
						<?php if ($_logoImage = trim($this->params->get('logo_image', ''))) : ?>
							<?php
								$_img = JYAML::getImage(
									// Image src
									$this->getUrl('root', $_logoImage),
									// Image alt
									JFactory::getApplication()->get('sitename'),
									// Thumbnailer options (see http://www.jyaml.de/en/documentation/jyaml-4-joomla/programming-and-development/jyaml-get-image.html)
									array(),
									// Additional attributes
									'class="site-logo"'
								);

								// Output image without modifications
								echo $_img->toHtml();
							?>
						<?php else : ?>
							<?php echo JFactory::getApplication()->get('sitename'); ?>
						<?php endif; ?>
					</a>
				</h1>
				<?php if (($_sloganText = trim($this->params->get('slogan_text', '')))) : ?>
					<span class="slogan"><?php
						$_sloganText = JFactory::getLanguage()->hasKey($_sloganText) ? JText::_($_sloganText) : $_sloganText;
						echo htmlspecialchars($_sloganText, ENT_COMPAT, JFactory::getDocument()->getCharSet());
					?></span>
				<?php endif; ?>
			</header>

			<?php if ($this->countModules('nav_main')) : ?>
			<nav id="nav" role="navigation">
				<?php echo $this->addSkiplink('navigation', 'JYAML_SKIPLINK_MAIN_NAVIGATION'); ?>
				<jdoc:include type="jyaml" name="nav_main" />
			</nav>
			<?php endif; ?>

			<div id="main">
				<div class="ym-column">
					<div class="ym-col3">
						<div class="ym-cbox ym-clearfix">
							<div class="ym-contain-fl">
								<?php echo $this->addSkiplink('content', 'JYAML_SKIPLINK_MAIN_CONTENT', ''); ?>

								<div id="errorboxoutline">
									<h1 id="errorboxheader">
										<?php echo $errorDocument->error->getCode(); ?> - <?php echo $errorDocument->error->getMessage(); ?>
									</h1>
									<div id="errorboxbody">
										<div class="subcolumns">
											<div class="c66l">
												<div class="subcl">
													<p>
														<strong><?php echo JText::_('JERROR_LAYOUT_NOT_ABLE_TO_VISIT'); ?></strong>
													</p>
													<ol>
														<li><?php echo JText::_('JERROR_LAYOUT_AN_OUT_OF_DATE_BOOKMARK_FAVOURITE'); ?></li>
														<li><?php echo JText::_('JERROR_LAYOUT_SEARCH_ENGINE_OUT_OF_DATE_LISTING'); ?></li>
														<li><?php echo JText::_('JERROR_LAYOUT_MIS_TYPED_ADDRESS'); ?></li>
														<li><?php echo JText::_('JERROR_LAYOUT_YOU_HAVE_NO_ACCESS_TO_THIS_PAGE'); ?></li>
														<li><?php echo JText::_('JERROR_LAYOUT_REQUESTED_RESOURCE_WAS_NOT_FOUND'); ?></li>
														<li><?php echo JText::_('JERROR_LAYOUT_ERROR_HAS_OCCURRED_WHILE_PROCESSING_YOUR_REQUEST'); ?></li>
													</ol>
												</div>
											</div>
											<div class="c33r">
												<p>
													<strong><?php echo JText::_('JERROR_LAYOUT_PLEASE_TRY_ONE_OF_THE_FOLLOWING_PAGES'); ?></strong>
												</p>
												<ul>
													<li>
														<a href="<?php echo $errorDocument->baseurl; ?>/" title="<?php echo JText::_('JERROR_LAYOUT_GO_TO_THE_HOME_PAGE'); ?>">
															<?php echo JText::_('JERROR_LAYOUT_HOME_PAGE'); ?>
														</a>
													</li>
												</ul>
											</div>
										</div>
										<p class="info">
											<?php echo JText::_('JERROR_LAYOUT_PLEASE_CONTACT_THE_SYSTEM_ADMINISTRATOR'); ?>:<br />
											<em><?php echo $errorDocument->error->getMessage(); ?> (<?php echo htmlspecialchars(JRoute::_(JURI::getInstance()->toString())); ?>)</em>
										</p>
										<?php if ($errorDocument->debug) : ?>
										<div id="techinfo">
											<?php echo $errorDocument->renderBacktrace(); ?>
										</div>
										<?php endif; ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<footer role="contentinfo">
				<div class="ym-cbox ym-clearfix">
					© <?php echo date('Y'); ?> <?php echo JFactory::getApplication()->get('sitename'); ?>

					<span class="float_right a-right" style="border-left:1px solid #ccc; padding-left:1em; margin-top:.5em;">
						<!-- **********************************************************************
							(de) Folgende Rückverlinkungen dürfen nur entfernt werden, wenn
									Sie eine JYAML und/oder eine YAML Lizenz besitzen.

							(en) Following backlinks may be only removed, if
									you are owner of a JYAML and/or a YAML license.

								:: http://www.jyaml.de
								:: http://www.yaml.de
						*********************************************************************** -->
						Layout based on
						<a href="http://www.jyaml.de/" target="_blank">JYAML</a> and
						<a href="http://www.yaml.de/" target="_blank">YAML</a>
						<!-- ****************************************************************** -->

						<br /><small>powered by <a href="http://www.hieblmedia.de/" target="_blank">HieblMedia</a></small>
					</span>
				</div>
			</footer>
		</div>
	</div>
</body>
</html>
