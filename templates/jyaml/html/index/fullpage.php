<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  Template
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

/* Note:
 * -----------------------------------------------------------------------------
 *
 * If your wish to REMOVE THE BACKLINK from the footer of HTML-Documents
 * PLEASE READ THE JYAML AND YAML LICENCE CONDITIONS:
 * - (JYAML) http://www.jyaml.de/en/license
 * - (YAML)  http://www.yaml.de/en/license/license-conditions.html
 *
 */

defined('_JEXEC') or die;

// Tell Joomla the template is HTML5
$this->setHtml5(true);

// Mobile viewport optimisation
$this->setMetaData('viewport', 'width=device-width, initial-scale=1.0');

?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>">
<head>
<jdoc:include type="head" />
</head>
<body class="no-js">
	<script type="text/javascript">document.body.className=(document.body.className.replace(/(\s|^)no-js(\s|$)/, '$1$2') + ' js').replace(/^\s+|\s+$/g, "");</script>
	<a id="top" class="skiplink-anchor"></a>

	<?php if ($this->params->get('showjsalert', 0)) : ?>
	<noscript class="jswarn-global jsremove">
		<div>
			<strong><?php echo JText::_('JYAML_JSALERT_TITLE'); ?></strong><br />
			<?php echo JText::_('JYAML_JSALERT_DESC'); ?>
		</div>
	</noscript>
	<?php endif; ?>

	<!-- skip link navigation -->
	<jdoc:include type="jyaml-skiplinks" outer="ul" wrap="li" />

	<header role="banner">
		<div class="ym-wrapper">
			<div class="ym-wbox">
				<?php if ($this->countModules('nav_top')) : ?>
				<nav id="topnav" role="navigation">
					<jdoc:include type="jyaml" name="nav_top" />
				</nav>
				<?php endif; ?>
				<h1>
					<a href="<?php echo $this->getHomepageLink(); ?>">
						<?php if ($_logoImage = trim($this->params->get('logo_image', ''))) : ?>
							<?php
								$_img = JYAML::getImage(
									// Image src
									$this->getUrl('root', $_logoImage),
									// Image alt
									JFactory::getApplication()->get('sitename'),
									// Thumbnailer options (see http://www.jyaml.de/en/documentation/jyaml-4-joomla/programming-and-development/jyaml-get-image.html)
									array(),
									// Additional attributes
									'class="site-logo"'
								);

								// Output image without modifications
								echo $_img->toHtml();
							?>
						<?php else : ?>
							<?php echo JFactory::getApplication()->get('sitename'); ?>
						<?php endif; ?>
					</a>
				</h1>
				<?php if (($_sloganText = trim($this->params->get('slogan_text', '')))) : ?>
					<span class="slogan"><?php
						$_sloganText = JFactory::getLanguage()->hasKey($_sloganText) ? JText::_($_sloganText) : $_sloganText;
						echo htmlspecialchars($_sloganText, ENT_COMPAT, JFactory::getDocument()->getCharSet());
					?></span>
				<?php endif; ?>
				<?php if ($_headerImage = $this->getHeaderImage()) : ?>
					<div class="header_image">
						<?php echo JYAML::getImage($this->getUrl('root', $_headerImage))->toHtml(); ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</header>

	<?php if ($this->countModules('nav_main')) : ?>
	<nav id="nav" role="navigation">
		<div class="ym-wrapper">
			<div class="ym-wbox">
				<?php echo $this->addSkiplink('navigation', 'JYAML_SKIPLINK_MAIN_NAVIGATION', '', '', false); ?>
				<jdoc:include type="jyaml" name="nav_main" />
			</div>
		</div>
	</nav>
	<?php endif; ?>

	<?php if ($this->countModules('breadcrumb')) : ?>
	<div id="breadcrumbs" role="contentinfo">
		<div class="ym-wrapper">
			<div class="ym-wbox">
				<jdoc:include type="modules" name="breadcrumb" style="raw" />
			</div>
		</div>
	</div>
	<?php endif; ?>

	<?php if ($this->hasMessages()) : ?>
	<div id="system-messages" role="complementary">
		<div class="ym-wrapper">
			<div class="ym-wbox">
				<div id="system-messages-content" class="ym-clearfix">
					<jdoc:include type="message" />
				</div>
			</div>
		</div>
	</div>
	<?php endif; ?>

	<?php if ($this->countModules('content_top')) : ?>
	<div class="main_top" role="complementary">
		<div class="ym-wrapper">
			<div class="ym-wbox">
				<jdoc:include type="jyaml" name="main_top" />
			</div>
		</div>
	</div>
	<?php endif; ?>

	<?php if ($this->countModules('col1_content OR col2_content OR col3_content')) : ?>
	<div id="main">
		<div class="ym-wrapper">
			<div class="ym-wbox">
				<div class="ym-column ym-column-main">
					<?php if ($this->countModules('col1_content')) : ?>
					<div class="ym-col1" role="<?php echo $this->countModules('col1_content:#component') ? 'main' : 'complementary'; ?>">
						<div class="ym-cbox ym-clearfix">
							<?php if ($this->countModules('col1_content:#component')) : ?><div class="ym-contain-fl"><?php endif; ?>
								<?php echo $this->addSkiplink('content', 'JYAML_SKIPLINK_MAIN_CONTENT', '', 'col1_content:#component'); ?>
								<jdoc:include type="jyaml" name="col1_content" />
							<?php if ($this->countModules('col1_content:#component')) : ?></div><?php endif; ?>
						</div>
					</div>
					<?php endif; ?>

					<?php if ($this->countModules('col2_content')) : ?>
					<div class="ym-col2" role="<?php echo $this->countModules('col2_content:#component') ? 'main' : 'complementary'; ?>">
						<div class="ym-cbox ym-clearfix">
							<?php if ($this->countModules('col2_content:#component')) : ?><div class="ym-contain-fl"><?php endif; ?>
								<?php echo $this->addSkiplink('content', 'JYAML_SKIPLINK_MAIN_CONTENT', '', 'col2_content:#component'); ?>
								<jdoc:include type="jyaml" name="col2_content" />
							<?php if ($this->countModules('col2_content:#component')) : ?></div><?php endif; ?>
						</div>
					</div>
					<?php endif; ?>

					<div class="ym-col3" role="<?php echo $this->countModules('col3_content:#component') ? 'main' : 'complementary'; ?>">
						<div class="ym-cbox ym-clearfix">
							<?php if ($this->countModules('col3_content:#component')) : ?><div class="ym-contain-fl"><?php endif; ?>
								<?php echo $this->addSkiplink('content', 'JYAML_SKIPLINK_MAIN_CONTENT', '', 'col3_content:#component'); ?>
								<jdoc:include type="jyaml" name="col3_content" />
							<?php if ($this->countModules('col3_content:#component')) : ?></div><?php endif; ?>
						</div>
						<!-- IE6 and IE7 column clearing to simulate equal height -->
						<div class="ym-ie-clearing">&nbsp;</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php endif; ?>

	<?php if ($this->countModules('main_bottom')) : ?>
	<div class="main_bottom" role="complementary">
		<div class="ym-wrapper">
			<div class="ym-wbox">
				<jdoc:include type="jyaml" name="main_bottom" />
			</div>
		</div>
	</div>
	<?php endif; ?>

	<footer role="contentinfo">
		<div class="ym-wrapper">
			<div class="ym-wbox">
				<div class="ym-cbox ym-clearfix">
					© <?php echo date('Y'); ?> <?php echo JFactory::getApplication()->get('sitename'); ?>

					<span class="float_right a-right" style="border-left:1px solid #ccc; padding-left:1em; margin-top:.5em;">
						<!-- **********************************************************************
							(de) Folgende Rückverlinkungen dürfen nur entfernt werden, wenn
									Sie eine JYAML und/oder eine YAML Lizenz besitzen.

							(en) Following backlinks may be only removed, if
									you are owner of a JYAML and/or a YAML license.

								:: http://www.jyaml.de
								:: http://www.yaml.de
						*********************************************************************** -->
						Layout based on
						<a href="http://www.jyaml.de/" target="_blank">JYAML</a> and
						<a href="http://www.yaml.de/" target="_blank">YAML</a>
						<!-- ****************************************************************** -->

						<br /><small>powered by <a href="http://www.hieblmedia.de/" target="_blank">HieblMedia</a></small>
					</span>
				</div>
			</div>
		</div>
	</footer>
</body>
</html>
