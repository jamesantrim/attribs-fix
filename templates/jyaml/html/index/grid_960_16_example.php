<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  Template
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

/* Note:
 * -----------------------------------------------------------------------------
 *
 * If your wish to REMOVE THE BACKLINK from the footer of HTML-Documents
 * PLEASE READ THE JYAML AND YAML LICENCE CONDITIONS:
 * - (JYAML) http://www.jyaml.de/en/license
 * - (YAML)  http://www.yaml.de/en/license/license-conditions.html
 *
 */

defined('_JEXEC') or die;

// Tell Joomla the template is HTML5
$this->setHtml5(true);

// Mobile viewport optimisation
$this->setMetaData('viewport', 'width=device-width, initial-scale=1.0');

/*
 * This is a simple example for the 960gs port of YAML4
 * Details see: http://www.yaml.de/docs/index.html#yaml-grids
 */

// Add the additional stylesheet for 960 Grid with 16 columns
$this->addStylesheet($this->getUrl('css', 'screen.grid960.css'));

?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>">
<head>
<jdoc:include type="head" />
</head>
<body class="no-js">
	<script type="text/javascript">document.body.className=(document.body.className.replace(/(\s|^)no-js(\s|$)/, '$1$2') + ' js').replace(/^\s+|\s+$/g, "");</script>
	<a id="top" class="skiplink-anchor"></a>

	<?php if ($this->params->get('showjsalert', 0)) : ?>
	<noscript class="jswarn-global jsremove">
		<div>
			<strong><?php echo JText::_('JYAML_JSALERT_TITLE'); ?></strong><br />
			<?php echo JText::_('JYAML_JSALERT_DESC'); ?>
		</div>
	</noscript>
	<?php endif; ?>

	<!-- skip link navigation -->
	<jdoc:include type="jyaml-skiplinks" outer="ul" wrap="li" />

	<div class="ym-wrapper">
		<div class="ym-wbox">
			<div class="ym-gbox">
				<header role="banner">
					<?php if ($this->countModules('nav_top')) : ?>
					<nav id="topnav" role="navigation">
						<jdoc:include type="jyaml" name="nav_top" />
					</nav>
					<?php endif; ?>
					<h1>
						<a href="<?php echo $this->getHomepageLink(); ?>">
						<?php if ($_logoImage = trim($this->params->get('logo_image', ''))) : ?>
							<?php
								$_img = JYAML::getImage(
									// Image src
									$this->getUrl('root', $_logoImage),
									// Image alt
									JFactory::getApplication()->get('sitename'),
									// Thumbnailer options (see http://www.jyaml.de/en/documentation/jyaml-4-joomla/programming-and-development/jyaml-get-image.html)
									array(),
									// Additional attributes
									'class="site-logo"'
								);

								// Output image without modifications
								echo $_img->toHtml();
							?>
						<?php else : ?>
							<?php echo JFactory::getApplication()->get('sitename'); ?>
						<?php endif; ?>
						</a>
					</h1>
					<?php if (($_sloganText = trim($this->params->get('slogan_text', '')))) : ?>
						<span class="slogan"><?php
							$_sloganText = JFactory::getLanguage()->hasKey($_sloganText) ? JText::_($_sloganText) : $_sloganText;
							echo htmlspecialchars($_sloganText, ENT_COMPAT, JFactory::getDocument()->getCharSet());
						?></span>
					<?php endif; ?>
					<?php if ($_headerImage = $this->getHeaderImage()) : ?>
						<div class="header_image">
							<?php echo JYAML::getImage($this->getUrl('root', $_headerImage))->toHtml(); ?>
						</div>
					<?php endif; ?>
				</header>
			</div>

			<?php if ($this->countModules('nav_main')) : ?>
			<div class="ym-gbox">
				<nav id="nav" role="navigation">
					<?php echo $this->addSkiplink('navigation', 'JYAML_SKIPLINK_MAIN_NAVIGATION', '', '', false); ?>
					<jdoc:include type="jyaml" name="nav_main" />
				</nav>
			</div>
			<?php endif; ?>

			<?php if ($this->countModules('breadcrumb')) : ?>
			<div class="ym-gbox">
				<div id="breadcrumbs" role="contentinfo">
					<jdoc:include type="modules" name="breadcrumb" style="raw" />
				</div>
			</div>
			<?php endif; ?>

			<?php if ($this->hasMessages()) : ?>
			<div class="ym-gbox">
				<div id="system-messages" role="complementary">
					<div id="system-messages-content" class="ym-clearfix">
						<jdoc:include type="message" />
					</div>
				</div>
			</div>
			<?php endif; ?>

			<div class="grid960-demo">
				<h2 class="ym-gbox">12 Column Grid Demo</h2>
				<p class="ym-gbox">
					YAML4 CSS-Class Documentation: <a href="http://www.yaml.de/docs/index.html#yaml-grids">http://www.yaml.de/docs/index.html#yaml-grids</a>
				</p>
				<div class="ym-gbox">940</div>
				<div class="ym-grid linearize-level-1">
					<div class="ym-g960-1 ym-gl">
						<div class="ym-gbox">60</div>
					</div>
					<div class="ym-g960-11 ym-gl">
						<div class="ym-gbox">860</div>
					</div>
					<div class="ym-g960-2 ym-gl">
						<div class="ym-gbox">140</div>
					</div>
					<div class="ym-g960-10 ym-gl">
						<div class="ym-gbox">780</div>
					</div>
					<div class="ym-g960-3 ym-gl">
						<div class="ym-gbox">220</div>
					</div>
					<div class="ym-g960-9 ym-gl">
						<div class="ym-gbox">700</div>
					</div>
					<div class="ym-g960-4 ym-gl">
						<div class="ym-gbox">300</div>
					</div>
					<div class="ym-g960-8 ym-gl">
						<div class="ym-gbox">620</div>
					</div>
					<div class="ym-g960-5 ym-gl">
						<div class="ym-gbox">380</div>
					</div>
					<div class="ym-g960-7 ym-gl">
						<div class="ym-gbox">540</div>
					</div>
					<div class="ym-g960-6 ym-gl">
						<div class="ym-gbox">460</div>
					</div>
					<div class="ym-g960-6 ym-gl">
						<div class="ym-gbox">460</div>
					</div>
				</div>

				<h3 class="ym-gbox">Push & Pull Classes Available</h3>
				<div class="ym-grid linearize-level-1">
					<div class="ym-g960-1 ym-gl">
						<div class="ym-gbox">60</div>
					</div>
				</div>
				<div class="ym-grid linearize-level-1">
					<div class="ym-g960-1 ym-push-1 ym-gl">
						<div class="ym-gbox">60</div>
					</div>
				</div>
				<div class="ym-grid linearize-level-1">
					<div class="ym-g960-1 ym-push-2 ym-gl">
						<div class="ym-gbox">60</div>
					</div>
				</div>
				<div class="ym-grid linearize-level-1">
					<div class="ym-g960-1 ym-push-3 ym-gl">
						<div class="ym-gbox">60</div>
					</div>
				</div>
				<div class="ym-grid linearize-level-1">
					<div class="ym-g960-1 ym-push-4 ym-gl">
						<div class="ym-gbox">60</div>
					</div>
				</div>
				<div class="ym-grid linearize-level-1">
					<div class="ym-g960-1 ym-push-5 ym-gl">
						<div class="ym-gbox">60</div>
					</div>
				</div>
				<div class="ym-grid linearize-level-1">
					<div class="ym-g960-1 ym-push-6 ym-gl">
						<div class="ym-gbox">60</div>
					</div>
				</div>
				<div class="ym-grid linearize-level-1">
					<div class="ym-g960-1 ym-push-7 ym-gl">
						<div class="ym-gbox">60</div>
					</div>
				</div>
				<div class="ym-grid linearize-level-1">
					<div class="ym-g960-1 ym-push-8 ym-gl">
						<div class="ym-gbox">60</div>
					</div>
				</div>
				<div class="ym-grid linearize-level-1">
					<div class="ym-g960-1 ym-push-9 ym-gl">
						<div class="ym-gbox">60</div>
					</div>
				</div>
				<div class="ym-grid linearize-level-1">
					<div class="ym-g960-1 ym-push-10 ym-gl">
						<div class="ym-gbox">60</div>
					</div>
				</div>
				<div class="ym-grid linearize-level-1">
					<div class="ym-g960-1 ym-push-11 ym-gl">
						<div class="ym-gbox">60</div>
					</div>
				</div>
				<div class="ym-grid linearize-level-1">
					<div class="ym-g960-3 ym-gl">
						<div class="ym-gbox">220</div>
					</div>
					<div class="ym-g960-3 ym-gl">
						<div class="ym-gbox">220</div>
					</div>
					<div class="ym-g960-1 ym-gl">
						<div class="ym-gbox">60</div>
					</div>
					<div class="ym-g960-5 ym-gl">
						<div class="ym-gbox">380</div>
					</div>
				</div>
				<div class="ym-grid linearize-level-1">
					<div class="ym-g960-1 ym-gl">
						<div class="ym-gbox">60</div>
					</div>
					<div class="ym-g960-5 ym-gl">
						<div class="ym-gbox">380</div>
					</div>
					<div class="ym-g960-3 ym-gl">
						<div class="ym-gbox">220</div>
					</div>
					<div class="ym-g960-3 ym-gl">
						<div class="ym-gbox">220</div>
					</div>
				</div>
			</div>

			<div class="ym-grid">
				<div class="ym-g960-3 ym-gl">
					<div class="ym-gbox">
						<jdoc:include type="jyaml" name="col1_content" />
					</div>
				</div>
				<div class="ym-g960-6 ym-gl">
					<div class="ym-gbox">
						<jdoc:include type="jyaml" name="col3_content" />
					</div>
				</div>
				<div class="ym-g960-3 ym-gl">
					<div class="ym-gbox">
						<jdoc:include type="jyaml" name="col2_content" />
					</div>
				</div>
			</div>

			<div class="ym-gbox">
				<footer role="contentinfo">
					<div class="ym-cbox ym-clearfix">
						© <?php echo date('Y'); ?> <?php echo JFactory::getApplication()->get('sitename'); ?>

						<span class="float_right a-right" style="border-left:1px solid #ccc; padding-left:1em; margin-top:.5em;">
							<!-- **********************************************************************
								(de) Folgende Rückverlinkungen dürfen nur entfernt werden, wenn
										Sie eine JYAML und/oder eine YAML Lizenz besitzen.

								(en) Following backlinks may be only removed, if
										you are owner of a JYAML and/or a YAML license.

									:: http://www.jyaml.de
									:: http://www.yaml.de
							*********************************************************************** -->
							Layout based on
							<a href="http://www.jyaml.de/" target="_blank">JYAML</a> and
							<a href="http://www.yaml.de/" target="_blank">YAML</a>
							<!-- ****************************************************************** -->

							<br /><small>powered by <a href="http://www.hieblmedia.de/" target="_blank">HieblMedia</a></small>
						</span>
					</div>
				</footer>
			</div>

		</div>
	</div>
</body>
</html>
