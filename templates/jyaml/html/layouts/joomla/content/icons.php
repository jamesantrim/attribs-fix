<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  JHtml
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

defined('JPATH_BASE') or die;

$jyaml = JYAML::getDocument();

if ($jyaml->params->get('load_jui_bootstrap', 0))
{
	include JPATH_ROOT . '/layouts/joomla/content/icons.php';
	return;
}

$canEdit = $displayData['params']->get('access-edit');

?>
<div class="icons">
	<div class="icons-content ym-clearfix">
		<?php if (empty($displayData['print'])) : ?>
			<?php if ($canEdit || $displayData['params']->get('show_print_icon') || $displayData['params']->get('show_email_icon')) : ?>
				<ul class="actions">
					<?php // Note the actions class is deprecated. Use dropdown-menu instead. ?>
					<?php if ($displayData['params']->get('show_print_icon')) : ?>
						<li class="print-icon"><?php echo JHtml::_('icon.print_popup', $displayData['item'], $displayData['params']); ?> </li>
					<?php endif; ?>
					<?php if ($displayData['params']->get('show_email_icon')) : ?>
						<li class="email-icon"><?php echo JHtml::_('icon.email', $displayData['item'], $displayData['params']); ?> </li>
					<?php endif; ?>
					<?php if ($canEdit) : ?>
						<li class="edit-icon"><?php echo JHtml::_('icon.edit', $displayData['item'], $displayData['params']); ?> </li>
					<?php endif; ?>
				</ul>
			<?php endif; ?>
		<?php else : ?>
			<?php echo JHtml::_('icon.print_screen', $displayData['item'], $displayData['params']); ?>
		<?php endif; ?>
	</div>
</div>
