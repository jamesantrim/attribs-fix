========================================================================
Since Joomla 3.2 partial templates names JLayout can be shared between
extensions. And these layouts can be overwritten with the template also.

Joomla! Documentation:
> http://docs.joomla.org/J3.2:JLayout_Improvements_for_Joomla!
> http://docs.joomla.org/J3.x:Sharing_layouts_across_views_or_extensions_with_JLayout

========================================================================

============================
Overriding layout templates:
----------------------------
Search in: [JOOMLA_ROOT]/layouts/path/to/file.php
Copy to  : [JOOMLA_ROOT]/templates/layouts/path/to/file.php
