<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  Template
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

defined('_JEXEC') or die;

$showHome = $params->get('showHome');
$isHome   = JYAML::getDocument()->isHomepage();
$homeLink = JRoute::_('index.php?Itemid=' . $app->getMenu()->getDefault()->id);
$homeText = htmlspecialchars($params->get('homeText', JText::_('MOD_BREADCRUMBS_HOME')));

?>
<div class="breadcrumbs<?php echo $moduleclass_sfx; ?>">
<?php if ($params->get('showHere', 1)) : ?>
	<span class="showHere"><?php echo JText::_('MOD_BREADCRUMBS_HERE'); ?></span>
<?php endif; ?>
<?php if (!$showHome && $count && !$isHome) : /* If show home is disabled create a hidden home link */ ?>
	<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="ym-hideme">
		<a href="<?php echo $homeLink; ?>" class="path" itemprop="url"><span itemprop="title"><?php echo $homeText; ?></span></a>
	</span>
<?php endif; ?>
<?php for ($i = 0; $i < $count; $i ++) : /* If not the last item in the breadcrumbs add the separator */ ?>
	<?php if ($i < $count - 1) : ?>
		<?php if (!empty($list[$i]->link)) : ?>
			<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
				<a href="<?php echo $list[$i]->link; ?>" class="path" itemprop="url"><span itemprop="title"><?php echo $list[$i]->name; ?></span></a>
			</span>
		<?php else : ?>
			<span><?php echo $list[$i]->name; ?></span>
		<?php endif; ?>
		<?php if ($i < $count - 2) : ?>
			<?php echo ' ' . $separator . ' '; ?>
		<?php endif; ?>
	<?php elseif ($params->get('showLast', 1)) : /* When $i == $count -1 and 'showLast' is true */ ?>
		<?php if ($i > 0) : ?>
			<?php echo ' ' . $separator . ' '; ?>
		<?php endif; ?>
		<span><?php echo $list[$i]->name; ?></span>
	<?php endif; ?>
<?php endfor; ?>
</div>
