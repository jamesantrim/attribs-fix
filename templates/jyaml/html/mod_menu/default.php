<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  JYAML.Template
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

// No direct access.
defined('_JEXEC') or die;

// Note. It is important to remove spaces between elements.

// Get the document class reference
$document = JFactory::getDocument();

// Get the jyaml class reference
$jyaml = JYAML::getDocument();

if (!isset($menu))
{
	$menu = JFactory::getApplication()->getMenu();
}

// Init params
$dynamicTitle = $jyaml->getExtConfig('mod_menu', 'replaceModuleTitle', 0, $params);
$disableClick = $jyaml->getExtConfig('mod_menu', 'disableClickActiveLink', false, $params);
$hideHome = $jyaml->getExtConfig('mod_menu', 'hideHomeWhenHome', 0, $params);

$mobileNav = ($params->get('jyaml_mobilenav_enable', 0) && $jyaml->params->get('layout_responsive'));
if ($mobileNav)
{
	$menueId = trim($params->get('tag_id'), '');
	if (!$menueId)
	{
		$menueId = 'menu-' . $module->id . '-' . mt_rand(1000, 9999);
		$params->set('tag_id', $menueId);
	}
	$mobileNavOptions = array();
	$mobileNavOptions[] = "width_threshold:" . (int) $params->get('jyaml_mobilenav_with_threshold', 768);
	$mobileNavOptions[] = "show_active_text:" . ($params->get('jyaml_mobilenav_show_active_text', '1') ? 'true' : 'false');
	$mobileNavOptions[] = "title_text:'" . JText::_('JYAML_TPL_MOBILENAV_TITLE_TEXT') . "'";
	$mobileNavOptions[] = "insert_selector:'" . $params->get('jyaml_mobilenav_insert_selector', 'body') . "'";
	$mobileNavOptions[] = "insert_method:'" . $params->get('jyaml_mobilenav_insert_method', 'prepend') . "'";
	$mobileNavOptions[] = "active_selector:'.current_link span:not(.subtitle)'";

	JHtml::_('jyaml.mobilenav', '#' . $menueId, $mobileNavOptions);
	$jyaml->addStylesheet($jyaml->getUrl('css', 'navigation.mobile.css'));
}

// Get the layout name
$layout = $params->get('layout');
$layout = explode(':', $layout);
$layout = trim((isset($layout[1]) ? $layout[1] : $layout[0]));

// Id attribute
$idAttribute = '';
if ($params->get('tag_id') != null)
{
	$tag = $params->get('tag_id') . '';
	$idAttribute = ' id="' . $tag . '"';
}
$ulRootAttributes = '';
if ($params->get('ul_root_attributes') != null)
{
	$ulRootAttributes = ' ' . trim($params->get('ul_root_attributes'));
}

// Some preperations
$maxCounter = array();
$maxSubtitles = array();

foreach ($list as $i => $item)
{
	// Fix for the Joomla issue #24628 (http://joomlacode.org/gf/project/joomla/tracker/?action=TrackerItemEdit&tracker_item_id=24628)
	$list[$i]->title = str_replace('&amp;amp;', '&amp;', $list[$i]->title);

	$lpHash = $item->level . '-' . $item->parent_id;

	// Helper to determine maximal subtitles to blow up items with equal new lines
	if (!isset($maxSubtitles[$lpHash]))
	{
		$maxSubtitles[$lpHash] = 0;
	}

	$_subtitles = $item->params->get('jyaml_menuitemsubtitles');
	if ($_subtitles && is_array($_subtitles))
	{
		$_subtitlesCount = count($_subtitles);

		if ($_subtitlesCount > $maxSubtitles[$lpHash])
		{
			$maxSubtitles[$lpHash] = $_subtitlesCount;
		}
	}
	else
	{
		// Init subtitles value as array if isn't set as it
		$item->params->set('jyaml_menuitemsubtitles', array());
	}

	// Helper to determine last-item
	if (!isset($maxCounter[$lpHash]))
	{
		$maxCounter[$lpHash] = 0;
	}
	else
	{
		$maxCounter[$lpHash]++;
	}

	// Dynamic module title
	if (isset($dynamicTitle) && $dynamicTitle)
	{
		if ($item->parent_id && $item->level == 2)
		{
			$parentItem = $menu->getItem($item->parent_id);

			if ($parentItem)
			{
				$moduleTitle = $parentItem->title;
				$module->title = $moduleTitle;

				// Unset var for prepare module title at once
				unset($dynamicTitle);
			}
		}
	}
}

// More accessibility for section navigations (startLevel > 1 in most cases is a section navigation)
if ($params->get('startLevel') > 1 && count($list))
{
	if ($jyaml->countModules($module->position))
	{
		echo $jyaml->addSkiplink('navigation-section-' . $module->id, 'JYAML_SKIPLINK_SECTION_NAVIGATION', $module->title, '', false);
	}
}

?><ul <?php echo 'class="menu' . $params->get('class_sfx') . '"' . $idAttribute . $ulRootAttributes; ?>>
<?php

$homeId = (int) $jyaml->getHomepageLink(null, true);
$isHome = (int) $jyaml->isHomepage();
$counter = array();

foreach ($list as $i => &$item)
{
	$class = 'item-' . $item->id . ' ';

	$menuItemContent = $item->params->get('jyaml_menuitemcontent', '');
	if (is_object($menuItemContent))
	{
		$menuItemContent = \Joomla\Utilities\ArrayHelper::fromObject($menuItemContent);
	}
	if (isset($menuItemContent['dummy']))
	{
		unset($menuItemContent['dummy']);
	}

	$pageClassSfx = trim($item->params->get('pageclass_sfx', ''));
	$aliasID = $item->params->get('aliasoptions', 0);

	$lpHash = $item->level . '-' . $item->parent_id;

	// Calculate real level
	$viewLevel = $item->level - $params->get('startLevel');

	// Prepare subtitles
	$item->origTitle = $item->title;
	if (isset($maxSubtitles[$lpHash]) && $maxSubtitles[$lpHash] > 0)
	{
		$subTitles = $item->params->get('jyaml_menuitemsubtitles', array());

		// Required to set the original title without subtitles and html after render this module
		$_orgTitle = $item->title;

		$subnames = array();
		$subnames[-1] = '<span class="maintitle">' . trim($item->title) . '</span>';

		for ($y = 0; $y < $maxSubtitles[$lpHash]; $y++)
		{
			if (isset($subTitles[$y]))
			{
				$_sTitle = trim($subTitles[$y]);
				$subnames[$y] = '<span class="subtitle subtitle-' . $y . ($_sTitle == '' ? ' subtitle-empty' : '') . '">' .
												($_sTitle == '' ? '&nbsp;' : $_sTitle) . '</span>';
			}
			else
			{
				$subnames[$y] = '<span class="subtitle subtitle-' . $y . ' subtitle-empty">&nbsp;</span>';
			}
		}
		$item->title = implode("\n", $subnames);
	}
	else
	{
		unset($_orgTitle);
		unset($subnames);
	}

	if (!isset($counter[$lpHash]))
	{
		$counter[$lpHash] = 0;
	}

	if ($maxCounter[$lpHash] == 0)
	{
		$class .= 'item-once ';
	}
	else
	{
		if ($counter[$lpHash] == 0)
		{
			$class .= 'item-first ';
		}

		if ($counter[$lpHash] == $maxCounter[$lpHash])
		{
			$class .= 'item-last ';
		}
	}

	// Item count class
	$class .= 'item-count' . $counter[$lpHash] . ' ';

	$isActive = false;
	if (in_array($item->id, $path) || in_array($aliasID, $path))
	{
		$class .= 'active ';

		if ($item->deeper)
		{
			$class .= 'active-parent ';
		}

		$isActive = true;

		$anchorCss = $item->params->get('menu-anchor_css', '');
		if (strpos($anchorCss, 'active_link') === false)
		{
			$anchorCss .= ' active_link';
		}
		$item->params->set('menu-anchor_css', trim($anchorCss));
	}

	$isCurrent = false;
	if ($item->id == $active_id || $aliasID == $active_id)
	{
		$class .= 'current ';
		$isCurrent = true;

		$anchorCss = $item->params->get('menu-anchor_css', '');
		if (strpos($anchorCss, 'current_link') === false)
		{
			$anchorCss .= ' current_link';
		}
		$item->params->set('menu-anchor_css', trim($anchorCss));
	}

	$class .= 'level' . $item->level . ' ';
	$class .= 'viewLevel' . $viewLevel . ' ';

	if ($item->deeper)
	{
		$class .= 'parent ';
	}

	if ($item->type)
	{
		$class .= 'type-' . $item->type . ' ';
	}

	if ($menuItemContent)
	{
		$class .= 'hasMenuItemContent ';
	}

	if ($pageClassSfx)
	{
		$class .= 'page-sfx-' . $pageClassSfx . ' ';

		if ($isActive)
		{
			$class .= 'page-sfx-' . $pageClassSfx . '-active ';
		}
		if ($isCurrent)
		{
			$class .= 'page-sfx-' . $pageClassSfx . '-current ';
		}
	}

	// Create class attribute
	if (!empty($class))
	{
		$class = ' class="' . trim($class) . '"';
	}

	// Hide the home item if hideHomeWhenHome
	$hide = '';

	if ($hideHome == '2' || ($hideHome == '1' && $isHome))
	{
		$_realId = $jyaml->getRealMenuItemId($item);

		// Only hide home has no subitems
		if ((($homeId && $_realId == $homeId) || $item->home))
		{
			$hide = ' style="display:none !important;"';
		}
	}

	echo '<li' . $class . $hide . '>';

	if ($layout == 'dropdown' && $item->type == 'separator')
	{
		// Dropdown require always an link for a full accessaible keybord navigation
		$item->type = 'url';
		$item->link = 'javascript:return false;';
		$item->flink = 'javascript:return false;';
		$item->params->set('menu-anchor_css',
			trim($item->params->get('menu-anchor_css', '') . ' separator')
		);
	}

	// Render the menu item.
	$linkText = '';
	switch ($item->type)
	{
		case 'separator':
		case 'url':
		case 'component':
			ob_start();
			require JModuleHelper::getLayoutPath('mod_menu', 'default_' . $item->type);
			$linkText = ob_get_clean();
			break;

		default:
			ob_start();
			require JModuleHelper::getLayoutPath('mod_menu', 'default_url');
			$linkText = ob_get_clean();
			break;
	}

	// Outline the inner html-text with an additional span
	$linkText = preg_replace(
		'#<a(.*)>(.*)</a>#Uis',
		'<a\\1><span class="item-title">\\2</span></a>',
		$linkText
	);

	// Exclude for dropdown (link is necessary for accessibility)
	if ($layout != 'dropdown')
	{
		if ($linkText && ($disableClick == 'NA' && $isActive) || ($disableClick == 'NC' && $isCurrent))
		{
			// Make link non clickable (replace <a/> to <strong/>)
			$linkText = preg_replace(
				'#<a.*>(.*)</a>#Uis',
				'<strong' . ($anchorCss ? ' class="' . $anchorCss . '"' : '') . '>\\1</strong>',
				$linkText
			);
		}
	}

	if ($menuItemContent)
	{
		$menuItemContents = $jyaml->convertPositionArrayToJdoc('menuitemcontent', array('menuitemcontent' => $menuItemContent));

		if ($menuItemContents)
		{
			echo '<div class="menuitem-content ym-clearfix">' . $menuItemContents . '</div>';
		}
	}
	else
	{
		echo trim($linkText);
	}

	$ulClass = '';

	// Dropdown specific
	if ($layout == 'dropdown')
	{
		$ulClass = 'dropitem ';

		if (($dropitemDirClassV = $item->params->get('jyaml_dropdown_direction_vertical', '')))
		{
			$ulClass .= 'dropitem-' . $dropitemDirClassV . ' ';
		}
		if (($dropitemDirClassH = $item->params->get('jyaml_dropdown_direction_horizontal', '')))
		{
			$ulClass .= 'dropitem-' . $dropitemDirClassH . ' ';
		}
		if ($viewLevel == 0 && empty($dropitemDirClassV) && empty($dropitemDirClassH) && !empty($defaultDirection))
		{
			$ulClass .= 'dropitem-' . $defaultDirection . ' ';
		}

		if ($isActive)
		{
			$ulClass .= 'dropitemActive dropitemActive' . $viewLevel . ' ';
		}
		if ($isCurrent)
		{
			$ulClass .= 'dropitemCurrent dropitemCurrent' . $viewLevel . ' ';
		}

		$ulClass .= 'dropitemLevel' . $viewLevel . ' ';
	}

	// Create class attribute
	if (!empty($ulClass))
	{
		$ulClass = ' class="' . trim($ulClass) . '"';
	}

	// The next item is deeper.
	if ($item->deeper)
	{
		if ($layout == 'dropdown')
		{
			echo '<div' . $ulClass . '><div class="dropitem-content ym-clearfix"><ul>';
		}
		else
		{
			echo '<ul' . $ulClass . '>';
		}
	}

	// The next item is shallower.
	elseif ($item->shallower)
	{
		echo '</li>';

		if ($layout == 'dropdown')
		{
			echo str_repeat('</ul></div></div></li>', $item->level_diff);
		}
		else
		{
			echo str_repeat('</ul></li>', $item->level_diff);
		}
	}

	// The next item is on the same level.
	else {
		echo '</li>';
	}

	if (isset($_orgTitle))
	{
		$item->title = $_orgTitle;
	}

	$counter[$lpHash]++;
}
?></ul>
