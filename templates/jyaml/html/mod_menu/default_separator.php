<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  JYAML.Template
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

// No direct access.
defined('_JEXEC') or die;

// Note. It is important to remove spaces between elements.
$class = $item->params->get('menu-anchor_css', '') ? 'separator ' . $item->params->get('menu-anchor_css', '') : 'separator';
$title = $item->params->get('menu-anchor_title', '') ? 'title="' . $item->params->get('menu-anchor_title', '') . '" ' : '';
if ($item->params->get('menu_image', ''))
{
	$item->params->get('menu_text', 1) ?
		$linktype = '<img src="' . $item->params->get('menu_image', '') . '" alt="' . $item->origTitle . '" />'
										. '<span class="image-title">' . $item->title . '</span> ' :
		$linktype = '<img src="' . $item->params->get('menu_image', '') . '" alt="' . $item->origTitle . '" />';
}
else
{
	$linktype = $item->title;
}

?><span class="<?php echo $class; ?>"><?php echo $title; ?><?php echo $linktype; ?></span>
