<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  JYAML.Template
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

// No direct access.
defined('_JEXEC') or die;

// Note. It is important to remove spaces between elements.

$class = $item->params->get('menu-anchor_css', '') ? 'class="' . $item->params->get('menu-anchor_css', '') . '" ' : '';
$title = $item->params->get('menu-anchor_title', '') ? 'title="' . $item->params->get('menu-anchor_title', '') . '" ' : '';
if ($item->params->get('menu_image', ''))
{
$item->params->get('menu_text', 1) ?
		$linktype = '<img src="' . $item->params->get('menu_image', '') . '" alt="' . $item->origTitle . '" />'
										. '<span class="image-title">' . $item->title . '</span> ' :
		$linktype = '<img src="' . $item->params->get('menu_image', '') . '" alt="' . $item->origTitle . '" />';
}
else
{
	$linktype = $item->title;
}

$rel = ' ';
$rel = $item->params->get('robots', '') . ' ';
$rel = trim($rel);
if ($rel)
{
	$rel = ' rel="' . $rel . '"';
}

switch ($item->browserNav)
{
	default:
	case 0:
?><a <?php echo $class; ?>href="<?php echo $item->flink; ?>" <?php echo $title . $rel; ?>><?php echo $linktype; ?></a><?php
		break;
	case 1:
		// _blank
?><a <?php echo $class; ?>href="<?php echo $item->flink; ?>" target="_blank" <?php echo $title . $rel; ?>><?php echo $linktype; ?></a><?php
		break;
	case 2:
		// Window.open
		$options = 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,' . $params->get('window_open');
		$onclick = "window.open(this.href,'targetWindow','" . $options . "');return false;";
		$href = $item->flink;
?><a <?php echo $class; ?>href="<?php echo $href; ?>" onclick="<?php echo $onclick; ?>" <?php echo $title . $rel; ?>><?php echo $linktype; ?></a><?php
		break;
}
