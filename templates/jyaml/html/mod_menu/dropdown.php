<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  JYAML.Template
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

// No direct access.
defined('_JEXEC') or die;

if (empty($list))
{
	return;
}

// Get the jyaml class reference
$jyaml = JYAML::getDocument();

// Generate an unique id
$dropdownId = 'dropdown-' . $module->id . '-' . mt_rand(1000, 9999);

// Get orientation and default direction
$orientation = $params->get('jyaml_dropdown_orientation', 'horizontal-down');
$linearSubMenu = $params->get('jyaml_dropdown_linear_submenu', '0');
$tmp = explode('-', $orientation);
$orientation = $tmp[0];
$defaultDirection = (isset($tmp[1]) ? $tmp[1] : 'down');
$orientationClass = 'dropdown-' . $orientation . ' ';
if ($linearSubMenu)
{
	$orientationClass .= 'dropdown-linear ';
}

// Set id for ul
$params->set('tag_id', $dropdownId);

// Add scripts and stylesheets
$jyaml->addStylesheet($jyaml->getUrl('css', 'navigation.dropdown.css'));
$jyaml->addStylesheet($jyaml->getUrl('css', 'patch.navigation.dropdown.css'), 'text/css', 'all', '', 'if lte IE 8');

$effects = $params->get('jyaml_dropdown_effects', "['slide', 'fade']", 'null');
if ($effects == 'show'){
	$effects = 'null';
}
$options = array();
$options[] = "subMenuSelector:'.dropitem'";
$options[] = "orientation:'" . $orientation . "'";
$options[] = "linearSubMenu:" . ($linearSubMenu ? 'true' : 'false');
$options[] = "effects:" . $effects;
$options[] = "opacity:" . (float) $params->get('jyaml_dropdown_opacity', '0.90');
$options[] = "hideDelay:" . (int) $params->get('jyaml_dropdown_hide_delay', '750');
$options[] = "showDuration:" . (int) $params->get('jyaml_dropdown_show_duration', '750');
$options[] = "showTransition:'" . $params->get('jyaml_dropdown_show_transition', 'swing') . "'";
$options[] = "hideDuration:" . (int) $params->get('jyaml_dropdown_hide_duration', '250');
$options[] = "hideTransition:'" . $params->get('jyaml_dropdown_hide_transition', 'swing') . "'";
$options[] = "stretchWidthFistSubMenu:" . ($params->get('jyaml_dropdown_stretch_width_first_submenu', '1') ? 'true' : 'false');

$mobileNav = $params->get('jyaml_mobilenav_enable', 0);
if ($mobileNav)
{
	$options[] = "onBeforeInitialize:function(){var el=\$(this),clone=el.clone().attr('id', '');$('li ul',clone).unwrap().unwrap();el.data('jyaml-mobilenav-element', clone);el.data('jyaml-mobilenav-rel', el);}";
}

JHtml::_('jyaml.dropdown', '#' . $dropdownId, $options);

$classes = array();
$classes[] = 'dropdown';
if (trim($orientationClass))
{
	$classes[] = trim($orientationClass);
}
if (($moduleClassSuffix = $params->get('moduleclass_sfx', '')))
{
	$classes[] = 'dropdown-sfx-' . $moduleClassSuffix;
}
$classes[] = 'ym-clearfix';

?>
<div class="<?php echo implode(' ', $classes); ?>">
	<?php require JModuleHelper::getLayoutPath('mod_menu', 'default'); ?>
</div>
