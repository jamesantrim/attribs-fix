<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  JYAML.Template
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

// No direct access.
defined('_JEXEC') or die;

if (empty($list))
{
	return;
}

// Get the jyaml class reference
$jyaml = JYAML::getDocument();

$jyaml->addStylesheet($jyaml->getUrl('css', 'navigation.vlist.css'));

$classes = array();
$classes[] = 'ym-vlist';
if (($moduleClassSuffix = $params->get('moduleclass_sfx', '')))
{
	$classes[] = 'ym-vlist-sfx-' . $moduleClassSuffix;
}
?>
<div class="<?php echo implode(' ', $classes); ?>">
	<?php require JModuleHelper::getLayoutPath('mod_menu', 'default'); ?>
</div>
