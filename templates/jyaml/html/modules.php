<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  Template
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

/*
 * Note:
 * In JYAML the modChrome Styles are extended for
 * adjustment in the template configuration.
 *
 * You can add your own modCrhome Style if you add one in the modChrome folder.
 * (Look in the /html/modChrome/ directory and orientation up to the existing files)
 */

// No direct access.
defined('_JEXEC') or die;

// Import styles from the /modChrome/ subdirectory
JYAML::getDocument()->importModChromeStyles();
