<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  Template
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

/* Note:
 * -----------------------------------------------------------------------------
 *
 * If your wish to REMOVE THE BACKLINK from the footer of HTML-Documents
 * PLEASE READ THE JYAML AND YAML LICENCE CONDITIONS:
 * - (JYAML) http://www.jyaml.de/en/license
 * - (YAML)  http://www.yaml.de/en/license/license-conditions.html
 *
 */

defined('_JEXEC') or die;

$app = JFactory::getApplication();

// Tell Joomla the template is HTML5
$this->setHtml5(true);

// Mobile viewport optimisation
$this->setMetaData('viewport', 'width=device-width, initial-scale=1.0');

// Set http status 503 (Service Temporarily Unavailable).
// This prevent search engines to indexing the offline page.
JResponse::setHeader('status', '503');

// Add forms stylesheet as default for the login form
$this->addStylesheet(
	$this->getUrl('css', 'screen.forms.css')
);

JHtml::_('behavior.keepalive');
JHtml::_('jyaml.validator');

?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>">
<head>
<jdoc:include type="head" />
</head>
<body class="no-js">
	<script type="text/javascript">document.body.className=(document.body.className.replace(/(\s|^)no-js(\s|$)/, '$1$2') + ' js').replace(/^\s+|\s+$/g, "");</script>

	<?php if ($this->params->get('showjsalert', 0)) : ?>
	<noscript class="jswarn-global jsremove">
		<div>
			<strong><?php echo JText::_('JYAML_JSALERT_TITLE'); ?></strong><br />
			<?php echo JText::_('JYAML_JSALERT_DESC'); ?>
		</div>
	</noscript>
	<?php endif; ?>

	<div class="ym-wrapper">
		<div class="ym-wbox">
			<header role="banner">
				<h1>
					<a href="<?php echo $this->getHomepageLink(); ?>">
						<?php
							$_logoImage = trim($this->params->get('logo_image', ''));
							$_offlineImage = trim($app->get('offline_image'));
							$_logoImage = $_offlineImage ? $_offlineImage :  $_logoImage;
						?>
						<?php if ($_logoImage) : ?>
							<?php
								$_img = JYAML::getImage(
									// Image src
									$this->getUrl('root', $_logoImage),
									// Image alt
									JFactory::getApplication()->get('sitename'),
									// Thumbnailer options (see http://www.jyaml.de/en/documentation/jyaml-4-joomla/programming-and-development/jyaml-get-image.html)
									array(),
									// Additional attributes
									'class="site-logo"'
								);

								// Output image without modifications
								echo $_img->toHtml();
							?>
						<?php else : ?>
							<?php echo JFactory::getApplication()->get('sitename'); ?>
						<?php endif; ?>
					</a>
				</h1>
				<?php if (($_sloganText = trim($this->params->get('slogan_text', '')))) : ?>
					<span class="slogan"><?php
						$_sloganText = JFactory::getLanguage()->hasKey($_sloganText) ? JText::_($_sloganText) : $_sloganText;
						echo htmlspecialchars($_sloganText, ENT_COMPAT, JFactory::getDocument()->getCharSet());
					?></span>
				<?php endif; ?>
			</header>

			<?php if ($this->hasMessages()) : ?>
			<div id="system-messages" role="complementary">
				<div id="system-messages-content" class="ym-clearfix">
					<jdoc:include type="message" />
				</div>
			</div>
			<?php endif; ?>

			<div id="main">
				<div class="ym-grid">
					<div class="ym-g66 ym-gl">
						<div class="ym-gbox">
							<p class="warning offline-message">
								<?php
									$offlineMessage = trim($app->get('offline_message'));
									if ($app->get('display_offline_message', 1) == 2)
									{
										$offlineMessage = JText::_('JOFFLINE_MESSAGE');
									}
									echo $offlineMessage;
								?>
							</p>
						</div>
					</div>
					<div class="ym-g33 ym-gr">
						<div class="ym-gbox">
							<form action="index.php" method="post" name="login" id="form-login" class="ym-form ym-full jyaml-form-validate">
								<fieldset>
									<div class="ym-fbox ym-fbox-text">
										<label for="username"><?php echo JText::_('JGLOBAL_USERNAME') ?> <sup class="ym-required">*</sup></label>
										<input name="username" id="username" type="text" class="required" alt="<?php echo JText::_('JGLOBAL_USERNAME') ?>" size="18" />
									</div>
									<div class="ym-fbox ym-fbox-text">
										<label for="passwd"><?php echo JText::_('JGLOBAL_PASSWORD') ?> <sup class="ym-required">*</sup></label>
										<input type="password" name="password" class="required" size="18" alt="<?php echo JText::_('JGLOBAL_PASSWORD') ?>" id="passwd" />
									</div>
									<div class="ym-fbox ym-fbox-check">
										<input type="checkbox" name="remember" value="yes" alt="<?php echo JText::_('JGLOBAL_REMEMBER_ME') ?>" id="remember" />
										<label for="remember"><?php echo JText::_('JGLOBAL_REMEMBER_ME') ?></label>
									</div>
								</fieldset>

								<div class="ym-fbox-footer ym-fbox-button">
									<button class="ym-button" type="submit" name="Submit"><?php echo JText::_('JLOGIN') ?></button>
								</div>

								<input type="hidden" name="option" value="com_users" />
								<input type="hidden" name="task" value="user.login" />
								<input type="hidden" name="return" value="<?php echo base64_encode(JURI::base()) ?>" />
								<?php echo JHtml::_('form.token'); ?>
							</form>
						</div>
					</div>
				</div>
			</div>

			<footer role="contentinfo">
				<div class="ym-cbox ym-clearfix">
					© <?php echo date('Y'); ?> <?php echo JFactory::getApplication()->get('sitename'); ?>

					<span class="float_right a-right" style="border-left:1px solid #ccc; padding-left:1em; margin-top:.5em;">
						<!-- **********************************************************************
							(de) Folgende Rückverlinkungen dürfen nur entfernt werden, wenn
									Sie eine JYAML und/oder eine YAML Lizenz besitzen.

							(en) Following backlinks may be only removed, if
									you are owner of a JYAML and/or a YAML license.

								:: http://www.jyaml.de
								:: http://www.yaml.de
						*********************************************************************** -->
						Layout based on
						<a href="http://www.jyaml.de/" target="_blank">JYAML</a> and
						<a href="http://www.yaml.de/" target="_blank">YAML</a>
						<!-- ****************************************************************** -->

						<br /><small>powered by <a href="http://www.hieblmedia.de/" target="_blank">HieblMedia</a></small>
					</span>
				</div>
			</footer>
		</div>
	</div>
</body>
</html>
