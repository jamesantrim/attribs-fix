<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  Template
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

defined('_JEXEC') or die;

/**
 * Pagination Footer
 *
 * @param   array  $list  Parameters
 *
 * @return string Html
 */
function Pagination_List_footer($list)
{
	$html = "<div class=\"list-footer\">\n";

	$html .= "\n<div class=\"limit\">" . JText::_('JGLOBAL_DISPLAY_NUM') . $list['limitfield'] . "</div>";
	$html .= $list['pageslinks'];
	$html .= "\n<div class=\"counter\">" . $list['pagescounter'] . "</div>";

	$html .= "\n<input type=\"hidden\" name=\"" . $list['prefix'] . "limitstart\" value=\"" . $list['limitstart'] . "\" />";
	$html .= "\n</div>";

	return $html;
}

/**
 * Pagination List Render
 *
 * @param   array  $list  Parameters
 *
 * @return string Html
 */
function Pagination_List_render($list)
{
	$html = '';
	$html .= '<div class="pagination-box ym-clearfix">';
	$html .= '<ul class="ym-clearfix">';
	$html .= '<li class="pagination-start">' . $list['start']['data'] . '</li>';
	$html .= '<li class="pagination-prev">' . $list['previous']['data'] . '</li>';

	foreach ($list['pages'] as $page)
	{
		$class = '';
		$class = 'pagenum ';
		if (!$page['active'])
		{
			$class .= 'active-page ';
		}
		$class = trim($class);

		$html .= '<li' . ($class ? ' class="' . $class . '"' : '') . '>' . $page['data'] . '</li>';
	}

	$html .= '<li class="pagination-next">' . $list['next']['data'] . '</li>';
	$html .= '<li class="pagination-end">' . $list['end']['data'] . '</li>';
	$html .= '</ul>';
	$html .= '</div>';

	return $html;
}

/**
 * Pagination Active Item
 *
 * @param   array  &$item  List item
 *
 * @return string Html
 */
function Pagination_Item_active(&$item)
{
	return '<a href="' . $item->link . '" class="pagenav">' . $item->text . '</a>';
}

/**
 * Pagination Inactive Item
 *
 * @param   array  &$item  List item
 *
 * @return string Html
 */
function Pagination_Item_inactive(&$item)
{
	return '<span class="pagenav">' . $item->text . '</span>';
}
