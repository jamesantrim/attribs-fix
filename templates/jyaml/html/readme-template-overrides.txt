========================================================================
You can override module and component templates they are in MVC written.

Joomla! Documentation search query for more details and examples:
> http://docs.joomla.org/Special:Search?search=template+overrides&go=Go


Note:
Folders without Prefix the 'com_' or 'mod_' are HTML-Document templates
for the JYAML framework (like index or component). These files you can
switch in the template settings.
========================================================================


============================
Overriding module templates:
----------------------------
Search in: /modules/[module]/tmpl/*.php
Copy to: /templates/[template]/html/[module]/*.php


===============================
Overriding component templates:
-------------------------------
Search in: /components/[component]/views/[view]/tmpl/*.php
Copy to: /templates/[template]/html/[component]/[view]/*.php

If exists, the file in the template html folder will be automaticly used by Joomla!.


========================
Example with mod_search:
------------------------
Copy the file '/modules/mod_search/tmpl/default.php'
into '/templates/jyaml/html/mod_search/default.php'
Now you can make changes of the output in the copied file.