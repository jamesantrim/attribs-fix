<?php
/**
 * JYAML - Template Framework includes YAML for Joomla!
 *
 * All rights reserved. The JYAML project is a template to manage and
 * configure Joomla!-Templates with the YAML XHTML/CSS Framework
 * - http://www.yaml.de
 *
 * -----------------------------------------------------------------------------
 *
 * @package     JYAML
 * @subpackage  Loader
 *
 * @author      Reinhard Hiebl <reinhard@hieblmedia.com>
 * @copyright   Copyright (C) 2006 - 2016, HieblMedia (Reinhard Hiebl)
 * @license     http://www.jyaml.de/en/license Creative Commons Attribution 3.0
 * @link        http://www.jyaml.de
 */

defined('_JEXEC') or die;

/**
 * JYAML Autoloader for js and css files
 *
 * @package     JYAML
 * @subpackage  Loader
 * @since       4.0.0
 */
class JYAML_Autoload extends JYAMLloader
{
	/**
	 * setFiles
	 *
	 * @return void
	 */
	protected function setFiles()
	{
		// Add stylesheets
		$this->addFile('stylesheet',
			array(
				'file' => '{$source.theme}base.layout.css',
				'media' => 'all',
				'IEconditionalComment' => '',
				'position' => ''
			)
		);
		$this->addFile('stylesheet',
			array(
				'file' => '{$source.theme}base.patch.css',
				'media' => 'all',
				'IEconditionalComment' => 'if lte IE 7',
				'position' => ''
			)
		);
		$this->addFile('stylesheet',
			array(
				'file' => '{$source.theme}base.print.css',
				'media' => 'print',
				'IEconditionalComment' => '',
				'position' => ''
			)
		);
		$this->addFile('stylesheet',
			array(
				'file' => '{$source.theme}screen.responsive.css',
				'media' => 'all',
				'IEconditionalComment' => '',
				'position' => '',
				'mode' => 'after',
				'ifconfig' => array('key' => 'layout_responsive', 'default' => '1', 'equal' => '1')
			)
		);
		if ('rtl' == JFactory::getDocument()->getDirection())
		{
			// Additional stylesheet for RTL changes
			$this->addFile('stylesheet',
				array(
					'file' => '{$source.theme}base.layout-rtl.css',
					'media' => 'all',
					'IEconditionalComment' => '',
					'position' => '',
					'mode' => 'after'
				)
			);
		}

		// Add scripts
		$this->addFile('script',
			array(
				'file' => 'libraries/jyaml/assets/js/focusfix.min.js',
				'IEconditionalComment' => '',
				'position' => ''
			)
		);
		$this->addFile('script',
			array(
				'file' => "if(typeof(YAML_focusFix)!='undefined'){YAML_focusFix.init()};",
				'IEconditionalComment' => '',
				'position' => 'beforeBodyEnds',
				'inline' => true
			)
		);
		$this->addFile('script',
			array(
				'file' => '{$source.theme}tmpl.js',
				'IEconditionalComment' => '',
				'position' => '',
				'mode' => 'after'
			)
		);

		// Html5 tags enabler for IE < 9
		$this->addFile('script',
			array(
				'file' => 'media/jui/js/html5.js',
				'IEconditionalComment' => 'if lt IE 9',
				'position' => ''
			)
		);
	}
}
